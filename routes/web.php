<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group([
    'prefix' => '{locale?}',
    'where' => ['locale' => '[a-zA-Z]{2}']
], function () {

    Route::get('/', function () {
        return view('index', ['active_link' => '', 'title' => 'Koralis']);
    });

    Route::get('/case-studies', function () {
        return view('case-studies', ['active_link' => 'case-studies', 'title' => 'Koralis - Case studies']);
    });

    Route::get('/career', function () {
        return view('career', ['active_link' => 'career', 'title' => 'Koralis - Career']);
    });

    Route::get('/career/senior-php-developer', function () {
        return view('senior-php-developer', ['active_link' => 'career', 'title' => 'Koralis - Senior  PHP Developer']);
    });

    Route::get('/career/senior-frontend-developer', function () {
        return view('senior-frontend-developer', ['active_link' => 'career', 'title' => 'Koralis - Senior Frontend Developer']);
    });

    Route::get('/what-we-do', function () {
        return view('wwd', ['active_link' => 'wwd', 'title' => 'Koralis - What We Do']);
    });

    Route::get('/case-studies/{case}', function ($lang, $case) {
        return view("partials.brands.$case", ['active_link' => 'case-studies', 'title' => 'Koralis - Case Studies']);
    });
});
Route::post('send-hire-form', 'MailController@sendHireForm');

Route::post('send-appliance-form', 'MailController@sendAppliance');

