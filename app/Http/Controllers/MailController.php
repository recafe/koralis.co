<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;
use PHPMailer\PHPMailer\PHPMailer;

class MailController extends Controller
{
    public function sendHireForm(Request $request)
    {
        if (isset($_POST['form'])) {
            parse_str($_POST['form'], $data);
            $message = "Vardas ir pavardė: " . $data['name_surname'] . " <br> \n ";
            $message .= "El. paštas: " . $data['email'] . " <br> \n ";
            $message .= "Mob. telefonas: " . $data['mobile'] . " <br> \n ";
            $message .= "Miestas: " . $data['city'] . " <br> \n ";
            $message .= "Linkedin: https://www.linkedin.com/" . $data['linkedin'] . " <br> \n ";
            $message .= "Skills'ai: " . $_POST['skills'] . " <br> \n ";
            $this->sendMail($data['name_surname'], $data['email'], $message, 'Dėl darbo pozicijos');
        }

        if (isset($_POST['letter'])) {
            parse_str($_POST['data'], $data);
            $message = $_POST['letter'];
            $this->sendMail($data['name'], $data['email'], $message, 'Dėl projekto įgyvendinimo');
        }
    }

    private function sendMail($name, $email, $message, $subject)
    {
        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->Host = 'smtp.mailtrap.io';
        $mail->Port = 465;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "ef1d8e8c449d5c";
        $mail->Password = "ba15d06ba9eb83";
        $mail->setFrom('4232352345@gnau.com', '1233 name very good');
        $mail->addAddress('ernestas.luza@koralis.co', 'Koralis');
        $mail->Subject = 'Subject very good';

//        if (isset($_FILES['uploadfile']) && sizeof($_FILES['uploadfile']) >= 2) {
//            for ($i = 0; $i <= sizeof($_FILES['uploadfile']) - 1; $i++) {
//                $mail->AddAttachment($_FILES['uploadfile']['tmp_name'][$i], $_FILES['uploadfile']['name'][$i]);
//            }
//        } else {
//            $mail->AddAttachment($_FILES['uploadfile']['tmp_name'], $_FILES['uploadfile']['name']);
//
//        }
        $mail->msgHTML($message);
        $mail->send();
    }

    public function sendAppliance()
    {
        if(isset($_POST['form'])) {
            parse_str($_POST['form'], $data);
            $message = "Linkedin URL: ".$data['textas']." <br> \n ";
            $this->sendMailAplience($message);
        }
    }

    private function sendMailAplience( $message)
    {
        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->Host = 'smtp.mailtrap.io';
        $mail->Port = 465;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "ef1d8e8c449d5c";
        $mail->Password = "ba15d06ba9eb83";
        $mail->setFrom('4232352345@gnau.com', '1233 name very good');
        $mail->addAddress('ernestas.luza@koralis.co', 'Koralis');
        $mail->Subject = 'Subject very good';


//        if (sizeof($_FILES['uploadfile']) >= 2) {
//            for ($i = 0; $i <= sizeof($_FILES['uploadfile'])-1; $i++) {
//                $mail->AddAttachment($_FILES['uploadfile']['tmp_name'][$i], $_FILES['uploadfile']['name'][$i]);
//            }
//        } else {
//            $mail->AddAttachment($_FILES['uploadfile']['tmp_name'], $_FILES['uploadfile']['name']);
//
//        }

        $mail->msgHTML($message . $_SERVER['HTTP_REFERER']);
        $mail->send();
    }

}
