Dropzone.autoDiscover = false;

var myDropzone = new Dropzone('#files', {
    url: "/send-appliance-form",
    maxFilesize: 1000,
    maxFiles: 3,
    paramName: "uploadfile",
    maxThumbnailFilesize: 5,
    autoProcessQueue:false,
    uploadMultiple: true,


    init: function() {

        this.on("sending", function(file, xhr, formData){

            var data = $('#test-form').serialize();

            formData.append("form", data);
        });

        this.on('addedfile', function(file) {
            count = count + 1;
            $('.file .result > strong').text(count);
        });

    }
});
