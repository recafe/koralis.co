
$(document).ready(function(){
    $('.owl-element').on('initialized.owl.carousel changed.owl.carousel', function(e) {
        if (!e.namespace) return
        var carousel = e.relatedTarget
        $('#info').text(carousel.relative(carousel.current()) + 1 +' ' + '/' + ' ' + carousel.items().length)
    }).owlCarousel({
        margin: 10,
        responsiveClass: true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1,
                margin: 20
            }
        }
    });
});


//Carousel Progress Bar
initSlider();

function initSlider() {
    $(".owl-element").owlCarousel({
        items: 1,
        loop: true,
        autoplayTimeout: 15000,
        autoplay: true,
        onInitialized: startProgressBar,
        onTranslate: resetProgressBar,
        onTranslated: startProgressBar
    });

}

function startProgressBar() {
    // apply keyframe animation
    $(".slide-progress").css({
        width: "100%",
        transition: "width 13000ms"
    });

}

function resetProgressBar() {
    $(".slide-progress").css({
        width: 0,
        transition: "width 0s"
    });
}


/*Office Carousel owl-dot's content based by dot index  */
$(document).ready(function(){
    var dot = $('.demo-slider .owl-dot');
    dot.each(function () {
        var index = $(this).index();

        if (index === 0){
            $(this).text('AMSTERDAM');
        }else if(index === 1){
                $(this).text('ATLANTA');
        }
        else if(index === 2){
            $(this).text('BRAZIL');
        }
        else if(index === 3){
            $(this).text('LONDON');
        }
        else if(index === 4){
            $(this).text('NEW YORK');
        }
        else if(index === 5){
            $(this).text('RIYADH');
        }
        else if(index === 6){
            $(this).text('SIDNEY');
        }
        else if(index === 7){
            $(this).text('TOKYO');
        }
        else{

        }
    });


    $('.owl-office').on('initialized.owl.carousel changed.owl.carousel', function(e) {
        if (!e.namespace) return;
        var carousel = e.relatedTarget;
        $('#infoOffice').text(carousel.relative(carousel.current()) + 1 +' ' + '/' + ' ' + carousel.items().length);
    }).owlCarousel({
        loop: true,
        margin: 10,
        dotsContainer: '#numbering',
        responsiveClass: true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1,
                margin: 20
            }
        }
    });

});




//Carousel Progress Bar For Office Carousel

officeSlider();

function officeSlider() {
    $('.owl-office').owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        onInitialized: startOfficeProgressBar,
        onTranslate: resetOfficeProgressBar,
        autoplayTimeout: 8000,
        onTranslated: startOfficeProgressBar
    });

}

function startOfficeProgressBar() {
    // apply keyframe animation
    $(".slide-progress-office").css({
        width: "100%",
        transition: "width 7400ms"
    });

}

function resetOfficeProgressBar() {
    $(".slide-progress-office").css({
        width: 0,
        transition: "width 0s"
    });
}


