
$(document).ready(function () {
    $('.artists-slider').slick({
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        centerMode: true,
        variableWidth: true,
        centerPadding: '60px',
        speed: -100,
        autoplaySpeed: 2000,
        draggable: false,
        swipe: false,
        cssEase: 'cubic-bezier(0.950, 0.050, 0.795, 0.035)',
        autoplay: false,
        slidesToSlide:1,
        slidesToShow: 3,
        responsive: [

            {
                breakpoint: 1024,
                settings: {
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },

            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },

            {
                breakpoint: 480,
                settings: {
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });

});


// Trigger fade in as window scrolls
$(window).on('scroll load', function(){

    $(".artists-block .container .entry > *").each( function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight()/8;
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        if( bottom_of_window > bottom_of_object ){
            $(this).css({'opacity':'1', 'transform': 'translateY(' + 0 + 'px)'});
        }

    });

    $(".process-block .container .entry > *").each( function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight()/8;
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        if( bottom_of_window > bottom_of_object ){
            $(this).css({'opacity':'1', 'transform': 'translateY(' + 0 + 'px)'});
        }
    });
});