
var lastScrollTop = 0;
var count = 0;

/* Newmood auto play video*/
$(function() {
    $(window).scroll(function() {

        var video = document.getElementById("newmood-video");
        var div = $('.featured-titles');
        if ( div.is( ':in-viewport' ) ) {
            video.play();
        }
    });
});

$('#submit-hire-form').on('click', function() {

    var text = $(".text-filled").html();
    var data = $('#start').serialize();

    $.ajax({
        url: '/mail.php',
        type: "post",
        dataType:"json",
        data: {letter:text, data:data},

        success: function (response) {
            $(this).addClass("succ").text($(this).attr("data-done"));
        }
    });
});

$('#submit-position-form').on('click', function(event) {


    $('#position-form').validate({ // initialize the plugin
        rules: {
            name_surname: {
                required: true
            },
            email: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;  // suppresses error message text
        },
        error: function() {
            $(this).addClass("error");
            return false;
        }
    });

    if ($('#position-form').valid()) {
        $(this).addClass("succ").text($(this).attr("data-done"));
    } else {
        return false;
	}

	event.preventDefault();

    if (myDropzone.getQueuedFiles().length > 0) {
        myDropzone.processQueue();
    } else {
        var skills = $.map($(".drop-holer li"), function( i ) {
            return $(i).text();
        });

        var data = $('#position-form').serialize();

		if (formError == false) {
            $.ajax({
                url: '/mail.php',
                type: "post",
                dataType:"json",
                data: {form:data, skills:skills},

                success: function (response) {

                }
            });
		}

    }

});

function mobileCheck() {

	var st = $(this).scrollTop();
	if (st > lastScrollTop){
		$("body").removeClass("scroll-up").addClass("scroll-down");
	} else {
		$("body").removeClass("scroll-down").addClass("scroll-up");
	}
	lastScrollTop = st;
	if ($(".case-title").length) {
		if (st > 100){
		// if (st > $(".case-title").height()){
			$("body").addClass("scrolled");
		} else {
			$("body").removeClass("scrolled");
		}
	} else {
		if (st > $(window).height()*.8){
			$("body").addClass("scrolled");
		} else {
			$("body").removeClass("scrolled");
		}
	}

	if ($(".right-pagination").length) {
		if ($(window).scrollTop() > $(".body").offset().top ) {
			if ($(window).scrollTop() < $(".looking").offset().top - $(window).height() ) {
				$("body").addClass("visible-dots");
				$(".right-pagination").removeAttr("style");
			} else {
				$(".right-pagination").css({ top: $(".looking").offset().top - $(window).height() - $(window).height()/2 });
				$("body").removeClass("visible-dots");
			}
		} else {
			$(".right-pagination").removeAttr("style");
			$("body").removeClass("visible-dots");
		}
	}

	if (!$(".home").length) {
		var fea  = $(".featured-title");
		if ($(".case-title").length) {
			fea  = $(".case-title");
		}
		if (fea.length) {
			var diffLogo = fea.height() - 40 - $(".header .logo").height()/2;
			if ($(window).scrollTop() > diffLogo ) {
				if ($(".color-invert").length) {
					var logoR = false;
					$(".color-invert").each(function() {
						if ($(window).scrollTop() > $(this).offset().top - 40 - $(".header .logo").height()/2 ) {
							if ($(window).scrollTop() < $(this).offset().top - 40 - $(".header .logo").height()/2 +$(this).outerHeight() ) {
								logoR = true;
							} else {
							}
						} else {
						}
					});
					if (logoR) {
						$(".logo").removeClass("reverse");
					} else {
						$(".logo").addClass("reverse");
					}
				} else {
					$(".logo").addClass("reverse");
				}
			} else {
				if ($(".color-invert").length) {
					var logoR = false;
					$(".color-invert").each(function() {
						if ($(window).scrollTop() > $(this).offset().top - 40 - $(".header .logo").height()/2 ) {
							if ($(window).scrollTop() < $(this).offset().top - 40 - $(".header .logo").height()/2 +$(this).outerHeight() ) {
								logoR = true;
							} else {
							}
						} else {
						}
					});
					if (logoR) {
						$(".logo").addClass("reverse");
					} else {
						$(".logo").removeClass("reverse");
					}
				} else {
					$(".logo").removeClass("reverse");
				}
			}
		}
	}
};

function validateEmail(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}


$(document).ready(function() {
	if(!('ontouchstart' in window || navigator.maxTouchPoints)){
		$('html').addClass('no-touch');
		$(".text-overlay .text, .team-box section, .technologies-box ul li, .job-list article, .clients-list .logo-c, .clients-list .image > img, .cols .col, .text-box form label, .instagram-list article, .clients-logo ul, .text-box h2, .text-box .entry, .body .services-list article, .founders blockquote, .founders .ceo, .founders .entry, .founders .btn, .text-row .entry-cols, .images-cols img, .typography-text .col, .typography-colors .col, .images-full, .partners-full ul li, .video-full, .clients-full h2, .clients-full .newmood-brands-title , .clients-full ul li, .center-cols .col, .try-case .entry, .quote-box blockquote, .service-box .ico, .service-box h2, .service-box .desc, .service-box .related, .quote-box-image blockquote, .two-cols .col h3, .text-box .job-apply section article, .job-form fieldset, .position button").each(function() {
			var cur = $(this);
			if (cur.isOnScreen()) {
				if (!cur.hasClass("visible")) {
					cur.addClass("visible");
				};
			};
			$(window).scroll(function() {
				if (cur.isOnScreen()) {
					if (!cur.hasClass("visible")) {
						cur.addClass("visible");
					};
				};
			});
		});
		var svg = $('.services-list svg');
		var svgDraw = svg.drawsvg();
		$('.services-list svg').each(function() {
			var svg = $(this);
			if (svg.isOnScreen()) {
				if (!svg.hasClass("visible")) {
					svg.addClass("visible");
					window.setTimeout(function() {
						svgDraw.drawsvg('animate');
						window.setTimeout(function() {
							svg.addClass("visible0");
						}, 700);
					}, 3500);
				};
			};
			$(window).scroll(function() {
				if (svg.isOnScreen()) {
					if (!svg.hasClass("visible")) {
						svgDraw.drawsvg('animate');
						svg.addClass("visible");
					};
				};
			});
		});
	} else {
		$('html').addClass('touch');
	};

	if ($("#bar").length) {

		var range = document.getElementById('bar');

		noUiSlider.create(range, {
			start: [57, 75],
			connect: true,
			format: wNumb({
				decimals: 0
			}),
			range: {
				'min': 0,
				'max': 150
			}
		});
		range.noUiSlider.on('update', function( values, handle ) {
			$(".tab-content .value").eq(0).text(values[0]);
			$(".tab-content .value").eq(1).text(values[1]);
			$(".data-budget1").text(values[0]);
			$(".data-budget2").text(values[1]);
		});
	}

	if ($( ".drop-holer" ).length) {

		$("body").addClass("drag-available");
		$( ".drop-holer" ).sortable({
			connectWith: "#love-items, #skills, #live"
		});
		$( "#love-items, #skills, #live" ).sortable({
			connectWith: ".drop-holer",
			start         : function(event,ui){
				$(".drop-area").addClass("drag");
			},
			stop         : function(event,ui){
				$(".drop-area").removeClass("drag");
				$(".ui-sortable").each(function() {
					if ($(this).children().length) {
						$(this).removeClass("empty");
					} else {
						$(this).addClass("empty");
					}
				});
				if ($(".text-box .job-apply .drop-holer li").length) {
					$(".text-box .job-apply").addClass("added");
				} else {
					$(".text-box .job-apply").removeClass("added");
				}
			}
		});
	}

	$(".file input").change(function() {
		if ($(this).get(0).files.length) {
			$(".to-hide").hide();
			$(".file strong").text($(this).get(0).files.length);
		} else {
			$(".to-hide").show();
			$(".file strong").text("0");
		}
	});

	$(".phone select").crfs();

	$(".tab-content .prev").click(function() {
		var cur = $(".tab-content.active");
		$(".tab-content.active").removeClass("active");
		cur.prev().addClass("active");
		return false;
	});

/*	$("button").click(function() {
        if ($('#position-form').valid() || $('#hire-form').valid()) {
            $(this).addClass("succ").text($(this).attr("data-done"));
        }

		return false;
	});var timer;*/


	// $(".wor .next").mousedown(function() {
	// 	var i = 0;
	// 	var sfu = $(".dragscroll").scrollLeft();
	// 	timer = setInterval(function(){ $(".dragscroll").scrollLeft(sfu+10*i); i++; }, 1000/60);
	// });

	// $(".wor .next").mouseup(function() {
	// 	clearInterval(timer);
	// });
	// $(".wor .prev").mousedown(function() {
	// 	var i = 0;
	// 	var sfu = $(".dragscroll").scrollLeft();
	// 	timer = setInterval(function(){ $(".dragscroll").scrollLeft(sfu-10*i); i++; }, 1000/60);
	// });

	// $(".wor .prev").mouseup(function() {
	// 	clearInterval(timer);
	// });

	$(".tab-content .next").click(function() {
		var cur = $(".tab-content.active");
		var error;
		var first = "";
		var valid = true;
		$(".data-name").text($("#start input[name=name]").val());
		$(".data-email").text($("#start input[name=email]").val());
		$(".data-email").parent().attr("href", "mailto:"+$("#start input[name=email]").val());
		$(".data-phone").text($("#start input[name=phone]").val());
		$(".data-company").text($("#start input[name=company]").val());
		$(".data-url").text($("#start input[name=web]").val());
		$(".data-desc").text($("#start textarea").val());
		$("#start .temp").remove();
		$("#start input[type=checkbox]:checked").each(function(index) {
			if (index > 0) {
				$(".data-services").after("<span class='temp'>, </span>")
			}
			$(".data-services").after("<a href='' class='temp'>"+$(this).next().text()+"</a>")
		});
		$(".data-time").text($("#start .date-radios input:checked").next().text());
		cur.find("input, textarea").each(function() {
			if ($(this).attr("required") == "required") {
				if ($(this).val().length < 1) {
					valid = false;
					$(this).addClass("error");
					if (!first) {
						first = $(this);
					}
				} else {
					$(this).removeClass("error");
					if ($(this).attr("type")== "email") {
						if (!validateEmail($(this).val())) {
							$(this).addClass("error");
							if (!first) {
								first = $(this);
							}
						}
					}
				}
			}
			if ($(this).attr("type")== "checkbox") {
				if (cur.find("input[type=checkbox]:checked").length) {
					$(".start-modal h4").removeClass("error");
					valid = true;
				} else {
					$(".start-modal h4").addClass("error");
					$('.start-modal').stop().animate({
						scrollTop: $(".start-modal h4").offset().top - 40
					}, 300);
					valid = false;
				}
			}
			if ($(this).attr("type")== "radio") {
				if (cur.find("input[type=radio]:checked").length) {
					cur.find("label").removeClass("error");
					valid = true;
				} else {
					cur.find("label").addClass("error");
					valid = false;
				}
			}
		});
		if (valid) {
			$(".tab-content.active").removeClass("active");
			cur.next().addClass("active");
			$('.start-modal').stop().animate({
				scrollTop: 0
			}, 300);
		} else {
			if (first) {
				first.focus();
			}
		}
		return false;
	});

	$(".ico-down").click(function() {
		$('html, body').stop().animate({
			scrollTop: $(".featured-title").height()
		}, 1000);
		return false;
	});
	$("[href='#contact']").click(function() {
		$("body").toggleClass("active-contact");
		return false;
	});
	$(".contact-modal .close").click(function() {
		$("body").removeClass("active-contact");
		return false;
	});
	$("[href='#plan']").click(function() {
		$("body").toggleClass("active-start");
		return false;
	});
	$(".start-modal .close").click(function() {
		$("body").removeClass("active-start");
		return false;
	});
	$(".menu-trigger").click(function() {
		$("body").toggleClass("active-menu");
	});
	$(".clients-list .back").click(function() {
		$("body").removeClass("active-modal");
		$(this).parentsUntil("article").parent("article").removeClass("active");
		return false;
	});
	$(".menu .langs .trigger").click(function() {
		$(this).parent().toggleClass("hover");
		return false;
	});
	$(".clients-list .more, .load-more").click(function() {
		window.setTimeout(function() {
			$("body").addClass("active-modal");
		}, 300);

		var svgIcon = $('.ico-animation svg');
		var svgDraw = svgIcon.drawsvg();
		$(".client-detail").each(function() {
			var svg = $(this);
			if (svg.isOnScreen()) {
				if (!svg.hasClass("visible")) {
					svg.addClass("visible");
					svgDraw.drawsvg('animate');
				};
			};
			svg.scroll(function() {
				if (svg.isOnScreen(svg)) {
					if (!svg.hasClass("visible")) {
						svgDraw.drawsvg('animate');
						svg.addClass("visible");
					};
				};
			});
		});
		$(this).parentsUntil("article").parent("article").addClass("active");
		return false;
	});
	$(".form-control").on("change keyup", function() {
		if ($(this).val().length) {
			$(this).addClass("selected");
		} else {
			$(this).removeClass("selected");
		}
	});
	$(document).keyup(function(e) {
		if (e.keyCode === 27) $('.back').click();
	});
	var event = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) ? "touchstart" : "click";

	$(document).on(event, function (e) {
		if ( $(e.target).closest('menu .langs').length === 0 ) {
			$(".menu .langs").removeClass("hover");
		}
	});

	mobileCheck();
	$(window).resize(function() {
		mobileCheck();
	});
	$(window).scroll(function() {
		mobileCheck();
	});

	var tiltSettings = [ {
		movement: {
			imgWrapper : {
				translation : {x: 5, y: 5, z: 0},
				reverseAnimation : {duration : 800, easing : 'easeOutQuart'}
			},
		}
	}];

	[].slice.call(document.querySelectorAll('.instagram-list article > a')).forEach(function(el, pos) {
		new TiltFx(el, tiltSettings[0]);
	});

	$('.media').fancybox({
		margin: 50,
		width		: '90%',
		height		: '90%',
		padding: 0,
		helpers : {
			media : {}
		}
	});
});

$(window).on("load", function() {
	$.ready.then(function(){
		if ($(".home").length) {
			window.setTimeout(function() {
				$("body").addClass("loaded");
				$("video source").each(function() {
					var sourceFile = $(this).attr("data-src");
					$(this).attr("src", sourceFile);
					var video = this.parentElement;
					video.load();
					video.play();
				});
			}, 500);
		} else {
			$("body").addClass("loaded");
		}
		$('.instagram-list section').masonry({
			columnWidth: 1
		});
	});
});
