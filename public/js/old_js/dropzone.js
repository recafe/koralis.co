Dropzone.autoDiscover = false;

var myDropzone = new Dropzone('#files', {
    url: "/mail.php",
    maxFilesize: 100,
    maxFiles: 3,
    paramName: "uploadfile",
    maxThumbnailFilesize: 5,
    autoProcessQueue:false,
    uploadMultiple: true,


    init: function() {

        this.on("sending", function(file, xhr, formData){

            var skills = $.map($(".drop-holer li"), function( i ) {
                return $(i).text();
            });

            var data = $('#position-form').serialize();

            formData.append("skills", skills);
            formData.append("form", data);
        });

        this.on('addedfile', function(file) {
            count = count + 1;
            $('.file .result > strong').text(count);
        });

    }
});