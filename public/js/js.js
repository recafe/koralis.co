$(document).ready(function() {

  $(".center-video .play").click(function() {
    $(".center-video").addClass("playing").find("iframe").attr("src", $(".center-video").find("iframe").attr("src")+"&autoplay=1");
      setTimeout(function(){ $(".center-video").removeClass("playing"); }, 2000);
  });

  $.fn.isOnScreen = function(){

    var win = $(window);

    var viewport = {
      top : win.scrollTop(),
      left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height()/1.5;

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

  };

  function is_touch_device() {
    return 'ontouchstart' in window || navigator.maxTouchPoints;
  };

  var ieUse = false;
  if (/*@cc_on !@*/false && ( document.documentMode === 9 || document.documentMode === 10) ) { ieUse = true; };

  var animatedItems = $('.js-from-bottom');

  if (!is_touch_device()) {
    if (ieUse) {
      $("body").addClass("ie");
    } else {
      animatedItems.addClass("opacity0")
      animatedItems.each(function() {
        var cur = $(this);
        if (cur.isOnScreen()) {
          if (!cur.hasClass("animated")) {
            cur.addClass("animated fadeInUp");
          };
        };
        $(window).scroll(function() {
          if (cur.isOnScreen()) {
            if (!cur.hasClass("animated")) {
              cur.addClass("animated fadeInUp");
            };
          };
        });
      });
    }
  }

});

$("#applyID").on('click','a',function(event){
    event.preventDefault;

    var body = $("html, body");

    body.stop().animate({scrollTop: $('#carID').offset().top}, '500', 'swing', function() {

    });
    return false;

});