var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();
$(window).scroll(function () {
  var path = window.location.pathname;
  if (!path.match('/positions/')) {
    var fixed = $(".logo");
    var fixed_position = $(".logo").offset().top;
    var fixed_height = $(".logo").height();
    var toCross_position = $(".start-project").offset().top;
    var toCross_height = $(".start-project").height();

    if (fixed_position + fixed_height < toCross_position) {
      fixed.addClass('reverse');
    } else if (fixed_position > toCross_position + toCross_height) {
      fixed.addClass('reverse');
    } else {
      fixed.removeClass('reverse');
    }
  }
});

function mobileCheck() {
  var st = $(this).scrollTop();
  if (Math.abs(lastScrollTop - st) <= delta)
    return;

  if (st > lastScrollTop) {
    // Scroll Down
    if (st > 82) {
      $('header').removeClass('nav-down').addClass('nav-up');
    }

  } else if (st > 82) {
    if ($('body').hasClass('home')) {
      $('header').addClass('nav-background');
    }
    // Scroll Up
    if (st + $(window).height() < $(document).height()) {
      $('header').removeClass('nav-up').addClass('nav-down');
      $(".menu-trigger span").css({'background-color': '#F3333E'});


      if (st <= 82) {
        $('header').removeClass('nav-background').addClass('nav-top');
      } else {
        $('header').removeClass('nav-top');
      }
    }
  } else if (st <= 5) {
    $('header').removeClass('nav-background').removeClass('nav-down').addClass('nav-top');
  }

  lastScrollTop = st;

  if ($(".right-pagination").length && $(window).scrollTop() > $(".body").offset().top) {
    if ($(window).scrollTop() < $(".looking").offset().top - $(window).height()) {
      $("body").addClass("visible-dots"), $(".right-pagination").removeAttr("style");
    } else {
      $(".right-pagination").css({
        top: $(".looking").offset().top - $(window).height() - $(window).height() / 2
      });
      $("body").removeClass("visible-dots");
    }
  } else {
    $(".right-pagination").removeAttr("style");
    $("body").removeClass("visible-dots");
  }
  if (!$(".home").length && !$('.fadingBG').length) {
    var e = $(".featured-title");
    if ($(".case-title").length && (e = $(".case-title")), e.length) {
      var i = e.height() - 40 - $(".header .logo").height() / 2;
      if ($(window).scrollTop() > i)
        if ($(".color-invert").length) {
          var n = !1;
          if (n) {
            $(".logo").removeClass("reverse");
            $('.menu-trigger span').css({'background': '#FFF'});
            $('.header').removeClass('nav-background');
          } else {
            $(".logo").addClass("reverse");
            $('.menu-trigger span').css({'background-color': '#F3333E'});
            $('.header').addClass('nav-background');
          }
        } else $(".logo").addClass("reverse");
      else if ($(".color-invert").length) {
        var n = !1;
        if (n) {
          $(".logo").addClass("reverse");
          $('.menu-trigger span').css({'background-color': '#F3333E'});
          $('.header').addClass('nav-background');
        } else {
          $(".logo").removeClass("reverse");
          $('.menu-trigger span').css({'background': '#FFF'});
          $('.header').removeClass('nav-background');
        }
      } else {
        $(".logo").removeClass("reverse");
        $('.menu-trigger span').css({'background': '#FFF'});
        $('.header').removeClass('nav-background');
      }
    }
  }
};

function validateEmail(t) {
  var e = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return e.test(t)
}

!function (t, e) {
  "object" == typeof exports && "undefined" != typeof module ? e(require("jquery"), require("window")) : "function" == typeof define && define.amd ? define("isInViewport", ["jquery", "window"], e) : e(t.$, t.window)
}(this, function (t, e) {
  "use strict";

  function i(e) {
    var i = this;
    if (1 === arguments.length && "function" == typeof e && (e = [e]), !(e instanceof Array)) throw new SyntaxError("isInViewport: Argument(s) passed to .do/.run should be a function or an array of functions");
    return e.forEach(function (e) {
      "function" != typeof e ? (console.warn("isInViewport: Argument(s) passed to .do/.run should be a function or an array of functions"), console.warn("isInViewport: Ignoring non-function values in array and moving on")) : [].slice.call(i).forEach(function (i) {
        return e.call(t(i))
      })
    }), this
  }

  function n(e) {
    var i = t("<div></div>").css({width: "100%"});
    e.append(i);
    var n = e.width() - i.width();
    return i.remove(), n
  }

  function o(i, s) {
    var r = i.getBoundingClientRect(), a = r.top, l = r.bottom, c = r.left, h = r.right,
      u = t.extend({tolerance: 0, viewport: e}, s), d = !1, p = u.viewport.jquery ? u.viewport : t(u.viewport);
    p.length || (console.warn("isInViewport: The viewport selector you have provided matches no element on page."), console.warn("isInViewport: Defaulting to viewport as window"), p = t(e));
    var f = p.height(), m = p.width(), g = p[0].toString();
    if (p[0] !== e && "[object Window]" !== g && "[object DOMWindow]" !== g) {
      var v = p[0].getBoundingClientRect();
      a -= v.top, l -= v.top, c -= v.left, h -= v.left, o.scrollBarWidth = o.scrollBarWidth || n(p), m -= o.scrollBarWidth
    }
    return u.tolerance = ~~Math.round(parseFloat(u.tolerance)), u.tolerance < 0 && (u.tolerance = f + u.tolerance), h <= 0 || c >= m ? d : d = u.tolerance ? a <= u.tolerance && l >= u.tolerance : l > 0 && a <= f
  }

  function s(e) {
    if (e) {
      var i = e.split(",");
      return 1 === i.length && isNaN(i[0]) && (i[1] = i[0], i[0] = void 0), {
        tolerance: i[0] ? i[0].trim() : void 0,
        viewport: i[1] ? t(i[1].trim()) : void 0
      }
    }
    return {}
  }

  t = "default" in t ? t["default"] : t, e = "default" in e ? e["default"] : e, t.extend(t.expr[":"], {
    "in-viewport": t.expr.createPseudo ? t.expr.createPseudo(function (t) {
      return function (e) {
        return o(e, s(t))
      }
    }) : function (t, e, i) {
      return o(t, s(i[3]))
    }
  }), t.fn.isInViewport = function (t) {
    return this.filter(function (e, i) {
      return o(i, t)
    })
  }, t.fn.run = i
}), function (t, e) {
  "function" == typeof define && define.amd ? define(["exports"], e) : e("undefined" != typeof exports ? exports : t.dragscroll = {})
}(this, function (t) {
  var e, i, n = window, o = document, s = "mousemove", r = "mouseup", a = "mousedown", l = "EventListener",
    c = "add" + l, h = "remove" + l, u = [], d = function (t, l) {
      for (t = 0; t < u.length;) l = u[t++], l = l.container || l, l[h](a, l.md, 0), n[h](r, l.mu, 0), n[h](s, l.mm, 0);
      for (u = [].slice.call(o.getElementsByClassName("dragscroll")), t = 0; t < u.length;) !function (t, l, h, u, d, p) {
        (p = t.container || t)[c](a, p.md = function (e) {
          t.hasAttribute("nochilddrag") && o.elementFromPoint(e.pageX, e.pageY) != p || (u = 1, l = e.clientX, h = e.clientY, e.preventDefault())
        }, 0), n[c](r, p.mu = function () {
          u = 0
        }, 0), n[c](s, p.mm = function (n) {
          u && ((d = t.scroller || t).scrollLeft -= e = -l + (l = n.clientX), d.scrollTop -= i = -h + (h = n.clientY), t == o.body && ((d = o.documentElement).scrollLeft -= e, d.scrollTop -= i))
        }, 0)
      }(u[t++])
    };
  "complete" == o.readyState ? d() : n[c]("load", d, 0), t.reset = d
}), !function (t, e) {
  "function" == typeof define && define.amd ? define("jquery-bridget/jquery-bridget", ["jquery"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("jquery")) : t.jQueryBridget = e(t, t.jQuery)
}(window, function (t, e) {
  "use strict";

  function i(i, s, a) {
    function l(t, e, n) {
      var o, s = "$()." + i + '("' + e + '")';
      return t.each(function (t, l) {
        var c = a.data(l, i);
        if (!c) return void r(i + " not initialized. Cannot call methods, i.e. " + s);
        var h = c[e];
        if (!h || "_" == e.charAt(0)) return void r(s + " is not a valid method");
        var u = h.apply(c, n);
        o = void 0 === o ? u : o
      }), void 0 !== o ? o : t
    }

    function c(t, e) {
      t.each(function (t, n) {
        var o = a.data(n, i);
        o ? (o.option(e), o._init()) : (o = new s(n, e), a.data(n, i, o))
      })
    }

    a = a || e || t.jQuery, a && (s.prototype.option || (s.prototype.option = function (t) {
      a.isPlainObject(t) && (this.options = a.extend(!0, this.options, t))
    }), a.fn[i] = function (t) {
      if ("string" == typeof t) {
        var e = o.call(arguments, 1);
        return l(this, t, e)
      }
      return c(this, t), this
    }, n(a))
  }

  function n(t) {
    !t || t && t.bridget || (t.bridget = i)
  }

  var o = Array.prototype.slice, s = t.console, r = "undefined" == typeof s ? function () {
  } : function (t) {
    s.error(t)
  };
  return n(e || t.jQuery), i
}), function (t, e) {
  "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", e) : "object" == typeof module && module.exports ? module.exports = e() : t.EvEmitter = e()
}("undefined" != typeof window ? window : this, function () {
  function t() {
  }

  var e = t.prototype;
  return e.on = function (t, e) {
    if (t && e) {
      var i = this._events = this._events || {}, n = i[t] = i[t] || [];
      return -1 == n.indexOf(e) && n.push(e), this
    }
  }, e.once = function (t, e) {
    if (t && e) {
      this.on(t, e);
      var i = this._onceEvents = this._onceEvents || {}, n = i[t] = i[t] || {};
      return n[e] = !0, this
    }
  }, e.off = function (t, e) {
    var i = this._events && this._events[t];
    if (i && i.length) {
      var n = i.indexOf(e);
      return -1 != n && i.splice(n, 1), this
    }
  }, e.emitEvent = function (t, e) {
    var i = this._events && this._events[t];
    if (i && i.length) {
      var n = 0, o = i[n];
      e = e || [];
      for (var s = this._onceEvents && this._onceEvents[t]; o;) {
        var r = s && s[o];
        r && (this.off(t, o), delete s[o]), o.apply(this, e), n += r ? 0 : 1, o = i[n]
      }
      return this
    }
  }, t
}), function (t, e) {
  "use strict";
  "function" == typeof define && define.amd ? define("get-size/get-size", [], function () {
    return e()
  }) : "object" == typeof module && module.exports ? module.exports = e() : t.getSize = e()
}(window, function () {
  "use strict";

  function t(t) {
    var e = parseFloat(t), i = -1 == t.indexOf("%") && !isNaN(e);
    return i && e
  }

  function e() {
  }

  function i() {
    for (var t = {
      width: 0,
      height: 0,
      innerWidth: 0,
      innerHeight: 0,
      outerWidth: 0,
      outerHeight: 0
    }, e = 0; c > e; e++) {
      var i = l[e];
      t[i] = 0
    }
    return t
  }

  function n(t) {
    var e = getComputedStyle(t);
    return e || a("Style returned " + e + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), e
  }

  function o() {
    if (!h) {
      h = !0;
      var e = document.createElement("div");
      e.style.width = "200px", e.style.padding = "1px 2px 3px 4px", e.style.borderStyle = "solid", e.style.borderWidth = "1px 2px 3px 4px", e.style.boxSizing = "border-box";
      var i = document.body || document.documentElement;
      i.appendChild(e);
      var o = n(e);
      s.isBoxSizeOuter = r = 200 == t(o.width), i.removeChild(e)
    }
  }

  function s(e) {
    if (o(), "string" == typeof e && (e = document.querySelector(e)), e && "object" == typeof e && e.nodeType) {
      var s = n(e);
      if ("none" == s.display) return i();
      var a = {};
      a.width = e.offsetWidth, a.height = e.offsetHeight;
      for (var h = a.isBorderBox = "border-box" == s.boxSizing, u = 0; c > u; u++) {
        var d = l[u], p = s[d], f = parseFloat(p);
        a[d] = isNaN(f) ? 0 : f
      }
      var m = a.paddingLeft + a.paddingRight, g = a.paddingTop + a.paddingBottom,
        v = a.marginLeft + a.marginRight, y = a.marginTop + a.marginBottom,
        b = a.borderLeftWidth + a.borderRightWidth, w = a.borderTopWidth + a.borderBottomWidth, x = h && r,
        _ = t(s.width);
      _ !== !1 && (a.width = _ + (x ? 0 : m + b));
      var C = t(s.height);
      return C !== !1 && (a.height = C + (x ? 0 : g + w)), a.innerWidth = a.width - (m + b), a.innerHeight = a.height - (g + w), a.outerWidth = a.width + v, a.outerHeight = a.height + y, a
    }
  }

  var r, a = "undefined" == typeof console ? e : function (t) {
      console.error(t)
    },
    l = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
    c = l.length, h = !1;
  return s
}), function (t, e) {
  "use strict";
  "function" == typeof define && define.amd ? define("desandro-matches-selector/matches-selector", e) : "object" == typeof module && module.exports ? module.exports = e() : t.matchesSelector = e()
}(window, function () {
  "use strict";
  var t = function () {
    var t = Element.prototype;
    if (t.matches) return "matches";
    if (t.matchesSelector) return "matchesSelector";
    for (var e = ["webkit", "moz", "ms", "o"], i = 0; i < e.length; i++) {
      var n = e[i], o = n + "MatchesSelector";
      if (t[o]) return o
    }
  }();
  return function (e, i) {
    return e[t](i)
  }
}), function (t, e) {
  "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["desandro-matches-selector/matches-selector"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("desandro-matches-selector")) : t.fizzyUIUtils = e(t, t.matchesSelector)
}(window, function (t, e) {
  var i = {};
  i.extend = function (t, e) {
    for (var i in e) t[i] = e[i];
    return t
  }, i.modulo = function (t, e) {
    return (t % e + e) % e
  }, i.makeArray = function (t) {
    var e = [];
    if (Array.isArray(t)) e = t; else if (t && "number" == typeof t.length) for (var i = 0; i < t.length; i++) e.push(t[i]); else e.push(t);
    return e
  }, i.removeFrom = function (t, e) {
    var i = t.indexOf(e);
    -1 != i && t.splice(i, 1)
  }, i.getParent = function (t, i) {
    for (; t != document.body;) if (t = t.parentNode, e(t, i)) return t
  }, i.getQueryElement = function (t) {
    return "string" == typeof t ? document.querySelector(t) : t
  }, i.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t)
  }, i.filterFindElements = function (t, n) {
    t = i.makeArray(t);
    var o = [];
    return t.forEach(function (t) {
      if (t instanceof HTMLElement) {
        if (!n) return void o.push(t);
        e(t, n) && o.push(t);
        for (var i = t.querySelectorAll(n), s = 0; s < i.length; s++) o.push(i[s])
      }
    }), o
  }, i.debounceMethod = function (t, e, i) {
    var n = t.prototype[e], o = e + "Timeout";
    t.prototype[e] = function () {
      var t = this[o];
      t && clearTimeout(t);
      var e = arguments, s = this;
      this[o] = setTimeout(function () {
        n.apply(s, e), delete s[o]
      }, i || 100)
    }
  }, i.docReady = function (t) {
    var e = document.readyState;
    "complete" == e || "interactive" == e ? t() : document.addEventListener("DOMContentLoaded", t)
  }, i.toDashed = function (t) {
    return t.replace(/(.)([A-Z])/g, function (t, e, i) {
      return e + "-" + i
    }).toLowerCase()
  };
  var n = t.console;
  return i.htmlInit = function (e, o) {
    i.docReady(function () {
      var s = i.toDashed(o), r = "data-" + s, a = document.querySelectorAll("[" + r + "]"),
        l = document.querySelectorAll(".js-" + s), c = i.makeArray(a).concat(i.makeArray(l)),
        h = r + "-options", u = t.jQuery;
      c.forEach(function (t) {
        var i, s = t.getAttribute(r) || t.getAttribute(h);
        try {
          i = s && JSON.parse(s)
        } catch (a) {
          return void(n && n.error("Error parsing " + r + " on " + t.className + ": " + a))
        }
        var l = new e(t, i);
        u && u.data(t, o, l)
      })
    })
  }, i
}), function (t, e) {
  "function" == typeof define && define.amd ? define("outlayer/item", ["ev-emitter/ev-emitter", "get-size/get-size"], e) : "object" == typeof module && module.exports ? module.exports = e(require("ev-emitter"), require("get-size")) : (t.Outlayer = {}, t.Outlayer.Item = e(t.EvEmitter, t.getSize))
}(window, function (t, e) {
  "use strict";

  function i(t) {
    for (var e in t) return !1;
    return e = null, !0
  }

  function n(t, e) {
    t && (this.element = t, this.layout = e, this.position = {x: 0, y: 0}, this._create())
  }

  function o(t) {
    return t.replace(/([A-Z])/g, function (t) {
      return "-" + t.toLowerCase()
    })
  }

  var s = document.documentElement.style, r = "string" == typeof s.transition ? "transition" : "WebkitTransition",
    a = "string" == typeof s.transform ? "transform" : "WebkitTransform",
    l = {WebkitTransition: "webkitTransitionEnd", transition: "transitionend"}[r], c = {
      transform: a,
      transition: r,
      transitionDuration: r + "Duration",
      transitionProperty: r + "Property",
      transitionDelay: r + "Delay"
    }, h = n.prototype = Object.create(t.prototype);
  h.constructor = n, h._create = function () {
    this._transn = {ingProperties: {}, clean: {}, onEnd: {}}, this.css({position: "absolute"})
  }, h.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t)
  }, h.getSize = function () {
    this.size = e(this.element)
  }, h.css = function (t) {
    var e = this.element.style;
    for (var i in t) {
      var n = c[i] || i;
      e[n] = t[i]
    }
  }, h.getPosition = function () {
    var t = getComputedStyle(this.element), e = this.layout._getOption("originLeft"),
      i = this.layout._getOption("originTop"), n = t[e ? "left" : "right"], o = t[i ? "top" : "bottom"],
      s = this.layout.size, r = -1 != n.indexOf("%") ? parseFloat(n) / 100 * s.width : parseInt(n, 10),
      a = -1 != o.indexOf("%") ? parseFloat(o) / 100 * s.height : parseInt(o, 10);
    r = isNaN(r) ? 0 : r, a = isNaN(a) ? 0 : a, r -= e ? s.paddingLeft : s.paddingRight, a -= i ? s.paddingTop : s.paddingBottom, this.position.x = r, this.position.y = a
  }, h.layoutPosition = function () {
    var t = this.layout.size, e = {}, i = this.layout._getOption("originLeft"),
      n = this.layout._getOption("originTop"), o = i ? "paddingLeft" : "paddingRight", s = i ? "left" : "right",
      r = i ? "right" : "left", a = this.position.x + t[o];
    e[s] = this.getXValue(a), e[r] = "";
    var l = n ? "paddingTop" : "paddingBottom", c = n ? "top" : "bottom", h = n ? "bottom" : "top",
      u = this.position.y + t[l];
    e[c] = this.getYValue(u), e[h] = "", this.css(e), this.emitEvent("layout", [this])
  }, h.getXValue = function (t) {
    var e = this.layout._getOption("horizontal");
    return this.layout.options.percentPosition && !e ? t / this.layout.size.width * 100 + "%" : t + "px"
  }, h.getYValue = function (t) {
    var e = this.layout._getOption("horizontal");
    return this.layout.options.percentPosition && e ? t / this.layout.size.height * 100 + "%" : t + "px"
  }, h._transitionTo = function (t, e) {
    this.getPosition();
    var i = this.position.x, n = this.position.y, o = parseInt(t, 10), s = parseInt(e, 10),
      r = o === this.position.x && s === this.position.y;
    if (this.setPosition(t, e), r && !this.isTransitioning) return void this.layoutPosition();
    var a = t - i, l = e - n, c = {};
    c.transform = this.getTranslate(a, l), this.transition({
      to: c,
      onTransitionEnd: {transform: this.layoutPosition},
      isCleaning: !0
    })
  }, h.getTranslate = function (t, e) {
    var i = this.layout._getOption("originLeft"), n = this.layout._getOption("originTop");
    return t = i ? t : -t, e = n ? e : -e, "translate3d(" + t + "px, " + e + "px, 0)"
  }, h.goTo = function (t, e) {
    this.setPosition(t, e), this.layoutPosition()
  }, h.moveTo = h._transitionTo, h.setPosition = function (t, e) {
    this.position.x = parseInt(t, 10), this.position.y = parseInt(e, 10)
  }, h._nonTransition = function (t) {
    this.css(t.to), t.isCleaning && this._removeStyles(t.to);
    for (var e in t.onTransitionEnd) t.onTransitionEnd[e].call(this)
  }, h.transition = function (t) {
    if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(t);
    var e = this._transn;
    for (var i in t.onTransitionEnd) e.onEnd[i] = t.onTransitionEnd[i];
    for (i in t.to) e.ingProperties[i] = !0, t.isCleaning && (e.clean[i] = !0);
    if (t.from) {
      this.css(t.from);
      var n = this.element.offsetHeight;
      n = null
    }
    this.enableTransition(t.to), this.css(t.to), this.isTransitioning = !0
  };
  var u = "opacity," + o(a);
  h.enableTransition = function () {
    if (!this.isTransitioning) {
      var t = this.layout.options.transitionDuration;
      t = "number" == typeof t ? t + "ms" : t, this.css({
        transitionProperty: u,
        transitionDuration: t,
        transitionDelay: this.staggerDelay || 0
      }), this.element.addEventListener(l, this, !1)
    }
  }, h.onwebkitTransitionEnd = function (t) {
    this.ontransitionend(t)
  }, h.onotransitionend = function (t) {
    this.ontransitionend(t)
  };
  var d = {"-webkit-transform": "transform"};
  h.ontransitionend = function (t) {
    if (t.target === this.element) {
      var e = this._transn, n = d[t.propertyName] || t.propertyName;
      if (delete e.ingProperties[n], i(e.ingProperties) && this.disableTransition(), n in e.clean && (this.element.style[t.propertyName] = "", delete e.clean[n]), n in e.onEnd) {
        var o = e.onEnd[n];
        o.call(this), delete e.onEnd[n]
      }
      this.emitEvent("transitionEnd", [this])
    }
  }, h.disableTransition = function () {
    this.removeTransitionStyles(), this.element.removeEventListener(l, this, !1), this.isTransitioning = !1
  }, h._removeStyles = function (t) {
    var e = {};
    for (var i in t) e[i] = "";
    this.css(e)
  };
  var p = {transitionProperty: "", transitionDuration: "", transitionDelay: ""};
  return h.removeTransitionStyles = function () {
    this.css(p)
  }, h.stagger = function (t) {
    t = isNaN(t) ? 0 : t, this.staggerDelay = t + "ms"
  }, h.removeElem = function () {
    this.element.parentNode.removeChild(this.element), this.css({display: ""}), this.emitEvent("remove", [this])
  }, h.remove = function () {
    return r && parseFloat(this.layout.options.transitionDuration) ? (this.once("transitionEnd", function () {
      this.removeElem()
    }), void this.hide()) : void this.removeElem()
  }, h.reveal = function () {
    delete this.isHidden, this.css({display: ""});
    var t = this.layout.options, e = {}, i = this.getHideRevealTransitionEndProperty("visibleStyle");
    e[i] = this.onRevealTransitionEnd, this.transition({
      from: t.hiddenStyle,
      to: t.visibleStyle,
      isCleaning: !0,
      onTransitionEnd: e
    })
  }, h.onRevealTransitionEnd = function () {
    this.isHidden || this.emitEvent("reveal")
  }, h.getHideRevealTransitionEndProperty = function (t) {
    var e = this.layout.options[t];
    if (e.opacity) return "opacity";
    for (var i in e) return i
  }, h.hide = function () {
    this.isHidden = !0, this.css({display: ""});
    var t = this.layout.options, e = {}, i = this.getHideRevealTransitionEndProperty("hiddenStyle");
    e[i] = this.onHideTransitionEnd, this.transition({
      from: t.visibleStyle,
      to: t.hiddenStyle,
      isCleaning: !0,
      onTransitionEnd: e
    })
  }, h.onHideTransitionEnd = function () {
    this.isHidden && (this.css({display: "none"}), this.emitEvent("hide"))
  }, h.destroy = function () {
    this.css({position: "", left: "", right: "", top: "", bottom: "", transition: "", transform: ""})
  }, n
}), function (t, e) {
  "use strict";
  "function" == typeof define && define.amd ? define("outlayer/outlayer", ["ev-emitter/ev-emitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function (i, n, o, s) {
    return e(t, i, n, o, s)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("ev-emitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : t.Outlayer = e(t, t.EvEmitter, t.getSize, t.fizzyUIUtils, t.Outlayer.Item)
}(window, function (t, e, i, n, o) {
  "use strict";

  function s(t, e) {
    var i = n.getQueryElement(t);
    if (!i) return void(l && l.error("Bad element for " + this.constructor.namespace + ": " + (i || t)));
    this.element = i, c && (this.$element = c(this.element)), this.options = n.extend({}, this.constructor.defaults), this.option(e);
    var o = ++u;
    this.element.outlayerGUID = o, d[o] = this, this._create();
    var s = this._getOption("initLayout");
    s && this.layout()
  }

  function r(t) {
    function e() {
      t.apply(this, arguments)
    }

    return e.prototype = Object.create(t.prototype), e.prototype.constructor = e, e
  }

  function a(t) {
    if ("number" == typeof t) return t;
    var e = t.match(/(^\d*\.?\d*)(\w*)/), i = e && e[1], n = e && e[2];
    if (!i.length) return 0;
    i = parseFloat(i);
    var o = f[n] || 1;
    return i * o
  }

  var l = t.console, c = t.jQuery, h = function () {
  }, u = 0, d = {};
  s.namespace = "outlayer", s.Item = o, s.defaults = {
    containerStyle: {position: "relative"},
    initLayout: !0,
    originLeft: !0,
    originTop: !0,
    resize: !0,
    resizeContainer: !0,
    transitionDuration: "0.4s",
    hiddenStyle: {opacity: 0, transform: "scale(0.001)"},
    visibleStyle: {opacity: 1, transform: "scale(1)"}
  };
  var p = s.prototype;
  n.extend(p, e.prototype), p.option = function (t) {
    n.extend(this.options, t)
  }, p._getOption = function (t) {
    var e = this.constructor.compatOptions[t];
    return e && void 0 !== this.options[e] ? this.options[e] : this.options[t]
  }, s.compatOptions = {
    initLayout: "isInitLayout",
    horizontal: "isHorizontal",
    layoutInstant: "isLayoutInstant",
    originLeft: "isOriginLeft",
    originTop: "isOriginTop",
    resize: "isResizeBound",
    resizeContainer: "isResizingContainer"
  }, p._create = function () {
    this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), n.extend(this.element.style, this.options.containerStyle);
    var t = this._getOption("resize");
    t && this.bindResize()
  }, p.reloadItems = function () {
    this.items = this._itemize(this.element.children)
  }, p._itemize = function (t) {
    for (var e = this._filterFindItemElements(t), i = this.constructor.Item, n = [], o = 0; o < e.length; o++) {
      var s = e[o], r = new i(s, this);
      n.push(r)
    }
    return n
  }, p._filterFindItemElements = function (t) {
    return n.filterFindElements(t, this.options.itemSelector)
  }, p.getItemElements = function () {
    return this.items.map(function (t) {
      return t.element
    })
  }, p.layout = function () {
    this._resetLayout(), this._manageStamps();
    var t = this._getOption("layoutInstant"), e = void 0 !== t ? t : !this._isLayoutInited;
    this.layoutItems(this.items, e), this._isLayoutInited = !0
  }, p._init = p.layout, p._resetLayout = function () {
    this.getSize()
  }, p.getSize = function () {
    this.size = i(this.element)
  }, p._getMeasurement = function (t, e) {
    var n, o = this.options[t];
    o ? ("string" == typeof o ? n = this.element.querySelector(o) : o instanceof HTMLElement && (n = o), this[t] = n ? i(n)[e] : o) : this[t] = 0
  }, p.layoutItems = function (t, e) {
    t = this._getItemsForLayout(t), this._layoutItems(t, e), this._postLayout()
  }, p._getItemsForLayout = function (t) {
    return t.filter(function (t) {
      return !t.isIgnored
    })
  }, p._layoutItems = function (t, e) {
    if (this._emitCompleteOnItems("layout", t), t && t.length) {
      var i = [];
      t.forEach(function (t) {
        var n = this._getItemLayoutPosition(t);
        n.item = t, n.isInstant = e || t.isLayoutInstant, i.push(n)
      }, this), this._processLayoutQueue(i)
    }
  }, p._getItemLayoutPosition = function () {
    return {x: 0, y: 0}
  }, p._processLayoutQueue = function (t) {
    this.updateStagger(), t.forEach(function (t, e) {
      this._positionItem(t.item, t.x, t.y, t.isInstant, e)
    }, this)
  }, p.updateStagger = function () {
    var t = this.options.stagger;
    return null === t || void 0 === t ? void(this.stagger = 0) : (this.stagger = a(t), this.stagger)
  }, p._positionItem = function (t, e, i, n, o) {
    n ? t.goTo(e, i) : (t.stagger(o * this.stagger), t.moveTo(e, i))
  }, p._postLayout = function () {
    this.resizeContainer()
  }, p.resizeContainer = function () {
    var t = this._getOption("resizeContainer");
    if (t) {
      var e = this._getContainerSize();
      e && (this._setContainerMeasure(e.width, !0), this._setContainerMeasure(e.height, !1))
    }
  }, p._getContainerSize = h, p._setContainerMeasure = function (t, e) {
    if (void 0 !== t) {
      var i = this.size;
      i.isBorderBox && (t += e ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth), t = Math.max(t, 0), this.element.style[e ? "width" : "height"] = t + "px"
    }
  }, p._emitCompleteOnItems = function (t, e) {
    function i() {
      o.dispatchEvent(t + "Complete", null, [e])
    }

    function n() {
      r++, r == s && i()
    }

    var o = this, s = e.length;
    if (!e || !s) return void i();
    var r = 0;
    e.forEach(function (e) {
      e.once(t, n)
    })
  }, p.dispatchEvent = function (t, e, i) {
    var n = e ? [e].concat(i) : i;
    if (this.emitEvent(t, n), c) if (this.$element = this.$element || c(this.element), e) {
      var o = c.Event(e);
      o.type = t, this.$element.trigger(o, i)
    } else this.$element.trigger(t, i)
  }, p.ignore = function (t) {
    var e = this.getItem(t);
    e && (e.isIgnored = !0)
  }, p.unignore = function (t) {
    var e = this.getItem(t);
    e && delete e.isIgnored
  }, p.stamp = function (t) {
    t = this._find(t), t && (this.stamps = this.stamps.concat(t), t.forEach(this.ignore, this))
  }, p.unstamp = function (t) {
    t = this._find(t), t && t.forEach(function (t) {
      n.removeFrom(this.stamps, t), this.unignore(t)
    }, this)
  }, p._find = function (t) {
    return t ? ("string" == typeof t && (t = this.element.querySelectorAll(t)), t = n.makeArray(t)) : void 0
  }, p._manageStamps = function () {
    this.stamps && this.stamps.length && (this._getBoundingRect(), this.stamps.forEach(this._manageStamp, this))
  }, p._getBoundingRect = function () {
    var t = this.element.getBoundingClientRect(), e = this.size;
    this._boundingRect = {
      left: t.left + e.paddingLeft + e.borderLeftWidth,
      top: t.top + e.paddingTop + e.borderTopWidth,
      right: t.right - (e.paddingRight + e.borderRightWidth),
      bottom: t.bottom - (e.paddingBottom + e.borderBottomWidth)
    }
  }, p._manageStamp = h, p._getElementOffset = function (t) {
    var e = t.getBoundingClientRect(), n = this._boundingRect, o = i(t), s = {
      left: e.left - n.left - o.marginLeft,
      top: e.top - n.top - o.marginTop,
      right: n.right - e.right - o.marginRight,
      bottom: n.bottom - e.bottom - o.marginBottom
    };
    return s
  }, p.handleEvent = n.handleEvent, p.bindResize = function () {
    t.addEventListener("resize", this), this.isResizeBound = !0
  }, p.unbindResize = function () {
    t.removeEventListener("resize", this), this.isResizeBound = !1
  }, p.onresize = function () {
    this.resize()
  }, n.debounceMethod(s, "onresize", 100), p.resize = function () {
    this.isResizeBound && this.needsResizeLayout() && this.layout()
  }, p.needsResizeLayout = function () {
    var t = i(this.element), e = this.size && t;
    return e && t.innerWidth !== this.size.innerWidth
  }, p.addItems = function (t) {
    var e = this._itemize(t);
    return e.length && (this.items = this.items.concat(e)), e
  }, p.appended = function (t) {
    var e = this.addItems(t);
    e.length && (this.layoutItems(e, !0), this.reveal(e))
  }, p.prepended = function (t) {
    var e = this._itemize(t);
    if (e.length) {
      var i = this.items.slice(0);
      this.items = e.concat(i), this._resetLayout(), this._manageStamps(), this.layoutItems(e, !0), this.reveal(e), this.layoutItems(i)
    }
  }, p.reveal = function (t) {
    if (this._emitCompleteOnItems("reveal", t), t && t.length) {
      var e = this.updateStagger();
      t.forEach(function (t, i) {
        t.stagger(i * e), t.reveal()
      })
    }
  }, p.hide = function (t) {
    if (this._emitCompleteOnItems("hide", t), t && t.length) {
      var e = this.updateStagger();
      t.forEach(function (t, i) {
        t.stagger(i * e), t.hide()
      })
    }
  }, p.revealItemElements = function (t) {
    var e = this.getItems(t);
    this.reveal(e)
  }, p.hideItemElements = function (t) {
    var e = this.getItems(t);
    this.hide(e)
  }, p.getItem = function (t) {
    for (var e = 0; e < this.items.length; e++) {
      var i = this.items[e];
      if (i.element == t) return i
    }
  }, p.getItems = function (t) {
    t = n.makeArray(t);
    var e = [];
    return t.forEach(function (t) {
      var i = this.getItem(t);
      i && e.push(i)
    }, this), e
  }, p.remove = function (t) {
    var e = this.getItems(t);
    this._emitCompleteOnItems("remove", e), e && e.length && e.forEach(function (t) {
      t.remove(), n.removeFrom(this.items, t)
    }, this)
  }, p.destroy = function () {
    var t = this.element.style;
    t.height = "", t.position = "", t.width = "", this.items.forEach(function (t) {
      t.destroy()
    }), this.unbindResize();
    var e = this.element.outlayerGUID;
    delete d[e], delete this.element.outlayerGUID, c && c.removeData(this.element, this.constructor.namespace)
  }, s.data = function (t) {
    t = n.getQueryElement(t);
    var e = t && t.outlayerGUID;
    return e && d[e]
  }, s.create = function (t, e) {
    var i = r(s);
    return i.defaults = n.extend({}, s.defaults), n.extend(i.defaults, e), i.compatOptions = n.extend({}, s.compatOptions), i.namespace = t, i.data = s.data, i.Item = r(o), n.htmlInit(i, t), c && c.bridget && c.bridget(t, i), i
  };
  var f = {ms: 1, s: 1e3};
  return s.Item = o, s
}), function (t, e) {
  "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size"], e) : "object" == typeof module && module.exports ? module.exports = e(require("outlayer"), require("get-size")) : t.Masonry = e(t.Outlayer, t.getSize)
}(window, function (t, e) {
  var i = t.create("masonry");
  return i.compatOptions.fitWidth = "isFitWidth", i.prototype._resetLayout = function () {
    this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns(), this.colYs = [];
    for (var t = 0; t < this.cols; t++) this.colYs.push(0);
    this.maxY = 0
  }, i.prototype.measureColumns = function () {
    if (this.getContainerWidth(), !this.columnWidth) {
      var t = this.items[0], i = t && t.element;
      this.columnWidth = i && e(i).outerWidth || this.containerWidth
    }
    var n = this.columnWidth += this.gutter, o = this.containerWidth + this.gutter, s = o / n, r = n - o % n,
      a = r && 1 > r ? "round" : "floor";
    s = Math[a](s), this.cols = Math.max(s, 1)
  }, i.prototype.getContainerWidth = function () {
    var t = this._getOption("fitWidth"), i = t ? this.element.parentNode : this.element, n = e(i);
    this.containerWidth = n && n.innerWidth
  }, i.prototype._getItemLayoutPosition = function (t) {
    t.getSize();
    var e = t.size.outerWidth % this.columnWidth, i = e && 1 > e ? "round" : "ceil",
      n = Math[i](t.size.outerWidth / this.columnWidth);
    n = Math.min(n, this.cols);
    for (var o = this._getColGroup(n), s = Math.min.apply(Math, o), r = o.indexOf(s), a = {
      x: this.columnWidth * r,
      y: s
    }, l = s + t.size.outerHeight, c = this.cols + 1 - o.length, h = 0; c > h; h++) this.colYs[r + h] = l;
    return a
  }, i.prototype._getColGroup = function (t) {
    if (2 > t) return this.colYs;
    for (var e = [], i = this.cols + 1 - t, n = 0; i > n; n++) {
      var o = this.colYs.slice(n, n + t);
      e[n] = Math.max.apply(Math, o)
    }
    return e
  }, i.prototype._manageStamp = function (t) {
    var i = e(t), n = this._getElementOffset(t), o = this._getOption("originLeft"), s = o ? n.left : n.right,
      r = s + i.outerWidth, a = Math.floor(s / this.columnWidth);
    a = Math.max(0, a);
    var l = Math.floor(r / this.columnWidth);
    l -= r % this.columnWidth ? 0 : 1, l = Math.min(this.cols - 1, l);
    for (var c = this._getOption("originTop"), h = (c ? n.top : n.bottom) + i.outerHeight, u = a; l >= u; u++) this.colYs[u] = Math.max(h, this.colYs[u])
  }, i.prototype._getContainerSize = function () {
    this.maxY = Math.max.apply(Math, this.colYs);
    var t = {height: this.maxY};
    return this._getOption("fitWidth") && (t.width = this._getContainerFitWidth()), t
  }, i.prototype._getContainerFitWidth = function () {
    for (var t = 0, e = this.cols; --e && 0 === this.colYs[e];) t++;
    return (this.cols - t) * this.columnWidth - this.gutter
  }, i.prototype.needsResizeLayout = function () {
    var t = this.containerWidth;
    return this.getContainerWidth(), t != this.containerWidth
  }, i
}), function (t, e, i, n) {
  var o = function (n, o) {
    this.elem = n, this.$elem = t(n), this.options = o, this.metadata = this.$elem.data("plugin-options"), this.$win = t(e), this.sections = {}, this.didScroll = !1, this.$doc = t(i), this.docHeight = this.$doc.height()
  };
  o.prototype = {
    defaults: {
      navItems: "a",
      currentClass: "current",
      changeHash: !1,
      easing: "swing",
      filter: "",
      scrollSpeed: 750,
      scrollThreshold: .5,
      begin: !1,
      end: !1,
      scrollChange: !1
    }, init: function () {
      return this.config = t.extend({}, this.defaults, this.options, this.metadata), this.$nav = this.$elem.find(this.config.navItems), "" !== this.config.filter && (this.$nav = this.$nav.filter(this.config.filter)), this.$nav.on("click.onePageNav", t.proxy(this.handleClick, this)), this.getPositions(), this.bindInterval(), this.$win.on("resize.onePageNav", t.proxy(this.getPositions, this)), this
    }, adjustNav: function (t, e) {
      t.$elem.find("." + t.config.currentClass).removeClass(t.config.currentClass), e.addClass(t.config.currentClass)
    }, bindInterval: function () {
      var t, e = this;
      e.$win.on("scroll.onePageNav", function () {
        e.didScroll = !0
      }), e.t = setInterval(function () {
        t = e.$doc.height(), e.didScroll && (e.didScroll = !1, e.scrollChange()), t !== e.docHeight && (e.docHeight = t, e.getPositions())
      }, 250)
    }, getHash: function (t) {
      return t.attr("href").split("#")[1]
    }, getPositions: function () {
      var e, i, n, o = this;
      o.$nav.each(function () {
        e = o.getHash(t(this)), n = t("#" + e), n.length && (i = n.offset().top, o.sections[e] = Math.round(i))
      })
    }, getSection: function (t) {
      var e = null, i = Math.round(this.$win.height() * this.config.scrollThreshold);
      for (var n in this.sections) this.sections[n] - i < t && (e = n);
      return e
    }, handleClick: function (i) {
      var n = this, o = t(i.currentTarget), s = o.parent(), r = "#" + n.getHash(o);
      s.hasClass(n.config.currentClass) || (n.config.begin && n.config.begin(), n.adjustNav(n, s), n.unbindInterval(), n.scrollTo(r, function () {
        n.config.changeHash && (e.location.hash = r), n.bindInterval(), n.config.end && n.config.end()
      })), i.preventDefault()
    }, scrollChange: function () {
      var i, n = this.$win.scrollTop(), o = this.getSection(n);
      null !== o && (i = this.$elem.find('a[href$="#' + o + '"]').parent(), i.hasClass(this.config.currentClass) || (this.adjustNav(this, i), t(".scroll-page").removeClass("visible"), e.setTimeout(function () {
        t(i.find("a").attr("href")).addClass("visible")
      }, 0), this.config.scrollChange && this.config.scrollChange(i)))
    }, scrollTo: function (e, i) {
      var n = t(e).offset().top;
      t("html, body").animate({scrollTop: n}, this.config.scrollSpeed, this.config.easing, i)
    }, unbindInterval: function () {
      clearInterval(this.t), this.$win.unbind("scroll.onePageNav")
    }
  }, o.defaults = o.prototype.defaults, t.fn.onePageNav = function (t) {
    return this.each(function () {
      new o(this, t).init()
    })
  }
}(jQuery, window, document), $.fn.isOnScreen = function (t) {
  if (!t) var t = $(window);
  var e = {top: t.scrollTop(), left: t.scrollLeft()};
  e.right = e.left + t.width(), e.bottom = e.top + .9 * t.height();
  var i = this.offset();
  return i.right = i.left + this.outerWidth(), i.bottom = i.top + this.outerHeight(), !(e.right < i.left || e.left > i.right || e.bottom < i.top || e.top > i.bottom)
}, !function (t, e) {
  "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", e) : "object" == typeof module && module.exports ? module.exports = e() : t.EvEmitter = e()
}("undefined" != typeof window ? window : this, function () {
  function t() {
  }

  var e = t.prototype;
  return e.on = function (t, e) {
    if (t && e) {
      var i = this._events = this._events || {}, n = i[t] = i[t] || [];
      return -1 == n.indexOf(e) && n.push(e), this
    }
  }, e.once = function (t, e) {
    if (t && e) {
      this.on(t, e);
      var i = this._onceEvents = this._onceEvents || {}, n = i[t] = i[t] || {};
      return n[e] = !0, this
    }
  }, e.off = function (t, e) {
    var i = this._events && this._events[t];
    if (i && i.length) {
      var n = i.indexOf(e);
      return -1 != n && i.splice(n, 1), this
    }
  }, e.emitEvent = function (t, e) {
    var i = this._events && this._events[t];
    if (i && i.length) {
      var n = 0, o = i[n];
      e = e || [];
      for (var s = this._onceEvents && this._onceEvents[t]; o;) {
        var r = s && s[o];
        r && (this.off(t, o), delete s[o]), o.apply(this, e), n += r ? 0 : 1, o = i[n]
      }
      return this
    }
  }, t
}), function (t, e) {
  "use strict";
  "function" == typeof define && define.amd ? define(["ev-emitter/ev-emitter"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("ev-emitter")) : t.imagesLoaded = e(t, t.EvEmitter)
}(window, function (t, e) {
  function i(t, e) {
    for (var i in e) t[i] = e[i];
    return t
  }

  function n(t) {
    var e = [];
    if (Array.isArray(t)) e = t; else if ("number" == typeof t.length) for (var i = 0; i < t.length; i++) e.push(t[i]); else e.push(t);
    return e
  }

  function o(t, e, s) {
    return this instanceof o ? ("string" == typeof t && (t = document.querySelectorAll(t)), this.elements = n(t), this.options = i({}, this.options), "function" == typeof e ? s = e : i(this.options, e), s && this.on("always", s), this.getImages(), a && (this.jqDeferred = new a.Deferred), void setTimeout(function () {
      this.check()
    }.bind(this))) : new o(t, e, s)
  }

  function s(t) {
    this.img = t
  }

  function r(t, e) {
    this.url = t, this.element = e, this.img = new Image
  }

  var a = t.jQuery, l = t.console;
  o.prototype = Object.create(e.prototype), o.prototype.options = {}, o.prototype.getImages = function () {
    this.images = [], this.elements.forEach(this.addElementImages, this)
  }, o.prototype.addElementImages = function (t) {
    "IMG" == t.nodeName && this.addImage(t), this.options.background === !0 && this.addElementBackgroundImages(t);
    var e = t.nodeType;
    if (e && c[e]) {
      for (var i = t.querySelectorAll("img"), n = 0; n < i.length; n++) {
        var o = i[n];
        this.addImage(o)
      }
      if ("string" == typeof this.options.background) {
        var s = t.querySelectorAll(this.options.background);
        for (n = 0; n < s.length; n++) {
          var r = s[n];
          this.addElementBackgroundImages(r)
        }
      }
    }
  };
  var c = {1: !0, 9: !0, 11: !0};
  return o.prototype.addElementBackgroundImages = function (t) {
    var e = getComputedStyle(t);
    if (e) for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(e.backgroundImage); null !== n;) {
      var o = n && n[2];
      o && this.addBackground(o, t), n = i.exec(e.backgroundImage)
    }
  }, o.prototype.addImage = function (t) {
    var e = new s(t);
    this.images.push(e)
  }, o.prototype.addBackground = function (t, e) {
    var i = new r(t, e);
    this.images.push(i)
  }, o.prototype.check = function () {
    function t(t, i, n) {
      setTimeout(function () {
        e.progress(t, i, n)
      })
    }

    var e = this;
    return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function (e) {
      e.once("progress", t), e.check()
    }) : void this.complete()
  }, o.prototype.progress = function (t, e, i) {
    this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !t.isLoaded, this.emitEvent("progress", [this, t, e]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, t), this.progressedCount == this.images.length && this.complete(), this.options.debug && l && l.log("progress: " + i, t, e)
  }, o.prototype.complete = function () {
    var t = this.hasAnyBroken ? "fail" : "done";
    if (this.isComplete = !0, this.emitEvent(t, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
      var e = this.hasAnyBroken ? "reject" : "resolve";
      this.jqDeferred[e](this)
    }
  }, s.prototype = Object.create(e.prototype), s.prototype.check = function () {
    var t = this.getIsImageComplete();
    return t ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void(this.proxyImage.src = this.img.src))
  }, s.prototype.getIsImageComplete = function () {
    return this.img.complete && void 0 !== this.img.naturalWidth
  }, s.prototype.confirm = function (t, e) {
    this.isLoaded = t, this.emitEvent("progress", [this, this.img, e])
  }, s.prototype.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t)
  }, s.prototype.onload = function () {
    this.confirm(!0, "onload"), this.unbindEvents()
  }, s.prototype.onerror = function () {
    this.confirm(!1, "onerror"), this.unbindEvents()
  }, s.prototype.unbindEvents = function () {
    this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
  }, r.prototype = Object.create(s.prototype), r.prototype.check = function () {
    this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url;
    var t = this.getIsImageComplete();
    t && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents())
  }, r.prototype.unbindEvents = function () {
    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
  }, r.prototype.confirm = function (t, e) {
    this.isLoaded = t, this.emitEvent("progress", [this, this.element, e])
  }, o.makeJQueryPlugin = function (e) {
    e = e || t.jQuery, e && (a = e, a.fn.imagesLoaded = function (t, e) {
      var i = new o(this, t, e);
      return i.jqDeferred.promise(a(this))
    })
  }, o.makeJQueryPlugin(), o
}), !function (t) {
  "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof module && module.exports ? module.exports = function (e, i) {
    return void 0 === i && (i = "undefined" != typeof window ? require("jquery") : require("jquery")(e)), t(i), i
  } : t(jQuery)
}(function (t) {
  "use strict";
  var e = "drawsvg", i = {duration: 1e3, stagger: 200, easing: "swing", reverse: !1, callback: t.noop},
    n = function () {
      var n = function (n, o) {
        var s = this, r = t.extend(i, o);
        s.$elm = t(n), s.$elm.is("svg") && (s.options = r, s.$paths = s.$elm.find("path"), s.totalDuration = r.duration + r.stagger * s.$paths.length, s.duration = r.duration / s.totalDuration, s.$paths.each(function (t, e) {
          var i = e.getTotalLength();
          e.pathLen = i, e.delay = r.stagger * t / s.totalDuration, e.style.strokeDasharray = [i, i].join(" "), e.style.strokeDashoffset = i
        }), s.$elm.attr("class", function (t, i) {
          return [i, e + "-initialized"].join(" ")
        }))
      };
      return n.prototype.getVal = function (e, i) {
        return 1 - t.easing[i](e, e, 0, 1, 1)
      }, n.prototype.progress = function (t) {
        var e = this, i = e.options, n = e.duration;
        e.$paths.each(function (o, s) {
          var r = s.style;
          if (1 === t) r.strokeDashoffset = 0; else if (0 === t) r.strokeDashoffset = s.pathLen + "px"; else if (t >= s.delay && t <= n + s.delay) {
            var a = (t - s.delay) / n;
            r.strokeDashoffset = e.getVal(a, i.easing) * s.pathLen * (i.reverse ? -1 : 1) + "px"
          }
        })
      }, n.prototype.animate = function () {
        var i = this;
        i.$elm.attr("class", function (t, i) {
          return [i, e + "-animating"].join(" ")
        }), t({len: 0}).animate({len: 1}, {
          easing: "linear", duration: i.totalDuration, step: function (t, e) {
            i.progress.call(i, t / e.end)
          }, complete: function () {
            i.options.callback.call(this), i.$elm.attr("class", function (t, i) {
              return i.replace(e + "-animating", "")
            })
          }
        })
      }, n
    }();
  t.fn[e] = function (i, o) {
    return this.each(function () {
      var s = t.data(this, e);
      s && "" + i === i && s[i] ? s[i](o) : t.data(this, e, new n(this, i))
    })
  }
}), function (t, e) {
  "function" == typeof define && define.amd ? define([], e) : "object" == typeof module && module.exports ? module.exports = e() : t.anime = e()
}(this, function () {
  var t, e = {
      duration: 1e3,
      delay: 0,
      loop: !1,
      autoplay: !0,
      direction: "normal",
      easing: "easeOutElastic",
      elasticity: 400,
      round: !1,
      begin: void 0,
      update: void 0,
      complete: void 0
    },
    i = "translateX translateY translateZ rotate rotateX rotateY rotateZ scale scaleX scaleY scaleZ skewX skewY".split(" "),
    n = {
      arr: function (t) {
        return Array.isArray(t)
      }, obj: function (t) {
        return -1 < Object.prototype.toString.call(t).indexOf("Object")
      }, svg: function (t) {
        return t instanceof SVGElement
      }, dom: function (t) {
        return t.nodeType || n.svg(t)
      }, num: function (t) {
        return !isNaN(parseInt(t))
      }, str: function (t) {
        return "string" == typeof t
      }, fnc: function (t) {
        return "function" == typeof t
      }, und: function (t) {
        return "undefined" == typeof t
      }, nul: function (t) {
        return "null" == typeof t
      }, hex: function (t) {
        return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(t)
      }, rgb: function (t) {
        return /^rgb/.test(t)
      }, hsl: function (t) {
        return /^hsl/.test(t)
      }, col: function (t) {
        return n.hex(t) || n.rgb(t) || n.hsl(t)
      }
    }, o = function () {
      var t = {}, e = {
        Sine: function (t) {
          return 1 - Math.cos(t * Math.PI / 2)
        }, Circ: function (t) {
          return 1 - Math.sqrt(1 - t * t)
        }, Elastic: function (t, e) {
          if (0 === t || 1 === t) return t;
          var i = 1 - Math.min(e, 998) / 1e3, n = t / 1 - 1;
          return -(Math.pow(2, 10 * n) * Math.sin(2 * (n - i / (2 * Math.PI) * Math.asin(1)) * Math.PI / i))
        }, Back: function (t) {
          return t * t * (3 * t - 2)
        }, Bounce: function (t) {
          for (var e, i = 4; t < ((e = Math.pow(2, --i)) - 1) / 11;) ;
          return 1 / Math.pow(4, 3 - i) - 7.5625 * Math.pow((3 * e - 2) / 22 - t, 2)
        }
      };
      return ["Quad", "Cubic", "Quart", "Quint", "Expo"].forEach(function (t, i) {
        e[t] = function (t) {
          return Math.pow(t, i + 2)
        }
      }), Object.keys(e).forEach(function (i) {
        var n = e[i];
        t["easeIn" + i] = n, t["easeOut" + i] = function (t, e) {
          return 1 - n(1 - t, e)
        }, t["easeInOut" + i] = function (t, e) {
          return .5 > t ? n(2 * t, e) / 2 : 1 - n(-2 * t + 2, e) / 2
        }, t["easeOutIn" + i] = function (t, e) {
          return .5 > t ? (1 - n(1 - 2 * t, e)) / 2 : (n(2 * t - 1, e) + 1) / 2
        }
      }), t.linear = function (t) {
        return t
      }, t
    }(), s = function (t) {
      return n.str(t) ? t : t + ""
    }, r = function (t) {
      return t.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase()
    }, a = function (t) {
      if (n.col(t)) return !1;
      try {
        return document.querySelectorAll(t)
      } catch (e) {
        return !1
      }
    }, l = function (t) {
      return t.reduce(function (t, e) {
        return t.concat(n.arr(e) ? l(e) : e)
      }, [])
    }, c = function (t) {
      return n.arr(t) ? t : (n.str(t) && (t = a(t) || t), t instanceof NodeList || t instanceof HTMLCollection ? [].slice.call(t) : [t])
    }, h = function (t, e) {
      return t.some(function (t) {
        return t === e
      })
    }, u = function (t, e) {
      var i = {};
      return t.forEach(function (t) {
        var n = JSON.stringify(e.map(function (e) {
          return t[e]
        }));
        i[n] = i[n] || [], i[n].push(t)
      }), Object.keys(i).map(function (t) {
        return i[t]
      })
    }, d = function (t) {
      return t.filter(function (t, e, i) {
        return i.indexOf(t) === e
      })
    }, p = function (t) {
      var e, i = {};
      for (e in t) i[e] = t[e];
      return i
    }, f = function (t, e) {
      for (var i in e) t[i] = n.und(t[i]) ? e[i] : t[i];
      return t
    }, m = function (t) {
      t = t.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function (t, e, i, n) {
        return e + e + i + i + n + n
      });
      var e = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(t);
      t = parseInt(e[1], 16);
      var i = parseInt(e[2], 16), e = parseInt(e[3], 16);
      return "rgb(" + t + "," + i + "," + e + ")"
    }, g = function (t) {
      t = /hsl\((\d+),\s*([\d.]+)%,\s*([\d.]+)%\)/g.exec(t);
      var e = parseInt(t[1]) / 360, i = parseInt(t[2]) / 100, n = parseInt(t[3]) / 100;
      if (t = function (t, e, i) {
        return 0 > i && (i += 1), 1 < i && --i, i < 1 / 6 ? t + 6 * (e - t) * i : .5 > i ? e : i < 2 / 3 ? t + (e - t) * (2 / 3 - i) * 6 : t
      }, 0 == i) i = n = e = n; else var o = .5 > n ? n * (1 + i) : n + i - n * i, s = 2 * n - o,
        i = t(s, o, e + 1 / 3), n = t(s, o, e), e = t(s, o, e - 1 / 3);
      return "rgb(" + 255 * i + "," + 255 * n + "," + 255 * e + ")"
    }, v = function (t) {
      return /([\+\-]?[0-9|auto\.]+)(%|px|pt|em|rem|in|cm|mm|ex|pc|vw|vh|deg)?/.exec(t)[2]
    }, y = function (t, e, i) {
      return v(e) ? e : -1 < t.indexOf("translate") ? v(i) ? e + v(i) : e + "px" : -1 < t.indexOf("rotate") || -1 < t.indexOf("skew") ? e + "deg" : e
    }, b = function (t, e) {
      if (e in t.style) return getComputedStyle(t).getPropertyValue(r(e)) || "0"
    }, w = function (t, e) {
      var i = -1 < e.indexOf("scale") ? 1 : 0, n = t.style.transform;
      if (!n) return i;
      for (var o = /(\w+)\((.+?)\)/g, s = [], r = [], a = []; s = o.exec(n);) r.push(s[1]), a.push(s[2]);
      return n = a.filter(function (t, i) {
        return r[i] === e
      }), n.length ? n[0] : i
    }, x = function (t, e) {
      return n.dom(t) && h(i, e) ? "transform" : n.dom(t) && (t.getAttribute(e) || n.svg(t) && t[e]) ? "attribute" : n.dom(t) && "transform" !== e && b(t, e) ? "css" : n.nul(t[e]) || n.und(t[e]) ? void 0 : "object"
    }, _ = function (t, e) {
      switch (x(t, e)) {
        case"transform":
          return w(t, e);
        case"css":
          return b(t, e);
        case"attribute":
          return t.getAttribute(e)
      }
      return t[e] || 0
    }, C = function (t, e, i) {
      return n.col(e) ? e = n.rgb(e) ? e : n.hex(e) ? m(e) : n.hsl(e) ? g(e) : void 0 : v(e) ? e : (t = v(v(t.to) ? t.to : t.from), !t && i && (t = v(i)), t ? e + t : e)
    }, P = function (t) {
      var e = /-?\d*\.?\d+/g;
      return {original: t, numbers: s(t).match(e) ? s(t).match(e).map(Number) : [0], strings: s(t).split(e)}
    }, S = function (t, e, i) {
      return e.reduce(function (e, n, o) {
        return n = n ? n : i[o - 1], e + t[o - 1] + n
      })
    }, E = function (t) {
      return t = t ? l(n.arr(t) ? t.map(c) : c(t)) : [], t.map(function (t, e) {
        return {target: t, id: e}
      })
    }, k = function (t, e, i, n) {
      return "transform" === i ? (i = t + "(" + y(t, e.from, e.to) + ")", e = t + "(" + y(t, e.to) + ")") : (t = "css" === i ? b(n, t) : void 0, i = C(e, e.from, t), e = C(e, e.to, t)), {
        from: P(i),
        to: P(e)
      }
    }, I = function (t, e) {
      var i = [];
      return t.forEach(function (o, s) {
        var r = o.target;
        return e.forEach(function (e) {
          var a = x(r, e.name);
          if (a) {
            var l;
            l = e.name;
            var h = e.value, h = c(n.fnc(h) ? h(r, s) : h);
            l = {
              from: 1 < h.length ? h[0] : _(r, l),
              to: 1 < h.length ? h[1] : h[0]
            }, h = p(e), h.animatables = o, h.type = a, h.from = k(e.name, l, h.type, r).from, h.to = k(e.name, l, h.type, r).to, h.round = n.col(l.from) || h.round ? 1 : 0, h.delay = (n.fnc(h.delay) ? h.delay(r, s, t.length) : h.delay) / F.speed, h.duration = (n.fnc(h.duration) ? h.duration(r, s, t.length) : h.duration) / F.speed, i.push(h)
          }
        })
      }), i
    }, $ = function (t, e) {
      var i = I(t, e);
      return u(i, ["name", "from", "to", "delay", "duration"]).map(function (t) {
        var e = p(t[0]);
        return e.animatables = t.map(function (t) {
          return t.animatables
        }), e.totalDuration = e.delay + e.duration, e
      })
    }, O = function (t, e) {
      t.tweens.forEach(function (i) {
        var n = i.from, o = t.duration - (i.delay + i.duration);
        i.from = i.to, i.to = n, e && (i.delay = o)
      }), t.reversed = !t.reversed
    }, T = function (t) {
      if (t.length) return Math.max.apply(Math, t.map(function (t) {
        return t.totalDuration
      }))
    }, L = function (t) {
      var e = [], i = [];
      return t.tweens.forEach(function (t) {
        "css" !== t.type && "transform" !== t.type || (e.push("css" === t.type ? r(t.name) : "transform"), t.animatables.forEach(function (t) {
          i.push(t.target)
        }))
      }), {properties: d(e).join(", "), elements: d(i)}
    }, M = function (t) {
      var e = L(t);
      e.elements.forEach(function (t) {
        t.style.willChange = e.properties
      })
    }, A = function (t) {
      L(t).elements.forEach(function (t) {
        t.style.removeProperty("will-change")
      })
    }, z = function (t, e) {
      var i = t.path, n = t.value * e, o = function (o) {
        return o = o || 0, i.getPointAtLength(1 < e ? t.value + o : n + o)
      }, s = o(), r = o(-1), o = o(1);
      switch (t.name) {
        case"translateX":
          return s.x;
        case"translateY":
          return s.y;
        case"rotate":
          return 180 * Math.atan2(o.y - r.y, o.x - r.x) / Math.PI
      }
    }, H = function (t, e) {
      var i = Math.min(Math.max(e - t.delay, 0), t.duration) / t.duration, n = t.to.numbers.map(function (e, n) {
        var s = t.from.numbers[n], r = o[t.easing](i, t.elasticity), s = t.path ? z(t, r) : s + r * (e - s);
        return s = t.round ? Math.round(s * t.round) / t.round : s
      });
      return S(n, t.to.strings, t.from.strings)
    }, D = function (e, i) {
      var n;
      e.currentTime = i, e.progress = i / e.duration * 100;
      for (var o = 0; o < e.tweens.length; o++) {
        var s = e.tweens[o];
        s.currentValue = H(s, i);
        for (var r = s.currentValue, a = 0; a < s.animatables.length; a++) {
          var l = s.animatables[a], c = l.id, l = l.target, h = s.name;
          switch (s.type) {
            case"css":
              l.style[h] = r;
              break;
            case"attribute":
              l.setAttribute(h, r);
              break;
            case"object":
              l[h] = r;
              break;
            case"transform":
              n || (n = {}), n[c] || (n[c] = []), n[c].push(r)
          }
        }
      }
      if (n) for (o in t || (t = (b(document.body, "transform") ? "" : "-webkit-") + "transform"), n) e.animatables[o].target.style[t] = n[o].join(" ");
      e.settings.update && e.settings.update(e)
    }, N = function (t) {
      var i = {};
      i.animatables = E(t.targets), i.settings = f(t, e);
      var o, s = i.settings, r = [];
      for (o in t) if (!e.hasOwnProperty(o) && "targets" !== o) {
        var a = n.obj(t[o]) ? p(t[o]) : {value: t[o]};
        a.name = o, r.push(f(a, s))
      }
      return i.properties = r, i.tweens = $(i.animatables, i.properties), i.duration = T(i.tweens) || t.duration, i.currentTime = 0, i.progress = 0, i.ended = !1, i
    }, W = [], j = 0, R = function () {
      var t = function () {
        j = requestAnimationFrame(e)
      }, e = function (e) {
        if (W.length) {
          for (var i = 0; i < W.length; i++) W[i].tick(e);
          t()
        } else cancelAnimationFrame(j), j = 0
      };
      return t
    }(), F = function (t) {
      var e = N(t), i = {};
      return e.tick = function (t) {
        e.ended = !1, i.start || (i.start = t), i.current = Math.min(Math.max(i.last + t - i.start, 0), e.duration), D(e, i.current);
        var o = e.settings;
        o.begin && i.current >= o.delay && (o.begin(e), o.begin = void 0), i.current >= e.duration && (o.loop ? (i.start = t, "alternate" === o.direction && O(e, !0), n.num(o.loop) && o.loop--) : (e.ended = !0, e.pause(), o.complete && o.complete(e)), i.last = 0)
      }, e.seek = function (t) {
        D(e, t / 100 * e.duration)
      }, e.pause = function () {
        A(e);
        var t = W.indexOf(e);
        -1 < t && W.splice(t, 1)
      }, e.play = function (t) {
        e.pause(), t && (e = f(N(f(t, e.settings)), e)), i.start = 0, i.last = e.ended ? 0 : e.currentTime, t = e.settings, "reverse" === t.direction && O(e), "alternate" !== t.direction || t.loop || (t.loop = 1), M(e), W.push(e), j || R()
      }, e.restart = function () {
        e.reversed && O(e), e.pause(), e.seek(0), e.play()
      }, e.settings.autoplay && e.play(), e
    };
  return F.version = "1.1.1", F.speed = 1, F.list = W, F.remove = function (t) {
    t = l(n.arr(t) ? t.map(c) : c(t));
    for (var e = W.length - 1; 0 <= e; e--) for (var i = W[e], o = i.tweens, s = o.length - 1; 0 <= s; s--) for (var r = o[s].animatables, a = r.length - 1; 0 <= a; a--) h(t, r[a].target) && (r.splice(a, 1), r.length || o.splice(s, 1), o.length || i.pause())
  }, F.easings = o, F.getValue = _, F.path = function (t) {
    return t = n.str(t) ? a(t)[0] : t, {path: t, value: t.getTotalLength()}
  }, F.random = function (t, e) {
    return Math.floor(Math.random() * (e - t + 1)) + t
  }, F
}), function (t) {
  "use strict";

  function e(t, e) {
    for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
    return t
  }

  function i(e) {
    var i = 0, n = 0;
    if (!e) var e = t.event;
    return e.pageX || e.pageY ? (i = e.pageX, n = e.pageY) : (e.clientX || e.clientY) && (i = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft, n = e.clientY + document.body.scrollTop + document.documentElement.scrollTop), {
      x: i,
      y: n
    }
  }

  function n(t, i) {
    this.DOM = {}, this.DOM.el = t, this.options = e({}, this.options), e(this.options, i), this._init()
  }

  n.prototype.options = {
    movement: {
      imgWrapper: {
        translation: {x: 0, y: 0, z: 0},
        rotation: {x: -5, y: 5, z: 0},
        reverseAnimation: {duration: 1200, easing: "easeOutElastic", elasticity: 600}
      },
      lines: {
        translation: {x: 10, y: 10, z: [0, 10]},
        reverseAnimation: {duration: 1e3, easing: "easeOutExpo", elasticity: 600}
      },
      caption: {
        translation: {x: 20, y: 20, z: 0},
        rotation: {x: 0, y: 0, z: 0},
        reverseAnimation: {duration: 1500, easing: "easeOutElastic", elasticity: 600}
      },
      shine: {
        translation: {x: 50, y: 50, z: 0},
        reverseAnimation: {duration: 1200, easing: "easeOutElastic", elasticity: 600}
      }
    }
  }, n.prototype._init = function () {
    this.DOM.animatable = {}, this.DOM.animatable.imgWrapper = this.DOM.el.querySelector(".figure"), this.DOM.animatable.lines = this.DOM.el.querySelector(".tilter__deco--lines"), this.DOM.animatable.caption = this.DOM.el.querySelector(".tilter__caption"), this.DOM.animatable.overlay = this.DOM.el.querySelector(".tilter__deco--overlay"), this.DOM.animatable.shine = this.DOM.el.querySelector(".tilter__deco--shine > div"), this._initEvents()
  }, n.prototype._initEvents = function () {
    var t = this;
    this.mouseenterFn = function () {
      for (var e in t.DOM.animatable) anime.remove(t.DOM.animatable[e])
    }, this.mousemoveFn = function (e) {
      requestAnimationFrame(function () {
        t._layout(e)
      })
    }, this.mouseleaveFn = function (e) {
      requestAnimationFrame(function () {
        for (var e in t.DOM.animatable) void 0 != t.options.movement[e] && anime({
          targets: t.DOM.animatable[e],
          duration: void 0 != t.options.movement[e].reverseAnimation ? t.options.movement[e].reverseAnimation.duration || 0 : 1,
          easing: void 0 != t.options.movement[e].reverseAnimation ? t.options.movement[e].reverseAnimation.easing || "linear" : "linear",
          elasticity: void 0 != t.options.movement[e].reverseAnimation ? t.options.movement[e].reverseAnimation.elasticity || null : null,
          scaleX: 1,
          scaleY: 1,
          scaleZ: 1,
          translateX: 0,
          translateY: 0,
          translateZ: 0,
          rotateX: 0,
          rotateY: 0,
          rotateZ: 0
        })
      })
    }, this.DOM.el.addEventListener("mousemove", this.mousemoveFn), this.DOM.el.addEventListener("mouseleave", this.mouseleaveFn), this.DOM.el.addEventListener("mouseenter", this.mouseenterFn)
  }, n.prototype._layout = function (t) {
    var e = i(t), n = {
      left: document.body.scrollLeft + document.documentElement.scrollLeft,
      top: document.body.scrollTop + document.documentElement.scrollTop
    }, o = this.DOM.el.getBoundingClientRect(), s = {x: e.x - o.left - n.left, y: e.y - o.top - n.top};
    for (var r in this.DOM.animatable) if (void 0 != this.DOM.animatable[r] && void 0 != this.options.movement[r]) {
      var a = void 0 != this.options.movement[r] ? this.options.movement[r].translation || {
        x: 0,
        y: 0,
        z: 0
      } : {x: 0, y: 0, z: 0}, l = void 0 != this.options.movement[r] ? this.options.movement[r].rotation || {
        x: 0,
        y: 0,
        z: 0
      } : {x: 0, y: 0, z: 0}, c = function (t) {
        for (var e in t) void 0 == t[e] ? t[e] = [0, 0] : "number" == typeof t[e] && (t[e] = [-1 * t[e], t[e]])
      };
      c(a), c(l);
      var h = {
        translation: {
          x: (a.x[1] - a.x[0]) / o.width * s.x + a.x[0],
          y: (a.y[1] - a.y[0]) / o.height * s.y + a.y[0],
          z: (a.z[1] - a.z[0]) / o.height * s.y + a.z[0]
        },
        rotation: {
          x: (l.x[1] - l.x[0]) / o.height * s.y + l.x[0],
          y: (l.y[1] - l.y[0]) / o.width * s.x + l.y[0],
          z: (l.z[1] - l.z[0]) / o.width * s.x + l.z[0]
        }
      };
      this.DOM.animatable[r].style.WebkitTransform = this.DOM.animatable[r].style.transform = "translateX(" + h.translation.x + "px) translateY(" + h.translation.y + "px) translateZ(" + h.translation.z + "px) rotateX(" + h.rotation.x + "deg) rotateY(" + h.rotation.y + "deg) rotateZ(" + h.rotation.z + "deg)"
    }
  }, t.TiltFx = n
}(window), function (t, e, i, n) {
  var o = i("html"), s = i(t), r = i(e), a = i.fancybox = function () {
    a.open.apply(this, arguments)
  }, l = navigator.userAgent.match(/msie/i), c = null, h = e.createTouch !== n, u = function (t) {
    return t && t.hasOwnProperty && t instanceof i
  }, d = function (t) {
    return t && "string" === i.type(t)
  }, p = function (t) {
    return d(t) && 0 < t.indexOf("%")
  }, f = function (t, e) {
    var i = parseInt(t, 10) || 0;
    return e && p(t) && (i *= a.getViewport()[e] / 100), Math.ceil(i)
  }, m = function (t, e) {
    return f(t, e) + "px"
  };
  i.extend(a, {
    version: "2.1.5",
    defaults: {
      padding: 15,
      margin: 20,
      width: 800,
      height: 600,
      minWidth: 100,
      minHeight: 100,
      maxWidth: 9999,
      maxHeight: 9999,
      pixelRatio: 1,
      autoSize: !0,
      autoHeight: !1,
      autoWidth: !1,
      autoResize: !0,
      autoCenter: !h,
      fitToView: !0,
      aspectRatio: !1,
      topRatio: .5,
      leftRatio: .5,
      scrolling: "auto",
      wrapCSS: "",
      arrows: !0,
      closeBtn: !0,
      closeClick: !1,
      nextClick: !1,
      mouseWheel: !0,
      autoPlay: !1,
      playSpeed: 3e3,
      preload: 3,
      modal: !1,
      loop: !0,
      ajax: {dataType: "html", headers: {"X-fancyBox": !0}},
      iframe: {scrolling: "auto", preload: !0},
      swf: {wmode: "transparent", allowfullscreen: "true", allowscriptaccess: "always"},
      keys: {
        next: {13: "left", 34: "up", 39: "left", 40: "up"},
        prev: {8: "right", 33: "down", 37: "right", 38: "down"},
        close: [27],
        play: [32],
        toggle: [70]
      },
      direction: {next: "left", prev: "right"},
      scrollOutside: !0,
      index: 0,
      type: null,
      href: null,
      content: null,
      title: null,
      tpl: {
        wrap: '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
        image: '<img class="fancybox-image" src="{href}" alt="" />',
        iframe: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (l ? ' allowtransparency="true"' : "") + "></iframe>",
        error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
        closeBtn: '<a title="Close" id="close-fancybox" class="fancybox-item fancybox-close" href="javascript:;"></a>',
        next: '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
        prev: '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
      },
      openEffect: "fade",
      openSpeed: 250,
      openEasing: "swing",
      openOpacity: !0,
      openMethod: "zoomIn",
      closeEffect: "fade",
      closeSpeed: 250,
      closeEasing: "swing",
      closeOpacity: !0,
      closeMethod: "zoomOut",
      nextEffect: "elastic",
      nextSpeed: 250,
      nextEasing: "swing",
      nextMethod: "changeIn",
      prevEffect: "elastic",
      prevSpeed: 250,
      prevEasing: "swing",
      prevMethod: "changeOut",
      helpers: {overlay: !0, title: !0},
      onCancel: i.noop,
      beforeLoad: i.noop,
      afterLoad: i.noop,
      beforeShow: i.noop,
      afterShow: i.noop,
      beforeChange: i.noop,
      beforeClose: i.noop,
      afterClose: i.noop
    },
    group: {},
    opts: {},
    previous: null,
    coming: null,
    current: null,
    isActive: !1,
    isOpen: !1,
    isOpened: !1,
    wrap: null,
    skin: null,
    outer: null,
    inner: null,
    player: {timer: null, isActive: !1},
    ajaxLoad: null,
    imgPreload: null,
    transitions: {},
    helpers: {},
    open: function (t, e) {
      if (t && (i.isPlainObject(e) || (e = {}), !1 !== a.close(!0))) return i.isArray(t) || (t = u(t) ? i(t).get() : [t]), i.each(t, function (o, s) {
        var r, l, c, h, p, f = {};
        "object" === i.type(s) && (s.nodeType && (s = i(s)), u(s) ? (f = {
          href: s.data("fancybox-href") || s.attr("href"),
          title: s.data("fancybox-title") || s.attr("title"),
          isDom: !0,
          element: s
        }, i.metadata && i.extend(!0, f, s.metadata())) : f = s), r = e.href || f.href || (d(s) ? s : null), l = e.title !== n ? e.title : f.title || "", h = (c = e.content || f.content) ? "html" : e.type || f.type, !h && f.isDom && (h = s.data("fancybox-type"), h || (h = (h = s.prop("class").match(/fancybox\.(\w+)/)) ? h[1] : null)), d(r) && (h || (a.isImage(r) ? h = "image" : a.isSWF(r) ? h = "swf" : "#" === r.charAt(0) ? h = "inline" : d(s) && (h = "html", c = s)), "ajax" === h && (p = r.split(/\s+/, 2), r = p.shift(), p = p.shift())), c || ("inline" === h ? r ? c = i(d(r) ? r.replace(/.*(?=#[^\s]+$)/, "") : r) : f.isDom && (c = s) : "html" === h ? c = r : !h && !r && f.isDom && (h = "inline", c = s)), i.extend(f, {
          href: r,
          type: h,
          content: c,
          title: l,
          selector: p
        }), t[o] = f
      }), a.opts = i.extend(!0, {}, a.defaults, e), e.keys !== n && (a.opts.keys = !!e.keys && i.extend({}, a.defaults.keys, e.keys)), a.group = t, a._start(a.opts.index)
    },
    cancel: function () {
      var t = a.coming;
      t && !1 !== a.trigger("onCancel") && (a.hideLoading(), a.ajaxLoad && a.ajaxLoad.abort(), a.ajaxLoad = null, a.imgPreload && (a.imgPreload.onload = a.imgPreload.onerror = null), t.wrap && t.wrap.stop(!0, !0).trigger("onReset").remove(), a.coming = null, a.current || a._afterZoomOut(t))
    },
    close: function (t) {
      a.cancel(), !1 !== a.trigger("beforeClose") && (a.unbindEvents(), a.isActive && (a.isOpen && !0 !== t ? (a.isOpen = a.isOpened = !1, a.isClosing = !0, i(".fancybox-item, .fancybox-nav").remove(), a.wrap.stop(!0, !0).removeClass("fancybox-opened"), a.transitions[a.current.closeMethod]()) : (i(".fancybox-wrap").stop(!0).trigger("onReset").remove(), a._afterZoomOut())))
    },
    play: function (t) {
      var e = function () {
        clearTimeout(a.player.timer)
      }, i = function () {
        e(), a.current && a.player.isActive && (a.player.timer = setTimeout(a.next, a.current.playSpeed))
      }, n = function () {
        e(), r.unbind(".player"), a.player.isActive = !1, a.trigger("onPlayEnd")
      };
      !0 === t || !a.player.isActive && !1 !== t ? a.current && (a.current.loop || a.current.index < a.group.length - 1) && (a.player.isActive = !0, r.bind({
        "onCancel.player beforeClose.player": n,
        "onUpdate.player": i,
        "beforeLoad.player": e
      }), i(), a.trigger("onPlayStart")) : n()
    },
    next: function (t) {
      var e = a.current;
      e && (d(t) || (t = e.direction.next), a.jumpto(e.index + 1, t, "next"))
    },
    prev: function (t) {
      var e = a.current;
      e && (d(t) || (t = e.direction.prev), a.jumpto(e.index - 1, t, "prev"))
    },
    jumpto: function (t, e, i) {
      var o = a.current;
      o && (t = f(t), a.direction = e || o.direction[t >= o.index ? "next" : "prev"], a.router = i || "jumpto", o.loop && (0 > t && (t = o.group.length + t % o.group.length), t %= o.group.length), o.group[t] !== n && (a.cancel(), a._start(t)))
    },
    reposition: function (t, e) {
      var n, o = a.current, s = o ? o.wrap : null;
      s && (n = a._getPosition(e), t && "scroll" === t.type ? (delete n.position, s.stop(!0, !0).animate(n, 200)) : (s.css(n), o.pos = i.extend({}, o.dim, n)))
    },
    update: function (t) {
      var e = t && t.type, i = !e || "orientationchange" === e;
      i && (clearTimeout(c), c = null), a.isOpen && !c && (c = setTimeout(function () {
        var n = a.current;
        n && !a.isClosing && (a.wrap.removeClass("fancybox-tmp"), (i || "load" === e || "resize" === e && n.autoResize) && a._setDimension(), "scroll" === e && n.canShrink || a.reposition(t), a.trigger("onUpdate"), c = null)
      }, i && !h ? 0 : 300))
    },
    toggle: function (t) {
      a.isOpen && (a.current.fitToView = "boolean" === i.type(t) ? t : !a.current.fitToView, h && (a.wrap.removeAttr("style").addClass("fancybox-tmp"), a.trigger("onUpdate")), a.update())
    },
    hideLoading: function () {
      r.unbind(".loading"), i("#fancybox-loading").remove()
    },
    showLoading: function () {
      var t, e;
      a.hideLoading(), t = i('<div id="fancybox-loading"><div></div></div>').click(a.cancel).appendTo("body"), r.bind("keydown.loading", function (t) {
        27 === (t.which || t.keyCode) && (t.preventDefault(), a.cancel())
      }), a.defaults.fixed || (e = a.getViewport(), t.css({
        position: "absolute",
        top: .5 * e.h + e.y,
        left: .5 * e.w + e.x
      }))
    },
    getViewport: function () {
      var e = a.current && a.current.locked || !1, i = {x: s.scrollLeft(), y: s.scrollTop()};
      return e ? (i.w = e[0].clientWidth, i.h = e[0].clientHeight) : (i.w = h && t.innerWidth ? t.innerWidth : s.width(), i.h = h && t.innerHeight ? t.innerHeight : s.height()), i
    },
    unbindEvents: function () {
      a.wrap && u(a.wrap) && a.wrap.unbind(".fb"), r.unbind(".fb"), s.unbind(".fb")
    },
    bindEvents: function () {
      var t, e = a.current;
      e && (s.bind("orientationchange.fb" + (h ? "" : " resize.fb") + (e.autoCenter && !e.locked ? " scroll.fb" : ""), a.update), (t = e.keys) && r.bind("keydown.fb", function (o) {
        var s = o.which || o.keyCode, r = o.target || o.srcElement;
        return (27 !== s || !a.coming) && void(!o.ctrlKey && !o.altKey && !o.shiftKey && !o.metaKey && (!r || !r.type && !i(r).is("[contenteditable]")) && i.each(t, function (t, r) {
          return 1 < e.group.length && r[s] !== n ? (a[t](r[s]), o.preventDefault(), !1) : -1 < i.inArray(s, r) ? (a[t](), o.preventDefault(), !1) : void 0
        }))
      }), i.fn.mousewheel && e.mouseWheel && a.wrap.bind("mousewheel.fb", function (t, n, o, s) {
        for (var r = i(t.target || null), l = !1; r.length && !l && !r.is(".fancybox-skin") && !r.is(".fancybox-wrap");) l = r[0] && !(r[0].style.overflow && "hidden" === r[0].style.overflow) && (r[0].clientWidth && r[0].scrollWidth > r[0].clientWidth || r[0].clientHeight && r[0].scrollHeight > r[0].clientHeight), r = i(r).parent();
        0 !== n && !l && 1 < a.group.length && !e.canShrink && (0 < s || 0 < o ? a.prev(0 < s ? "down" : "left") : (0 > s || 0 > o) && a.next(0 > s ? "up" : "right"), t.preventDefault())
      }))
    },
    trigger: function (t, e) {
      var n, o = e || a.coming || a.current;
      if (o) {
        if (i.isFunction(o[t]) && (n = o[t].apply(o, Array.prototype.slice.call(arguments, 1))), !1 === n) return !1;
        o.helpers && i.each(o.helpers, function (e, n) {
          n && a.helpers[e] && i.isFunction(a.helpers[e][t]) && a.helpers[e][t](i.extend(!0, {}, a.helpers[e].defaults, n), o)
        }), r.trigger(t)
      }
    },
    isImage: function (t) {
      return d(t) && t.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i)
    },
    isSWF: function (t) {
      return d(t) && t.match(/\.(swf)((\?|#).*)?$/i)
    },
    _start: function (t) {
      var e, n, o = {};
      if (t = f(t), e = a.group[t] || null, !e) return !1;
      if (o = i.extend(!0, {}, a.opts, e), e = o.margin, n = o.padding, "number" === i.type(e) && (o.margin = [e, e, e, e]), "number" === i.type(n) && (o.padding = [n, n, n, n]), o.modal && i.extend(!0, o, {
        closeBtn: !1,
        closeClick: !1,
        nextClick: !1,
        arrows: !1,
        mouseWheel: !1,
        keys: null,
        helpers: {overlay: {closeClick: !1}}
      }), o.autoSize && (o.autoWidth = o.autoHeight = !0), "auto" === o.width && (o.autoWidth = !0), "auto" === o.height && (o.autoHeight = !0), o.group = a.group, o.index = t, a.coming = o, !1 === a.trigger("beforeLoad")) a.coming = null; else {
        if (n = o.type, e = o.href, !n) return a.coming = null, !(!a.current || !a.router || "jumpto" === a.router) && (a.current.index = t, a[a.router](a.direction));
        if (a.isActive = !0, "image" !== n && "swf" !== n || (o.autoHeight = o.autoWidth = !1, o.scrolling = "visible"), "image" === n && (o.aspectRatio = !0), "iframe" === n && h && (o.scrolling = "scroll"), o.wrap = i(o.tpl.wrap).addClass("fancybox-" + (h ? "mobile" : "desktop") + " fancybox-type-" + n + " fancybox-tmp " + o.wrapCSS).appendTo(o.parent || "body"), i.extend(o, {
          skin: i(".fancybox-skin", o.wrap),
          outer: i(".fancybox-outer", o.wrap),
          inner: i(".fancybox-inner", o.wrap)
        }), i.each(["Top", "Right", "Bottom", "Left"], function (t, e) {
          o.skin.css("padding" + e, m(o.padding[t]))
        }), a.trigger("onReady"), "inline" === n || "html" === n) {
          if (!o.content || !o.content.length) return a._error("content")
        } else if (!e) return a._error("href");
        "image" === n ? a._loadImage() : "ajax" === n ? a._loadAjax() : "iframe" === n ? a._loadIframe() : a._afterLoad()
      }
    },
    _error: function (t) {
      i.extend(a.coming, {
        type: "html",
        autoWidth: !0,
        autoHeight: !0,
        minWidth: 0,
        minHeight: 0,
        scrolling: "no",
        hasError: t,
        content: a.coming.tpl.error
      }), a._afterLoad()
    },
    _loadImage: function () {
      var t = a.imgPreload = new Image;
      t.onload = function () {
        this.onload = this.onerror = null, a.coming.width = this.width / a.opts.pixelRatio, a.coming.height = this.height / a.opts.pixelRatio, a._afterLoad()
      }, t.onerror = function () {
        this.onload = this.onerror = null, a._error("image")
      }, t.src = a.coming.href, !0 !== t.complete && a.showLoading()
    },
    _loadAjax: function () {
      var t = a.coming;
      a.showLoading(), a.ajaxLoad = i.ajax(i.extend({}, t.ajax, {
        url: t.href, error: function (t, e) {
          a.coming && "abort" !== e ? a._error("ajax", t) : a.hideLoading()
        }, success: function (e, i) {
          "success" === i && (t.content = e, a._afterLoad())
        }
      }))
    },
    _loadIframe: function () {
      var t = a.coming,
        e = i(t.tpl.iframe.replace(/\{rnd\}/g, (new Date).getTime())).attr("scrolling", h ? "auto" : t.iframe.scrolling).attr("src", t.href);
      i(t.wrap).bind("onReset", function () {
        try {
          i(this).find("iframe").hide().attr("src", "//about:blank").end().empty()
        } catch (t) {
        }
      }), t.iframe.preload && (a.showLoading(), e.one("load", function () {
        i(this).data("ready", 1), h || i(this).bind("load.fb", a.update), i(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show(), a._afterLoad()
      })), t.content = e.appendTo(t.inner), t.iframe.preload || a._afterLoad()
    },
    _preloadImages: function () {
      var t, e, i = a.group, n = a.current, o = i.length, s = n.preload ? Math.min(n.preload, o - 1) : 0;
      for (e = 1; e <= s; e += 1) t = i[(n.index + e) % o], "image" === t.type && t.href && ((new Image).src = t.href)
    },
    _afterLoad: function () {
      var t, e, n, o, s, r = a.coming, l = a.current;
      if (a.hideLoading(), r && !1 !== a.isActive) if (!1 === a.trigger("afterLoad", r, l)) r.wrap.stop(!0).trigger("onReset").remove(), a.coming = null; else {
        switch (l && (a.trigger("beforeChange", l), l.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove()), a.unbindEvents(), t = r.content, e = r.type, n = r.scrolling, i.extend(a, {
          wrap: r.wrap,
          skin: r.skin,
          outer: r.outer,
          inner: r.inner,
          current: r,
          previous: l
        }), o = r.href, e) {
          case"inline":
          case"ajax":
          case"html":
            r.selector ? t = i("<div>").html(t).find(r.selector) : u(t) && (t.data("fancybox-placeholder") || t.data("fancybox-placeholder", i('<div class="fancybox-placeholder"></div>').insertAfter(t).hide()), t = t.show().detach(), r.wrap.bind("onReset", function () {
              i(this).find(t).length && t.hide().replaceAll(t.data("fancybox-placeholder")).data("fancybox-placeholder", !1)
            }));
            break;
          case"image":
            t = r.tpl.image.replace("{href}", o);
            break;
          case"swf":
            t = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + o + '"></param>', s = "", i.each(r.swf, function (e, i) {
              t += '<param name="' + e + '" value="' + i + '"></param>', s += " " + e + '="' + i + '"'
            }), t += '<embed src="' + o + '" type="application/x-shockwave-flash" width="100%" height="100%"' + s + "></embed></object>";
        }
        (!u(t) || !t.parent().is(r.inner)) && r.inner.append(t), a.trigger("beforeShow"), r.inner.css("overflow", "yes" === n ? "scroll" : "no" === n ? "hidden" : n), a._setDimension(), a.reposition(), a.isOpen = !1, a.coming = null, a.bindEvents(), a.isOpened ? l.prevMethod && a.transitions[l.prevMethod]() : i(".fancybox-wrap").not(r.wrap).stop(!0).trigger("onReset").remove(), a.transitions[a.isOpened ? r.nextMethod : r.openMethod](), a._preloadImages()
      }
    },
    _setDimension: function () {
      var t, e, n, o, s, r, l, c, h, u = a.getViewport(), d = 0, g = !1, v = !1, g = a.wrap, y = a.skin,
        b = a.inner, w = a.current, v = w.width, x = w.height, _ = w.minWidth, C = w.minHeight, P = w.maxWidth,
        S = w.maxHeight, E = w.scrolling, k = w.scrollOutside ? w.scrollbarWidth : 0, I = w.margin,
        $ = f(I[1] + I[3]), O = f(I[0] + I[2]);
      if (g.add(y).add(b).width("auto").height("auto").removeClass("fancybox-tmp"), I = f(y.outerWidth(!0) - y.width()), t = f(y.outerHeight(!0) - y.height()), e = $ + I, n = O + t, o = p(v) ? (u.w - e) * f(v) / 100 : v, s = p(x) ? (u.h - n) * f(x) / 100 : x, "iframe" === w.type) {
        if (h = w.content, w.autoHeight && 1 === h.data("ready")) try {
          h[0].contentWindow.document.location && (b.width(o).height(9999), r = h.contents().find("body"), k && r.css("overflow-x", "hidden"), s = r.outerHeight(!0))
        } catch (T) {
        }
      } else (w.autoWidth || w.autoHeight) && (b.addClass("fancybox-tmp"), w.autoWidth || b.width(o), w.autoHeight || b.height(s), w.autoWidth && (o = b.width()), w.autoHeight && (s = b.height()), b.removeClass("fancybox-tmp"));
      if (v = f(o), x = f(s), c = o / s, _ = f(p(_) ? f(_, "w") - e : _), P = f(p(P) ? f(P, "w") - e : P), C = f(p(C) ? f(C, "h") - n : C), S = f(p(S) ? f(S, "h") - n : S), r = P, l = S, w.fitToView && (P = Math.min(u.w - e, P), S = Math.min(u.h - n, S)), e = u.w - $, O = u.h - O, w.aspectRatio ? (v > P && (v = P, x = f(v / c)), x > S && (x = S, v = f(x * c)), v < _ && (v = _, x = f(v / c)), x < C && (x = C, v = f(x * c))) : (v = Math.max(_, Math.min(v, P)), w.autoHeight && "iframe" !== w.type && (b.width(v), x = b.height()), x = Math.max(C, Math.min(x, S))), w.fitToView) if (b.width(v).height(x), g.width(v + I), u = g.width(), $ = g.height(), w.aspectRatio) for (; (u > e || $ > O) && v > _ && x > C && !(19 < d++);) x = Math.max(C, Math.min(S, x - 10)), v = f(x * c), v < _ && (v = _, x = f(v / c)), v > P && (v = P, x = f(v / c)), b.width(v).height(x), g.width(v + I), u = g.width(), $ = g.height(); else v = Math.max(_, Math.min(v, v - (u - e))), x = Math.max(C, Math.min(x, x - ($ - O)));
      k && "auto" === E && x < s && v + I + k < e && (v += k), b.width(v).height(x), g.width(v + I), u = g.width(), $ = g.height(), g = (u > e || $ > O) && v > _ && x > C, v = w.aspectRatio ? v < r && x < l && v < o && x < s : (v < r || x < l) && (v < o || x < s), i.extend(w, {
        dim: {
          width: m(u),
          height: m($)
        },
        origWidth: o,
        origHeight: s,
        canShrink: g,
        canExpand: v,
        wPadding: I,
        hPadding: t,
        wrapSpace: $ - y.outerHeight(!0),
        skinSpace: y.height() - x
      }), !h && w.autoHeight && x > C && x < S && !v && b.height("auto")
    },
    _getPosition: function (t) {
      var e = a.current, i = a.getViewport(), n = e.margin, o = a.wrap.width() + n[1] + n[3],
        s = a.wrap.height() + n[0] + n[2], n = {position: "absolute", top: n[0], left: n[3]};
      return e.autoCenter && e.fixed && !t && s <= i.h && o <= i.w ? n.position = "fixed" : e.locked || (n.top += i.y, n.left += i.x), n.top = m(Math.max(n.top, n.top + (i.h - s) * e.topRatio)), n.left = m(Math.max(n.left, n.left + (i.w - o) * e.leftRatio)), n
    },
    _afterZoomIn: function () {
      var t = a.current;
      t && (a.isOpen = a.isOpened = !0, a.wrap.css("overflow", "visible").addClass("fancybox-opened"), a.update(), (t.closeClick || t.nextClick && 1 < a.group.length) && a.inner.css("cursor", "pointer").bind("click.fb", function (e) {
        !i(e.target).is("a") && !i(e.target).parent().is("a") && (e.preventDefault(), a[t.closeClick ? "close" : "next"]())
      }), t.closeBtn && i(t.tpl.closeBtn).appendTo(a.skin).bind("click.fb", function (t) {
        t.preventDefault(), a.close()
      }), t.arrows && 1 < a.group.length && ((t.loop || 0 < t.index) && i(t.tpl.prev).appendTo(a.outer).bind("click.fb", a.prev), (t.loop || t.index < a.group.length - 1) && i(t.tpl.next).appendTo(a.outer).bind("click.fb", a.next)), a.trigger("afterShow"), t.loop || t.index !== t.group.length - 1 ? a.opts.autoPlay && !a.player.isActive && (a.opts.autoPlay = !1, a.play()) : a.play(!1))
    },
    _afterZoomOut: function (t) {
      t = t || a.current, i(".fancybox-wrap").trigger("onReset").remove(), i.extend(a, {
        group: {},
        opts: {},
        router: !1,
        current: null,
        isActive: !1,
        isOpened: !1,
        isOpen: !1,
        isClosing: !1,
        wrap: null,
        skin: null,
        outer: null,
        inner: null
      }), a.trigger("afterClose", t)
    }
  }), a.transitions = {
    getOrigPosition: function () {
      var t = a.current, e = t.element, i = t.orig, n = {}, o = 50, s = 50, r = t.hPadding, l = t.wPadding,
        c = a.getViewport();
      return !i && t.isDom && e.is(":visible") && (i = e.find("img:first"), i.length || (i = e)), u(i) ? (n = i.offset(), i.is("img") && (o = i.outerWidth(), s = i.outerHeight())) : (n.top = c.y + (c.h - s) * t.topRatio, n.left = c.x + (c.w - o) * t.leftRatio), ("fixed" === a.wrap.css("position") || t.locked) && (n.top -= c.y, n.left -= c.x), n = {
        top: m(n.top - r * t.topRatio),
        left: m(n.left - l * t.leftRatio),
        width: m(o + l),
        height: m(s + r)
      }
    }, step: function (t, e) {
      var i, n, o = e.prop;
      n = a.current;
      var s = n.wrapSpace, r = n.skinSpace;
      "width" !== o && "height" !== o || (i = e.end === e.start ? 1 : (t - e.start) / (e.end - e.start), a.isClosing && (i = 1 - i), n = "width" === o ? n.wPadding : n.hPadding, n = t - n, a.skin[o](f("width" === o ? n : n - s * i)), a.inner[o](f("width" === o ? n : n - s * i - r * i)))
    }, zoomIn: function () {
      var t = a.current, e = t.pos, n = t.openEffect, o = "elastic" === n, s = i.extend({opacity: 1}, e);
      delete s.position, o ? (e = this.getOrigPosition(), t.openOpacity && (e.opacity = .1)) : "fade" === n && (e.opacity = .1), a.wrap.css(e).animate(s, {
        duration: "none" === n ? 0 : t.openSpeed,
        easing: t.openEasing,
        step: o ? this.step : null,
        complete: a._afterZoomIn
      })
    }, zoomOut: function () {
      var t = a.current, e = t.closeEffect, i = "elastic" === e, n = {opacity: .1};
      i && (n = this.getOrigPosition(), t.closeOpacity && (n.opacity = .1)), a.wrap.animate(n, {
        duration: "none" === e ? 0 : t.closeSpeed,
        easing: t.closeEasing,
        step: i ? this.step : null,
        complete: a._afterZoomOut
      })
    }, changeIn: function () {
      var t, e = a.current, i = e.nextEffect, n = e.pos, o = {opacity: 1}, s = a.direction;
      n.opacity = .1, "elastic" === i && (t = "down" === s || "up" === s ? "top" : "left", "down" === s || "right" === s ? (n[t] = m(f(n[t]) - 200), o[t] = "+=200px") : (n[t] = m(f(n[t]) + 200), o[t] = "-=200px")), "none" === i ? a._afterZoomIn() : a.wrap.css(n).animate(o, {
        duration: e.nextSpeed,
        easing: e.nextEasing,
        complete: a._afterZoomIn
      })
    }, changeOut: function () {
      var t = a.previous, e = t.prevEffect, n = {opacity: .1}, o = a.direction;
      "elastic" === e && (n["down" === o || "up" === o ? "top" : "left"] = ("up" === o || "left" === o ? "-" : "+") + "=200px"), t.wrap.animate(n, {
        duration: "none" === e ? 0 : t.prevSpeed,
        easing: t.prevEasing,
        complete: function () {
          i(this).trigger("onReset").remove()
        }
      })
    }
  }, a.helpers.overlay = {
    defaults: {closeClick: !0, speedOut: 200, showEarly: !0, css: {}, locked: !h, fixed: !0},
    overlay: null,
    fixed: !1,
    el: i("html"),
    create: function (t) {
      t = i.extend({}, this.defaults, t), this.overlay && this.close(), this.overlay = i('<div class="fancybox-overlay"></div>').appendTo(a.coming ? a.coming.parent : t.parent), this.fixed = !1, t.fixed && a.defaults.fixed && (this.overlay.addClass("fancybox-overlay-fixed"), this.fixed = !0)
    },
    open: function (t) {
      var e = this;
      t = i.extend({}, this.defaults, t), this.overlay ? this.overlay.unbind(".overlay").width("auto").height("auto") : this.create(t), this.fixed || (s.bind("resize.overlay", i.proxy(this.update, this)), this.update()), t.closeClick && this.overlay.bind("click.overlay", function (t) {
        if (i(t.target).hasClass("fancybox-overlay")) return a.isActive ? a.close() : e.close(), !1
      }), this.overlay.css(t.css).show()
    },
    close: function () {
      var t, e;
      s.unbind("resize.overlay"), this.el.hasClass("fancybox-lock") && (i(".fancybox-margin").removeClass("fancybox-margin"), t = s.scrollTop(), e = s.scrollLeft(), this.el.removeClass("fancybox-lock"), s.scrollTop(t).scrollLeft(e)), i(".fancybox-overlay").remove().hide(), i.extend(this, {
        overlay: null,
        fixed: !1
      })
    },
    update: function () {
      var t, i = "100%";
      this.overlay.width(i).height("100%"), l ? (t = Math.max(e.documentElement.offsetWidth, e.body.offsetWidth), r.width() > t && (i = r.width())) : r.width() > s.width() && (i = r.width()), this.overlay.width(i).height(r.height())
    },
    onReady: function (t, e) {
      var n = this.overlay;
      i(".fancybox-overlay").stop(!0, !0), n || this.create(t), t.locked && this.fixed && e.fixed && (n || (this.margin = r.height() > s.height() && i("html").css("margin-right").replace("px", "")), e.locked = this.overlay.append(e.wrap), e.fixed = !1), !0 === t.showEarly && this.beforeShow.apply(this, arguments)
    },
    beforeShow: function (t, e) {
      var n, o;
      e.locked && (!1 !== this.margin && (i("*").filter(function () {
        return "fixed" === i(this).css("position") && !i(this).hasClass("fancybox-overlay") && !i(this).hasClass("fancybox-wrap")
      }).addClass("fancybox-margin"), this.el.addClass("fancybox-margin")), n = s.scrollTop(), o = s.scrollLeft(), this.el.addClass("fancybox-lock"), s.scrollTop(n).scrollLeft(o)), this.open(t)
    },
    onUpdate: function () {
      this.fixed || this.update()
    },
    afterClose: function (t) {
      this.overlay && !a.coming && this.overlay.fadeOut(t.speedOut, i.proxy(this.close, this))
    }
  }, a.helpers.title = {
    defaults: {type: "float", position: "bottom"}, beforeShow: function (t) {
      var e = a.current, n = e.title, o = t.type;
      if (i.isFunction(n) && (n = n.call(e.element, e)), d(n) && "" !== i.trim(n)) {
        switch (e = i('<div class="fancybox-title fancybox-title-' + o + '-wrap">' + n + "</div>"), o) {
          case"inside":
            o = a.skin;
            break;
          case"outside":
            o = a.wrap;
            break;
          case"over":
            o = a.inner;
            break;
          default:
            o = a.skin, e.appendTo("body"), l && e.width(e.width()), e.wrapInner('<span class="child"></span>'), a.current.margin[2] += Math.abs(f(e.css("margin-bottom")))
        }
        e["top" === t.position ? "prependTo" : "appendTo"](o)
      }
    }
  }, i.fn.fancybox = function (t) {
    var e, n = i(this), o = this.selector || "", s = function (s) {
      var r, l, c = i(this).blur(), h = e;
      !s.ctrlKey && !s.altKey && !s.shiftKey && !s.metaKey && !c.is(".fancybox-wrap") && (r = t.groupAttr || "data-fancybox-group", l = c.attr(r), l || (r = "rel", l = c.get(0)[r]), l && "" !== l && "nofollow" !== l && (c = o.length ? i(o) : n, c = c.filter("[" + r + '="' + l + '"]'), h = c.index(this)), t.index = h, !1 !== a.open(c, t) && s.preventDefault())
    };
    return t = t || {}, e = t.index || 0, o && !1 !== t.live ? r.undelegate(o, "click.fb-start").delegate(o + ":not('.fancybox-item, .fancybox-nav')", "click.fb-start", s) : n.unbind("click.fb-start").bind("click.fb-start", s), this.filter("[data-fancybox-start=1]").trigger("click"), this
  }, r.ready(function () {
    var e, s;
    if (i.scrollbarWidth === n && (i.scrollbarWidth = function () {
      var t = i('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"),
        e = t.children(), e = e.innerWidth() - e.height(99).innerWidth();
      return t.remove(), e
    }), i.support.fixedPosition === n) {
      e = i.support, s = i('<div style="position:fixed;top:20px;"></div>').appendTo("body");
      var r = 20 === s[0].offsetTop || 15 === s[0].offsetTop;
      s.remove(), e.fixedPosition = r
    }
    i.extend(a.defaults, {
      scrollbarWidth: i.scrollbarWidth(),
      fixed: i.support.fixedPosition,
      parent: i("body")
    }), e = i(t).width(), o.addClass("fancybox-lock-test"), s = i(t).width(), o.removeClass("fancybox-lock-test"), i("<style type='text/css'>.fancybox-margin{margin-right:" + (s - e) + "px;}</style>").appendTo("head")
  })
}(window, document, jQuery), function (t) {
  "use strict";
  var e = t.fancybox, i = function (e, i, n) {
    return n = n || "", "object" === t.type(n) && (n = t.param(n, !0)), t.each(i, function (t, i) {
      e = e.replace("$" + t, i || "")
    }), n.length && (e += (e.indexOf("?") > 0 ? "&" : "?") + n), e
  };
  e.helpers.media = {
    defaults: {
      youtube: {
        matcher: /(youtube\.com|youtu\.be|youtube-nocookie\.com)\/(watch\?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*)).*/i,
        params: {autoplay: 1, autohide: 1, fs: 1, rel: 0, hd: 1, wmode: "opaque", enablejsapi: 1},
        type: "iframe",
        url: "//www.youtube.com/embed/$3"
      },
      vimeo: {
        matcher: /(?:vimeo(?:pro)?.com)\/(?:[^\d]+)?(\d+)(?:.*)/,
        params: {autoplay: 1, hd: 1, show_title: 1, show_byline: 1, show_portrait: 0, fullscreen: 1},
        type: "iframe",
        url: "//player.vimeo.com/video/$1"
      },
      metacafe: {
        matcher: /metacafe.com\/(?:watch|fplayer)\/([\w\-]{1,10})/,
        params: {autoPlay: "yes"},
        type: "swf",
        url: function (e, i, n) {
          return n.swf.flashVars = "playerVars=" + t.param(i, !0), "//www.metacafe.com/fplayer/" + e[1] + "/.swf"
        }
      },
      dailymotion: {
        matcher: /dailymotion.com\/video\/(.*)\/?(.*)/,
        params: {additionalInfos: 0, autoStart: 1},
        type: "swf",
        url: "//www.dailymotion.com/swf/video/$1"
      },
      twitvid: {
        matcher: /twitvid\.com\/([a-zA-Z0-9_\-\?\=]+)/i,
        params: {autoplay: 0},
        type: "iframe",
        url: "//www.twitvid.com/embed.php?guid=$1"
      },
      twitpic: {
        matcher: /twitpic\.com\/(?!(?:place|photos|events)\/)([a-zA-Z0-9\?\=\-]+)/i,
        type: "image",
        url: "//twitpic.com/show/full/$1/"
      },
      instagram: {
        matcher: /(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,
        type: "image",
        url: "//$1/p/$2/media/?size=l"
      },
      google_maps: {
        matcher: /maps\.google\.([a-z]{2,3}(\.[a-z]{2})?)\/(\?ll=|maps\?)(.*)/i,
        type: "iframe",
        url: function (t) {
          return "//maps.google." + t[1] + "/" + t[3] + t[4] + "&output=" + (t[4].indexOf("layer=c") > 0 ? "svembed" : "embed")
        }
      }
    }, beforeLoad: function (e, n) {
      var o, s, r, a, l = n.href || "", c = !1;
      for (o in e) if (e.hasOwnProperty(o) && (s = e[o], r = l.match(s.matcher))) {
        c = s.type, a = t.extend(!0, {}, s.params, n[o] || (t.isPlainObject(e[o]) ? e[o].params : null)), l = "function" === t.type(s.url) ? s.url.call(this, r, a, n) : i(s.url, r, a);
        break
      }
      c && (n.href = l, n.type = c, n.autoHeight = !1)
    }
  }
}(jQuery), !function (t) {
  "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? module.exports = t() : window.noUiSlider = t()
}(function () {
  "use strict";

  function t(t, e) {
    var i = document.createElement("div");
    return c(i, e), t.appendChild(i), i
  }

  function e(t) {
    return t.filter(function (t) {
      return !this[t] && (this[t] = !0)
    }, {})
  }

  function i(t, e) {
    return Math.round(t / e) * e
  }

  function n(t, e) {
    var i = t.getBoundingClientRect(), n = t.ownerDocument, o = n.documentElement, s = d();
    return /webkit.*Chrome.*Mobile/i.test(navigator.userAgent) && (s.x = 0), e ? i.top + s.y - o.clientTop : i.left + s.x - o.clientLeft
  }

  function o(t) {
    return "number" == typeof t && !isNaN(t) && isFinite(t)
  }

  function s(t, e, i) {
    i > 0 && (c(t, e), setTimeout(function () {
      h(t, e)
    }, i))
  }

  function r(t) {
    return Math.max(Math.min(t, 100), 0)
  }

  function a(t) {
    return Array.isArray(t) ? t : [t]
  }

  function l(t) {
    t = String(t);
    var e = t.split(".");
    return e.length > 1 ? e[1].length : 0
  }

  function c(t, e) {
    t.classList ? t.classList.add(e) : t.className += " " + e
  }

  function h(t, e) {
    t.classList ? t.classList.remove(e) : t.className = t.className.replace(new RegExp("(^|\\b)" + e.split(" ").join("|") + "(\\b|$)", "gi"), " ")
  }

  function u(t, e) {
    return t.classList ? t.classList.contains(e) : new RegExp("\\b" + e + "\\b").test(t.className)
  }

  function d() {
    var t = void 0 !== window.pageXOffset, e = "CSS1Compat" === (document.compatMode || ""),
      i = t ? window.pageXOffset : e ? document.documentElement.scrollLeft : document.body.scrollLeft,
      n = t ? window.pageYOffset : e ? document.documentElement.scrollTop : document.body.scrollTop;
    return {x: i, y: n}
  }

  function p() {
    return window.navigator.pointerEnabled ? {
      start: "pointerdown",
      move: "pointermove",
      end: "pointerup"
    } : window.navigator.msPointerEnabled ? {
      start: "MSPointerDown",
      move: "MSPointerMove",
      end: "MSPointerUp"
    } : {start: "mousedown touchstart", move: "mousemove touchmove", end: "mouseup touchend"}
  }

  function f(t, e) {
    return 100 / (e - t)
  }

  function m(t, e) {
    return 100 * e / (t[1] - t[0])
  }

  function g(t, e) {
    return m(t, t[0] < 0 ? e + Math.abs(t[0]) : e - t[0])
  }

  function v(t, e) {
    return e * (t[1] - t[0]) / 100 + t[0]
  }

  function y(t, e) {
    for (var i = 1; t >= e[i];) i += 1;
    return i
  }

  function b(t, e, i) {
    if (i >= t.slice(-1)[0]) return 100;
    var n, o, s, r, a = y(i, t);
    return n = t[a - 1], o = t[a], s = e[a - 1], r = e[a], s + g([n, o], i) / f(s, r)
  }

  function w(t, e, i) {
    if (i >= 100) return t.slice(-1)[0];
    var n, o, s, r, a = y(i, e);
    return n = t[a - 1], o = t[a], s = e[a - 1], r = e[a], v([n, o], (i - s) * f(s, r))
  }

  function x(t, e, n, o) {
    if (100 === o) return o;
    var s, r, a = y(o, t);
    return n ? (s = t[a - 1], r = t[a], o - s > (r - s) / 2 ? r : s) : e[a - 1] ? t[a - 1] + i(o - t[a - 1], e[a - 1]) : o
  }

  function _(t, e, i) {
    var n;
    if ("number" == typeof e && (e = [e]), "[object Array]" !== Object.prototype.toString.call(e)) throw new Error("noUiSlider (" + Y + "): 'range' contains invalid value.");
    if (n = "min" === t ? 0 : "max" === t ? 100 : parseFloat(t), !o(n) || !o(e[0])) throw new Error("noUiSlider (" + Y + "): 'range' value isn't numeric.");
    i.xPct.push(n), i.xVal.push(e[0]), n ? i.xSteps.push(!isNaN(e[1]) && e[1]) : isNaN(e[1]) || (i.xSteps[0] = e[1]), i.xHighestCompleteStep.push(0)
  }

  function C(t, e, i) {
    if (!e) return !0;
    i.xSteps[t] = m([i.xVal[t], i.xVal[t + 1]], e) / f(i.xPct[t], i.xPct[t + 1]);
    var n = (i.xVal[t + 1] - i.xVal[t]) / i.xNumSteps[t], o = Math.ceil(Number(n.toFixed(3)) - 1),
      s = i.xVal[t] + i.xNumSteps[t] * o;
    i.xHighestCompleteStep[t] = s
  }

  function P(t, e, i, n) {
    this.xPct = [], this.xVal = [], this.xSteps = [n || !1], this.xNumSteps = [!1], this.xHighestCompleteStep = [], this.snap = e, this.direction = i;
    var o, s = [];
    for (o in t) t.hasOwnProperty(o) && s.push([t[o], o]);
    for (s.length && "object" == typeof s[0][0] ? s.sort(function (t, e) {
      return t[0][0] - e[0][0]
    }) : s.sort(function (t, e) {
      return t[0] - e[0]
    }), o = 0; o < s.length; o++) _(s[o][1], s[o][0], this);
    for (this.xNumSteps = this.xSteps.slice(0), o = 0; o < this.xNumSteps.length; o++) C(o, this.xNumSteps[o], this)
  }

  function S(t, e) {
    if (!o(e)) throw new Error("noUiSlider (" + Y + "): 'step' is not numeric.");
    t.singleStep = e
  }

  function E(t, e) {
    if ("object" != typeof e || Array.isArray(e)) throw new Error("noUiSlider (" + Y + "): 'range' is not an object.");
    if (void 0 === e.min || void 0 === e.max) throw new Error("noUiSlider (" + Y + "): Missing 'min' or 'max' in 'range'.");
    if (e.min === e.max) throw new Error("noUiSlider (" + Y + "): 'range' 'min' and 'max' cannot be equal.");
    t.spectrum = new P(e, t.snap, t.dir, t.singleStep)
  }

  function k(t, e) {
    if (e = a(e), !Array.isArray(e) || !e.length) throw new Error("noUiSlider (" + Y + "): 'start' option is incorrect.");
    t.handles = e.length, t.start = e
  }

  function I(t, e) {
    if (t.snap = e, "boolean" != typeof e) throw new Error("noUiSlider (" + Y + "): 'snap' option must be a boolean.")
  }

  function $(t, e) {
    if (t.animate = e, "boolean" != typeof e) throw new Error("noUiSlider (" + Y + "): 'animate' option must be a boolean.")
  }

  function O(t, e) {
    if (t.animationDuration = e, "number" != typeof e) throw new Error("noUiSlider (" + Y + "): 'animationDuration' option must be a number.")
  }

  function T(t, e) {
    var i, n = [!1];
    if ("lower" === e ? e = [!0, !1] : "upper" === e && (e = [!1, !0]), e === !0 || e === !1) {
      for (i = 1; i < t.handles; i++) n.push(e);
      n.push(!1)
    } else {
      if (!Array.isArray(e) || !e.length || e.length !== t.handles + 1) throw new Error("noUiSlider (" + Y + "): 'connect' option doesn't match handle count.");
      n = e
    }
    t.connect = n
  }

  function L(t, e) {
    switch (e) {
      case"horizontal":
        t.ort = 0;
        break;
      case"vertical":
        t.ort = 1;
        break;
      default:
        throw new Error("noUiSlider (" + Y + "): 'orientation' option is invalid.")
    }
  }

  function M(t, e) {
    if (!o(e)) throw new Error("noUiSlider (" + Y + "): 'margin' option must be numeric.");
    if (0 !== e && (t.margin = t.spectrum.getMargin(e), !t.margin)) throw new Error("noUiSlider (" + Y + "): 'margin' option is only supported on linear sliders.")
  }

  function A(t, e) {
    if (!o(e)) throw new Error("noUiSlider (" + Y + "): 'limit' option must be numeric.");
    if (t.limit = t.spectrum.getMargin(e), !t.limit || t.handles < 2) throw new Error("noUiSlider (" + Y + "): 'limit' option is only supported on linear sliders with 2 or more handles.")
  }

  function z(t, e) {
    if (!o(e)) throw new Error("noUiSlider (" + Y + "): 'padding' option must be numeric.");
    if (0 !== e) {
      if (t.padding = t.spectrum.getMargin(e), !t.padding) throw new Error("noUiSlider (" + Y + "): 'padding' option is only supported on linear sliders.");
      if (t.padding < 0) throw new Error("noUiSlider (" + Y + "): 'padding' option must be a positive number.");
      if (t.padding >= 50) throw new Error("noUiSlider (" + Y + "): 'padding' option must be less than half the range.")
    }
  }

  function H(t, e) {
    switch (e) {
      case"ltr":
        t.dir = 0;
        break;
      case"rtl":
        t.dir = 1;
        break;
      default:
        throw new Error("noUiSlider (" + Y + "): 'direction' option was not recognized.")
    }
  }

  function D(t, e) {
    if ("string" != typeof e) throw new Error("noUiSlider (" + Y + "): 'behaviour' must be a string containing options.");
    var i = e.indexOf("tap") >= 0, n = e.indexOf("drag") >= 0, o = e.indexOf("fixed") >= 0,
      s = e.indexOf("snap") >= 0, r = e.indexOf("hover") >= 0;
    if (o) {
      if (2 !== t.handles) throw new Error("noUiSlider (" + Y + "): 'fixed' behaviour must be used with 2 handles");
      M(t, t.start[1] - t.start[0])
    }
    t.events = {tap: i || s, drag: n, fixed: o, snap: s, hover: r}
  }

  function N(t, e) {
    if (e !== !1) if (e === !0) {
      t.tooltips = [];
      for (var i = 0; i < t.handles; i++) t.tooltips.push(!0)
    } else {
      if (t.tooltips = a(e), t.tooltips.length !== t.handles) throw new Error("noUiSlider (" + Y + "): must pass a formatter for all handles.");
      t.tooltips.forEach(function (t) {
        if ("boolean" != typeof t && ("object" != typeof t || "function" != typeof t.to)) throw new Error("noUiSlider (" + Y + "): 'tooltips' must be passed a formatter or 'false'.")
      })
    }
  }

  function W(t, e) {
    if (t.format = e, "function" == typeof e.to && "function" == typeof e.from) return !0;
    throw new Error("noUiSlider (" + Y + "): 'format' requires 'to' and 'from' methods.")
  }

  function j(t, e) {
    if (void 0 !== e && "string" != typeof e && e !== !1) throw new Error("noUiSlider (" + Y + "): 'cssPrefix' must be a string or `false`.");
    t.cssPrefix = e
  }

  function R(t, e) {
    if (void 0 !== e && "object" != typeof e) throw new Error("noUiSlider (" + Y + "): 'cssClasses' must be an object.");
    if ("string" == typeof t.cssPrefix) {
      t.cssClasses = {};
      for (var i in e) e.hasOwnProperty(i) && (t.cssClasses[i] = t.cssPrefix + e[i])
    } else t.cssClasses = e
  }

  function F(t, e) {
    if (e !== !0 && e !== !1) throw new Error("noUiSlider (" + Y + "): 'useRequestAnimationFrame' option should be true (default) or false.");
    t.useRequestAnimationFrame = e
  }

  function B(t) {
    var e = {margin: 0, limit: 0, padding: 0, animate: !0, animationDuration: 300, format: V}, i = {
      step: {r: !1, t: S},
      start: {r: !0, t: k},
      connect: {r: !0, t: T},
      direction: {r: !0, t: H},
      snap: {r: !1, t: I},
      animate: {r: !1, t: $},
      animationDuration: {r: !1, t: O},
      range: {r: !0, t: E},
      orientation: {r: !1, t: L},
      margin: {r: !1, t: M},
      limit: {r: !1, t: A},
      padding: {r: !1, t: z},
      behaviour: {r: !0, t: D},
      format: {r: !1, t: W},
      tooltips: {r: !1, t: N},
      cssPrefix: {r: !1, t: j},
      cssClasses: {r: !1, t: R},
      useRequestAnimationFrame: {r: !1, t: F}
    }, n = {
      connect: !1,
      direction: "ltr",
      behaviour: "tap",
      orientation: "horizontal",
      cssPrefix: "noUi-",
      cssClasses: {
        target: "target",
        base: "base",
        origin: "origin",
        handle: "handle",
        handleLower: "handle-lower",
        handleUpper: "handle-upper",
        horizontal: "horizontal",
        vertical: "vertical",
        background: "background",
        connect: "connect",
        ltr: "ltr",
        rtl: "rtl",
        draggable: "draggable",
        drag: "state-drag",
        tap: "state-tap",
        active: "active",
        tooltip: "tooltip",
        pips: "pips",
        pipsHorizontal: "pips-horizontal",
        pipsVertical: "pips-vertical",
        marker: "marker",
        markerHorizontal: "marker-horizontal",
        markerVertical: "marker-vertical",
        markerNormal: "marker-normal",
        markerLarge: "marker-large",
        markerSub: "marker-sub",
        value: "value",
        valueHorizontal: "value-horizontal",
        valueVertical: "value-vertical",
        valueNormal: "value-normal",
        valueLarge: "value-large",
        valueSub: "value-sub"
      },
      useRequestAnimationFrame: !0
    };
    Object.keys(i).forEach(function (o) {
      if (void 0 === t[o] && void 0 === n[o]) {
        if (i[o].r) throw new Error("noUiSlider (" + Y + "): '" + o + "' is required.");
        return !0
      }
      i[o].t(e, void 0 === t[o] ? n[o] : t[o])
    }), e.pips = t.pips;
    var o = [["left", "top"], ["right", "bottom"]];
    return e.style = o[e.dir][e.ort], e.styleOposite = o[e.dir ? 0 : 1][e.ort], e
  }

  function q(i, o, l) {
    function f(e, i) {
      var n = t(e, o.cssClasses.origin), s = t(n, o.cssClasses.handle);
      return s.setAttribute("data-handle", i), 0 === i ? c(s, o.cssClasses.handleLower) : i === o.handles - 1 && c(s, o.cssClasses.handleUpper), n
    }

    function m(e, i) {
      return !!i && t(e, o.cssClasses.connect)
    }

    function g(t, e) {
      it = [], nt = [], nt.push(m(e, t[0]));
      for (var i = 0; i < o.handles; i++) it.push(f(e, i)), lt[i] = i, nt.push(m(e, t[i + 1]))
    }

    function v(e) {
      c(e, o.cssClasses.target), 0 === o.dir ? c(e, o.cssClasses.ltr) : c(e, o.cssClasses.rtl), 0 === o.ort ? c(e, o.cssClasses.horizontal) : c(e, o.cssClasses.vertical), et = t(e, o.cssClasses.base)
    }

    function y(e, i) {
      return !!o.tooltips[i] && t(e.firstChild, o.cssClasses.tooltip)
    }

    function b() {
      var t = it.map(y);
      G("update", function (e, i, n) {
        if (t[i]) {
          var s = e[i];
          o.tooltips[i] !== !0 && (s = o.tooltips[i].to(n[i])), t[i].innerHTML = s
        }
      })
    }

    function w(t, e, i) {
      if ("range" === t || "steps" === t) return ht.xVal;
      if ("count" === t) {
        if (!e) throw new Error("noUiSlider (" + Y + "): 'values' required for mode 'count'.");
        var n, o = 100 / (e - 1), s = 0;
        for (e = []; (n = s++ * o) <= 100;) e.push(n);
        t = "positions"
      }
      return "positions" === t ? e.map(function (t) {
        return ht.fromStepping(i ? ht.getStep(t) : t)
      }) : "values" === t ? i ? e.map(function (t) {
        return ht.fromStepping(ht.getStep(ht.toStepping(t)))
      }) : e : void 0
    }

    function x(t, i, n) {
      function o(t, e) {
        return (t + e).toFixed(7) / 1
      }

      var s = {}, r = ht.xVal[0], a = ht.xVal[ht.xVal.length - 1], l = !1, c = !1, h = 0;
      return n = e(n.slice().sort(function (t, e) {
        return t - e
      })), n[0] !== r && (n.unshift(r), l = !0), n[n.length - 1] !== a && (n.push(a), c = !0), n.forEach(function (e, r) {
        var a, u, d, p, f, m, g, v, y, b, w = e, x = n[r + 1];
        if ("steps" === i && (a = ht.xNumSteps[r]), a || (a = x - w), w !== !1 && void 0 !== x) for (a = Math.max(a, 1e-7), u = w; u <= x; u = o(u, a)) {
          for (p = ht.toStepping(u), f = p - h, v = f / t, y = Math.round(v), b = f / y, d = 1; d <= y; d += 1) m = h + d * b, s[m.toFixed(5)] = ["x", 0];
          g = n.indexOf(u) > -1 ? 1 : "steps" === i ? 2 : 0, !r && l && (g = 0), u === x && c || (s[p.toFixed(5)] = [u, g]), h = p
        }
      }), s
    }

    function _(t, e, i) {
      function n(t, e) {
        var i = e === o.cssClasses.value, n = i ? d : p, s = i ? h : u;
        return e + " " + n[o.ort] + " " + s[t]
      }

      function s(t, e, i) {
        return 'class="' + n(i[1], e) + '" style="' + o.style + ": " + t + '%"'
      }

      function r(t, n) {
        n[1] = n[1] && e ? e(n[0], n[1]) : n[1], l += "<div " + s(t, o.cssClasses.marker, n) + "></div>", n[1] && (l += "<div " + s(t, o.cssClasses.value, n) + ">" + i.to(n[0]) + "</div>")
      }

      var a = document.createElement("div"), l = "",
        h = [o.cssClasses.valueNormal, o.cssClasses.valueLarge, o.cssClasses.valueSub],
        u = [o.cssClasses.markerNormal, o.cssClasses.markerLarge, o.cssClasses.markerSub],
        d = [o.cssClasses.valueHorizontal, o.cssClasses.valueVertical],
        p = [o.cssClasses.markerHorizontal, o.cssClasses.markerVertical];
      return c(a, o.cssClasses.pips), c(a, 0 === o.ort ? o.cssClasses.pipsHorizontal : o.cssClasses.pipsVertical), Object.keys(t).forEach(function (e) {
        r(e, t[e])
      }), a.innerHTML = l, a
    }

    function C(t) {
      var e = t.mode, i = t.density || 1, n = t.filter || !1, o = t.values || !1, s = t.stepped || !1,
        r = w(e, o, s), a = x(i, e, r), l = t.format || {to: Math.round};
      return rt.appendChild(_(a, n, l))
    }

    function P() {
      var t = et.getBoundingClientRect(), e = "offset" + ["Width", "Height"][o.ort];
      return 0 === o.ort ? t.width || et[e] : t.height || et[e]
    }

    function S(t, e, i, n) {
      var s = function (e) {
        return !rt.hasAttribute("disabled") && !u(rt, o.cssClasses.tap) && !!(e = E(e, n.pageOffset)) && !(t === st.start && void 0 !== e.buttons && e.buttons > 1) && (!n.hover || !e.buttons) && (e.calcPoint = e.points[o.ort], void i(e, n))
      }, r = [];
      return t.split(" ").forEach(function (t) {
        e.addEventListener(t, s, !1), r.push([t, s])
      }), r
    }

    function E(t, e) {
      t.preventDefault();
      var i, n, o = 0 === t.type.indexOf("touch"), s = 0 === t.type.indexOf("mouse"),
        r = 0 === t.type.indexOf("pointer");
      if (0 === t.type.indexOf("MSPointer") && (r = !0), o) {
        if (t.touches.length > 1) return !1;
        i = t.changedTouches[0].pageX, n = t.changedTouches[0].pageY
      }
      return e = e || d(), (s || r) && (i = t.clientX + e.x, n = t.clientY + e.y), t.pageOffset = e, t.points = [i, n], t.cursor = s || r, t
    }

    function k(t) {
      var e = t - n(et, o.ort), i = 100 * e / P();
      return o.dir ? 100 - i : i
    }

    function I(t) {
      var e = 100, i = !1;
      return it.forEach(function (n, o) {
        if (!n.hasAttribute("disabled")) {
          var s = Math.abs(at[o] - t);
          s < e && (i = o, e = s)
        }
      }), i
    }

    function $(t, e, i, n) {
      var o = i.slice(), s = [!t, t], r = [t, !t];
      n = n.slice(), t && n.reverse(), n.length > 1 ? n.forEach(function (t, i) {
        var n = N(o, t, o[t] + e, s[i], r[i]);
        n === !1 ? e = 0 : (e = n - o[t], o[t] = n)
      }) : s = r = [!0];
      var a = !1;
      n.forEach(function (t, n) {
        a = F(t, i[t] + e, s[n], r[n]) || a
      }), a && n.forEach(function (t) {
        O("update", t), O("slide", t)
      })
    }

    function O(t, e, i) {
      Object.keys(dt).forEach(function (n) {
        var s = n.split(".")[0];
        t === s && dt[n].forEach(function (t) {
          t.call(ot, ut.map(o.format.to), e, ut.slice(), i || !1, at.slice())
        })
      })
    }

    function T(t, e) {
      "mouseout" === t.type && "HTML" === t.target.nodeName && null === t.relatedTarget && M(t, e)
    }

    function L(t, e) {
      if (navigator.appVersion.indexOf("MSIE 9") === -1 && 0 === t.buttons && 0 !== e.buttonsProperty) return M(t, e);
      var i = (o.dir ? -1 : 1) * (t.calcPoint - e.startCalcPoint), n = 100 * i / e.baseSize;
      $(i > 0, n, e.locations, e.handleNumbers)
    }

    function M(t, e) {
      ct && (h(ct, o.cssClasses.active), ct = !1), t.cursor && (document.body.style.cursor = "", document.body.removeEventListener("selectstart", document.body.noUiListener)), document.documentElement.noUiListeners.forEach(function (t) {
        document.documentElement.removeEventListener(t[0], t[1])
      }), h(rt, o.cssClasses.drag), R(), e.handleNumbers.forEach(function (t) {
        O("set", t), O("change", t), O("end", t)
      })
    }

    function A(t, e) {
      if (1 === e.handleNumbers.length) {
        var i = it[e.handleNumbers[0]];
        if (i.hasAttribute("disabled")) return !1;
        ct = i.children[0], c(ct, o.cssClasses.active)
      }
      t.preventDefault(), t.stopPropagation();
      var n = S(st.move, document.documentElement, L, {
          startCalcPoint: t.calcPoint,
          baseSize: P(),
          pageOffset: t.pageOffset,
          handleNumbers: e.handleNumbers,
          buttonsProperty: t.buttons,
          locations: at.slice()
        }), s = S(st.end, document.documentElement, M, {handleNumbers: e.handleNumbers}),
        r = S("mouseout", document.documentElement, T, {handleNumbers: e.handleNumbers});
      if (document.documentElement.noUiListeners = n.concat(s, r), t.cursor) {
        document.body.style.cursor = getComputedStyle(t.target).cursor, it.length > 1 && c(rt, o.cssClasses.drag);
        var a = function () {
          return !1
        };
        document.body.noUiListener = a, document.body.addEventListener("selectstart", a, !1)
      }
      e.handleNumbers.forEach(function (t) {
        O("start", t)
      })
    }

    function z(t) {
      t.stopPropagation();
      var e = k(t.calcPoint), i = I(e);
      return i !== !1 && (o.events.snap || s(rt, o.cssClasses.tap, o.animationDuration), F(i, e, !0, !0), R(), O("slide", i, !0), O("set", i, !0), O("change", i, !0), O("update", i, !0), void(o.events.snap && A(t, {handleNumbers: [i]})))
    }

    function H(t) {
      var e = k(t.calcPoint), i = ht.getStep(e), n = ht.fromStepping(i);
      Object.keys(dt).forEach(function (t) {
        "hover" === t.split(".")[0] && dt[t].forEach(function (t) {
          t.call(ot, n)
        })
      })
    }

    function D(t) {
      t.fixed || it.forEach(function (t, e) {
        S(st.start, t.children[0], A, {handleNumbers: [e]})
      }), t.tap && S(st.start, et, z, {}), t.hover && S(st.move, et, H, {hover: !0}), t.drag && nt.forEach(function (e, i) {
        if (e !== !1 && 0 !== i && i !== nt.length - 1) {
          var n = it[i - 1], s = it[i], r = [e];
          c(e, o.cssClasses.draggable), t.fixed && (r.push(n.children[0]), r.push(s.children[0])), r.forEach(function (t) {
            S(st.start, t, A, {handles: [n, s], handleNumbers: [i - 1, i]})
          })
        }
      })
    }

    function N(t, e, i, n, s) {
      return it.length > 1 && (n && e > 0 && (i = Math.max(i, t[e - 1] + o.margin)), s && e < it.length - 1 && (i = Math.min(i, t[e + 1] - o.margin))), it.length > 1 && o.limit && (n && e > 0 && (i = Math.min(i, t[e - 1] + o.limit)), s && e < it.length - 1 && (i = Math.max(i, t[e + 1] - o.limit))), o.padding && (0 === e && (i = Math.max(i, o.padding)), e === it.length - 1 && (i = Math.min(i, 100 - o.padding))), i = ht.getStep(i), i = r(i), i !== t[e] && i
    }

    function W(t) {
      return t + "%"
    }

    function j(t, e) {
      at[t] = e, ut[t] = ht.fromStepping(e);
      var i = function () {
        it[t].style[o.style] = W(e), q(t), q(t + 1)
      };
      window.requestAnimationFrame && o.useRequestAnimationFrame ? window.requestAnimationFrame(i) : i()
    }

    function R() {
      lt.forEach(function (t) {
        var e = at[t] > 50 ? -1 : 1, i = 3 + (it.length + e * t);
        it[t].childNodes[0].style.zIndex = i
      })
    }

    function F(t, e, i, n) {
      return e = N(at, t, e, i, n), e !== !1 && (j(t, e), !0)
    }

    function q(t) {
      if (nt[t]) {
        var e = 0, i = 100;
        0 !== t && (e = at[t - 1]), t !== nt.length - 1 && (i = at[t]), nt[t].style[o.style] = W(e), nt[t].style[o.styleOposite] = W(100 - i)
      }
    }

    function U(t, e) {
      null !== t && t !== !1 && ("number" == typeof t && (t = String(t)), t = o.format.from(t), t === !1 || isNaN(t) || F(e, ht.toStepping(t), !1, !1))
    }

    function V(t, e) {
      var i = a(t), n = void 0 === at[0];
      e = void 0 === e || !!e, i.forEach(U), o.animate && !n && s(rt, o.cssClasses.tap, o.animationDuration), lt.forEach(function (t) {
        F(t, at[t], !0, !1)
      }), R(), lt.forEach(function (t) {
        O("update", t), null !== i[t] && e && O("set", t)
      })
    }

    function X(t) {
      V(o.start, t)
    }

    function Q() {
      var t = ut.map(o.format.to);
      return 1 === t.length ? t[0] : t
    }

    function Z() {
      for (var t in o.cssClasses) o.cssClasses.hasOwnProperty(t) && h(rt, o.cssClasses[t]);
      for (; rt.firstChild;) rt.removeChild(rt.firstChild);
      delete rt.noUiSlider
    }

    function K() {
      return at.map(function (t, e) {
        var i = ht.getNearbySteps(t), n = ut[e], o = i.thisStep.step, s = null;
        o !== !1 && n + o > i.stepAfter.startValue && (o = i.stepAfter.startValue - n), s = n > i.thisStep.startValue ? i.thisStep.step : i.stepBefore.step !== !1 && n - i.stepBefore.highestStep, 100 === t ? o = null : 0 === t && (s = null);
        var r = ht.countStepDecimals();
        return null !== o && o !== !1 && (o = Number(o.toFixed(r))), null !== s && s !== !1 && (s = Number(s.toFixed(r))), [s, o]
      })
    }

    function G(t, e) {
      dt[t] = dt[t] || [], dt[t].push(e), "update" === t.split(".")[0] && it.forEach(function (t, e) {
        O("update", e)
      })
    }

    function J(t) {
      var e = t && t.split(".")[0], i = e && t.substring(e.length);
      Object.keys(dt).forEach(function (t) {
        var n = t.split(".")[0], o = t.substring(n.length);
        e && e !== n || i && i !== o || delete dt[t]
      })
    }

    function tt(t, e) {
      var i = Q(), n = ["margin", "limit", "padding", "range", "animate", "snap", "step", "format"];
      n.forEach(function (e) {
        void 0 !== t[e] && (l[e] = t[e])
      });
      var s = B(l);
      n.forEach(function (e) {
        void 0 !== t[e] && (o[e] = s[e])
      }), s.spectrum.direction = ht.direction, ht = s.spectrum, o.margin = s.margin, o.limit = s.limit, o.padding = s.padding, at = [], V(t.start || i, e)
    }

    var et, it, nt, ot, st = p(), rt = i, at = [], lt = [], ct = !1, ht = o.spectrum, ut = [], dt = {};
    if (rt.noUiSlider) throw new Error("noUiSlider (" + Y + "): Slider was already initialized.");
    return v(rt), g(o.connect, et), ot = {
      destroy: Z,
      steps: K,
      on: G,
      off: J,
      get: Q,
      set: V,
      reset: X,
      __moveHandles: function (t, e, i) {
        $(t, e, at, i)
      },
      options: l,
      updateOptions: tt,
      target: rt,
      pips: C
    }, D(o.events), V(o.start), o.pips && C(o.pips), o.tooltips && b(), ot
  }

  function U(t, e) {
    if (!t.nodeName) throw new Error("noUiSlider (" + Y + "): create requires a single element.");
    var i = B(e, t), n = q(t, i, e);
    return t.noUiSlider = n, n
  }

  var Y = "9.2.0";
  P.prototype.getMargin = function (t) {
    var e = this.xNumSteps[0];
    if (e && t / e % 1 !== 0) throw new Error("noUiSlider (" + Y + "): 'limit', 'margin' and 'padding' must be divisible by step.");
    return 2 === this.xPct.length && m(this.xVal, t)
  }, P.prototype.toStepping = function (t) {
    return t = b(this.xVal, this.xPct, t)
  }, P.prototype.fromStepping = function (t) {
    return w(this.xVal, this.xPct, t)
  }, P.prototype.getStep = function (t) {
    return t = x(this.xPct, this.xSteps, this.snap, t)
  }, P.prototype.getNearbySteps = function (t) {
    var e = y(t, this.xPct);
    return {
      stepBefore: {
        startValue: this.xVal[e - 2],
        step: this.xNumSteps[e - 2], highestStep: this.xHighestCompleteStep[e - 2]
      },
      thisStep: {
        startValue: this.xVal[e - 1],
        step: this.xNumSteps[e - 1],
        highestStep: this.xHighestCompleteStep[e - 1]
      },
      stepAfter: {
        startValue: this.xVal[e - 0],
        step: this.xNumSteps[e - 0],
        highestStep: this.xHighestCompleteStep[e - 0]
      }
    }
  }, P.prototype.countStepDecimals = function () {
    var t = this.xNumSteps.map(l);
    return Math.max.apply(null, t)
  }, P.prototype.convert = function (t) {
    return this.getStep(this.toStepping(t))
  };
  var V = {
    to: function (t) {
      return void 0 !== t && t.toFixed(2)
    }, from: Number
  };
  return {version: Y, create: U}
}), function (t) {
  "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? module.exports = t() : window.wNumb = t()
}(function () {
  "use strict";

  function t(t) {
    return t.split("").reverse().join("")
  }

  function e(t, e) {
    return t.substring(0, e.length) === e
  }

  function i(t, e) {
    return t.slice(-1 * e.length) === e
  }

  function n(t, e, i) {
    if ((t[e] || t[i]) && t[e] === t[i]) throw new Error(e)
  }

  function o(t) {
    return "number" == typeof t && isFinite(t)
  }

  function s(t, e) {
    return t = t.toString().split("e"), t = Math.round(+(t[0] + "e" + (t[1] ? +t[1] + e : e))), t = t.toString().split("e"), (+(t[0] + "e" + (t[1] ? +t[1] - e : -e))).toFixed(e)
  }

  function r(e, i, n, r, a, l, c, h, u, d, p, f) {
    var m, g, v, y = f, b = "", w = "";
    return l && (f = l(f)), !!o(f) && (e !== !1 && 0 === parseFloat(f.toFixed(e)) && (f = 0), f < 0 && (m = !0, f = Math.abs(f)), e !== !1 && (f = s(f, e)), f = f.toString(), f.indexOf(".") !== -1 ? (g = f.split("."), v = g[0], n && (b = n + g[1])) : v = f, i && (v = t(v).match(/.{1,3}/g), v = t(v.join(t(i)))), m && h && (w += h), r && (w += r), m && u && (w += u), w += v, w += b, a && (w += a), d && (w = d(w, y)), w)
  }

  function a(t, n, s, r, a, l, c, h, u, d, p, f) {
    var m, g = "";
    return p && (f = p(f)), !(!f || "string" != typeof f) && (h && e(f, h) && (f = f.replace(h, ""), m = !0), r && e(f, r) && (f = f.replace(r, "")), u && e(f, u) && (f = f.replace(u, ""), m = !0), a && i(f, a) && (f = f.slice(0, -1 * a.length)), n && (f = f.split(n).join("")), s && (f = f.replace(s, ".")), m && (g += "-"), g += f, g = g.replace(/[^0-9\.\-.]/g, ""), "" !== g && (g = Number(g), c && (g = c(g)), !!o(g) && g))
  }

  function l(t) {
    var e, i, o, s = {};
    for (void 0 === t.suffix && (t.suffix = t.postfix), e = 0; e < u.length; e += 1) if (i = u[e], o = t[i], void 0 === o) "negative" !== i || s.negativeBefore ? "mark" === i && "." !== s.thousand ? s[i] = "." : s[i] = !1 : s[i] = "-"; else if ("decimals" === i) {
      if (!(o >= 0 && o < 8)) throw new Error(i);
      s[i] = o
    } else if ("encoder" === i || "decoder" === i || "edit" === i || "undo" === i) {
      if ("function" != typeof o) throw new Error(i);
      s[i] = o
    } else {
      if ("string" != typeof o) throw new Error(i);
      s[i] = o
    }
    return n(s, "mark", "thousand"), n(s, "prefix", "negative"), n(s, "prefix", "negativeBefore"), s
  }

  function c(t, e, i) {
    var n, o = [];
    for (n = 0; n < u.length; n += 1) o.push(t[u[n]]);
    return o.push(i), e.apply("", o)
  }

  function h(t) {
    return this instanceof h ? void("object" == typeof t && (t = l(t), this.to = function (e) {
      return c(t, r, e)
    }, this.from = function (e) {
      return c(t, a, e)
    })) : new h(t)
  }

  var u = ["decimals", "thousand", "mark", "prefix", "suffix", "encoder", "decoder", "negativeBefore", "negative", "edit", "undo"];
  return h
}), function (t) {
  function e() {
    t(".crf-sm.opened").length && (t(".crf-s.opened").removeClass("opened"), t(".crf-sm.opened").removeClass("opened").hide(), i.close.call())
  }

  t.fn.crfi = function () {
    this.change(function () {
      "radio" == t(this).attr("type") && t("input[name=" + t(this).attr("name") + "]").not(this).next(".crf").removeClass("checked"), t(this).prop("checked") ? t(this).next().addClass("checked") : t(this).next().removeClass("checked")
    }), this.not(".crf-i").each(function (e) {
      t(this).attr("id", "crf-input-" + e).css({
        position: "absolute",
        left: "-9999em"
      }).addClass("crf-i").next("label").addClass("crf").attr("for", "crf-input-" + e), t(this).prop("checked") && t(this).next().addClass("checked")
    })
  };
  var i, n = {
    init: function (n) {
      i = t.extend({
        select: function () {
        }, done: function () {
        }, open: function () {
        }, close: function () {
        }
      }, n), t(document).unbind("click.crfs").on("click.crfs", ".crf-s", function () {
        var n = t("div[data-id=" + t(this).attr("id") + "]");
        if (n.is(":visible")) return e(), !1;
        e();
        var o = t(this).outerHeight(), s = t(this).find("select").attr("class"), r = t(this).offset(),
          a = n.show().height();
        n.css({
          position: "absolute",
          left: "-9999em"
        }), t(this).addClass("opened"), n.addClass("opened " + s).css({
          left: r.left,
          top: r.top + o + a > t(document).height() ? r.top - a : r.top + o,
          width: t(this).outerWidth()
        }).show(), i.open.call()
      }), t(document).click(function (i) {
        return !(t(i.target).closest(".crf-sm.opened, .crf-s.opened").length > 0) && void e()
      }), t(window).resize(function () {
        var e = t(".crf-s.opened");
        if (e.length) {
          var i = t(".crf-sm.opened"), n = e.outerHeight(), o = e.offset(), s = i.height();
          i.css({
            left: o.left,
            top: o.top + n + s > t(document).height() ? o.top - s : o.top + n,
            width: e.outerWidth()
          })
        }
      }), t(document).on("click.crfs", ".crf-sm li", function () {
        var n = t(this).parentsUntil(".crf-sm").parent().attr("data-id"), o = t("#" + n).attr("class");
        return t("#" + n).attr("class", "crf-s").addClass(t(this).attr("class").replace("selected", "")).addClass(o.replace("hided-s", "").replace("opened", "")).find(".option").html(t(this).html()), t("#" + n).find("select").children().prop("selected", !1).eq(t(this).index()).prop("selected", !0).change(), t(this).parentsUntil(".crf-sm").parent().find(".selected").removeClass("selected"), t(this).addClass("selected"), e(), i.select.call(), !1
      }), this.each(function (e) {
        if (!t(this).hasClass("hided-s")) {
          t(this).addClass("hided-s").hide().wrap("<span class='crf-s " + t(this).attr("class") + "' id='crf-s-" + e + "' />").parent().append("<span class='option'><img src='" + t(this).find("option:selected").attr("data-img") + "'>" + t(this).find("option:selected").html() + "</span>");
          var n = t("<ul></ul>");
          t(this).children().each(function () {
            n.append("<li class='" + (void 0 != t(this).attr("class") ? t(this).attr("class") + "" : "") + (t(this).is(":selected") ? " selected" : "") + "'><img src='" + t(this).attr("data-img") + "'><span class='link'>" + t(this).html() + "</span></li>")
          }), t("<div class='crf-sm' data-id='crf-s-" + e + "'/>").append(n).appendTo("body"), i.done.call()
        }
      })
    }, hide: function () {
      e()
    }
  };
  t.fn.crfs = function (t) {
    return n[t] ? n[t].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof t && t ? void 0 : n.init.apply(this, arguments)
  }
}(jQuery), function (t) {
  "function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery)
}(function (t) {
  t.ui = t.ui || {}, t.ui.version = "1.12.1";
  var e = 0, i = Array.prototype.slice;
  t.cleanData = function (e) {
    return function (i) {
      var n, o, s;
      for (s = 0; null != (o = i[s]); s++) try {
        n = t._data(o, "events"), n && n.remove && t(o).triggerHandler("remove")
      } catch (r) {
      }
      e(i)
    }
  }(t.cleanData), t.widget = function (e, i, n) {
    var o, s, r, a = {}, l = e.split(".")[0];
    e = e.split(".")[1];
    var c = l + "-" + e;
    return n || (n = i, i = t.Widget), t.isArray(n) && (n = t.extend.apply(null, [{}].concat(n))), t.expr[":"][c.toLowerCase()] = function (e) {
      return !!t.data(e, c)
    }, t[l] = t[l] || {}, o = t[l][e], s = t[l][e] = function (t, e) {
      return this._createWidget ? void(arguments.length && this._createWidget(t, e)) : new s(t, e)
    }, t.extend(s, o, {
      version: n.version,
      _proto: t.extend({}, n),
      _childConstructors: []
    }), r = new i, r.options = t.widget.extend({}, r.options), t.each(n, function (e, n) {
      return t.isFunction(n) ? void(a[e] = function () {
        function t() {
          return i.prototype[e].apply(this, arguments)
        }

        function o(t) {
          return i.prototype[e].apply(this, t)
        }

        return function () {
          var e, i = this._super, s = this._superApply;
          return this._super = t, this._superApply = o, e = n.apply(this, arguments), this._super = i, this._superApply = s, e
        }
      }()) : void(a[e] = n)
    }), s.prototype = t.widget.extend(r, {widgetEventPrefix: o ? r.widgetEventPrefix || e : e}, a, {
      constructor: s,
      namespace: l,
      widgetName: e,
      widgetFullName: c
    }), o ? (t.each(o._childConstructors, function (e, i) {
      var n = i.prototype;
      t.widget(n.namespace + "." + n.widgetName, s, i._proto)
    }), delete o._childConstructors) : i._childConstructors.push(s), t.widget.bridge(e, s), s
  }, t.widget.extend = function (e) {
    for (var n, o, s = i.call(arguments, 1), r = 0, a = s.length; a > r; r++) for (n in s[r]) o = s[r][n], s[r].hasOwnProperty(n) && void 0 !== o && (e[n] = t.isPlainObject(o) ? t.isPlainObject(e[n]) ? t.widget.extend({}, e[n], o) : t.widget.extend({}, o) : o);
    return e
  }, t.widget.bridge = function (e, n) {
    var o = n.prototype.widgetFullName || e;
    t.fn[e] = function (s) {
      var r = "string" == typeof s, a = i.call(arguments, 1), l = this;
      return r ? this.length || "instance" !== s ? this.each(function () {
        var i, n = t.data(this, o);
        return "instance" === s ? (l = n, !1) : n ? t.isFunction(n[s]) && "_" !== s.charAt(0) ? (i = n[s].apply(n, a), i !== n && void 0 !== i ? (l = i && i.jquery ? l.pushStack(i.get()) : i, !1) : void 0) : t.error("no such method '" + s + "' for " + e + " widget instance") : t.error("cannot call methods on " + e + " prior to initialization; attempted to call method '" + s + "'")
      }) : l = void 0 : (a.length && (s = t.widget.extend.apply(null, [s].concat(a))), this.each(function () {
        var e = t.data(this, o);
        e ? (e.option(s || {}), e._init && e._init()) : t.data(this, o, new n(s, this))
      })), l
    }
  }, t.Widget = function () {
  }, t.Widget._childConstructors = [], t.Widget.prototype = {
    widgetName: "widget",
    widgetEventPrefix: "",
    defaultElement: "<div>",
    options: {classes: {}, disabled: !1, create: null},
    _createWidget: function (i, n) {
      n = t(n || this.defaultElement || this)[0], this.element = t(n), this.uuid = e++, this.eventNamespace = "." + this.widgetName + this.uuid, this.bindings = t(), this.hoverable = t(), this.focusable = t(), this.classesElementLookup = {}, n !== this && (t.data(n, this.widgetFullName, this), this._on(!0, this.element, {
        remove: function (t) {
          t.target === n && this.destroy()
        }
      }), this.document = t(n.style ? n.ownerDocument : n.document || n), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this.options = t.widget.extend({}, this.options, this._getCreateOptions(), i), this._create(), this.options.disabled && this._setOptionDisabled(this.options.disabled), this._trigger("create", null, this._getCreateEventData()), this._init()
    },
    _getCreateOptions: function () {
      return {}
    },
    _getCreateEventData: t.noop,
    _create: t.noop,
    _init: t.noop,
    destroy: function () {
      var e = this;
      this._destroy(), t.each(this.classesElementLookup, function (t, i) {
        e._removeClass(i, t)
      }), this.element.off(this.eventNamespace).removeData(this.widgetFullName), this.widget().off(this.eventNamespace).removeAttr("aria-disabled"), this.bindings.off(this.eventNamespace)
    },
    _destroy: t.noop,
    widget: function () {
      return this.element
    },
    option: function (e, i) {
      var n, o, s, r = e;
      if (0 === arguments.length) return t.widget.extend({}, this.options);
      if ("string" == typeof e) if (r = {}, n = e.split("."), e = n.shift(), n.length) {
        for (o = r[e] = t.widget.extend({}, this.options[e]), s = 0; n.length - 1 > s; s++) o[n[s]] = o[n[s]] || {}, o = o[n[s]];
        if (e = n.pop(), 1 === arguments.length) return void 0 === o[e] ? null : o[e];
        o[e] = i
      } else {
        if (1 === arguments.length) return void 0 === this.options[e] ? null : this.options[e];
        r[e] = i
      }
      return this._setOptions(r), this
    },
    _setOptions: function (t) {
      var e;
      for (e in t) this._setOption(e, t[e]);
      return this
    },
    _setOption: function (t, e) {
      return "classes" === t && this._setOptionClasses(e), this.options[t] = e, "disabled" === t && this._setOptionDisabled(e), this
    },
    _setOptionClasses: function (e) {
      var i, n, o;
      for (i in e) o = this.classesElementLookup[i], e[i] !== this.options.classes[i] && o && o.length && (n = t(o.get()), this._removeClass(o, i), n.addClass(this._classes({
        element: n,
        keys: i,
        classes: e,
        add: !0
      })))
    },
    _setOptionDisabled: function (t) {
      this._toggleClass(this.widget(), this.widgetFullName + "-disabled", null, !!t), t && (this._removeClass(this.hoverable, null, "ui-state-hover"), this._removeClass(this.focusable, null, "ui-state-focus"))
    },
    enable: function () {
      return this._setOptions({disabled: !1})
    },
    disable: function () {
      return this._setOptions({disabled: !0})
    },
    _classes: function (e) {
      function i(i, s) {
        var r, a;
        for (a = 0; i.length > a; a++) r = o.classesElementLookup[i[a]] || t(), r = t(e.add ? t.unique(r.get().concat(e.element.get())) : r.not(e.element).get()), o.classesElementLookup[i[a]] = r, n.push(i[a]), s && e.classes[i[a]] && n.push(e.classes[i[a]])
      }

      var n = [], o = this;
      return e = t.extend({
        element: this.element,
        classes: this.options.classes || {}
      }, e), this._on(e.element, {remove: "_untrackClassesElement"}), e.keys && i(e.keys.match(/\S+/g) || [], !0), e.extra && i(e.extra.match(/\S+/g) || []), n.join(" ")
    },
    _untrackClassesElement: function (e) {
      var i = this;
      t.each(i.classesElementLookup, function (n, o) {
        -1 !== t.inArray(e.target, o) && (i.classesElementLookup[n] = t(o.not(e.target).get()))
      })
    },
    _removeClass: function (t, e, i) {
      return this._toggleClass(t, e, i, !1)
    },
    _addClass: function (t, e, i) {
      return this._toggleClass(t, e, i, !0)
    },
    _toggleClass: function (t, e, i, n) {
      n = "boolean" == typeof n ? n : i;
      var o = "string" == typeof t || null === t,
        s = {extra: o ? e : i, keys: o ? t : e, element: o ? this.element : t, add: n};
      return s.element.toggleClass(this._classes(s), n), this
    },
    _on: function (e, i, n) {
      var o, s = this;
      "boolean" != typeof e && (n = i, i = e, e = !1), n ? (i = o = t(i), this.bindings = this.bindings.add(i)) : (n = i, i = this.element, o = this.widget()), t.each(n, function (n, r) {
        function a() {
          return e || s.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof r ? s[r] : r).apply(s, arguments) : void 0
        }

        "string" != typeof r && (a.guid = r.guid = r.guid || a.guid || t.guid++);
        var l = n.match(/^([\w:-]*)\s*(.*)$/), c = l[1] + s.eventNamespace, h = l[2];
        h ? o.on(c, h, a) : i.on(c, a)
      })
    },
    _off: function (e, i) {
      i = (i || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, e.off(i).off(i), this.bindings = t(this.bindings.not(e).get()), this.focusable = t(this.focusable.not(e).get()), this.hoverable = t(this.hoverable.not(e).get())
    },
    _delay: function (t, e) {
      function i() {
        return ("string" == typeof t ? n[t] : t).apply(n, arguments)
      }

      var n = this;
      return setTimeout(i, e || 0)
    },
    _hoverable: function (e) {
      this.hoverable = this.hoverable.add(e), this._on(e, {
        mouseenter: function (e) {
          this._addClass(t(e.currentTarget), null, "ui-state-hover")
        }, mouseleave: function (e) {
          this._removeClass(t(e.currentTarget), null, "ui-state-hover")
        }
      })
    },
    _focusable: function (e) {
      this.focusable = this.focusable.add(e), this._on(e, {
        focusin: function (e) {
          this._addClass(t(e.currentTarget), null, "ui-state-focus")
        }, focusout: function (e) {
          this._removeClass(t(e.currentTarget), null, "ui-state-focus")
        }
      })
    },
    _trigger: function (e, i, n) {
      var o, s, r = this.options[e];
      if (n = n || {}, i = t.Event(i), i.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), i.target = this.element[0], s = i.originalEvent) for (o in s) o in i || (i[o] = s[o]);
      return this.element.trigger(i, n), !(t.isFunction(r) && r.apply(this.element[0], [i].concat(n)) === !1 || i.isDefaultPrevented())
    }
  }, t.each({show: "fadeIn", hide: "fadeOut"}, function (e, i) {
    t.Widget.prototype["_" + e] = function (n, o, s) {
      "string" == typeof o && (o = {effect: o});
      var r, a = o ? o === !0 || "number" == typeof o ? i : o.effect || i : e;
      o = o || {}, "number" == typeof o && (o = {duration: o}), r = !t.isEmptyObject(o), o.complete = s, o.delay && n.delay(o.delay), r && t.effects && t.effects.effect[a] ? n[e](o) : a !== e && n[a] ? n[a](o.duration, o.easing, s) : n.queue(function (i) {
        t(this)[e](), s && s.call(n[0]), i()
      })
    }
  }), t.widget, t.extend(t.expr[":"], {
    data: t.expr.createPseudo ? t.expr.createPseudo(function (e) {
      return function (i) {
        return !!t.data(i, e)
      }
    }) : function (e, i, n) {
      return !!t.data(e, n[3])
    }
  }), t.fn.scrollParent = function (e) {
    var i = this.css("position"), n = "absolute" === i, o = e ? /(auto|scroll|hidden)/ : /(auto|scroll)/,
      s = this.parents().filter(function () {
        var e = t(this);
        return (!n || "static" !== e.css("position")) && o.test(e.css("overflow") + e.css("overflow-y") + e.css("overflow-x"))
      }).eq(0);
    return "fixed" !== i && s.length ? s : t(this[0].ownerDocument || document)
  }, t.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());
  var n = !1;
  t(document).on("mouseup", function () {
    n = !1
  }), t.widget("ui.mouse", {
    version: "1.12.1",
    options: {cancel: "input, textarea, button, select, option", distance: 1, delay: 0},
    _mouseInit: function () {
      var e = this;
      this.element.on("mousedown." + this.widgetName, function (t) {
        return e._mouseDown(t)
      }).on("click." + this.widgetName, function (i) {
        return !0 === t.data(i.target, e.widgetName + ".preventClickEvent") ? (t.removeData(i.target, e.widgetName + ".preventClickEvent"), i.stopImmediatePropagation(), !1) : void 0
      }), this.started = !1
    },
    _mouseDestroy: function () {
      this.element.off("." + this.widgetName), this._mouseMoveDelegate && this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate)
    },
    _mouseDown: function (e) {
      if (!n) {
        this._mouseMoved = !1, this._mouseStarted && this._mouseUp(e), this._mouseDownEvent = e;
        var i = this, o = 1 === e.which,
          s = !("string" != typeof this.options.cancel || !e.target.nodeName) && t(e.target).closest(this.options.cancel).length;
        return !(o && !s && this._mouseCapture(e)) || (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function () {
          i.mouseDelayMet = !0
        }, this.options.delay)), this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(e) !== !1, !this._mouseStarted) ? (e.preventDefault(), !0) : (!0 === t.data(e.target, this.widgetName + ".preventClickEvent") && t.removeData(e.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function (t) {
          return i._mouseMove(t)
        }, this._mouseUpDelegate = function (t) {
          return i._mouseUp(t)
        }, this.document.on("mousemove." + this.widgetName, this._mouseMoveDelegate).on("mouseup." + this.widgetName, this._mouseUpDelegate), e.preventDefault(), n = !0, !0))
      }
    },
    _mouseMove: function (e) {
      if (this._mouseMoved) {
        if (t.ui.ie && (!document.documentMode || 9 > document.documentMode) && !e.button) return this._mouseUp(e);
        if (!e.which) if (e.originalEvent.altKey || e.originalEvent.ctrlKey || e.originalEvent.metaKey || e.originalEvent.shiftKey) this.ignoreMissingWhich = !0; else if (!this.ignoreMissingWhich) return this._mouseUp(e)
      }
      return (e.which || e.button) && (this._mouseMoved = !0), this._mouseStarted ? (this._mouseDrag(e), e.preventDefault()) : (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, e) !== !1, this._mouseStarted ? this._mouseDrag(e) : this._mouseUp(e)), !this._mouseStarted)
    },
    _mouseUp: function (e) {
      this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, e.target === this._mouseDownEvent.target && t.data(e.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(e)), this._mouseDelayTimer && (clearTimeout(this._mouseDelayTimer), delete this._mouseDelayTimer), this.ignoreMissingWhich = !1, n = !1, e.preventDefault()
    },
    _mouseDistanceMet: function (t) {
      return Math.max(Math.abs(this._mouseDownEvent.pageX - t.pageX), Math.abs(this._mouseDownEvent.pageY - t.pageY)) >= this.options.distance
    },
    _mouseDelayMet: function () {
      return this.mouseDelayMet
    },
    _mouseStart: function () {
    },
    _mouseDrag: function () {
    },
    _mouseStop: function () {
    },
    _mouseCapture: function () {
      return !0
    }
  }), t.ui.plugin = {
    add: function (e, i, n) {
      var o, s = t.ui[e].prototype;
      for (o in n) s.plugins[o] = s.plugins[o] || [], s.plugins[o].push([i, n[o]])
    }, call: function (t, e, i, n) {
      var o, s = t.plugins[e];
      if (s && (n || t.element[0].parentNode && 11 !== t.element[0].parentNode.nodeType)) for (o = 0; s.length > o; o++) t.options[s[o][0]] && s[o][1].apply(t.element, i)
    }
  }, t.ui.safeActiveElement = function (t) {
    var e;
    try {
      e = t.activeElement
    } catch (i) {
      e = t.body
    }
    return e || (e = t.body), e.nodeName || (e = t.body), e
  }, t.ui.safeBlur = function (e) {
    e && "body" !== e.nodeName.toLowerCase() && t(e).trigger("blur")
  }, t.widget("ui.draggable", t.ui.mouse, {
    version: "1.12.1",
    widgetEventPrefix: "drag",
    options: {
      addClasses: !0,
      appendTo: "parent",
      axis: !1,
      connectToSortable: !1,
      containment: !1,
      cursor: "auto",
      cursorAt: !1,
      grid: !1,
      handle: !1,
      helper: "original",
      iframeFix: !1,
      opacity: !1,
      refreshPositions: !1,
      revert: !1,
      revertDuration: 500,
      scope: "default",
      scroll: !0,
      scrollSensitivity: 20,
      scrollSpeed: 20,
      snap: !1,
      snapMode: "both",
      snapTolerance: 20,
      stack: !1,
      zIndex: !1,
      drag: null,
      start: null,
      stop: null
    },
    _create: function () {
      "original" === this.options.helper && this._setPositionRelative(), this.options.addClasses && this._addClass("ui-draggable"), this._setHandleClassName(), this._mouseInit()
    },
    _setOption: function (t, e) {
      this._super(t, e), "handle" === t && (this._removeHandleClassName(), this._setHandleClassName())
    },
    _destroy: function () {
      return (this.helper || this.element).is(".ui-draggable-dragging") ? void(this.destroyOnClear = !0) : (this._removeHandleClassName(), void this._mouseDestroy())
    },
    _mouseCapture: function (e) {
      var i = this.options;
      return !(this.helper || i.disabled || t(e.target).closest(".ui-resizable-handle").length > 0) && (this.handle = this._getHandle(e), !!this.handle && (this._blurActiveElement(e), this._blockFrames(i.iframeFix === !0 ? "iframe" : i.iframeFix), !0))
    },
    _blockFrames: function (e) {
      this.iframeBlocks = this.document.find(e).map(function () {
        var e = t(this);
        return t("<div>").css("position", "absolute").appendTo(e.parent()).outerWidth(e.outerWidth()).outerHeight(e.outerHeight()).offset(e.offset())[0]
      })
    },
    _unblockFrames: function () {
      this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks)
    },
    _blurActiveElement: function (e) {
      var i = t.ui.safeActiveElement(this.document[0]), n = t(e.target);
      n.closest(i).length || t.ui.safeBlur(i)
    },
    _mouseStart: function (e) {
      var i = this.options;
      return this.helper = this._createHelper(e), this._addClass(this.helper, "ui-draggable-dragging"), this._cacheHelperProportions(), t.ui.ddmanager && (t.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(!0), this.offsetParent = this.helper.offsetParent(), this.hasFixedAncestor = this.helper.parents().filter(function () {
        return "fixed" === t(this).css("position")
      }).length > 0, this.positionAbs = this.element.offset(), this._refreshOffsets(e), this.originalPosition = this.position = this._generatePosition(e, !1), this.originalPageX = e.pageX, this.originalPageY = e.pageY, i.cursorAt && this._adjustOffsetFromHelper(i.cursorAt), this._setContainment(), this._trigger("start", e) === !1 ? (this._clear(), !1) : (this._cacheHelperProportions(), t.ui.ddmanager && !i.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this._mouseDrag(e, !0), t.ui.ddmanager && t.ui.ddmanager.dragStart(this, e), !0)
    },
    _refreshOffsets: function (t) {
      this.offset = {
        top: this.positionAbs.top - this.margins.top,
        left: this.positionAbs.left - this.margins.left,
        scroll: !1,
        parent: this._getParentOffset(),
        relative: this._getRelativeOffset()
      }, this.offset.click = {left: t.pageX - this.offset.left, top: t.pageY - this.offset.top}
    },
    _mouseDrag: function (e, i) {
      if (this.hasFixedAncestor && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(e, !0), this.positionAbs = this._convertPositionTo("absolute"), !i) {
        var n = this._uiHash();
        if (this._trigger("drag", e, n) === !1) return this._mouseUp(new t.Event("mouseup", e)), !1;
        this.position = n.position
      }
      return this.helper[0].style.left = this.position.left + "px", this.helper[0].style.top = this.position.top + "px", t.ui.ddmanager && t.ui.ddmanager.drag(this, e), !1
    },
    _mouseStop: function (e) {
      var i = this, n = !1;
      return t.ui.ddmanager && !this.options.dropBehaviour && (n = t.ui.ddmanager.drop(this, e)), this.dropped && (n = this.dropped, this.dropped = !1), "invalid" === this.options.revert && !n || "valid" === this.options.revert && n || this.options.revert === !0 || t.isFunction(this.options.revert) && this.options.revert.call(this.element, n) ? t(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function () {
        i._trigger("stop", e) !== !1 && i._clear()
      }) : this._trigger("stop", e) !== !1 && this._clear(), !1
    },
    _mouseUp: function (e) {
      return this._unblockFrames(), t.ui.ddmanager && t.ui.ddmanager.dragStop(this, e), this.handleElement.is(e.target) && this.element.trigger("focus"), t.ui.mouse.prototype._mouseUp.call(this, e)
    },
    cancel: function () {
      return this.helper.is(".ui-draggable-dragging") ? this._mouseUp(new t.Event("mouseup", {target: this.element[0]})) : this._clear(), this
    },
    _getHandle: function (e) {
      return !this.options.handle || !!t(e.target).closest(this.element.find(this.options.handle)).length
    },
    _setHandleClassName: function () {
      this.handleElement = this.options.handle ? this.element.find(this.options.handle) : this.element, this._addClass(this.handleElement, "ui-draggable-handle")
    },
    _removeHandleClassName: function () {
      this._removeClass(this.handleElement, "ui-draggable-handle")
    },
    _createHelper: function (e) {
      var i = this.options, n = t.isFunction(i.helper),
        o = n ? t(i.helper.apply(this.element[0], [e])) : "clone" === i.helper ? this.element.clone().removeAttr("id") : this.element;
      return o.parents("body").length || o.appendTo("parent" === i.appendTo ? this.element[0].parentNode : i.appendTo), n && o[0] === this.element[0] && this._setPositionRelative(), o[0] === this.element[0] || /(fixed|absolute)/.test(o.css("position")) || o.css("position", "absolute"), o
    },
    _setPositionRelative: function () {
      /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative")
    },
    _adjustOffsetFromHelper: function (e) {
      "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
        left: +e[0],
        top: +e[1] || 0
      }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top)
    },
    _isRootNode: function (t) {
      return /(html|body)/i.test(t.tagName) || t === this.document[0]
    },
    _getParentOffset: function () {
      var e = this.offsetParent.offset(), i = this.document[0];
      return "absolute" === this.cssPosition && this.scrollParent[0] !== i && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), e.top += this.scrollParent.scrollTop()), this._isRootNode(this.offsetParent[0]) && (e = {
        top: 0,
        left: 0
      }), {
        top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
        left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
      }
    },
    _getRelativeOffset: function () {
      if ("relative" !== this.cssPosition) return {top: 0, left: 0};
      var t = this.element.position(), e = this._isRootNode(this.scrollParent[0]);
      return {
        top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + (e ? 0 : this.scrollParent.scrollTop()),
        left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + (e ? 0 : this.scrollParent.scrollLeft())
      }
    },
    _cacheMargins: function () {
      this.margins = {
        left: parseInt(this.element.css("marginLeft"), 10) || 0,
        top: parseInt(this.element.css("marginTop"), 10) || 0,
        right: parseInt(this.element.css("marginRight"), 10) || 0,
        bottom: parseInt(this.element.css("marginBottom"), 10) || 0
      }
    },
    _cacheHelperProportions: function () {
      this.helperProportions = {width: this.helper.outerWidth(), height: this.helper.outerHeight()}
    },
    _setContainment: function () {
      var e, i, n, o = this.options, s = this.document[0];
      return this.relativeContainer = null, o.containment ? "window" === o.containment ? void(this.containment = [t(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, t(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, t(window).scrollLeft() + t(window).width() - this.helperProportions.width - this.margins.left, t(window).scrollTop() + (t(window).height() || s.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : "document" === o.containment ? void(this.containment = [0, 0, t(s).width() - this.helperProportions.width - this.margins.left, (t(s).height() || s.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : o.containment.constructor === Array ? void(this.containment = o.containment) : ("parent" === o.containment && (o.containment = this.helper[0].parentNode), i = t(o.containment), n = i[0], void(n && (e = /(scroll|auto)/.test(i.css("overflow")), this.containment = [(parseInt(i.css("borderLeftWidth"), 10) || 0) + (parseInt(i.css("paddingLeft"), 10) || 0), (parseInt(i.css("borderTopWidth"), 10) || 0) + (parseInt(i.css("paddingTop"), 10) || 0), (e ? Math.max(n.scrollWidth, n.offsetWidth) : n.offsetWidth) - (parseInt(i.css("borderRightWidth"), 10) || 0) - (parseInt(i.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (e ? Math.max(n.scrollHeight, n.offsetHeight) : n.offsetHeight) - (parseInt(i.css("borderBottomWidth"), 10) || 0) - (parseInt(i.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relativeContainer = i))) : void(this.containment = null)
    },
    _convertPositionTo: function (t, e) {
      e || (e = this.position);
      var i = "absolute" === t ? 1 : -1, n = this._isRootNode(this.scrollParent[0]);
      return {
        top: e.top + this.offset.relative.top * i + this.offset.parent.top * i - ("fixed" === this.cssPosition ? -this.offset.scroll.top : n ? 0 : this.offset.scroll.top) * i,
        left: e.left + this.offset.relative.left * i + this.offset.parent.left * i - ("fixed" === this.cssPosition ? -this.offset.scroll.left : n ? 0 : this.offset.scroll.left) * i
      }
    },
    _generatePosition: function (t, e) {
      var i, n, o, s, r = this.options, a = this._isRootNode(this.scrollParent[0]), l = t.pageX, c = t.pageY;
      return a && this.offset.scroll || (this.offset.scroll = {
        top: this.scrollParent.scrollTop(),
        left: this.scrollParent.scrollLeft()
      }), e && (this.containment && (this.relativeContainer ? (n = this.relativeContainer.offset(), i = [this.containment[0] + n.left, this.containment[1] + n.top, this.containment[2] + n.left, this.containment[3] + n.top]) : i = this.containment, t.pageX - this.offset.click.left < i[0] && (l = i[0] + this.offset.click.left), t.pageY - this.offset.click.top < i[1] && (c = i[1] + this.offset.click.top), t.pageX - this.offset.click.left > i[2] && (l = i[2] + this.offset.click.left), t.pageY - this.offset.click.top > i[3] && (c = i[3] + this.offset.click.top)), r.grid && (o = r.grid[1] ? this.originalPageY + Math.round((c - this.originalPageY) / r.grid[1]) * r.grid[1] : this.originalPageY, c = i ? o - this.offset.click.top >= i[1] || o - this.offset.click.top > i[3] ? o : o - this.offset.click.top >= i[1] ? o - r.grid[1] : o + r.grid[1] : o, s = r.grid[0] ? this.originalPageX + Math.round((l - this.originalPageX) / r.grid[0]) * r.grid[0] : this.originalPageX, l = i ? s - this.offset.click.left >= i[0] || s - this.offset.click.left > i[2] ? s : s - this.offset.click.left >= i[0] ? s - r.grid[0] : s + r.grid[0] : s), "y" === r.axis && (l = this.originalPageX), "x" === r.axis && (c = this.originalPageY)), {
        top: c - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.offset.scroll.top : a ? 0 : this.offset.scroll.top),
        left: l - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.offset.scroll.left : a ? 0 : this.offset.scroll.left)
      }
    },
    _clear: function () {
      this._removeClass(this.helper, "ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1, this.destroyOnClear && this.destroy()
    },
    _trigger: function (e, i, n) {
      return n = n || this._uiHash(), t.ui.plugin.call(this, e, [i, n, this], !0), /^(drag|start|stop)/.test(e) && (this.positionAbs = this._convertPositionTo("absolute"), n.offset = this.positionAbs), t.Widget.prototype._trigger.call(this, e, i, n)
    },
    plugins: {},
    _uiHash: function () {
      return {
        helper: this.helper,
        position: this.position,
        originalPosition: this.originalPosition,
        offset: this.positionAbs
      }
    }
  }), t.ui.plugin.add("draggable", "connectToSortable", {
    start: function (e, i, n) {
      var o = t.extend({}, i, {item: n.element});
      n.sortables = [], t(n.options.connectToSortable).each(function () {
        var i = t(this).sortable("instance");
        i && !i.options.disabled && (n.sortables.push(i), i.refreshPositions(), i._trigger("activate", e, o))
      })
    }, stop: function (e, i, n) {
      var o = t.extend({}, i, {item: n.element});
      n.cancelHelperRemoval = !1, t.each(n.sortables, function () {
        var t = this;
        t.isOver ? (t.isOver = 0, n.cancelHelperRemoval = !0, t.cancelHelperRemoval = !1, t._storedCSS = {
          position: t.placeholder.css("position"),
          top: t.placeholder.css("top"),
          left: t.placeholder.css("left")
        }, t._mouseStop(e), t.options.helper = t.options._helper) : (t.cancelHelperRemoval = !0, t._trigger("deactivate", e, o))
      })
    }, drag: function (e, i, n) {
      t.each(n.sortables, function () {
        var o = !1, s = this;
        s.positionAbs = n.positionAbs, s.helperProportions = n.helperProportions, s.offset.click = n.offset.click, s._intersectsWith(s.containerCache) && (o = !0, t.each(n.sortables, function () {
          return this.positionAbs = n.positionAbs, this.helperProportions = n.helperProportions, this.offset.click = n.offset.click, this !== s && this._intersectsWith(this.containerCache) && t.contains(s.element[0], this.element[0]) && (o = !1), o
        })), o ? (s.isOver || (s.isOver = 1, n._parent = i.helper.parent(), s.currentItem = i.helper.appendTo(s.element).data("ui-sortable-item", !0), s.options._helper = s.options.helper, s.options.helper = function () {
          return i.helper[0]
        }, e.target = s.currentItem[0], s._mouseCapture(e, !0), s._mouseStart(e, !0, !0), s.offset.click.top = n.offset.click.top, s.offset.click.left = n.offset.click.left, s.offset.parent.left -= n.offset.parent.left - s.offset.parent.left, s.offset.parent.top -= n.offset.parent.top - s.offset.parent.top, n._trigger("toSortable", e), n.dropped = s.element, t.each(n.sortables, function () {
          this.refreshPositions()
        }), n.currentItem = n.element, s.fromOutside = n), s.currentItem && (s._mouseDrag(e), i.position = s.position)) : s.isOver && (s.isOver = 0, s.cancelHelperRemoval = !0, s.options._revert = s.options.revert, s.options.revert = !1, s._trigger("out", e, s._uiHash(s)), s._mouseStop(e, !0), s.options.revert = s.options._revert, s.options.helper = s.options._helper, s.placeholder && s.placeholder.remove(), i.helper.appendTo(n._parent), n._refreshOffsets(e), i.position = n._generatePosition(e, !0), n._trigger("fromSortable", e), n.dropped = !1, t.each(n.sortables, function () {
          this.refreshPositions()
        }))
      })
    }
  }), t.ui.plugin.add("draggable", "cursor", {
    start: function (e, i, n) {
      var o = t("body"), s = n.options;
      o.css("cursor") && (s._cursor = o.css("cursor")), o.css("cursor", s.cursor)
    }, stop: function (e, i, n) {
      var o = n.options;
      o._cursor && t("body").css("cursor", o._cursor)
    }
  }),
    t.ui.plugin.add("draggable", "opacity", {
      start: function (e, i, n) {
        var o = t(i.helper), s = n.options;
        o.css("opacity") && (s._opacity = o.css("opacity")), o.css("opacity", s.opacity)
      }, stop: function (e, i, n) {
        var o = n.options;
        o._opacity && t(i.helper).css("opacity", o._opacity)
      }
    }), t.ui.plugin.add("draggable", "scroll", {
    start: function (t, e, i) {
      i.scrollParentNotHidden || (i.scrollParentNotHidden = i.helper.scrollParent(!1)), i.scrollParentNotHidden[0] !== i.document[0] && "HTML" !== i.scrollParentNotHidden[0].tagName && (i.overflowOffset = i.scrollParentNotHidden.offset())
    }, drag: function (e, i, n) {
      var o = n.options, s = !1, r = n.scrollParentNotHidden[0], a = n.document[0];
      r !== a && "HTML" !== r.tagName ? (o.axis && "x" === o.axis || (n.overflowOffset.top + r.offsetHeight - e.pageY < o.scrollSensitivity ? r.scrollTop = s = r.scrollTop + o.scrollSpeed : e.pageY - n.overflowOffset.top < o.scrollSensitivity && (r.scrollTop = s = r.scrollTop - o.scrollSpeed)), o.axis && "y" === o.axis || (n.overflowOffset.left + r.offsetWidth - e.pageX < o.scrollSensitivity ? r.scrollLeft = s = r.scrollLeft + o.scrollSpeed : e.pageX - n.overflowOffset.left < o.scrollSensitivity && (r.scrollLeft = s = r.scrollLeft - o.scrollSpeed))) : (o.axis && "x" === o.axis || (e.pageY - t(a).scrollTop() < o.scrollSensitivity ? s = t(a).scrollTop(t(a).scrollTop() - o.scrollSpeed) : t(window).height() - (e.pageY - t(a).scrollTop()) < o.scrollSensitivity && (s = t(a).scrollTop(t(a).scrollTop() + o.scrollSpeed))), o.axis && "y" === o.axis || (e.pageX - t(a).scrollLeft() < o.scrollSensitivity ? s = t(a).scrollLeft(t(a).scrollLeft() - o.scrollSpeed) : t(window).width() - (e.pageX - t(a).scrollLeft()) < o.scrollSensitivity && (s = t(a).scrollLeft(t(a).scrollLeft() + o.scrollSpeed)))), s !== !1 && t.ui.ddmanager && !o.dropBehaviour && t.ui.ddmanager.prepareOffsets(n, e)
    }
  }), t.ui.plugin.add("draggable", "snap", {
    start: function (e, i, n) {
      var o = n.options;
      n.snapElements = [], t(o.snap.constructor !== String ? o.snap.items || ":data(ui-draggable)" : o.snap).each(function () {
        var e = t(this), i = e.offset();
        this !== n.element[0] && n.snapElements.push({
          item: this,
          width: e.outerWidth(),
          height: e.outerHeight(),
          top: i.top,
          left: i.left
        })
      })
    }, drag: function (e, i, n) {
      var o, s, r, a, l, c, h, u, d, p, f = n.options, m = f.snapTolerance, g = i.offset.left,
        v = g + n.helperProportions.width, y = i.offset.top, b = y + n.helperProportions.height;
      for (d = n.snapElements.length - 1; d >= 0; d--) l = n.snapElements[d].left - n.margins.left, c = l + n.snapElements[d].width, h = n.snapElements[d].top - n.margins.top, u = h + n.snapElements[d].height, l - m > v || g > c + m || h - m > b || y > u + m || !t.contains(n.snapElements[d].item.ownerDocument, n.snapElements[d].item) ? (n.snapElements[d].snapping && n.options.snap.release && n.options.snap.release.call(n.element, e, t.extend(n._uiHash(), {snapItem: n.snapElements[d].item})), n.snapElements[d].snapping = !1) : ("inner" !== f.snapMode && (o = m >= Math.abs(h - b), s = m >= Math.abs(u - y), r = m >= Math.abs(l - v), a = m >= Math.abs(c - g), o && (i.position.top = n._convertPositionTo("relative", {
        top: h - n.helperProportions.height,
        left: 0
      }).top), s && (i.position.top = n._convertPositionTo("relative", {
        top: u,
        left: 0
      }).top), r && (i.position.left = n._convertPositionTo("relative", {
        top: 0,
        left: l - n.helperProportions.width
      }).left), a && (i.position.left = n._convertPositionTo("relative", {
        top: 0,
        left: c
      }).left)), p = o || s || r || a, "outer" !== f.snapMode && (o = m >= Math.abs(h - y), s = m >= Math.abs(u - b), r = m >= Math.abs(l - g), a = m >= Math.abs(c - v), o && (i.position.top = n._convertPositionTo("relative", {
        top: h,
        left: 0
      }).top), s && (i.position.top = n._convertPositionTo("relative", {
        top: u - n.helperProportions.height,
        left: 0
      }).top), r && (i.position.left = n._convertPositionTo("relative", {
        top: 0,
        left: l
      }).left), a && (i.position.left = n._convertPositionTo("relative", {
        top: 0,
        left: c - n.helperProportions.width
      }).left)), !n.snapElements[d].snapping && (o || s || r || a || p) && n.options.snap.snap && n.options.snap.snap.call(n.element, e, t.extend(n._uiHash(), {snapItem: n.snapElements[d].item})), n.snapElements[d].snapping = o || s || r || a || p)
    }
  }), t.ui.plugin.add("draggable", "stack", {
    start: function (e, i, n) {
      var o, s = n.options, r = t.makeArray(t(s.stack)).sort(function (e, i) {
        return (parseInt(t(e).css("zIndex"), 10) || 0) - (parseInt(t(i).css("zIndex"), 10) || 0)
      });
      r.length && (o = parseInt(t(r[0]).css("zIndex"), 10) || 0, t(r).each(function (e) {
        t(this).css("zIndex", o + e)
      }), this.css("zIndex", o + r.length))
    }
  }), t.ui.plugin.add("draggable", "zIndex", {
    start: function (e, i, n) {
      var o = t(i.helper), s = n.options;
      o.css("zIndex") && (s._zIndex = o.css("zIndex")), o.css("zIndex", s.zIndex)
    }, stop: function (e, i, n) {
      var o = n.options;
      o._zIndex && t(i.helper).css("zIndex", o._zIndex)
    }
  }), t.ui.draggable, t.widget("ui.droppable", {
    version: "1.12.1",
    widgetEventPrefix: "drop",
    options: {
      accept: "*",
      addClasses: !0,
      greedy: !1,
      scope: "default",
      tolerance: "intersect",
      activate: null,
      deactivate: null,
      drop: null,
      out: null,
      over: null
    },
    _create: function () {
      var e, i = this.options, n = i.accept;
      this.isover = !1, this.isout = !0, this.accept = t.isFunction(n) ? n : function (t) {
        return t.is(n)
      }, this.proportions = function () {
        return arguments.length ? void(e = arguments[0]) : e ? e : e = {
          width: this.element[0].offsetWidth,
          height: this.element[0].offsetHeight
        }
      }, this._addToManager(i.scope), i.addClasses && this._addClass("ui-droppable")
    },
    _addToManager: function (e) {
      t.ui.ddmanager.droppables[e] = t.ui.ddmanager.droppables[e] || [], t.ui.ddmanager.droppables[e].push(this)
    },
    _splice: function (t) {
      for (var e = 0; t.length > e; e++) t[e] === this && t.splice(e, 1)
    },
    _destroy: function () {
      var e = t.ui.ddmanager.droppables[this.options.scope];
      this._splice(e)
    },
    _setOption: function (e, i) {
      if ("accept" === e) this.accept = t.isFunction(i) ? i : function (t) {
        return t.is(i)
      }; else if ("scope" === e) {
        var n = t.ui.ddmanager.droppables[this.options.scope];
        this._splice(n), this._addToManager(i)
      }
      this._super(e, i)
    },
    _activate: function (e) {
      var i = t.ui.ddmanager.current;
      this._addActiveClass(), i && this._trigger("activate", e, this.ui(i))
    },
    _deactivate: function (e) {
      var i = t.ui.ddmanager.current;
      this._removeActiveClass(), i && this._trigger("deactivate", e, this.ui(i))
    },
    _over: function (e) {
      var i = t.ui.ddmanager.current;
      i && (i.currentItem || i.element)[0] !== this.element[0] && this.accept.call(this.element[0], i.currentItem || i.element) && (this._addHoverClass(), this._trigger("over", e, this.ui(i)))
    },
    _out: function (e) {
      var i = t.ui.ddmanager.current;
      i && (i.currentItem || i.element)[0] !== this.element[0] && this.accept.call(this.element[0], i.currentItem || i.element) && (this._removeHoverClass(), this._trigger("out", e, this.ui(i)))
    },
    _drop: function (e, i) {
      var n = i || t.ui.ddmanager.current, s = !1;
      return !(!n || (n.currentItem || n.element)[0] === this.element[0]) && (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function () {
        var i = t(this).droppable("instance");
        return i.options.greedy && !i.options.disabled && i.options.scope === n.options.scope && i.accept.call(i.element[0], n.currentItem || n.element) && o(n, t.extend(i, {offset: i.element.offset()}), i.options.tolerance, e) ? (s = !0, !1) : void 0
      }), !s && (!!this.accept.call(this.element[0], n.currentItem || n.element) && (this._removeActiveClass(), this._removeHoverClass(), this._trigger("drop", e, this.ui(n)), this.element)))
    },
    ui: function (t) {
      return {
        draggable: t.currentItem || t.element,
        helper: t.helper,
        position: t.position,
        offset: t.positionAbs
      }
    },
    _addHoverClass: function () {
      this._addClass("ui-droppable-hover")
    },
    _removeHoverClass: function () {
      this._removeClass("ui-droppable-hover")
    },
    _addActiveClass: function () {
      this._addClass("ui-droppable-active")
    },
    _removeActiveClass: function () {
      this._removeClass("ui-droppable-active")
    }
  });
  var o = t.ui.intersect = function () {
    function t(t, e, i) {
      return t >= e && e + i > t
    }

    return function (e, i, n, o) {
      if (!i.offset) return !1;
      var s = (e.positionAbs || e.position.absolute).left + e.margins.left,
        r = (e.positionAbs || e.position.absolute).top + e.margins.top, a = s + e.helperProportions.width,
        l = r + e.helperProportions.height, c = i.offset.left, h = i.offset.top, u = c + i.proportions().width,
        d = h + i.proportions().height;
      switch (n) {
        case"fit":
          return s >= c && u >= a && r >= h && d >= l;
        case"intersect":
          return s + e.helperProportions.width / 2 > c && u > a - e.helperProportions.width / 2 && r + e.helperProportions.height / 2 > h && d > l - e.helperProportions.height / 2;
        case"pointer":
          return t(o.pageY, h, i.proportions().height) && t(o.pageX, c, i.proportions().width);
        case"touch":
          return (r >= h && d >= r || l >= h && d >= l || h > r && l > d) && (s >= c && u >= s || a >= c && u >= a || c > s && a > u);
        default:
          return !1
      }
    }
  }();
  t.ui.ddmanager = {
    current: null, droppables: {"default": []}, prepareOffsets: function (e, i) {
      var n, o, s = t.ui.ddmanager.droppables[e.options.scope] || [], r = i ? i.type : null,
        a = (e.currentItem || e.element).find(":data(ui-droppable)").addBack();
      t:for (n = 0; s.length > n; n++) if (!(s[n].options.disabled || e && !s[n].accept.call(s[n].element[0], e.currentItem || e.element))) {
        for (o = 0; a.length > o; o++) if (a[o] === s[n].element[0]) {
          s[n].proportions().height = 0;
          continue t
        }
        s[n].visible = "none" !== s[n].element.css("display"), s[n].visible && ("mousedown" === r && s[n]._activate.call(s[n], i), s[n].offset = s[n].element.offset(), s[n].proportions({
          width: s[n].element[0].offsetWidth,
          height: s[n].element[0].offsetHeight
        }))
      }
    }, drop: function (e, i) {
      var n = !1;
      return t.each((t.ui.ddmanager.droppables[e.options.scope] || []).slice(), function () {
        this.options && (!this.options.disabled && this.visible && o(e, this, this.options.tolerance, i) && (n = this._drop.call(this, i) || n), !this.options.disabled && this.visible && this.accept.call(this.element[0], e.currentItem || e.element) && (this.isout = !0, this.isover = !1, this._deactivate.call(this, i)))
      }), n
    }, dragStart: function (e, i) {
      e.element.parentsUntil("body").on("scroll.droppable", function () {
        e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, i)
      })
    }, drag: function (e, i) {
      e.options.refreshPositions && t.ui.ddmanager.prepareOffsets(e, i), t.each(t.ui.ddmanager.droppables[e.options.scope] || [], function () {
        if (!this.options.disabled && !this.greedyChild && this.visible) {
          var n, s, r, a = o(e, this, this.options.tolerance, i),
            l = !a && this.isover ? "isout" : a && !this.isover ? "isover" : null;
          l && (this.options.greedy && (s = this.options.scope, r = this.element.parents(":data(ui-droppable)").filter(function () {
            return t(this).droppable("instance").options.scope === s
          }), r.length && (n = t(r[0]).droppable("instance"), n.greedyChild = "isover" === l)), n && "isover" === l && (n.isover = !1, n.isout = !0, n._out.call(n, i)), this[l] = !0, this["isout" === l ? "isover" : "isout"] = !1, this["isover" === l ? "_over" : "_out"].call(this, i), n && "isout" === l && (n.isout = !1, n.isover = !0, n._over.call(n, i)))
        }
      })
    }, dragStop: function (e, i) {
      e.element.parentsUntil("body").off("scroll.droppable"), e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, i)
    }
  }, t.uiBackCompat !== !1 && t.widget("ui.droppable", t.ui.droppable, {
    options: {hoverClass: !1, activeClass: !1},
    _addActiveClass: function () {
      this._super(), this.options.activeClass && this.element.addClass(this.options.activeClass)
    },
    _removeActiveClass: function () {
      this._super(), this.options.activeClass && this.element.removeClass(this.options.activeClass)
    },
    _addHoverClass: function () {
      this._super(), this.options.hoverClass && this.element.addClass(this.options.hoverClass)
    },
    _removeHoverClass: function () {
      this._super(), this.options.hoverClass && this.element.removeClass(this.options.hoverClass)
    }
  }), t.ui.droppable, t.widget("ui.selectable", t.ui.mouse, {
    version: "1.12.1",
    options: {
      appendTo: "body",
      autoRefresh: !0,
      distance: 0,
      filter: "*",
      tolerance: "touch",
      selected: null,
      selecting: null,
      start: null,
      stop: null,
      unselected: null,
      unselecting: null
    },
    _create: function () {
      var e = this;
      this._addClass("ui-selectable"), this.dragged = !1, this.refresh = function () {
        e.elementPos = t(e.element[0]).offset(), e.selectees = t(e.options.filter, e.element[0]), e._addClass(e.selectees, "ui-selectee"), e.selectees.each(function () {
          var i = t(this), n = i.offset(),
            o = {left: n.left - e.elementPos.left, top: n.top - e.elementPos.top};
          t.data(this, "selectable-item", {
            element: this,
            $element: i,
            left: o.left,
            top: o.top,
            right: o.left + i.outerWidth(),
            bottom: o.top + i.outerHeight(),
            startselected: !1,
            selected: i.hasClass("ui-selected"),
            selecting: i.hasClass("ui-selecting"),
            unselecting: i.hasClass("ui-unselecting")
          })
        })
      }, this.refresh(), this._mouseInit(), this.helper = t("<div>"), this._addClass(this.helper, "ui-selectable-helper")
    },
    _destroy: function () {
      this.selectees.removeData("selectable-item"), this._mouseDestroy()
    },
    _mouseStart: function (e) {
      var i = this, n = this.options;
      this.opos = [e.pageX, e.pageY], this.elementPos = t(this.element[0]).offset(), this.options.disabled || (this.selectees = t(n.filter, this.element[0]), this._trigger("start", e), t(n.appendTo).append(this.helper), this.helper.css({
        left: e.pageX,
        top: e.pageY,
        width: 0,
        height: 0
      }), n.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function () {
        var n = t.data(this, "selectable-item");
        n.startselected = !0, e.metaKey || e.ctrlKey || (i._removeClass(n.$element, "ui-selected"), n.selected = !1, i._addClass(n.$element, "ui-unselecting"), n.unselecting = !0, i._trigger("unselecting", e, {unselecting: n.element}))
      }), t(e.target).parents().addBack().each(function () {
        var n, o = t.data(this, "selectable-item");
        return o ? (n = !e.metaKey && !e.ctrlKey || !o.$element.hasClass("ui-selected"), i._removeClass(o.$element, n ? "ui-unselecting" : "ui-selected")._addClass(o.$element, n ? "ui-selecting" : "ui-unselecting"), o.unselecting = !n, o.selecting = n, o.selected = n, n ? i._trigger("selecting", e, {selecting: o.element}) : i._trigger("unselecting", e, {unselecting: o.element}), !1) : void 0
      }))
    },
    _mouseDrag: function (e) {
      if (this.dragged = !0, !this.options.disabled) {
        var i, n = this, o = this.options, s = this.opos[0], r = this.opos[1], a = e.pageX, l = e.pageY;
        return s > a && (i = a, a = s, s = i), r > l && (i = l, l = r, r = i), this.helper.css({
          left: s,
          top: r,
          width: a - s,
          height: l - r
        }), this.selectees.each(function () {
          var i = t.data(this, "selectable-item"), c = !1, h = {};
          i && i.element !== n.element[0] && (h.left = i.left + n.elementPos.left, h.right = i.right + n.elementPos.left, h.top = i.top + n.elementPos.top, h.bottom = i.bottom + n.elementPos.top, "touch" === o.tolerance ? c = !(h.left > a || s > h.right || h.top > l || r > h.bottom) : "fit" === o.tolerance && (c = h.left > s && a > h.right && h.top > r && l > h.bottom), c ? (i.selected && (n._removeClass(i.$element, "ui-selected"), i.selected = !1), i.unselecting && (n._removeClass(i.$element, "ui-unselecting"), i.unselecting = !1), i.selecting || (n._addClass(i.$element, "ui-selecting"), i.selecting = !0, n._trigger("selecting", e, {selecting: i.element}))) : (i.selecting && ((e.metaKey || e.ctrlKey) && i.startselected ? (n._removeClass(i.$element, "ui-selecting"), i.selecting = !1, n._addClass(i.$element, "ui-selected"), i.selected = !0) : (n._removeClass(i.$element, "ui-selecting"), i.selecting = !1, i.startselected && (n._addClass(i.$element, "ui-unselecting"), i.unselecting = !0), n._trigger("unselecting", e, {unselecting: i.element}))), i.selected && (e.metaKey || e.ctrlKey || i.startselected || (n._removeClass(i.$element, "ui-selected"), i.selected = !1, n._addClass(i.$element, "ui-unselecting"), i.unselecting = !0, n._trigger("unselecting", e, {unselecting: i.element})))))
        }), !1
      }
    },
    _mouseStop: function (e) {
      var i = this;
      return this.dragged = !1, t(".ui-unselecting", this.element[0]).each(function () {
        var n = t.data(this, "selectable-item");
        i._removeClass(n.$element, "ui-unselecting"), n.unselecting = !1, n.startselected = !1, i._trigger("unselected", e, {unselected: n.element})
      }), t(".ui-selecting", this.element[0]).each(function () {
        var n = t.data(this, "selectable-item");
        i._removeClass(n.$element, "ui-selecting")._addClass(n.$element, "ui-selected"), n.selecting = !1, n.selected = !0, n.startselected = !0, i._trigger("selected", e, {selected: n.element})
      }), this._trigger("stop", e), this.helper.remove(), !1
    }
  }), t.widget("ui.sortable", t.ui.mouse, {
    version: "1.12.1",
    widgetEventPrefix: "sort",
    ready: !1,
    options: {
      appendTo: "parent",
      axis: !1,
      connectWith: !1,
      containment: !1,
      cursor: "auto",
      cursorAt: !1,
      dropOnEmpty: !0,
      forcePlaceholderSize: !1,
      forceHelperSize: !1,
      grid: !1,
      handle: !1,
      helper: "original",
      items: "> *",
      opacity: !1,
      placeholder: !1,
      revert: !1,
      scroll: !0,
      scrollSensitivity: 20,
      scrollSpeed: 20,
      scope: "default",
      tolerance: "intersect",
      zIndex: 1e3,
      activate: null,
      beforeStop: null,
      change: null,
      deactivate: null,
      out: null,
      over: null,
      receive: null,
      remove: null,
      sort: null,
      start: null,
      stop: null,
      update: null
    },
    _isOverAxis: function (t, e, i) {
      return t >= e && e + i > t
    },
    _isFloating: function (t) {
      return /left|right/.test(t.css("float")) || /inline|table-cell/.test(t.css("display"))
    },
    _create: function () {
      this.containerCache = {}, this._addClass("ui-sortable"), this.refresh(), this.offset = this.element.offset(), this._mouseInit(), this._setHandleClassName(), this.ready = !0
    },
    _setOption: function (t, e) {
      this._super(t, e), "handle" === t && this._setHandleClassName()
    },
    _setHandleClassName: function () {
      var e = this;
      this._removeClass(this.element.find(".ui-sortable-handle"), "ui-sortable-handle"), t.each(this.items, function () {
        e._addClass(this.instance.options.handle ? this.item.find(this.instance.options.handle) : this.item, "ui-sortable-handle")
      })
    },
    _destroy: function () {
      this._mouseDestroy();
      for (var t = this.items.length - 1; t >= 0; t--) this.items[t].item.removeData(this.widgetName + "-item");
      return this
    },
    _mouseCapture: function (e, i) {
      var n = null, o = !1, s = this;
      return !this.reverting && (!this.options.disabled && "static" !== this.options.type && (this._refreshItems(e), t(e.target).parents().each(function () {
        return t.data(this, s.widgetName + "-item") === s ? (n = t(this), !1) : void 0
      }), t.data(e.target, s.widgetName + "-item") === s && (n = t(e.target)), !!n && (!(this.options.handle && !i && (t(this.options.handle, n).find("*").addBack().each(function () {
        this === e.target && (o = !0)
      }), !o)) && (this.currentItem = n, this._removeCurrentsFromItems(), !0))))
    },
    _mouseStart: function (e, i, n) {
      var o, s, r = this.options;
      if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(e), this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), this.offset = this.currentItem.offset(), this.offset = {
        top: this.offset.top - this.margins.top,
        left: this.offset.left - this.margins.left
      }, t.extend(this.offset, {
        click: {left: e.pageX - this.offset.left, top: e.pageY - this.offset.top},
        parent: this._getParentOffset(),
        relative: this._getRelativeOffset()
      }), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), this.originalPosition = this._generatePosition(e), this.originalPageX = e.pageX, this.originalPageY = e.pageY, r.cursorAt && this._adjustOffsetFromHelper(r.cursorAt), this.domPosition = {
        prev: this.currentItem.prev()[0],
        parent: this.currentItem.parent()[0]
      }, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), r.containment && this._setContainment(), r.cursor && "auto" !== r.cursor && (s = this.document.find("body"), this.storedCursor = s.css("cursor"), s.css("cursor", r.cursor), this.storedStylesheet = t("<style>*{ cursor: " + r.cursor + " !important; }</style>").appendTo(s)), r.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), this.helper.css("opacity", r.opacity)), r.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), this.helper.css("zIndex", r.zIndex)), this.scrollParent[0] !== this.document[0] && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), this._trigger("start", e, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), !n) for (o = this.containers.length - 1; o >= 0; o--) this.containers[o]._trigger("activate", e, this._uiHash(this));
      return t.ui.ddmanager && (t.ui.ddmanager.current = this), t.ui.ddmanager && !r.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this.dragging = !0, this._addClass(this.helper, "ui-sortable-helper"), this._mouseDrag(e), !0
    },
    _mouseDrag: function (e) {
      var i, n, o, s, r = this.options, a = !1;
      for (this.position = this._generatePosition(e), this.positionAbs = this._convertPositionTo("absolute"), this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll && (this.scrollParent[0] !== this.document[0] && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - e.pageY < r.scrollSensitivity ? this.scrollParent[0].scrollTop = a = this.scrollParent[0].scrollTop + r.scrollSpeed : e.pageY - this.overflowOffset.top < r.scrollSensitivity && (this.scrollParent[0].scrollTop = a = this.scrollParent[0].scrollTop - r.scrollSpeed), this.overflowOffset.left + this.scrollParent[0].offsetWidth - e.pageX < r.scrollSensitivity ? this.scrollParent[0].scrollLeft = a = this.scrollParent[0].scrollLeft + r.scrollSpeed : e.pageX - this.overflowOffset.left < r.scrollSensitivity && (this.scrollParent[0].scrollLeft = a = this.scrollParent[0].scrollLeft - r.scrollSpeed)) : (e.pageY - this.document.scrollTop() < r.scrollSensitivity ? a = this.document.scrollTop(this.document.scrollTop() - r.scrollSpeed) : this.window.height() - (e.pageY - this.document.scrollTop()) < r.scrollSensitivity && (a = this.document.scrollTop(this.document.scrollTop() + r.scrollSpeed)), e.pageX - this.document.scrollLeft() < r.scrollSensitivity ? a = this.document.scrollLeft(this.document.scrollLeft() - r.scrollSpeed) : this.window.width() - (e.pageX - this.document.scrollLeft()) < r.scrollSensitivity && (a = this.document.scrollLeft(this.document.scrollLeft() + r.scrollSpeed))), a !== !1 && t.ui.ddmanager && !r.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e)), this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), i = this.items.length - 1; i >= 0; i--) if (n = this.items[i], o = n.item[0], s = this._intersectsWithPointer(n), s && n.instance === this.currentContainer && o !== this.currentItem[0] && this.placeholder[1 === s ? "next" : "prev"]()[0] !== o && !t.contains(this.placeholder[0], o) && ("semi-dynamic" !== this.options.type || !t.contains(this.element[0], o))) {
        if (this.direction = 1 === s ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(n)) break;
        this._rearrange(e, n), this._trigger("change", e, this._uiHash());
        break
      }
      return this._contactContainers(e), t.ui.ddmanager && t.ui.ddmanager.drag(this, e), this._trigger("sort", e, this._uiHash()), this.lastPositionAbs = this.positionAbs, !1
    },
    _mouseStop: function (e, i) {
      if (e) {
        if (t.ui.ddmanager && !this.options.dropBehaviour && t.ui.ddmanager.drop(this, e), this.options.revert) {
          var n = this, o = this.placeholder.offset(), s = this.options.axis, r = {};
          s && "x" !== s || (r.left = o.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollLeft)), s && "y" !== s || (r.top = o.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollTop)), this.reverting = !0, t(this.helper).animate(r, parseInt(this.options.revert, 10) || 500, function () {
            n._clear(e)
          })
        } else this._clear(e, i);
        return !1
      }
    },
    cancel: function () {
      if (this.dragging) {
        this._mouseUp(new t.Event("mouseup", {target: null})), "original" === this.options.helper ? (this.currentItem.css(this._storedCSS), this._removeClass(this.currentItem, "ui-sortable-helper")) : this.currentItem.show();
        for (var e = this.containers.length - 1; e >= 0; e--) this.containers[e]._trigger("deactivate", null, this._uiHash(this)), this.containers[e].containerCache.over && (this.containers[e]._trigger("out", null, this._uiHash(this)), this.containers[e].containerCache.over = 0)
      }
      return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), t.extend(this, {
        helper: null,
        dragging: !1,
        reverting: !1,
        _noFinalSort: null
      }), this.domPosition.prev ? t(this.domPosition.prev).after(this.currentItem) : t(this.domPosition.parent).prepend(this.currentItem)), this
    },
    serialize: function (e) {
      var i = this._getItemsAsjQuery(e && e.connected), n = [];
      return e = e || {}, t(i).each(function () {
        var i = (t(e.item || this).attr(e.attribute || "id") || "").match(e.expression || /(.+)[\-=_](.+)/);
        i && n.push((e.key || i[1] + "[]") + "=" + (e.key && e.expression ? i[1] : i[2]))
      }), !n.length && e.key && n.push(e.key + "="), n.join("&")
    },
    toArray: function (e) {
      var i = this._getItemsAsjQuery(e && e.connected), n = [];
      return e = e || {}, i.each(function () {
        n.push(t(e.item || this).attr(e.attribute || "id") || "")
      }), n
    },
    _intersectsWith: function (t) {
      var e = this.positionAbs.left, i = e + this.helperProportions.width, n = this.positionAbs.top,
        o = n + this.helperProportions.height, s = t.left, r = s + t.width, a = t.top, l = a + t.height,
        c = this.offset.click.top, h = this.offset.click.left,
        u = "x" === this.options.axis || n + c > a && l > n + c,
        d = "y" === this.options.axis || e + h > s && r > e + h, p = u && d;
      return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > t[this.floating ? "width" : "height"] ? p : e + this.helperProportions.width / 2 > s && r > i - this.helperProportions.width / 2 && n + this.helperProportions.height / 2 > a && l > o - this.helperProportions.height / 2
    },
    _intersectsWithPointer: function (t) {
      var e, i,
        n = "x" === this.options.axis || this._isOverAxis(this.positionAbs.top + this.offset.click.top, t.top, t.height),
        o = "y" === this.options.axis || this._isOverAxis(this.positionAbs.left + this.offset.click.left, t.left, t.width),
        s = n && o;
      return !!s && (e = this._getDragVerticalDirection(), i = this._getDragHorizontalDirection(), this.floating ? "right" === i || "down" === e ? 2 : 1 : e && ("down" === e ? 2 : 1))
    },
    _intersectsWithSides: function (t) {
      var e = this._isOverAxis(this.positionAbs.top + this.offset.click.top, t.top + t.height / 2, t.height),
        i = this._isOverAxis(this.positionAbs.left + this.offset.click.left, t.left + t.width / 2, t.width),
        n = this._getDragVerticalDirection(), o = this._getDragHorizontalDirection();
      return this.floating && o ? "right" === o && i || "left" === o && !i : n && ("down" === n && e || "up" === n && !e)
    },
    _getDragVerticalDirection: function () {
      var t = this.positionAbs.top - this.lastPositionAbs.top;
      return 0 !== t && (t > 0 ? "down" : "up")
    },
    _getDragHorizontalDirection: function () {
      var t = this.positionAbs.left - this.lastPositionAbs.left;
      return 0 !== t && (t > 0 ? "right" : "left")
    },
    refresh: function (t) {
      return this._refreshItems(t), this._setHandleClassName(), this.refreshPositions(), this
    },
    _connectWith: function () {
      var t = this.options;
      return t.connectWith.constructor === String ? [t.connectWith] : t.connectWith
    },
    _getItemsAsjQuery: function (e) {
      function i() {
        a.push(this)
      }

      var n, o, s, r, a = [], l = [], c = this._connectWith();
      if (c && e) for (n = c.length - 1; n >= 0; n--) for (s = t(c[n], this.document[0]), o = s.length - 1; o >= 0; o--) r = t.data(s[o], this.widgetFullName), r && r !== this && !r.options.disabled && l.push([t.isFunction(r.options.items) ? r.options.items.call(r.element) : t(r.options.items, r.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), r]);
      for (l.push([t.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
        options: this.options,
        item: this.currentItem
      }) : t(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]), n = l.length - 1; n >= 0; n--) l[n][0].each(i);
      return t(a)
    },
    _removeCurrentsFromItems: function () {
      var e = this.currentItem.find(":data(" + this.widgetName + "-item)");
      this.items = t.grep(this.items, function (t) {
        for (var i = 0; e.length > i; i++) if (e[i] === t.item[0]) return !1;
        return !0
      })
    },
    _refreshItems: function (e) {
      this.items = [], this.containers = [this];
      var i, n, o, s, r, a, l, c, h = this.items,
        u = [[t.isFunction(this.options.items) ? this.options.items.call(this.element[0], e, {item: this.currentItem}) : t(this.options.items, this.element), this]],
        d = this._connectWith();
      if (d && this.ready) for (i = d.length - 1; i >= 0; i--) for (o = t(d[i], this.document[0]), n = o.length - 1; n >= 0; n--) s = t.data(o[n], this.widgetFullName), s && s !== this && !s.options.disabled && (u.push([t.isFunction(s.options.items) ? s.options.items.call(s.element[0], e, {item: this.currentItem}) : t(s.options.items, s.element), s]), this.containers.push(s));
      for (i = u.length - 1; i >= 0; i--) for (r = u[i][1], a = u[i][0], n = 0, c = a.length; c > n; n++) l = t(a[n]), l.data(this.widgetName + "-item", r), h.push({
        item: l,
        instance: r,
        width: 0,
        height: 0,
        left: 0,
        top: 0
      })
    },
    refreshPositions: function (e) {
      this.floating = !!this.items.length && ("x" === this.options.axis || this._isFloating(this.items[0].item)), this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
      var i, n, o, s;
      for (i = this.items.length - 1; i >= 0; i--) n = this.items[i], n.instance !== this.currentContainer && this.currentContainer && n.item[0] !== this.currentItem[0] || (o = this.options.toleranceElement ? t(this.options.toleranceElement, n.item) : n.item, e || (n.width = o.outerWidth(), n.height = o.outerHeight()), s = o.offset(), n.left = s.left, n.top = s.top);
      if (this.options.custom && this.options.custom.refreshContainers) this.options.custom.refreshContainers.call(this); else for (i = this.containers.length - 1; i >= 0; i--) s = this.containers[i].element.offset(), this.containers[i].containerCache.left = s.left, this.containers[i].containerCache.top = s.top, this.containers[i].containerCache.width = this.containers[i].element.outerWidth(), this.containers[i].containerCache.height = this.containers[i].element.outerHeight();
      return this
    },
    _createPlaceholder: function (e) {
      e = e || this;
      var i, n = e.options;
      n.placeholder && n.placeholder.constructor !== String || (i = n.placeholder, n.placeholder = {
        element: function () {
          var n = e.currentItem[0].nodeName.toLowerCase(), o = t("<" + n + ">", e.document[0]);
          return e._addClass(o, "ui-sortable-placeholder", i || e.currentItem[0].className)._removeClass(o, "ui-sortable-helper"), "tbody" === n ? e._createTrPlaceholder(e.currentItem.find("tr").eq(0), t("<tr>", e.document[0]).appendTo(o)) : "tr" === n ? e._createTrPlaceholder(e.currentItem, o) : "img" === n && o.attr("src", e.currentItem.attr("src")), i || o.css("visibility", "hidden"), o
        }, update: function (t, o) {
          (!i || n.forcePlaceholderSize) && (o.height() || o.height(e.currentItem.innerHeight() - parseInt(e.currentItem.css("paddingTop") || 0, 10) - parseInt(e.currentItem.css("paddingBottom") || 0, 10)), o.width() || o.width(e.currentItem.innerWidth() - parseInt(e.currentItem.css("paddingLeft") || 0, 10) - parseInt(e.currentItem.css("paddingRight") || 0, 10)))
        }
      }), e.placeholder = t(n.placeholder.element.call(e.element, e.currentItem)), e.currentItem.after(e.placeholder), n.placeholder.update(e, e.placeholder)
    },
    _createTrPlaceholder: function (e, i) {
      var n = this;
      e.children().each(function () {
        t("<td>&#160;</td>", n.document[0]).attr("colspan", t(this).attr("colspan") || 1).appendTo(i)
      })
    },
    _contactContainers: function (e) {
      var i, n, o, s, r, a, l, c, h, u, d = null, p = null;
      for (i = this.containers.length - 1; i >= 0; i--) if (!t.contains(this.currentItem[0], this.containers[i].element[0])) if (this._intersectsWith(this.containers[i].containerCache)) {
        if (d && t.contains(this.containers[i].element[0], d.element[0])) continue;
        d = this.containers[i], p = i
      } else this.containers[i].containerCache.over && (this.containers[i]._trigger("out", e, this._uiHash(this)), this.containers[i].containerCache.over = 0);
      if (d) if (1 === this.containers.length) this.containers[p].containerCache.over || (this.containers[p]._trigger("over", e, this._uiHash(this)), this.containers[p].containerCache.over = 1); else {
        for (o = 1e4, s = null, h = d.floating || this._isFloating(this.currentItem), r = h ? "left" : "top", a = h ? "width" : "height", u = h ? "pageX" : "pageY", n = this.items.length - 1; n >= 0; n--) t.contains(this.containers[p].element[0], this.items[n].item[0]) && this.items[n].item[0] !== this.currentItem[0] && (l = this.items[n].item.offset()[r], c = !1, e[u] - l > this.items[n][a] / 2 && (c = !0), o > Math.abs(e[u] - l) && (o = Math.abs(e[u] - l), s = this.items[n], this.direction = c ? "up" : "down"));
        if (!s && !this.options.dropOnEmpty) return;
        if (this.currentContainer === this.containers[p]) return void(this.currentContainer.containerCache.over || (this.containers[p]._trigger("over", e, this._uiHash()), this.currentContainer.containerCache.over = 1));
        s ? this._rearrange(e, s, null, !0) : this._rearrange(e, null, this.containers[p].element, !0), this._trigger("change", e, this._uiHash()), this.containers[p]._trigger("change", e, this._uiHash(this)), this.currentContainer = this.containers[p], this.options.placeholder.update(this.currentContainer, this.placeholder), this.containers[p]._trigger("over", e, this._uiHash(this)), this.containers[p].containerCache.over = 1
      }
    },
    _createHelper: function (e) {
      var i = this.options,
        n = t.isFunction(i.helper) ? t(i.helper.apply(this.element[0], [e, this.currentItem])) : "clone" === i.helper ? this.currentItem.clone() : this.currentItem;
      return n.parents("body").length || t("parent" !== i.appendTo ? i.appendTo : this.currentItem[0].parentNode)[0].appendChild(n[0]), n[0] === this.currentItem[0] && (this._storedCSS = {
        width: this.currentItem[0].style.width,
        height: this.currentItem[0].style.height,
        position: this.currentItem.css("position"),
        top: this.currentItem.css("top"),
        left: this.currentItem.css("left")
      }), (!n[0].style.width || i.forceHelperSize) && n.width(this.currentItem.width()), (!n[0].style.height || i.forceHelperSize) && n.height(this.currentItem.height()), n
    },
    _adjustOffsetFromHelper: function (e) {
      "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
        left: +e[0],
        top: +e[1] || 0
      }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top)
    },
    _getParentOffset: function () {
      this.offsetParent = this.helper.offsetParent();
      var e = this.offsetParent.offset();
      return "absolute" === this.cssPosition && this.scrollParent[0] !== this.document[0] && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(),
        e.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === this.document[0].body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && t.ui.ie) && (e = {
        top: 0,
        left: 0
      }), {
        top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
        left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
      }
    },
    _getRelativeOffset: function () {
      if ("relative" === this.cssPosition) {
        var t = this.currentItem.position();
        return {
          top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
          left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
        }
      }
      return {top: 0, left: 0}
    },
    _cacheMargins: function () {
      this.margins = {
        left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
        top: parseInt(this.currentItem.css("marginTop"), 10) || 0
      }
    },
    _cacheHelperProportions: function () {
      this.helperProportions = {width: this.helper.outerWidth(), height: this.helper.outerHeight()}
    },
    _setContainment: function () {
      var e, i, n, o = this.options;
      "parent" === o.containment && (o.containment = this.helper[0].parentNode), ("document" === o.containment || "window" === o.containment) && (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, "document" === o.containment ? this.document.width() : this.window.width() - this.helperProportions.width - this.margins.left, ("document" === o.containment ? this.document.height() || document.body.parentNode.scrollHeight : this.window.height() || this.document[0].body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]), /^(document|window|parent)$/.test(o.containment) || (e = t(o.containment)[0], i = t(o.containment).offset(), n = "hidden" !== t(e).css("overflow"), this.containment = [i.left + (parseInt(t(e).css("borderLeftWidth"), 10) || 0) + (parseInt(t(e).css("paddingLeft"), 10) || 0) - this.margins.left, i.top + (parseInt(t(e).css("borderTopWidth"), 10) || 0) + (parseInt(t(e).css("paddingTop"), 10) || 0) - this.margins.top, i.left + (n ? Math.max(e.scrollWidth, e.offsetWidth) : e.offsetWidth) - (parseInt(t(e).css("borderLeftWidth"), 10) || 0) - (parseInt(t(e).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, i.top + (n ? Math.max(e.scrollHeight, e.offsetHeight) : e.offsetHeight) - (parseInt(t(e).css("borderTopWidth"), 10) || 0) - (parseInt(t(e).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top])
    },
    _convertPositionTo: function (e, i) {
      i || (i = this.position);
      var n = "absolute" === e ? 1 : -1,
        o = "absolute" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
        s = /(html|body)/i.test(o[0].tagName);
      return {
        top: i.top + this.offset.relative.top * n + this.offset.parent.top * n - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : s ? 0 : o.scrollTop()) * n,
        left: i.left + this.offset.relative.left * n + this.offset.parent.left * n - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : s ? 0 : o.scrollLeft()) * n
      }
    },
    _generatePosition: function (e) {
      var i, n, o = this.options, s = e.pageX, r = e.pageY,
        a = "absolute" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
        l = /(html|body)/i.test(a[0].tagName);
      return "relative" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), this.originalPosition && (this.containment && (e.pageX - this.offset.click.left < this.containment[0] && (s = this.containment[0] + this.offset.click.left), e.pageY - this.offset.click.top < this.containment[1] && (r = this.containment[1] + this.offset.click.top), e.pageX - this.offset.click.left > this.containment[2] && (s = this.containment[2] + this.offset.click.left), e.pageY - this.offset.click.top > this.containment[3] && (r = this.containment[3] + this.offset.click.top)), o.grid && (i = this.originalPageY + Math.round((r - this.originalPageY) / o.grid[1]) * o.grid[1], r = this.containment ? i - this.offset.click.top >= this.containment[1] && i - this.offset.click.top <= this.containment[3] ? i : i - this.offset.click.top >= this.containment[1] ? i - o.grid[1] : i + o.grid[1] : i, n = this.originalPageX + Math.round((s - this.originalPageX) / o.grid[0]) * o.grid[0], s = this.containment ? n - this.offset.click.left >= this.containment[0] && n - this.offset.click.left <= this.containment[2] ? n : n - this.offset.click.left >= this.containment[0] ? n - o.grid[0] : n + o.grid[0] : n)), {
        top: r - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : l ? 0 : a.scrollTop()),
        left: s - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : l ? 0 : a.scrollLeft())
      }
    },
    _rearrange: function (t, e, i, n) {
      i ? i[0].appendChild(this.placeholder[0]) : e.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? e.item[0] : e.item[0].nextSibling), this.counter = this.counter ? ++this.counter : 1;
      var o = this.counter;
      this._delay(function () {
        o === this.counter && this.refreshPositions(!n)
      })
    },
    _clear: function (t, e) {
      function i(t, e, i) {
        return function (n) {
          i._trigger(t, n, e._uiHash(e))
        }
      }

      this.reverting = !1;
      var n, o = [];
      if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
        for (n in this._storedCSS) ("auto" === this._storedCSS[n] || "static" === this._storedCSS[n]) && (this._storedCSS[n] = "");
        this.currentItem.css(this._storedCSS), this._removeClass(this.currentItem, "ui-sortable-helper")
      } else this.currentItem.show();
      for (this.fromOutside && !e && o.push(function (t) {
        this._trigger("receive", t, this._uiHash(this.fromOutside))
      }), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || e || o.push(function (t) {
        this._trigger("update", t, this._uiHash())
      }), this !== this.currentContainer && (e || (o.push(function (t) {
        this._trigger("remove", t, this._uiHash())
      }), o.push(function (t) {
        return function (e) {
          t._trigger("receive", e, this._uiHash(this))
        }
      }.call(this, this.currentContainer)), o.push(function (t) {
        return function (e) {
          t._trigger("update", e, this._uiHash(this))
        }
      }.call(this, this.currentContainer)))), n = this.containers.length - 1; n >= 0; n--) e || o.push(i("deactivate", this, this.containers[n])), this.containers[n].containerCache.over && (o.push(i("out", this, this.containers[n])), this.containers[n].containerCache.over = 0);
      if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), this.dragging = !1, e || this._trigger("beforeStop", t, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), this.cancelHelperRemoval || (this.helper[0] !== this.currentItem[0] && this.helper.remove(), this.helper = null), !e) {
        for (n = 0; o.length > n; n++) o[n].call(this, t);
        this._trigger("stop", t, this._uiHash())
      }
      return this.fromOutside = !1, !this.cancelHelperRemoval
    },
    _trigger: function () {
      t.Widget.prototype._trigger.apply(this, arguments) === !1 && this.cancel()
    },
    _uiHash: function (e) {
      var i = e || this;
      return {
        helper: i.helper,
        placeholder: i.placeholder || t([]),
        position: i.position,
        originalPosition: i.originalPosition,
        offset: i.positionAbs,
        item: i.currentItem,
        sender: e ? e.element : null
      }
    }
  })
});
var lastScrollTop = 0, count = 0;
$(document).ready(function (t) {
  $(".menu a, .start-project a").click(function (t) {
    if ("" != t.currentTarget.hash) {
      var e = window.location.origin + window.location.pathname + t.currentTarget.hash;
      return history.pushState && history.pushState(null, null, e), !1
    }
  });
  var e = "state" in window.history && null !== window.history.state, i = location.href;
  if ($(window).bind("popstate", function (t) {
    var n = !e && location.href == i;
    if (e = !0, !n) return $("body").removeClass("active-contact"), !1
  }), window.onpopstate = function (t) {
    if ("#contact-us" == document.location.hash || "#plan" == document.location.hash) switch (document.location.hash) {
      case"#contact-us":
        $("body").toggleClass("active-contact");
        break;
      case"#plan":
        $("body").toggleClass("active-start")
    } else $("body").removeClass("active-contact"), $("body").removeClass("active-start");
    return !1
  }, window.location.hash) {
    window.location.hash;
    switch (document.location.hash) {
      case"#contact-us":
        $("body").toggleClass("active-contact");
        break;
      case"#plan":
        $("body").toggleClass("active-start")
    }
    return !1
  }
}), $(function () {
  $(window).scroll(function () {
    var t = document.getElementById("newmood-video"), e = $(".featured-titles");
    e.is(":in-viewport") && t.play()
  })
}), $("#submit-hire-form").on("click", function () {
  $(this).addClass("succ").text($(this).attr("data-done"));
  $('#submit-hire-form').css('cursor', 'not-allowed');
  $('#submit-hire-form').attr("disabled", true);
  var t = $(".text-filled").html(), e = $("#start").serialize();
  $.ajax({
    url: "/mail.php", type: "post", dataType: "json", data: {letter: t, data: e}, success: function (t) {
    }
  })
}), $("#submit-position-form").on("click", function (t) {
  if ($("#position-form").validate({
    rules: {
      name_surname: {required: !0},
      email: {required: !0, email: !0},
      mobile: {required: !0, digits: !0}
    }, errorPlacement: function () {
      return !1
    }, error: function () {
      return $(this).addClass("error"), !1
    }
  }), !$("#position-form").valid()) return !1;
  if ($(this).addClass("succ").text($(this).attr("data-done")), t.preventDefault(), myDropzone.getQueuedFiles().length > 0) myDropzone.processQueue(); else {
    var e = $.map($(".drop-holer li"), function (t) {
      return $(t).text()
    }), i = $("#position-form").serialize();
    0 == formError && $.ajax({
      url: "/mail.php",
      type: "post",
      dataType: "json",
      data: {form: i, skills: e},
      success: function (t) {
      }
    })
  }
}), $(document).ready(function () {
  if ("ontouchstart" in window || navigator.maxTouchPoints) $("html").addClass("touch"); else {
    $("html").addClass("no-touch"), $(".text-overlay .text, .team-box section, .technologies-box ul li, .job-list article, .clients-list .logo-c, .clients-list .image > img, .cols .col, .text-box form label, .instagram-list article, .clients-logo ul, .text-box h2, .text-box .entry, .body .services-list article, .founders blockquote, .founders .ceo, .founders .entry, .founders .btn, .text-row .entry-cols, .images-cols img, .typography-text .col, .typography-colors .col, .images-full, .partners-full ul li, .video-full, .clients-full h2, .clients-full .newmood-brands-title , .clients-full ul li, .center-cols .col, .try-case .entry, .quote-box blockquote, .service-box .ico, .service-box h2, .service-box .desc, .service-box .related, .quote-box-image blockquote, .two-cols .col h3, .text-box .job-apply section article, .job-form fieldset, .position button").each(function () {
      var t = $(this);
      t.isOnScreen() && (t.hasClass("visible") || t.addClass("visible")), $(window).scroll(function () {
        t.isOnScreen() && (t.hasClass("visible") || t.addClass("visible"))
      })
    });
    var t = $(".services-list svg"), e = t.drawsvg();
    $(".services-list svg").each(function () {
      var t = $(this);
      t.isOnScreen() && (t.hasClass("visible") || (t.addClass("visible"), window.setTimeout(function () {
        e.drawsvg("animate"), window.setTimeout(function () {
          t.addClass("visible0")
        }, 700)
      }, 3500))), $(window).scroll(function () {
        t.isOnScreen() && (t.hasClass("visible") || (e.drawsvg("animate"), t.addClass("visible")))
      })
    })
  }
  if ($("#bar").length) {
    var i = document.getElementById("bar");
    noUiSlider.create(i, {
      start: [57, 75],
      connect: !0,
      format: wNumb({decimals: 0}),
      range: {min: 0, max: 150}
    }), i.noUiSlider.on("update", function (t, e) {
      $(".tab-content .value").eq(0).text(t[0]), $(".tab-content .value").eq(1).text(t[1]), $(".data-budget1").text(t[0]), $(".data-budget2").text(t[1])
    })
  }
  $(".drop-holer").length && ($("body").addClass("drag-available"), $(".drop-holer").sortable({connectWith: "#love-items, #skills, #live"}), $("#love-items, #skills, #live").sortable({
    connectWith: ".drop-holer",
    start: function (t, e) {
      $(".drop-area").addClass("drag")
    },
    stop: function (t, e) {
      $(".drop-area").removeClass("drag"), $(".ui-sortable").each(function () {
        $(this).children().length ? $(this).removeClass("empty") : $(this).addClass("empty")
      }), $(".text-box .job-apply .drop-holer li").length ? $(".text-box .job-apply").addClass("added") : $(".text-box .job-apply").removeClass("added")
    }
  })), $(".file input").change(function () {
    $(this).get(0).files.length ? ($(".to-hide").hide(), $(".file strong").text($(this).get(0).files.length)) : ($(".to-hide").show(), $(".file strong").text("0"))
  }), $(".phone select").crfs(), $(".tab-content .prev").click(function () {
    var t = $(".tab-content.active");
    return $(".tab-content.active").removeClass("active"), t.prev().addClass("active"), !1
  }), $(".tab-content .next").click(function () {
    var t = $(".tab-content.active"), e = "", i = !0;
    return $(".data-name").text($("#start input[name=name]").val()), $(".data-email").text($("#start input[name=email]").val()), $(".data-email").parent().attr("href", "mailto:" + $("#start input[name=email]").val()), $(".data-phone").text($("#start input[name=phone]").val()), $(".data-company").text($("#start input[name=company]").val()), $(".data-url").text($("#start input[name=web]").val()), $(".data-desc").text($("#start textarea").val()), $("#start .temp").remove(), $("#start input[type=checkbox]:checked").each(function (t) {
      t > 0 && $(".data-services").after("<span class='temp'>, </span>"), $(".data-services").after("<a href='' class='temp'>" + $(this).next().text() + "</a>")
    }), $(".data-time").text($("#start .date-radios input:checked").next().text()), t.find("input, textarea").each(function () {
      "required" == $(this).attr("required") && ($(this).val().length < 1 ? (i = !1, $(this).addClass("error"), e || (e = $(this))) : ($(this).removeClass("error"), "email" == $(this).attr("type") && (validateEmail($(this).val()) || ($(this).addClass("error"), e || (e = $(this)))))), "checkbox" == $(this).attr("type") && (t.find("input[type=checkbox]:checked").length ? ($(".start-modal h4").removeClass("error"), i = !0) : ($(".start-modal h4").addClass("error"), $(".start-modal").stop().animate({scrollTop: $(".start-modal h4").offset().top - 40}, 300), i = !1)), "radio" == $(this).attr("type") && (t.find("input[type=radio]:checked").length ? (t.find("label").removeClass("error"), i = !0) : (t.find("label").addClass("error"), i = !1))
    }), i ? ($(".tab-content.active").removeClass("active"), t.next().addClass("active"), $(".start-modal").stop().animate({scrollTop: 0}, 300)) : e && e.focus(), !1
  }), $(".ico-down").click(function () {
    return $("html, body").stop().animate({scrollTop: $(".featured-title").height()}, 1e3), !1
  }), $("[href='#contact']").click(function () {
    return $("body").toggleClass("active-contact"), !1
  }), $(".contact-modal .close").click(function () {
    return history.pushState(null, null, window.location.origin + window.location.pathname), $("body").removeClass("active-contact"), !1
  }), $("[href='#plan']").click(function () {
    return $("body").toggleClass("active-start"), !1
  }), $(".start-modal .close").click(function () {
    return history.pushState(null, null, window.location.origin + window.location.pathname), $("body").removeClass("active-start"), !1
  }), $(".menu-trigger").click(function () {
    if ($(".logo").hasClass("reverse")) {
      $(".logo").removeClass("reverse");
      $(".menu-trigger span").css({'background-color': '#F3333E'});

    } else {
      $(".logo").addClass("reverse");
      $(".menu-trigger span").css({'background-color': '#F3333E'});
    }
    $("body").toggleClass("active-menu");
    $(".menu-trigger").toggleClass("fixed-menu");
    $(".menu").toggleClass("fixed-menu");
    $(".header").toggleClass("fixed-menu");
    $(".menu-trigger").toggleClass("exit");


  }), $(".clients-list .back").click(function () {
    return $("body").removeClass("active-modal"), $(this).parentsUntil("article").parent("article").removeClass("active"), !1
  }), $(".menu .langs .trigger").click(function () {
    return $(this).parent().toggleClass("hover"), !1
  }), $(".clients-list .more, .load-more").click(function () {
    window.setTimeout(function () {
      $("body").addClass("active-modal")
    }, 300);
    var t = $(".ico-animation svg"), e = t.drawsvg();
    return $(".client-detail").each(function () {
      var t = $(this);
      t.isOnScreen() && (t.hasClass("visible") || (t.addClass("visible"), e.drawsvg("animate"))), t.scroll(function () {
        t.isOnScreen(t) && (t.hasClass("visible") || (e.drawsvg("animate"), t.addClass("visible")))
      })
    }), $(this).parentsUntil("article").parent("article").addClass("active"), !1
  }), $(".form-control").on("change keyup", function () {
    $(this).val().length ? $(this).addClass("selected") : $(this).removeClass("selected")
  }), $(document).keyup(function (t) {
    27 === t.keyCode && $(".back").click()
  });
  var n = navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? "touchstart" : "click";
  $(document).on(n, function (t) {
    0 === $(t.target).closest("menu .langs").length && $(".menu .langs").removeClass("hover")
  }), mobileCheck(), $(window).resize(function () {
    mobileCheck()
  }), $(window).scroll(function () {
    mobileCheck()
  });
  var o = [{
    movement: {
      imgWrapper: {
        translation: {x: 5, y: 5, z: 0},
        reverseAnimation: {duration: 800, easing: "easeOutQuart"}
      }
    }
  }];
  [].slice.call(document.querySelectorAll(".instagram-list article > a")).forEach(function (t, e) {
    new TiltFx(t, o[0])
  }), $(".media").fancybox({
    margin: 50,
    width: "90%",
    height: "90%",
    padding: 0,
    helpers: {
      media: {}
    },
    afterClose: function () {
      $('html, body').css({
        overflow: 'auto'
      });
    },
    afterShow: function () {
      $('html, body').css({
        overflow: 'hidden'
      });
    },
  })
}), $(window).on("load", function () {
  $.ready.then(function () {
    $(".home").length ? window.setTimeout(function () {
      $("body").addClass("loaded"), $("video source").each(function () {
        var t = $(this).attr("data-src");
        $(this).attr("src", t);
        var e = this.parentElement;
        e.load(), e.play()
      })
    }, 500) : $("body").addClass("loaded"), $(".instagram-list section").masonry({columnWidth: 1})
  })

});


$(window).scroll(function () {
  if ($(".menu-trigger span").css('background-color') === "rgb(255, 255, 255)") {
    $(".logo").removeClass("reverse");
  } else {
    $(".logo").addClass("reverse");
  }
});

$(window).scroll(function () {
  if ($('.menu-trigger').hasClass("fixed-menu")) {
    $(".header").addClass("mt0");
  } else {
    $(".header").removeClass("mt0");
  }
});


$(".menu-trigger").click(function () {
  if ($(".menu-trigger span").css('background-color') === "rgb(243, 51, 62)") {
    $(".logo").addClass("reverse");
  } else {
    $(".logo").removeClass("reverse");
  }
});

$(function () {
  $('#cookie_stop').click(function () {
    $('#cookie_disclaimer').slideUp();

    var nDays = 999;
    var cookieName = "disclaimer";
    var cookieValue = "true";

    var today = new Date();
    var expire = new Date();
    expire.setTime(today.getTime() + 3600000 * 24 * nDays);
    document.cookie = cookieName + "=" + escape(cookieValue) + ";expires=" + expire.toGMTString() + ";path=/";
  });
});


$(window).scroll(function () {
  /* Triggering logo visibility */
  if ($(window).width() > 1270) {
    $(".start-project ").each(function (i) {
      /* Declaring Div variables */
      var logoDiv = $(".logo");
      var startProjectDiv = $(".start-project");
      var footerBlock = $(".footer");

      /* Retrieve elements offset coordinates */
      var logoDiv_top = logoDiv.offset().top;
      var startProjectDiv_top = startProjectDiv.offset().top;
      var footerBlock_top = footerBlock.offset().top;

      /* Sum up elements offset coordinates and height */
      var logoDiv_bottom = logoDiv_top + logoDiv.height();
      var startProjectDiv_bottom = startProjectDiv_top + startProjectDiv.height();
      var footerBlock_bottom = footerBlock_top + footerBlock.height();

      if (logoDiv_bottom >= startProjectDiv_top && logoDiv_top < startProjectDiv_bottom) {
        $(".logo").css({
          'opacity': '0'
        })
      } else {
        $(".logo").css({
          'opacity': '1'
        })
      }
      // if (logoDiv_bottom >= footerBlock_top && logoDiv_top < footerBlock_bottom) {
      //   $(".logo").css({
      //     'opacity': '0'
      //   })
      // }
    });

    if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
      $(".logo").css({
        'opacity': '0'
      });
    }
  }
  /* Triggering fixed footer */
  if ($(window).width() > 640) {
    var div1 = $('.footer');
    var div2 = $('.hasFade');
    var div1_top = div1.offset().top;
    var div2_top = div2.offset().top;
    var div1_bottom = div1_top + div1.height();
    var div2_bottom = div2_top + div2.height();

    if (div1_bottom >= div2_top && div1_top < div2_bottom) {
      $('.page-content').css({'background': 'none'});
      $('.footer').css({'opacity': '0'});
    } else {
      $('.page-content').css({'background': '#ffffff'});
      $('.footer').css({'opacity': '1'});
      }
    }
});

