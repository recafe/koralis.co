$(document).ready(function () {
  $('.page-content').css({'background': '#ffffff'});

  $('#codeBtn, #designBtn, #supportBtn').css({'display': 'none'});

  $('.index-clients').css({'visibility': 'hidden'});


// Triggering Body element fade animation
  $(window).scroll(function () {
    fadeBgCheck();

    /* Triggering elements animation */
    $(".text-box-parent > *").each(function (i) {
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $(".textbox-leftSide").css({'opacity': '1', 'transform': 'translateX(' + 0 + 'px)'});
        $(".textbox-rightSide").css({'opacity': '1', 'transform': 'translateY(' + 0 + 'px)'});
      }
    });
  });

  function fadeBgCheck() {
    $(".logo-list:nth-child(n+1)").each(function (i) {
      var bottom_of_object;
      bottom_of_object = $(this).offset().top + $(this).outerHeight() + 100;
      if ($(window).width() === 768) {
        bottom_of_object = $(this).offset().top + $(this).outerHeight();
      }
      if ($(window).width() <= 767) {
        bottom_of_object = $(this).offset().top + $(this).outerHeight() - 500;
      }
      if ($(window).width() <= 480) {
        bottom_of_object = $(this).offset().top + $(this).outerHeight() - 350;
      }
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $('.page-content').css({
          'background': '#161a1b',
          '-webkit-transition': 'background 500ms linear',
          '-moz-transition': 'background 500ms linear',
          '-ms-transition': 'background 500ms linear',
          '-o-transition': 'background 500ms linear',
          'transition': 'background 500ms linear',
        });

        $('.clients-logo').removeClass("st0");

        $('.text-box.offer, .text-box-parent').css({'opacity': '0'});

        $('.work.center.index-clients .entry').css({'opacity': '1'});

        $('.index-clients').css({'visibility': 'visible'});
      } else {
        $('.page-content').css({
          'background': '#ffffff',
          '-webkit-transition': 'background 500ms linear',
          '-moz-transition': 'background 500ms linear',
          '-ms-transition': 'background 500ms linear',
          '-o-transition': 'background 500ms linear',
          'transition': 'background 500ms linear',
        });

        $('.text-box.offer, .text-box-parent').css({ 'opacity': '1' });

        $('.index-clients').css({ 'visibility': 'hidden' });

      }
    });

    $(".textbox-rightSide").each(function (i) {
      var bottom_of_object;
      bottom_of_object = $(this).offset().top + $(this).outerHeight() + 100;
      if ($(window).width() === 768) {
        bottom_of_object = $(this).offset().top + $(this).outerHeight() - 150;
      }
      if ($(window).width() <= 767) {
        bottom_of_object = $(this).offset().top + $(this).outerHeight() + 300;
      }
      if ($(window).width() <= 480) {
        bottom_of_object = $(this).offset().top + $(this).outerHeight() - 50;
      }
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if (bottom_of_window > bottom_of_object) {
        $('.page-content').css({
          'background': '#ffffff',
          '-webkit-transition': 'background 500ms linear',
          '-moz-transition': 'background 500ms linear',
          '-ms-transition': 'background 500ms linear',
          '-o-transition': 'background 500ms linear',
          'transition': 'background 500ms linear'
        });

        $('.work.center.index-clients .entry').css({'opacity': '0'});

        $('.index-clients').css({ 'visibility': 'hidden' });

        $('.text-box.offer, .text-box-parent').css({'opacity': '1'
        });
      }
    });
  }

});



