function getPathInfo() {
    var path = window.location.pathname;
    switch(path) {
        case '/en/case-studies': return ['#FFF', '#F3333E', true, '.featured-title']; // [Fade to, Fade from, Fade or not, Fade point class]
        case '/lt/case-studies': return ['#FFF', '#F3333E', true, '.featured-title']; // [Fade to, Fade from, Fade or not, Fade point class]
        case '/en/we-are-hiring': return ['#FFF', '#F3333E', true, '.featured-title'];
        case '/lt/we-are-hiring': return ['#FFF', '#F3333E', true, '.featured-title'];
        case '/en/what-we-do': return ['#FFF', '#F3333E', true, '.featured-title'];
        case '/lt/what-we-do': return ['#FFF', '#F3333E', true, '.featured-title'];
        //case '/case-studies/newmood': return ['#FFF', '#000', false, '.case-title'];
        // case '/case-studies/packpin': return ['#FFF', '#e51c23', true, '.case-title'];
        // case '/case-studies/deskbookers': return ['#FFF', '#393A73', true, '.case-title'];
        default: return ['', '', false, ''];
    }
}

var Info = getPathInfo();

function fade(color) {
    $('.fadingBG').css({
        'background': color,
        '-webkit-transition': 'background 1s',
        '-moz-transition': 'background 1s',
        '-o-transition': 'background 1s',
        'transition': 'background 1s'
    });
}

function fadeOut() {
    fade(Info[0]);
    $('.fadeGap').hide();
    $('#fadingBlock').show();
    $('.logo').addClass('reverse');
    $('.menu-trigger span').css({'background': '#F3333E'});
    $('.featured-title').css({
         'height': window.innerHeight * 0.85 + 'px'
    });
    // $('body').css({
    //     'background': '#FFF',
    // });
    $('.fadingBG').fadeOut(500);
    $('.header').addClass('nav-background');
}

function fadeIn() {
    fade(Info[1]);
    $('.featured-title').css({
        'height': '90vh'
    });
    $('#fadingBlock').fadeOut(1, function () {
        $('.fadeGap').show();
        $('.fadingBG').fadeIn(500);
        $('.fadingBG').css({
            'z-index': '9',
            'display': 'block'
        });
        // $('body').css({
        //     'background': '#F3333E',
        //     '-webkit-transition': 'background 500ms',
        //     '-moz-transition': 'background 500ms',
        //     '-ms-transition': 'background 500ms',
        //     '-o-transition': 'background 500ms',
        //     'transition': 'background 500ms'
        // });
    });
    $('.logo').removeClass('reverse');
    $('.menu-trigger span').css({'background': '#FFF'});
    $('.header').removeClass('nav-background');

}

$(document).ready(function() {
    if(performance.navigation.type == 1 || performance.navigation.type == 2) {
        if($('.body').is(':in-viewport') && !$('.featured-title .container h1').is(':in-viewport')) {
            $('.fadingBG').css({'background': '#FFF'});
            $('.fadeGap').hide();
            $('#fadingBlock').show();
            $('.logo').addClass('reverse');
            $('.menu-trigger span').css({'background': '#F3333E'});
            $('.header').addClass('nav-background');
        } else {
            $('.fadingBG').css({'background': '#F3333E'});
            $('.logo').removeClass('reverse');
            $('.menu-trigger span').css({'background': '#FFF'});
            $('.header').removeClass('nav-background');

        }
    }else {
        $('.fadingBG').css({'background': '#F3333E'});
    }
});

function fadeCheck() {
    var pos = window.scrollY;
    var fadePoint = $(Info[3]).height() * 0.5;
    if ($('.body').is(':in-viewport') && !$('.featured-title .container h1').is(':in-viewport') && !$('#fadingBlock').is(':in-viewport')) {
        if (pos > fadePoint + 100) {
            fadeOut();
        }
    } else if ($('.featured-title').is(':in-viewport')) {
        if (pos < fadePoint) {
            fadeIn();
        }
    }
}

if (Info[2]) {
    $(window).scroll(function () {
        $('.logo-c').removeClass('visible');
        fadeCheck();
        clearTimeout($.data(this, "scrollCheck"));
        $.data(this, "scrollCheck", setTimeout(function () {
            fadeCheck();
        }, 250));
    });
}

