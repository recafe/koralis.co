$('#submit-test-form').on('click', function(event) {
  $(this).prop('disabled',true);

  $('#test-form').validate({ // initialize the plugin
    rules: {
      textas: {
        required:{
          depends: function(element){
            if($('.dz-started').length === 0 || $('#textas').val() !== ''){
                return true;
            }else {
              return false;
            }
          }
        }
      },

    },
    errorPlacement: function(){
      return false;  // suppresses error message text
    },
    error: function() {
      $(this).addClass("error");
      return false;
    }
  });

  if ($('#test-form').valid()) {
    $(this).addClass("succ").text($(this).attr("data-done"));
    $('#submit-test-form').css('cursor', 'not-allowed');
  } else {
    $('#submit-test-form').prop('disabled',false);
    return false;
  }

  event.preventDefault();


    if (myDropzone.getQueuedFiles().length > 0) {
      myDropzone.processQueue();
    } else {

      var data = $('#test-form').serialize();

        $.ajax({
          url: '/send-appliance-form',
          type: "post",
          dataType:"json",
          data: {form:data},

          success: function (response) {

          }
        });


    }



});

