@extends('layout')

@section('css')
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
@endsection

@section('content')
    <body>
    <header class="header nav-top">
        <div class="navigation">
            <div class="menu-trigger"><span></span><span></span><span></span></div>
            <div class="logo-block">
                <a href="/{{app()->getLocale()}}" class="logo" title="Koralis">@lang('base.company_name')</a>
            </div>
        </div>
    </header>

    <div class="page-content">
        <div class="fadingBG"></div>

        <div id="fade-featured-title" class="featured-title hasFade">
            <div class="container">
                <h1>@lang('case-studies.case_studies')</h1>
                <h2>@lang('case-studies.we_are_proud')<br>@lang('case-studies.to_work')
                    <br>@lang('case-studies.with_these')</h2>
            </div>
        </div>

        @include('partials.follow-widget')

        <div class="body">
            <div class="fadeGap"></div>
            <div class="clients-list" id="fadingBlock">
                <div class="container">
                    <section>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(5, 24, 30, .9);">
                                    <img src="/images/Newmood.svg" alt="">
                                </div>
                                <a href="/{{app()->getLocale()}}/case-studies/newmood"><span
                                        class="hover">@lang('case-studies.view_case')</span><img
                                        src="/images/case_studies/Newmood_x1.jpg"
                                        srcset="/images/case_studies/Newmood_x2.jpg 2x" alt=""></a>
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.newmood.title')</h2>
                                    <p>@lang('case-studies.newmood.text')</p>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(208, 2, 27, .9);">
                                    <img src="/images/packpin.svg" alt="">
                                </div>
                                <a href="/{{app()->getLocale()}}/case-studies/packpin"><span
                                        class="hover">@lang('case-studies.view_case')</span><img
                                        src="/images/case_studies/Packpin_x1.jpg"
                                        srcset="/images/case_studies/Packpin_x2.jpg 2x" alt=""></a>
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.packpin.title')</h2>
                                    <p>@lang('case-studies.packpin.text')</p>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(57, 58, 115, .9);">
                                    <img src="/images/deskbookeriai.svg" alt="">
                                </div>
                                <a href="/{{app()->getLocale()}}/case-studies/deskbookers"><span
                                        class="hover">@lang('case-studies.view_case')</span><img
                                        src="/images/case_studies/Deskbookers_x1.jpg"
                                        srcset="/images/case_studies/Deskbookers_x2.jpg 2x" alt=""></a>
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.deskbookers.title')</h2>
                                    <p>@lang('case-studies.deskbookers.text')</p>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(203, 0, 107, .9);">
                                    <img src="/images/Moofe_123.svg" alt="">
                                </div>
                                <a href="/{{app()->getLocale()}}/case-studies/moofe"><span
                                        class="hover">@lang('case-studies.view_case')</span><img
                                        src="/images/case_studies/Moofe_x1.jpg"
                                        srcset="/images/case_studies/Moofe_x2.jpg 2x" alt=""></a>
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.moofe.title')</h2>
                                    <p>@lang('case-studies.moofe.text')</p>
                                </div>
                            </div>
                        </article>
                    </section>
                </div>
            </div>

            <div class="quote-center">
                <div class="container">
                    <blockquote>
                        <p>@lang('case-studies.moofe.cite')</p>
                        <cite>
                            <strong>@lang('case-studies.moofe.author')</strong>
                            <span class="par">@lang('case-studies.moofe.author_position')</span>
                            <img src="/images/Moofe_123-1.svg" style="width: 200px; margin-top: -35px;" alt="">
                        </cite>
                    </blockquote>
                </div>
            </div>
            <div class="clients-list">
                <div class="container">
                    <section>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(0, 182, 230, .9);">
                                    <img src="/images/JWT.svg" alt="">
                                </div>
                                <a href="/{{app()->getLocale()}}/case-studies/jwt"><span
                                        class="hover">@lang('case-studies.view_case')</span><img
                                        src="/images/case_studies/JWT_x1.jpg"
                                        srcset="/images/case_studies/JWT_x2.jpg 2x" alt=""></a>
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.jwt.title')</h2>
                                    <p>@lang('case-studies.jwt.text')</p>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(0, 43, 69, .9);">
                                    <img src="/images/DFDS.svg" alt="">
                                </div>
                                <a href="/{{app()->getLocale()}}/case-studies/dfds"><span
                                        class="hover">@lang('case-studies.view_case')</span><img
                                        src="/images/case_studies/DFDS_x1.jpg"
                                        srcset="/images/case_studies/DFDS_x2.jpg 2x" alt=""></a>
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.dfds.title')</h2>
                                    <p>@lang('case-studies.dfds.text')</p>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(24,27,43, .9);">
                                    <img src="/images/jazz/jazz%20logo.svg" alt="">
                                </div>
                                <a href="/{{app()->getLocale()}}/case-studies/jazz"><span
                                        class="hover">@lang('case-studies.view_case')</span><img
                                        src="/images/jazz/jazz_case.jpg" alt=""></a>
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.jazz.title')</h2>
                                    <p>@lang('case-studies.jazz.text')</p>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(5, 67, 128, .9);">
                                    <img src="/images/Disney.svg" alt="">
                                </div>
                                <img src="/images/case_studies/Disney_x1.jpg"
                                     srcset="/images/case_studies/Disney_x2.jpg 2x" alt="">
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.disney.title')</h2>
                                    <p>@lang('case-studies.disney.text')</p>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(145, 170, 157, .9);">
                                    <img src="/images/ASG.svg" alt="">
                                </div>
                                <img src="/images/case_studies/ASG_x1.jpg" srcset="/images/case_studies/ASG_x2.jpg 2x"
                                     alt="">
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.asg.title')</h2>
                                    <p>@lang('case-studies.asg.text')</p>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(237, 27, 46, .9);">
                                    <img src="/images/Coda_white.svg" alt="">
                                </div>
                                <img src="/images/case_studies/Coda_x1.jpg" srcset="/images/case_studies/Coda_x2.jpg 2x"
                                     alt="">
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.coda.title')</h2>
                                    <p>@lang('case-studies.coda.text')</p>
                                </div>
                            </div>
                        </article>

                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(0, 0, 0, .9);">
                                    <img src="/images/Nike.svg" alt="">
                                </div>
                                <img src="/images/case_studies/Nike_x1.jpg" srcset="/images/case_studies/Nike_x2.jpg 2x"
                                     alt="">
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.nike.title')</h2>
                                    <p>@lang('case-studies.nike.text')</p>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="image">
                                <div class="logo-c" style="background-color: rgba(228, 0, 45, .9);">
                                    <img src="/images/Pokernews.svg" alt="">
                                </div>
                                <img src="/images/case_studies/Pokernews_x1.jpg"
                                     srcset="/images/case_studies/Pokernews_x2.jpg 2x" alt="">
                            </div>
                            <div class="text">
                                <div class="entry">
                                    <h2>@lang('case-studies.pokernews.title')</h2>
                                    <p>@lang('case-studies.pokernews.text')</p>
                                </div>
                            </div>
                        </article>
                    </section>
                </div>
            </div>
        </div>

        <!-- / body -->
        <div class="start-project color-invert">
            <a href="#plan">
                <div class="text">
                    <div class="label">@lang('base.need_experts')</div>
                    <div class="start">@lang('base.hire_us')</div>
                </div>
            </a>
        </div>
    </div>
    </body>
@endsection

@section('js')
    <script src="/js/main.js"></script>
    <script src="/js/fadingbg.js"></script>
@endsection

