<?php
$availableJobPositions = [
    'SeniorDeveloper' => [
        'position' => 'Senior PHP Developer',
        'experience' => '4+ year experience',
        'date' => '2019.02.05-2019.04.31',
        'expireDate' => '2019.04.31',
        'link' => 'career/senior-php-developer',
    ],
    'SeniorFrontend' => [
        'position' => 'Senior Frontend Developer',
        'experience' => '4+ year experience',
        'date' => '2019.05.15-2019.07.15',
        'expireDate' => '2019.07.15',
        'link' => 'career/senior-frontend-developer',
    ]
];

// $availableIntershipPositions = [
//     'SalesAssistantIntership' => [
//         'position' => 'Sales assistant',
//         'experience' => 'Interested in IT & Research',
//         'date' => '2018.12.03-2019.04.02',
//         'expireDate' => '2019.02.02',
//         'link' => 'career/intership-sales-assistant',

//     ],
// ];

/* Set Variable for (menu / footer) to display available positions number */
$allPositionsCount = count($availableJobPositions);
?>

    <!--Job Positions-->

<?php /*Check if array is not empty --> */ if (!empty($availableJobPositions)) {?>
    <section>
        <h3>Job Positions: <strong><?= count($availableJobPositions)?></strong> open positions</h3>
        <?php foreach ($availableJobPositions as $singleJobPostion) { ?>
            <article>
                <a href="/{{app()->getLocale()}}/<?= $singleJobPostion['link']?>">
                    <!-- <div class="status">
                        raw
                    </div> -->
                    <div class="tt">
                        <h4><?= $singleJobPostion['position']?></h4>
                        <div class="exp">
                            <div> <?= $singleJobPostion['experience'] ?> </div>
                        </div>
                    </div>
                    <!-- <div class="date">
                        <div> <?= $singleJobPostion['date'] ?> </div>
                    </div> -->
                    <div class="more">
                        More
                    </div>
                </a>
            </article>
        <?php } ?>
    </section>
<?php };?>

    <!--Intership Positions-->

<?php /*Check if array is not empty --> */ if (isset($availableIntershipPositions) && !empty($availableIntershipPositions)) {?>
    <section>
        <h3>Interships: <strong><?=count($availableIntershipPositions)?></strong> open positions</h3>
        <?php foreach ($availableIntershipPositions as $singleInternPostion) { ?>
            <article>
                <a href="<?= $singleInternPostion['link']?>">
                    <!-- <div class="status">
                        raw
                    </div> -->
                    <div class="tt">
                        <h4><?= $singleInternPostion['position']?></h4>
                        <div class="exp">
                            <div> <?= $singleInternPostion['experience'] ?> </div>
                        </div>
                    </div>
                    <!-- <div class="date">
                        <div> <?= $singleInternPostion['date'] ?> </div>
                    </div> -->
                    <div class="more">
                        More
                    </div>
                </a>
            </article>
        <?php } ?>
    </section>
<?php };?>
