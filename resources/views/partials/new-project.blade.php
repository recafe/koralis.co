<div class="start-modal">
    <span class="close"></span>
    <div class="logo-container">
        <a href="/{{app()->getLocale()}}" class="logo white" title="Koralis">Koralis</a>
    </div>
    <form action="#" id="start">
        @csrf
        <div class="tab-content active">
            <div class="container">
                <h4>@lang('new-project.personal_information')</h4>
                <fieldset>
                    <label>@lang('new-project.my_name_is')</label>
                    <input type="text" pattern="[A-Za-z]{2,32}" name="name" required placeholder="@lang('new-project.your_name')" title="Letters Only" >
                    <label>@lang('new-project.you_can_reach_me_at')</label>
                    <input type="email" name="email" required placeholder="@lang('new-project.email')">
                    <br>
                    <label>@lang('new-project.or_call')</label>
                    <input type="number" name="phone" required placeholder="@lang('new-project.phone_no')">
                    <label>@lang('new-project.get_started')</label>
                </fieldset>
                <div class="steps">
                    <div class="count">
                        <span>01</span><span class="div">/</span>06
                    </div>
                    <div class="act">
                    </div>
                    <div class="act">
                        <a href="#" class="next">@lang('new-project.represent_business_next')</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="container">
                <h4>@lang('new-project.represent_business')</h4>
                <fieldset class="no-spaces">
                    <label>@lang('new-project.most_time_i_spend_at')</label>
                    <input type="text" name="company" pattern="[A-Za-z]{2,32}" required placeholder="@lang('new-project.company_name')">
                    <label>@lang('new-project.great_domain')</label>
                    <input type="text" name="web" required class="big" placeholder="@lang('new-project.url')">
                    <label>@lang('new-project.what_i_do')</label><br>
                    <label>@lang('new-project.what_i_do_answer')</label>
                    <textarea pattern="[A-Za-z]{2,32}" required placeholder="@lang('new-project.write_here')"></textarea>
                </fieldset>
                <div class="steps">
                    <div class="count">
                        <span>02</span><span class="div">/</span>06
                    </div>
                    <div class="act">
                        <a href="#" class="prev">@lang('new-project.personal_info')</a>
                    </div>
                    <div class="act">
                        <a href="#" class="next">@lang('new-project.we_are_interested_next')</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="container">
                <h4>@lang('new-project.we_are_interested')</h4>
                <fieldset class="no-spaces">
                    <div class="services-select">
                        <div class="ico">
                            <img src="/images/s1.svg" alt="">
                        </div>
                        <h5>@lang('new-project.tech_and_code')</h5>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.web_applications')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.api_development')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.e_commerce')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.business_intelligence')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.crm_solution')</span></label>
                    </div>
                    <div class="services-select">
                        <div class="ico">
                            <img src="/images/s2.svg" alt="">
                        </div>
                        <h5>@lang('new-project.design_ux')</h5>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.web_design')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.ui_ux_architecture')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.mobile_design')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.wireframing')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.analysis')</span></label>
                    </div>
                    <div class="services-select">
                        <div class="ico">
                            <img src="/images/s3.svg" alt="">
                        </div>
                        <h5>@lang('new-project.support')</h5>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.tech_support')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.project_managment')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.quality_assurance')</span></label>
                        <label><input name="services[]" required type="checkbox"><span>@lang('new-project.dedicated_teams')</span></label>
                    </div>
                </fieldset>
                <div class="steps">
                    <div class="count">
                        <span>03</span><span class="div">/</span>06
                    </div>
                    <div class="act">
                        <a href="#" class="prev">@lang('new-project.represent_business_back')</a>
                    </div>
                    <div class="act">
                        <a href="#" class="next">@lang('new-project.deadline_next')</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="container">
                <h4>@lang('new-project.deadline')</h4>
                <fieldset class="no-spaces">
                    <label>@lang('new-project.we_need_done_by')</label>
                    <div class="date-radios">
                        <label class="urgent"><input required type="radio" name="ra"><span>@lang('new-project.very_urgent')</span></label>
                        <label><input required type="radio" name="ra"><span>1 - 6 @lang('new-project.months')</span></label>
                        <label><input required type="radio" name="ra"><span>6 - 12 @lang('new-project.months')</span></label>
                        <label><input required type="radio" name="ra"><span>@lang('new-project.no_deadline')</span></label>
                    </div>
                </fieldset>
                <div class="steps">
                    <div class="count">
                        <span>04</span><span class="div">/</span>06
                    </div>
                    <div class="act">
                        <a href="#" class="prev">@lang('new-project.represent_business_back')</a>
                    </div>
                    <div class="act">
                        <a href="#" class="next">@lang('new-project.approx_budget_next')</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="container">
                <h4>@lang('new-project.approx_budget')</h4>
                <fieldset class="no-spaces">
                    <label>@lang('new-project.our_budget_between')</label>
                    <div class="range">
                        <div class="l-label">
                            O €
                        </div>
                        <div class="bar" id="bar">

                        </div>
                        <div class="r-label">
                            150k € @lang('new-project.and_up')
                        </div>
                    </div>
                </fieldset>
                <div class="steps">
                    <div class="count">
                        <span>05</span><span class="div">/</span>06
                    </div>
                    <div class="act">
                        <a href="#" class="prev">@lang('new-project.deadline_back')</a>
                    </div>
                    <div class="act">
                        <a href="#" class="next">@lang('new-project.check_and_send_next')</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="container">
                <h4>@lang('new-project.check_and_send')</h4>
                <div class="text-filled">
                    @lang('new-project.final_text')
                </div>
                <button class="submit" id="submit-hire-form" data-done="@lang('new-project.thx_bye')">@lang('new-project.send_now')</button>
                <div class="steps">
                    <div class="count">
                        <span>06</span><span class="div">/</span>06
                    </div>
                    <div class="act">
                        <a href="#" class="prev">@lang('new-project.approx_budget_back')</a>
                    </div>
                    <div class="act">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
