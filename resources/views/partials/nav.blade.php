<header class="header nav-top">
    <div class="navigation">
        <div class="menu-trigger"><span></span><span></span><span></span></div>
        <div class="logo-block">
            <a href="/" class="logo reverse" title="Koralis">Koralis</a>
        </div>
        <nav class="menu">
            <ul>
                <li class="{{ $active_link === 'case-studies' ?? 'current' }}"><a href="/{{app()->getLocale()}}/case-studies">@lang('footer.case_studies')</a></li>
                <li class="{{ $active_link === 'career' ?? 'current' }}"><a href="/{{app()->getLocale()}}/career">@lang('footer.career')<span class="count">2</span></a></li>
                <li class="{{ $active_link === 'wwd' ?? 'current' }}"><a href="/{{app()->getLocale()}}/what-we-do">@lang('footer.wwd')</a></li>
                <li><a href="#contact" data-location="#contact-us">@lang('footer.contact_us')</a></li>
                <li><a href="#plan" data-location="#plan">@lang('footer.need_experts')</a></li>
            </ul>
            <div class="mobile-social">
                <a href="https://www.facebook.com/koralis" target="_blank"><img src="/images/ico_fb.svg" alt=""></a>
                <a href="https://www.linkedin.com/company/recafe" target="_blank"><img src="/images/linked.svg" alt=""></a>
                <a href="https://www.instagram.com/koralislithuania/" target="_blank"><img src="/images/instragram.svg" alt=""></a>
                <a href="https://dribbble.com/Koralislt" target="_blank"><img src="/images/ico_dribble.svg" alt=""></a>
            </div>
        </nav>
    </div>
</header>
