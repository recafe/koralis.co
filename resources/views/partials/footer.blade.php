<footer class="footer">
    <div class="container">
        <div class="left">
            <a href="/{{app()->getLocale()}}"><span class="icon icon-logo"></span></a>
        </div>
        <nav class="footer-menu">
            <ul>
                <li><a href="/{{app()->getLocale()}}/case-studies">@lang('footer.case_studies')</a></li>
                <li><a href="/{{app()->getLocale()}}/career">@lang('footer.career')<span class="count">2</span></a></li>
                <li><a href="/{{app()->getLocale()}}/what-we-do">@lang('footer.wwd')</a></li>
                <li><a href="#contact" data-location="#contact-us">@lang('footer.contact_us')</a></li>
                <li><a href="#plan" data-location="#plan">@lang('footer.need_experts')</a></li>
                <li><a href="privacy-policy">@lang('footer.privacy_policy')</a></li>
            </ul>
        </nav>
        <div class="contacts">
            <div class="col">
                <h6>@lang('contact-modal.contact_us')</h6>
                <p>@lang('footer.we_are_keen_to_hear')</p>
                <a href="mailto: hello@koralis.co" class="big">@lang('contact-modal.contact_email')</a>
            </div>
            <div class="col">
                <h6>@lang('footer.location')</h6>
                <p>@lang('contact-modal.contact_address')</p>
                <p><a href="tel:+370 620 21915">@lang('contact-modal.contact_phone_no')</a></p>
            </div>
            <div class="col">
                <h6>@lang('footer.we_hiring')</h6>
                <p>@lang('footer.join_our_team')</p>
                <p><a href="mailto: cv@koralis.co">@lang('footer.cv_email')</a></p>
            </div>
        </div>
        <div class="copy">
            <ul class="social">
                <li><a href="https://www.facebook.com/koralis" target="_blank">Facebook</a></li>
                <li><a href="https://www.linkedin.com/company/recafe" target="_blank">LinkedIn</a></li>
                <li><a href="https://www.instagram.com/koralislithuania/" target="_blank">Instagram</a></li>
                <li><a href="https://dribbble.com/Koralislt" target="_blank">Dribbble</a></li>
            </ul>
            <p>@lang('footer.copy_right') © {{ now()->year }} Koralis Ltd.
                <br>@lang('footer.all_rights_reserved')</p>
            <p class="privacy-policy-footer"><a href="privacy-policy">@lang('footer.all_rights_reserved')</a></p>
        </div>
    </div>
</footer>


<?php if (!isset($_COOKIE["disclaimer"])) { ?>
<div class="cookie-bar" id="cookie_disclaimer">
    <div class="container">
        <div class="col">
            <p>
                @lang('footer.cookies_warning')
                <span><a href="/privacy-policy">@lang('footer.privacy_policy')</a></span>
            </p>
        </div>
        <div class="cookie-button" id="cookie_stop">
                <span class="accept-button">
                   @lang('footer.accept')
                </span>
        </div>
    </div>
</div>
<?php } ?>
