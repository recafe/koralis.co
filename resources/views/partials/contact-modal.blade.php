<div class="contact-modal">
    <span class="close"></span>
    <div class="container">
        <div class="text">
            <h6>@lang('contact-modal.contact_us')</h6>
            <h4>@lang('contact-modal.lets_make_contact')</h4>
            <p>@lang('contact-modal.contact_us_body')</p>
            <div class="mail">
                <a href="mailto: hello@koralis.co">@lang('contact-modal.contact_email')</a>
            </div>
            <p>@lang('contact-modal.contact_address')</p>
            <p>@lang('contact-modal.contact_phone_no')</p>
        </div>
    </div>
</div>
