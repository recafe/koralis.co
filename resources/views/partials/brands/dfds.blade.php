@extends('layout')

@section('content')

    <div class="dfds">
        <header class="header nav-top">
            <div class="navigation">
                <div class="menu-trigger"><span></span><span></span><span></span></div>
                <div class="logo-block">
                    <a href="/{{app()->getLocale()}}" class="logo" title="Koralis">@lang('base.company_name')</a>
                </div>
            </div>
        </header>
        <div class="page-content" style="background-color: #ffffff">
            <div class="case-title hasFade">
                <div class="container">
                    <div class="text">
                        <div class="case-logo">
                            <img src="/images/dfds/dfdslogo.svg" alt="">
                        </div>

                        <h1>@lang('dfds.dfds_title')</h1>
                        <p id="case-text">@lang('dfds.about_dfds')</p>
                    </div>

                </div>
                <div class="screen">
                    <img src="/images/dfds/macbook.png" alt="">
                </div>
            </div>

            <div class="map">
                <div class="body">

                    <div class="text-row-scratch-game">
                        <div class="text-row">
                            <div class="container">
                                <div class="entry">
                                    <h2>@lang('dfds.ferry_routes.title')</h2>
                                    <div class="entry-cols">
                                        <div class="col">
                                            <p>@lang('dfds.ferry_routes.text')</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="scroller">
                    <div class="text-box team team-box center">
                        <div class="wor"><!--
				<span class="prev"></span>
				<span class="next"></span> -->

                            <section class="desktop dragscroll">
                                <article>
                                    <img src="/images/dfds/photo/1x/photo1.png"
                                         srcset="/images/dfds/photo/2x/photo1.png 2x"
                                         alt="">
                                    <img src="/images/dfds/photo/1x/photo2.png"
                                         srcset="/images/dfds/photo/2x/photo2.png 2x"
                                         alt="">
                                </article>
                                <article>
                                    <img src="/images/dfds/photo/1x/photo3.png"
                                         srcset="/images/dfds/photo/2x/photo3.png 2x"
                                         alt="">
                                </article>
                                <article>
                                    <article>
                                        <img src="/images/dfds/photo/1x/photo4.png"
                                             srcset="/images/dfds/photo/2x/photo4.png 2x" alt="">
                                        <img src="/images/dfds/photo/1x/photo5.png"
                                             srcset="/images/dfds/photo/2x/photo5.png 2x" alt="">
                                    </article>
                            </section>


                            <section class="mobile dragscroll">
                                <article>
                                    <img src="/images/dfds/photo/1x/photo1.png"
                                         srcset="/images/dfds/photo/2x/photo1.png 2x"
                                         alt="">
                                </article>
                                <article>
                                    <img src="/images/dfds/photo/1x/photo2.png"
                                         srcset="/images/dfds/photo/2x/photo2.png 2x"
                                         alt="">
                                </article>
                                <article>
                                    <img src="/images/dfds/photo/1x/photo3.png"
                                         srcset="/images/dfds/photo/2x/photo3.png 2x"
                                         alt="">
                                </article>
                                <article>
                                    <img src="/images/dfds/photo/1x/photo4.png"
                                         srcset="/images/dfds/photo/2x/photo4.png 2x"
                                         alt="">
                                </article>
                                <article>
                                    <img src="/images/dfds/photo/1x/photo5.png"
                                         srcset="/images/dfds/photo/2x/photo5.png 2x"
                                         alt="">
                                </article>

                            </section>
                        </div>

                    </div>
                    <!-- -->

                </div>
            </div>


            <div class="text-row" id="scratch-game">
                <div class="container">
                    <div class="entry">
                        <h2>@lang('dfds.scratch_games.title')</h2>
                        <div class="entry-cols" id="scratch-paragraph">
                            <div class="col" id="scratch-col">
                                <p id="scratch-game-paragraph">@lang('dfds.scratch_games.text')</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <div class="phone_background">
                <div class="phones-cols-block">
                    <div class="phones-cols">
                        <div class="mobile-1">
                            <img src="/images/dfds/white_pens.png">
                        </div>
                        <div class="mobile-2">
                            <img src="/images/dfds/screen1.png">
                        </div>
                        <div class="mobile-3">
                            <img src="/images/dfds/screen2.png">
                        </div>
                        <div class="mobile-4">
                            <img src="/images/dfds/screen3.png">
                        </div>
                        <div class="mobile-5">
                            <img src="/images/dfds/pins.png">
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-box team team-box center newdfds">
                <div class="wor dfds">
                    <span class="prev"></span>
                    <span class="next"></span>
                    <section class="dragscroll">
                        <article>
                            <img src="/images/dfds/screen1.png">
                        </article>
                        <article>
                            <img src="/images/dfds/screen2.png">
                        </article>
                        <article>
                            <img src="/images/dfds/screen3.png">
                        </article>
                    </section>
                </div>
            </div>

            <div class="text-row" id="scratch-game">
                <div class="case-title" id="banner">
                    <div class="container" id="banners-mobile">
                        <div class="entry" id="banner_entry">
                            <h3 id="banners-h3">@lang('dfds.banners.title')</h3>
                            <div class="entry-cols" id="entry-cols-desktop">
                                <div class="col">
                                    <p>@lang('dfds.banners.first_paragraph')</p>
                                    <p id="scratch-game-second-paragraph">@lang('dfds.banners.second_paragraph')</p>
                                </div>
                            </div>

                            <div class="entry-cols" id="entry-cols-mobile">
                                <div class="col" id="col-mobile">
                                    <p>@lang('dfds.banners.third_paragraph')</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clients-full">
                <div class="container">
                    <h2>@lang('dfds.video')</h2>
                    <h2>@lang('dfds.video_time')</h2>
                </div>
            </div>
            <div class="video-full">
                <div class="container">
                    <a href="https://www.youtube.com/watch?v=0fFKGB9zNqE&t=41s" class="media"><img
                            src="/images/dfds/150.jpg"
                            alt=""></a>
                </div>
            </div>

            <div class="quote-center">
                <div class="container">
                    <blockquote>
                        <p>@lang('dfds.quotes.text')</p>
                        <cite>
                            <strong>@lang('dfds.quotes.author')</strong>
                            <p>@lang('dfds.quotes.author_position')</p>
                        </cite>
                    </blockquote>
                </div>
            </div>

            <div class="try-case">
                <div class="containerfooter">
                    <div class="entry" id="sail-desktop">
                        <h3>@lang('dfds.sail_with_dfds')</h3>
                        <p>@lang('dfds.sail_why')</p>
                    </div>

                    <div class="entry" id="sail-mobile">
                        <h3>@lang('dfds.sail_with_dfds')</h3>
                        <p>@lang('dfds.sail_why')</p>
                    </div>

                    <a href="https://dfds.com/" class="btn" target="_blank"><span>WWW.DFDS.COM</span></a>
                </div>
            </div>


            <!-- / body -->
            <div class="start-project color-invert">
                <a href="#plan">
                    <div class="text">
                        <div class="label">@lang('base.need_experts')</div>
                        <div class="start">@lang('base.hire_us')</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/js/main.min.js"></script>
    <script src="/js/fadingbg.js"></script>
@endsection

@section('css')
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
    <link rel="stylesheet" media="all" href="/css/dfds.css">
@endsection
