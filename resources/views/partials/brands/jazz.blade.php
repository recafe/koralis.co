@extends('layout')


@section('content')
    <body>
    <header class="header nav-top">
        <div class="navigation">
            <div class="menu-trigger"><span></span><span></span><span></span></div>
            <div class="logo-block">
                <a href="/{{app()->getLocale()}}" class="logo" title="Koralis">@lang('base.company_name')</a>
            </div>
        </div>
    </header>
    <div class="page-content" style="background-color: #ffffff">
        <div class="case-title hasFade" style="background-color: #181b2b;">
            <div class="container">
                <div class="text">
                    <div class="case-logo">
                        <img src="/images/jazz/jazz%20logo.svg" alt=""/>
                    </div>
                    <h1>@lang('jazz.about')</h1>
                    <p>@lang('jazz.about_text')</p>
                </div>
                <div class="screen">
                    <img src="/images/jazz/Macbook.png" alt="">
                </div>
            </div>
        </div>
        <div class="body">
            <!--Surreal Trumpeter -->
            <div class="text-row surreal-text-row" style="border-bottom: 1px solid #f2f2f2;">
                <div class="entry">
                    <div class="entry-cols">
                        <div class="col surreal-trumpeter">
                            <img id="trumpeter" src="/images/jazz/Trumpeter%20images/Trumpeter_x1.jpg"
                                 srcset="/images/jazz/Trumpeter%20images/Trumpeter_x2.jpg 2x" alt="">
                        </div>
                        <div class="col surreal-text">
                            <div class="text-block">
                                <h2>@lang('jazz.surreal.title')</h2>
                                <p id="surreal-text-desktop">@lang('jazz.surreal.first_paragraph')
                                </p>
                                <p id="surreal-text-desktop">
                                    @lang('jazz.surreal.second_paragraph')</p>
                                <p id="surreal-text-mobile">@lang('jazz.surreal.third_paragraph')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col surreal-trumpeter-mobile">
                    <img id="trumpeter" src="/images/jazz/Trumpeter%20images/Trumpeter_x1.jpg"
                         srcset="/images/jazz/Trumpeter%20images/Trumpeter_x2.jpg 2x" alt="">
                </div>
            </div>
            <!-- Web Design & Development -->
            <div class="text-row web-design-row">
                <div class="container">
                    <div class="entry">
                        <h2>@lang('jazz.web_design.title')</h2>
                        <div class="entry-cols">
                            <div class="col">
                                <p>@lang('jazz.web_design.text')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="web-design-screens">
                    <img class="design-screens"
                         src="/images/jazz/screenai.png" alt="">
                </div>
                <!--Mobile Screens -->
                <!-- Slider for Smarthphone-->
                <div class="scroller">
                    <div class="text-box team team-box center web-design-text-box">
                        <div class="wor">
                            <section class="mobile dragscroll">
                                <article>
                                    <img src="/images/jazz/Screen/x1/1.png"
                                         src="/images/jazz/Screen/x2/1.png" 2x" alt="">
                                </article>
                                <article>
                                    <img src="/images/jazz/Screen/x1/2.png"
                                         src="/images/jazz/Screen/x2/2.png" 2x" alt="">
                                </article>
                                <article>
                                    <img src="/images/jazz/Screen/x1/3.png"
                                         src="/images/jazz/Screen/x2/3.png" 2x" alt="">
                                </article>
                                </article>
                                <article>
                                    <img src="/images/jazz/Screen/x1/4.png"
                                         src="/images/jazz/Screen/x2/4.png" 2x" alt="">
                                </article>
                                </article>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="container"></div>
            </div>
            <!-- Top Artists Slider -->
            <div class="artists-block">
                <div class="container">
                    <div class="entry">
                        <h3 class=" artists-entry">@lang('jazz.artists.title')</h3>
                        <p>@lang('jazz.artists.first_paragraph')</p>
                        <p>
                            @lang('jazz.artist.second_paragraph')
                        </p>
                    </div>
                    <div class="iphoneX"><img
                            src="/images/jazz/Artists/x1/iphonex1.png"
                            src="/images/jazz/Artists/x2/iphonex2.png 2x" alt=""></div>
                    <div class="artists-slider">

                        <div><img
                                src="/images/jazz/Artists/x1/Till%20Brönner.png"
                                src="/images/jazz/Artists/x2/Till%20Brönner.png 2x" alt=""></div>
                        <div><img
                                src="/images/jazz/Artists/x1/Wojtek%20Pilichowski.png"
                                src="/images/jazz/Artists/x2/Wojtek%20Pilichowski.png 2x" alt=""></div>


                        <div><img
                                src="/images/jazz/Artists/x1/Funk‘n‘Stein.png"
                                src="/images/jazz/Artists/x2/Funk‘n‘Stein.png 2x" alt=""></div>


                        <div><img
                                src="/images/jazz/Artists/x1/Newlux.png"
                                src="/images/jazz/Artists/x2/Newlux.png 2x" alt=""></div>

                        <div><img
                                src="/images/jazz/Artists/x1/Jojo%20Mayer.png"
                                src="/images/jazz/Artists/x2/Jojo%20Mayer.png 2x" alt=""></div>

                    </div>
                    <div class="artists-slider-arrows">
                        <!--                <ul>-->
                        <!--                    <li class="prev"></li>-->
                        <!--                    <li class="next"></li>-->
                        <!--                </ul>-->
                    </div>
                </div>
            </div>
            <!--Overview of process Slider -->
            <div class="process-block">
                <div class="container">
                    <div class="entry">
                        <h3>@lang('jazz.overview.title')</h3>
                        <h3>@lang('jazz.overview.title')</h3>
                        <p>@lang('jazz.overview.text')</p>
                    </div>
                    <div id="homepage-slider" class="st-slider">

                        <input type="radio" class="cs_anchor radio" name="slider" id="slide1"/>
                        <input type="radio" class="cs_anchor radio" name="slider" id="slide2"/>
                        <input type="radio" class="cs_anchor radio" name="slider" id="play1" checked=""/>

                        <div class="images">
                            <div class="images-inner">
                                <div class="image-slide">
                                    <div class="image"><img class="a-image"
                                                            src="/images/jazz/Overview%20of%20process/x1/Photo_1.jpg"
                                                            src="/images/jazz/Overview%20of%20process/x2/Photo_1.jpg 2x"
                                                            alt="">
                                    </div>
                                </div>
                                <div class="image-slide">
                                    <div class="image"><img class=" a-image
                            "
                                                            src="/images/jazz/Overview%20of%20process/x1/Photo_2.jpg"
                                                            src="/images/jazz/Overview%20of%20process/x2/Photo_2.jpg 2x"
                                                            alt="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="labels">
                            <label for="slide1" class="label"></label>
                            <label for="slide2" class="label"></label>

                            <div class="fake-radio">
                                <label for="slide1" class="radio-btn"></label>

                                <label for="slide2" class="radio-btn"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Typography text-row -->
            <div class="text-row typography-text-row">
                <div class="container">
                    <div class="entry">
                        <h2>@lang('jazz.typography.slogan')</h2>
                        <div class="typography-text">
                            <div class="col abril">
                                <h6>Slogan</h6>
                                <div class="result">
                                    Aa
                                </div>
                                <div class="name">
                                    Abril Fatface
                                </div>
                                <div class="letters">
                                    <span>abcdefghijklmnopqrstuvwx</span><br>abcdefghijklmnopqrstuvwx<br>1234567890
                                </div>
                            </div>

                            <div class="col oswald">
                                <h6>@lang('jazz.typography.headings')</h6>
                                <div class="result">
                                    Aa
                                </div>
                                <div class="name">
                                    Oswald Light, Regular, Bold
                                </div>
                                <div class="letters">
                                    <span>abcdefghijklmnopqrstuvwx</span><br>abcdefghijklmnopqrstuvwx<br>1234567890
                                </div>
                            </div>

                            <div class="col montserrat">
                                <h6>@lang('jazz.typography.secondary')</h6>
                                <div class="result">
                                    Aa
                                </div>
                                <div class="name">
                                    Montserrat
                                </div>
                                <div class="letters">
                                    <span>abcdefghijklmnopqrstuvwx</span><br>abcdefghijklmnopqrstuvwx<br>1234567890
                                </div>
                            </div>

                        </div>
                        <div class="typography-colors">
                            <div class="col">
                                <div class="wave" style="background-image: url(/images/jazz/Waves/wave1.svg)"></div>
                                <h6>@lang('jazz.typography.ebony')</h6>
                                <p>#272b40</p>
                            </div>
                            <div class="col">
                                <div class="wave" style="background-image: url(/images/jazz/Waves/wave2.svg)"></div>
                                <h6>@lang('jazz.typography.comet')</h6>
                                <p>#606688</p>
                            </div>
                            <div class="col">
                                <div class="wave" style="background-image: url(/images/jazz/Waves/wave3.svg)"></div>
                                <h6>@lang('jazz.typography.dodger')</h6>
                                <p>#36b5ff</p>
                            </div>
                            <div class="col">
                                <div class="wave" style="background-image: url(/images/jazz/Waves/wave4.svg)"></div>
                                <h6>@lang('jazz.typography.white')</h6>
                                <p>#ffffff</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Print Design Block -->
            <div class="text-row print-design-row">
                <div class="container">
                    <div class="entry">
                        <h2>@lang('jazz.print_design.title')</h2>
                        <div class="entry-cols">
                            <div class="col">
                                <p>@lang('jazz.print_design.text')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <img id="print-design" src="/images/jazz/Print%20design/Print%20design_x1.png"
                     src="/images/jazz/Print%20design/Print%20design_x2.png 2x" alt="">

                <img id="print-design-mobile" src="/images/jazz/Print%20design/print_mobile.png" alt="">
            </div>

            <!--Photo gallery -->
            <div class="container gallery-container">
                <div class="text-row">
                    <div class="entry">
                        <h2>@lang('jazz.photo_gallery.title')</h2>
                        <div class="entry-cols">
                            <div class="col" id="solution-desktop">
                                <p>@lang('jazz.photo_gallery.first_paragraph')</p>
                            </div>
                            <div class="col" id="solution-desktop">
                                <p>@lang('jazz.photo_gallery.second_paragraph')
                                </p>
                            </div>
                            <div class="col" id="solution-mobile">
                                <p>@lang('jazz.photo_gallery.third_paragraph')
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="images-cols" id="img-cols1">
                    <img class="a-image" src="/images/jazz/Photo%20gallery/x1/foto_1.jpg"
                         src="/images/jazz/Photo%20gallery/x2/foto_1.jpg 2x" alt="">
                    <img class="b-image" src="/images/jazz/Photo%20gallery/x1/Photo_2.jpg"
                         src="/images/jazz/Photo%20gallery/x2/Photo_2.jpg 2x" alt="">

                </div>
                <div class="images-cols">
                    <img class="c-image" src="/images/jazz/Photo%20gallery/x1/Photo_3.jpg"
                         src="/images/jazz/Photo%20gallery/x2/Photo_3.jpg 2x" alt="">
                    <img class="d-image" src="/images/jazz/Photo%20gallery/x1/foto_4.jpg"
                         src="/images/jazz/Photo%20gallery/x2/foto_4.jpg 2x" alt="">
                </div>
                <!-- Slider for Smarthphone-->
                <div class="scroller">
                    <div class="text-box team team-box center">
                        <div class="wor">
                            <section class="mobile dragscroll">
                                <article>
                                    <img src="/images/jazz/Photo%20gallery/x1/foto_1.jpg"
                                         src="/images/jazz/Photo%20gallery/x2/foto_1.jpg 2x" alt=""></article>
                                <article>
                                    <img src="/images/jazz/Photo%20gallery/x1/Photo_2.jpg"
                                         src="/images/jazz/Photo%20gallery/x2/Photo_2.jpg 2x" alt=""></article>
                                <article>
                                    <img src="/images/jazz/Photo%20gallery/x1/Photo_3.jpg"
                                         src="/images/jazz/Photo%20gallery/x2/Photo_3.jpg 2x" alt="">
                                </article>
                                <article>
                                    <img src="/images/jazz/Photo%20gallery/x1/foto_4.jpg"
                                         src="/images/jazz/Photo%20gallery/x2/foto_4.jpg 2x" alt="">
                                </article>
                            </section>
                        </div>
                    </div>
                    <!-- -->

                </div>
            </div>

            <!-- Quote -->
            <div class="quote-center">
                <div class="container">
                    <blockquote>
                        <p>@lang('jazz.quotes.text')</p>
                        <cite>
                            <strong>@lang('jazz.quotes.author')</strong>
                            <p>@lang('jazz.quotes.author_position')</p>
                        </cite>
                    </blockquote>
                </div>
            </div>


            <!-- Browse Jazz -->
            <div class="try-case">
                <div class="container">
                    <div class="entry">
                        <h3>@lang('jazz.visit')</h3>
                        <p>@lang('jazz.visit_why')</p>
                    </div>
                    <a href="http://jazz.lt/" class="btn" target="_blank"><span>WWW.JAZZ.LT</span></a>
                </div>
            </div>

        </div>
        <!-- / body -->
        <div class="start-project color-invert">
            <a href="#plan">
                <div class="text">
                    <div class="label">@lang('base.need_experts')</div>
                    <div class="start">@lang('base.hire_us')</div>
                </div>
            </a>
        </div>
    </div>

    </body>
@endsection

@section('js')
    <script src="/js/main.min.js"></script>
    <script src="/js/casestudies/jazz.js"></script>
    <script src="/js/casestudies/slick.min.js"></script>
@endsection

@section('css')
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/jazz.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
    <link rel="stylesheet" media="all" href="/css/Slick/slick.css">
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
@endsection

