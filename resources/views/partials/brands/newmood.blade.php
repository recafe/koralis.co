@extends('layout')

@section('content')
    <body class="newmood">

    <div class="pre-loader">
        <span class="logo-w"></span>
    </div>

    <div class="page-content" style="background-color: #ffffff">
        <div class="case-title hasFade">
            <div class="container">
                <div class="text">
                    <div class="case-logo">
                        <img src="/images/newmood/Newmood.svg" alt="">
                    </div>
                    <h1>@lang('newmood.latest_fashions')</h1>
                    <p>@lang('newmood.newmood_description')</p>
                </div>
            </div>
            <div class="screen">
                <img src="/images/newmood-pc.png" alt="">
            </div>
        </div>

        <div class="body">
            <div class="text-row">
                <div class="container">
                    <div class="entry">
                        <h2>@lang('newmood.challenge')</h2>
                        <div class="entry-cols">
                            <div class="col">
                                <p>@lang('newmood.first_challenge')</p>
                            </div>
                            <div class="col">
                                <p>@lang('newmood.second_challenge')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="images-cols">
            <img src="/images/newmood/4.jpg" srcset="/images/newmood/4x2.jpg 2x" alt="">
            <img src="/images/newmood/3.jpg" srcset="/images/newmood/3x2.jpg 2x" alt="">
        </div>

        <div class="text-row">
            <div class="container">
                <div class="entry">
                    <h2>@lang('newmood.mobile_friendly.title')</h2>
                    <div class="entry-cols">
                        <div class="col">
                            <p>@lang('newmood.mobile_friendly.text')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="phones-cols-block">
            <div class="phones-cols">
                <div class="mobile-1">
                    <img src="/images/mobile-1.png">
                </div>
                <div class="mobile-2">
                    <img src="/images/mobile-2.png">
                </div>
                <div class="mobile-3">
                    <img src="/images/mobile-3.png">
                </div>
                <div class="mobile-4">
                    <img src="/images/mobile-4.png">
                </div>
                <div class="mobile-5">
                    <img src="/images/mobile-5.png">
                </div>
            </div>
        </div>
        <div class="text-box team team-box center newmood">
            <div class="wor newmood">
                <span class="prev"></span>
                <span class="next"></span>
                <section class="dragscroll">
                    <article>
                        <img src="/images/mobile-1-gal.png">
                    </article>
                    <article>
                        <img src="/images/mobile-2-gal.png">
                    </article>
                    <article>
                        <img src="/images/mobile-3-gal.png">
                    </article>
                    <article>
                        <img src="/images/mobile-4-gal.png">
                    </article>
                    <article>
                        <img src="/images/mobile-5-gal.png">
                    </article>
                </section>
            </div>
        </div>

        <div class="featured-titles">
            <div class="video">
                <video muted="" id="newmood-video" loop="">
                    <source src="/images/newmood/NEWMOOD.mp4" data-src="/images/newmood/NEWMOOD.mp4" type="video/mp4">
                </video>
            </div>
        </div>

        <div class="text-row">
            <div class="container">
                <div class="entry">
                    <h2>@lang('newmood.typography.title')</h2>
                    <div class="typography-text">
                        <div class="col roboto">
                            <h6>@lang('newmood.typography,primary')</h6>
                            <div class="result">
                                Aa Bb Cc
                            </div>
                        </div>
                        <div class="col roboto">
                            <div class="name">
                                Roboto Regular, Semi-bold
                            </div>
                        </div>
                        <div class="col roboto">
                            <div class="letters">
                                <span>abcdefghijklmnopqrstuvwx</span><br>abcdefghijklmnopqrstuvwx<br>1234567890
                            </div>
                        </div>
                    </div>

                    <div class="typography-colors">
                        <div class="col">
                            <div class="color-text">@lang('newmood.typography.primary_color')</div>
                            <div class="color" style="background: #000000"></div>
                            <h6>@lang('newmood.typography.black')</h6>
                            <p>#000000</p>
                        </div>
                        <div class="col">
                            <div class="color-text">@lang('newmood.secondary_colors')</div>
                            <div class="color" style="background: #cf131d"></div>
                            <h6>@lang('newmood.typography.crimson')</h6>
                            <p>#cf131d</p>
                        </div>
                        <div class="col">
                            <div class="color-text"></div>
                            <div class="color" style="background: #dddddd"></div>
                            <h6>@lang('newmood.typography.alto')</h6>
                            <p>#dddddd</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clients-full">
            <div class="container">
                <h2>Trademarks</h2>
                <h2>+150 brands</h2>
                <ul>
                    <li><img src="/images/newmood/adidas-4.svg" alt=""></li>
                    <li><img src="/images/newmood/nike-3.svg" alt=""></li>
                    <li><img src="/images/newmood/puma.svg" alt=""></li>
                    <li><img src="/images/newmood/Logo_lacoste.svg" alt=""></li>
                    <li><img src="/images/newmood/jack-jones.svg" alt=""></li>
                    <li><img src="/images/newmood/armani-jeans.svg" alt=""></li>
                    <li><img src="/images/newmood/Diesel_logo.svg" alt=""></li>
                    <li><img src="/images/newmood/levi-s.svg" alt=""></li>
                    <li><img src="/images/newmood/Vans.svg" alt=""></li>
                    <li><img src="/images/newmood/converse-logo3.svg" alt=""></li>
                    <li><img src="/images/newmood/Reebok.svg" alt=""></li>
                    <li><img src="/images/newmood/S.Oliver.svg" alt=""></li>
                </ul>
            </div>
        </div>

        <div class="text-row">
            <div class="container">
                <div class="entry">
                    <h2>@lang('newmood.solution.title')</h2>
                    <div class="entry-cols">
                        <div class="col">
                            <p>@lang('newmood.solution.first_paragraph')</p>
                        </div>

                        <div class="col">
                            <p>@lang('newmood.solution.second_paragraph')</p>
                        </div>
                    </div>
                    <div class="entry-cols newmood">
                        <div class="col">
                            <img src="/images/newmood/1.jpg" srcset="/images/newmood/1x2.jpg 2x" alt="">
                        </div>
                        <div class="col">
                            <img src="/images/newmood/2.jpg" srcset="/images/newmood/2x2.jpg 2x" alt="">
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="try-case">
            <div class="container">
                <div class="entry">
                    <h3>@lang('newmood.shop')</h3>
                    <p>@lang('newmood.free_shipping')</p>
                </div>
                <a href="https://newmood.lt/" class="btn" target="_blank"><span>WWW.NEWMOOD.LT</span></a>
            </div>
        </div>

        <!-- / body -->
        <div class="start-project color-invert">
            <a href="#plan">
                <div class="text">
                    <div class="label">@lang('base.need_experts')</div>
                    <div class="start">@lang('base.hire_us')</div>
                </div>
            </a>
        </div>
    </div>
    </body>
@endsection

@section('js')
    <script src="/js/main.min.js"></script>
@endsection

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
    <link rel="stylesheet" media="all" href="/css/newmood.css">
@endsection
