@extends('layout')

@section('content')
    <body>
    <header class="header nav-top">
        <div class="navigation">
            <div class="menu-trigger"><span></span><span></span><span></span></div>
            <div class="logo-block">
                <a href="/{{app()->getLocale()}}" class="logo" title="Koralis">@lang('base.company_name')</a>
            </div>
        </div>
    </header>
    <div class="page-content" style="background-color: #ffffff">
        <div class="case-title hasFade" style="background-color: #CB006B;">
            <div class="container">
                <div class="text">
                    <div class="case-logo">
                        <img src="/images/moofe/Moofe_logo.svg" alt="">
                    </div>
                    <h1>@lang('moofe.about')</h1>
                    <p>@lang('moofe.about_text')</p>
                </div>
            </div>
            <div class="screen">
                <img src="/images/moofe/macbook.png" alt="">
            </div>
        </div>
        <div class="body">
            <div class="text-row">
                <div class="container" id="mobile-container">
                    <div class="entry">
                        <h2>@lang('moofe.company_info.title')</h2>
                        <div class="entry-cols">
                            <div class="col" id="solution-desktop">
                                <p>@lang('moofe.company_info.first_paragraph')</p>
                            </div>
                            <div class="col" id="solution-desktop">
                                <p>@lang('moofe.company_info.second_paragraph')
                                </p>
                            </div>

                            <div class="col" id="company-mobile">
                                <p>@lang('moofe.company_info.third_paragraph')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="images-cols" id="img-cols1">
                <img class="a-image" src="/images/moofe/company/Photo_x1/Photo_1.png"
                     srcset="/images//moofe/company/photo/foto1.png" alt="">
                <img class="b-image" src="/images/moofe/company/Photo_x1/Photo_3.png"
                     srcset="/images/moofe/company/photo/foto3.png" alt="">

            </div>
            <div class="images-cols">
                <img class="c-image" src="/images/moofe/company/Photo_x1/Photo_2.png"
                     srcset="/images/moofe/company/photo/foto2.png" alt="">
                <img class="d-image" src="/images/moofe/company/Photo_x1/Photo_4.png"
                     srcset="/images/moofe/company/photo/foto4.png" alt="">

            </div>


            <div class="scroller">
                <div class="text-box team team-box center">
                    <div class="wor">


                        <section class="mobile dragscroll">
                            <article>
                                <img src="/images/moofe/company/Photo_x1/Photo_1.png"
                                     srcset="/images/moofe/company/photo/foto1.png" alt="">
                            </article>
                            <article>
                                <img src="/images/moofe/company/Photo_x1/Photo_2.png"
                                     srcset="/images/moofe/company/photo/foto2.png" alt="">
                            </article>
                            <article>
                                <img src="/images/moofe/company/Photo_x1/Photo_4.png"
                                     srcset="/images/moofe/company/photo/foto4.png" alt="">
                            </article>
                            <article>
                                <img src="/images/moofe/company/Photo_x1/Photo_3.png"
                                     srcset="/images/moofe/company/photo/foto3.png" alt="">
                            </article>
                        </section>
                    </div>

                </div>
                <!-- -->

            </div>

            <div class="clients-full">
                <div class="container">
                    <h2>@lang('moofe.moofie_clients')</h2>
                    <h2>@lang('moofe.automakers')</h2>
                    <ul>
                        <li><img src="/images/moofe/moofe-clients/Ferrari_logo.svg" alt=""></li>
                        <li><img src="/images/moofe/moofe-clients/Tesla_logo.svg" alt="" id="tesla-img"></li>
                        <li><img src="/images/moofe/moofe-clients/BMV_logo.svg" alt=""></li>
                        <li><img src="/images/moofe/moofe-clients/Toyota_logo.svg" alt=""></li>
                        <li><img src="/images/moofe/moofe-clients/Audi_logo.svg" alt=""></li>
                        <li><img src="/images/moofe/moofe-clients/Cadillac_logo.svg" alt=""></li>
                        <li><img src="/images/moofe/moofe-clients/lambo.svg" alt=""></li>
                        <li><img src="/images/moofe/moofe-clients/Renault_logo.svg" alt=""></li>


                    </ul>
                </div>
            </div>

            <!-- BACKGROUND IMAGE -->
            <div class="backgroundas">
                <div class="text-row" id="road-top">
                    <div class="container">
                        <div class="entry">
                            <div class="entry-cols">
                                <div class="col" id="solution-desktop">
                                    <h2>@lang('moofe.solution.title')</h2>
                                </div>
                            </div>
                            <div class="entry-cols">
                                <div class="col" id="solution-desktop">
                                    <p>@lang('moofe.solution.first_solution')</p>
                                </div>
                                <div class="col" id="solution-desktop">
                                    <p>@lang('moofe.solution.second_solution')</p>
                                </div>

                                <div class="col-mob">
                                    <h2>@lang('moofe.solution.title')</h2>
                                    <p>@lang('moofe.solution.third_solution')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="technologies-box">
                    <div class="container">
                        <h3>@lang('moofe.technologies_used')</h3>
                        <ul>
                            <li><a href="#"><img src="/images/moofe/technologies/php.png" alt=""></a></li>
                            <li><a href="#"><img src="/images/moofe/technologies/jquery.png" alt=""></a></li>
                            <li><a href="#"><img src="/images/moofe/technologies/htmlcss.png" alt=""></a></li>
                        </ul>
                        <ul>
                            <li><a href="#"><img src="/images/moofe/technologies/laravel.png" alt=""></a></li>
                        </ul>
                    </div>

                </div>

            </div>

            <div class="text-row" id="usability-desktop">
                <div class="container" id="container-usability">
                    <div class="center-cols">
                        <div class="col-text">
                            <h2>@lang('moofe.usability.usability_style')</h2>
                            <p>@lang('moofe.usability.first_paragraph')</p>
                        </div>
                        <div class="col">
                            <img src="/images/moofe/backendphoto.png" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-row" id="usability-mobile">
                <div class="container" id="container-usability">
                    <div class="center-cols">
                        <div class="col">
                            <img src="/images/moofe/backendphoto.png" alt="">
                        </div>
                        <div class="col-text">
                            <h2>@lang('moofe.usability.usability_style')</h2>
                            <p>@lang('moofe.usability.second_paragraph')</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="featured-titles">
                <div class="video">
                    <video muted="" id="newmood-video" loop="">
                        <source src="/images/moofe/car.mp4" data-src="/images/moofe/car.mp4" type="video/mp4">
                    </video>
                </div>
            </div>


            <div class="quote-center">
                <div class="container">
                    <blockquote>
                        <p>The knowledge and ability that Koralis has brought to our project has elevated it beyond our
                            expectations. I would not <br/> hesitate to recommend Koralis as a suitable partner in the
                            creation of online solutions for any business.</p>
                        <cite>
                            <strong>DOUGLAS FISHER</strong>
                            <p>Director & Co-Founder www.moofe.com</p>
                        </cite>
                    </blockquote>
                </div>
            </div>

            <div class="try-case">
                <div class="container">
                    <div class="entry">
                        <h3>Browse Moofe now</h3>
                        <p>Discover a great collection of high quality CG ready imagery for your next campaign.</p>
                    </div>
                    <a href="https://moofe.com/" class="btn" target="_blank"><span>WWW.MOOFE.COM</span></a>
                </div>
            </div>

        </div>
        <!-- / body -->
        <div class="start-project color-invert">
            <a href="#plan">
                <div class="text">
                    <div class="label">@lang('base.need_experts')</div>
                    <div class="start">@lang('base.hire_us')</div>
                </div>
            </a>
        </div>
    </div>

    </body>
@endsection

@section('js')
    <script src="/js/main.min.js"></script>
    <script src="/js/fadingbg.js"></script>
@endsection

@section('css')
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/moofe.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
@endsection
