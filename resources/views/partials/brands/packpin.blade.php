@extends('layout')

@section('content')
    <body class="packpin">

    <header class="header nav-top">
        <div class="navigation">
            <div class="menu-trigger"><span></span><span></span><span></span></div>
            <div class="logo-block">
                <a href="/{{app()->getLocale()}}" class="logo" title="Koralis">@lang('base.company_name')</a>
            </div>
        </div>
    </header>

    <div class="page-content" style="background-color: #ffffff">
        <div class="case-title hasFade" style="background-color: #e51c23;">
            <div class="container">
                <div class="text">
                    <div class="case-logo">
                        <img src="/images/packpin.svg" alt="" style="width: 293px; height: auto;"/>
                    </div>
                    <h1>@lang('packpin.ecommerce')</h1>
                    <p>@lang('packpin.packpin_description')</p>
                </div>
                <div class="screen">
                    <img src="/images/case2.png" alt="">
                </div>
            </div>
        </div>
        <div class="body">
            <div class="text-row">
                <div class="container">
                    <div class="entry">
                        <h2>@lang('packpin.challenge.title')</h2>
                        <div class="entry-cols">
                            <div class="col">
                                <p>@lang('packpin.challenge.first_challenge')</p>
                                <p>@lang('packpin.challenge.second_challenge')</p>
                            </div>
                            <div class="col">
                                <p>@lang('packpin.challenge.third_challenge')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="images-cols">
                <img src="/images/case3.jpg" srcset="/images/case3x2.jpg 2x" alt="">
                <img src="/images/case4.jpg" srcset="/images/case4x2.jpg 2x" alt="">
            </div>
            <div class="text-row">
                <div class="container">
                    <div class="entry">
                        <h2>@lang('packpin.typography.title')</h2>
                        <div class="typography-text">
                            <div class="col montserrat">
                                <h6>@lang('packpin.typography.primary')</h6>
                                <div class="result">
                                    Aa
                                </div>
                                <div class="name">
                                    Montserrat Regular, Semi-bold, Bold
                                </div>
                                <div class="letters">
                                    <span>abcdefghijklmnopqrstuvwx</span><br>abcdefghijklmnopqrstuvwx<br>1234567890
                                </div>
                            </div>
                            <div class="col roboto">
                                <h6>@lang('packpin.typography.secondary')</h6>
                                <div class="result">
                                    Aa
                                </div>
                                <div class="name">
                                    Roboto Regular, Semi-bold
                                </div>
                                <div class="letters">
                                    <span>abcdefghijklmnopqrstuvwx</span><br>abcdefghijklmnopqrstuvwx<br>1234567890
                                </div>
                            </div>
                        </div>
                        <div class="typography-colors">
                            <div class="col">
                                <div class="color" style="background: #d0021b"></div>
                                <h6>@lang('packpin.typography.monza')</h6>
                                <p>#d0021b</p>
                            </div>
                            <div class="col">
                                <div class="color" style="background: #ea1234"></div>
                                <h6>@lang('packpin.typography.crimson')</h6>
                                <p>#ea1234</p>
                            </div>
                            <div class="col">
                                <div class="color" style="background: #171a1d"></div>
                                <h6>@lang('packpin.typography.woodsmoke')</h6>
                                <p>#171a1d</p>
                            </div>
                            <div class="col">
                                <div class="color" style="background: #f7f7f7"></div>
                                <h6>@lang('packpin.typography.alabaster')</h6>
                                <p>#f7f7f7</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="images-full">
                <img src="/images/case6.jpg" alt="">
            </div>/
            <div class="partners-full">
                <div class="container">
                    <ul>
                        <li><a href="#"><img src="/images/packpin/woocommerce.svg" alt=""></a></li>
                        <li><a href="#"><img src="/images/packpin/magento.svg" alt=""></a></li>
                        <li><a href="#"><img src="/images/packpin/shopify.svg" alt=""></a></li>
                        <li><a href="#"><img src="/images/packpin/prestashop.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
            <div class="video-full">
                <div class="container">
                    <a href="https://www.youtube.com/watch?v=nHenAUXWDws" class="media"><img src="/images/case7.jpg"
                                                                                             alt=""></a>
                </div>
            </div>
            <div class="clients-full">
                <div class="container">
                    <h2>@lang('packpin.packpin_connects')</h2>
                    <ul>
                        <li><img src="/images/packpin/dpd.svg" alt=""></li>
                        <li><img src="/images/packpin/lpexpress.svg" alt=""></li>
                        <li><img src="/images/packpin/omniva.png" alt=""></li>
                        <li><img src="/images/packpin/lpastas.svg" alt=""></li>
                        <li><img src="/images/packpin/dhl.svg" alt=""></li>
                        <li><img src="/images/packpin/venipak.svg" alt=""></li>
                        <li><img src="/images/packpin/tnt.svg" alt=""></li>
                        <li><img src="/images/packpin/ups.svg" alt=""></li>
                        <li><img src="/images/packpin/fedex.svg" alt=""></li>
                        <li><img src="/images/packpin/yodel.svg" alt=""></li>
                        <li><img src="/images/packpin/postnl.svg" alt=""></li>

                        <li><span class="more">@lang('packpin.and_many_more')</span></li>
                    </ul>
                </div>
            </div>
            <div class="text-row">
                <div class="container">
                    <div class="center-cols">
                        <div class="col">
                            <h2>@lang('packpin.pricing')</h2>
                        </div>
                        <div class="col">
                            <img src="/images/case8.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="try-case">
                <div class="container">
                    <div class="entry">
                        <h3>@lang('packpin.try_packpin')</h3>
                        <p>@lang('packpin.why_try_packpin')</p>
                    </div>
                    <a href="https://packpin.com/" class="btn" target="_blank"><span>WWW.PACKPIN.COM</span></a>
                </div>
            </div>

        </div>
        <!-- / body -->

        <!-- / body -->
        <div class="start-project color-invert">
            <a href="#plan">
                <div class="text">
                    <div class="label">@lang('base.need_experts')</div>
                    <div class="start">@lang('base.hire_us')</div>
                </div>
            </a>
        </div>
    </div>
    </body>
@endsection

@section('js')
    <script src="/js/main.min.js"></script>
@endsection

@section('css')
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/packpin.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
@endsection
