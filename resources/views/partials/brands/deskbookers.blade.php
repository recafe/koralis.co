@extends('layout')

@section('content')
    <body class="deskbookers">
    <header class="header nav-top">
        <div class="navigation">
            <div class="menu-trigger"><span></span><span></span><span></span></div>
            <div class="logo-block">
                <a href="/{{app()->getLocale()}}" class="logo" title="Koralis">@lang('base.company_name')</a>
            </div>
        </div>
    </header>
    <div class="page-content" style="background-color: #ffffff">
        <div class="case-title hasFade" style="background-color: #393A73;">
            <div class="container">
                <div class="text">
                    <div class="case-logo">
                        <img src="/images/deskbookers/deskbookers.svg" alt="">
                    </div>
                    <h1>@lang('deskbookers.description')</h1>
                    <p>@lang('deskbookers.location')</p>
                </div>
            </div>
            <div class="screen">
                <img src="/images/deskbooker-pc.png" alt="">
            </div>
        </div>
        <div class="body">
            <div class="text-row">
                <div class="container">
                    <div class="entry">
                        <h2>@lang('deskbookers.company_info_title')</h2>
                        <div class="entry-cols">
                            <div class="col">
                                <p>@lang('deskbookers.company_info_text')</p>
                            </div>
                            <div class="col">
                                <p>@lang('deskbookers.company_info_text')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="images-cols">
                <img src="/images/desk-1.png" alt="">
                <img src="/images/desk-2.png" alt="">
            </div>

            <div class="logos-cols">
                <div class="container">
                    <div class="col">
                        <img src="/images/deskbookers/tmobile.svg">
                    </div>
                    <div class="col">
                        <img src="/images/deskbookers/microsoft.svg">
                    </div>
                    <div class="col">
                        <img src="/images/deskbookers/bi.svg">
                    </div>
                    <div class="col">
                        <img src="/images/deskbookers/ing.svg">
                    </div>
                </div>
            </div>

            <div class="text-row">
                <div class="container">
                    <div class="entry">
                        <div class="entry-cols">
                            <div class="col">
                                <h2>@lang('deskbookers.development.title')</h2>
                            </div>
                        </div>

                        <div class="entry-cols">
                            <div class="col">
                                <p>@lang('deskbookers.development.first_paragraph')</p>
                            </div>
                            <div class="col">
                                <p>@lang('deskbookers.development.second_paragraph')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="technologies-box">
                <div class="container">
                    <h3>@lang('deskbookers.technologies_we_used')</h3>
                    <ul>
                        <li><a href="#"><img src="/images/technologies/php.svg" alt=""></a></li>
                        <li><a href="#"><img src="/images/technologies/jquery.svg" alt=""></a></li>
                        <li><a href="#"><img src="/images/technologies/htmlcss.svg" alt=""></a></li>
                    </ul>
                    <!--<ul>
                        <li><a href="#"><img src="images/tech.svg" alt=""></a></li>
                        <li><a href="#"><img src="images/tech.svg" alt=""></a></li>
                    </ul>-->
                    <ul>
                        <li><a href="#"><img src="/images/technologies/mysql.svg" alt=""></a></li>
                        <li><a href="#"><img src="/images/technologies/angularjs.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>

            <div class="quote-center">
                <div class="container">
                    <blockquote>
                        <p>@lang('deskbookers.quote_about_koralis')</p>
                        <cite>
                            <strong>@lang('deskbookers.quote_about_koralis_author')</strong>
                            <p>@lang('deskbookers.quote_about_koralis_author_position')</p>
                        </cite>
                    </blockquote>
                </div>
            </div>

            <div class="try-case">
                <div class="container">
                    <div class="entry">
                        <h3>@lang('deskbookers.try_deskbookers')</h3>
                        <p>@lang('deskbookers.why_try_deskbookers')</p>
                    </div>
                    <a href="https://deskbookers.com/" class="btn" target="_blank"><span>WWW.DESKBOOKERS.COM</span></a>
                </div>
            </div>

        </div>
        <!-- / body -->
        <div class="start-project color-invert">
            <a href="#plan">
                <div class="text">
                    <div class="label">@lang('base.need_experts')</div>
                    <div class="start">@lang('base.hire_us')</div>
                </div>
            </a>
        </div>
    </div>
    </body>
@endsection


@section('js')
    <script src="/js/main.min.js"></script>
@endsection

@section('css')
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/deskbookers.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
@endsection
