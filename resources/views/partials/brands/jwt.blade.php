@extends('layout')

@section('content')
    <body>
    <header class="header nav-top">
        <div class="navigation">
            <div class="menu-trigger"><span></span><span></span><span></span></div>
            <div class="logo-block">
                <a href="/{{app()->getLocale()}}" class="logo reverse" title="Koralis">@lang('base.company_name')</a>
            </div>
        </div>
    </header>
    <div class="page-content" style="background-color: #ffffff">
        <div class="case-title hasFade" style="background-color: #07223F;">
            <div class="container">
                <div class="text">
                    <div class="case-logo">
                        <img src="/images/jwt/logo/jwtlogo.svg" alt="">
                    </div>
                    <h1>@lang('jwt.company_title')</h1>
                    <p>@lang('jwt.about_company')</p>
                </div>
            </div>
            <div class="screen">
                <img src="/images/jwt/Macbook.png" alt="">
            </div>
        </div>
        <div class="body">
            <div class="text-row">
                <div class="container" id="mobile-container">
                    <div class="entry">
                        <h2>@lang('jwt.company_info.title')</h2>
                        <div class="entry-cols">
                            <div class="col" id="solution-desktop">
                                <p>@lang('jwt.company_info.first_paragraph')</p>
                            </div>
                            <div class="col" id="solution-desktop">
                                <p> @lang('jwt.company_info.second_paragraph')
                                </p>
                            </div>

                            <div class="col" id="company-mobile">
                                <p>@lang('jwt.company_info.third_paragraph')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  Demos -->
        <section class="demo-slider" id="demos">
            <div class="row">
                <div class="large-12 columns">
                    <div class="carousel-inner-parts">
                        <div class="owl-carousel owl-theme owl-office" id="owl-carousel-office">
                            <div class="item" id="itemIDoffice">
                                <img id="office-img" src="/images/jwt/officex1/1_Amsterdam.jpg"
                                     srcset="/images/jwt/officex2/1_Amsterdam.jpg 2x" alt="">
                            </div>
                            <div class="item" id="itemIDoffice">
                                <img id="office-img" src="/images/jwt/officex1/_2Atlanta.jpg"
                                     srcset="/images/jwt/officex2/2_Atlanta.jpg 2x" alt="">
                            </div>
                            <div class="item" id="itemIDoffice">
                                <img id="office-img" src="/images/jwt/officex1/3_Brazil.jpg"
                                     srcset="/images/jwt/officex2/3_Brazil.jpg 2x" alt="">
                            </div>
                            <div class="item" id="itemIDoffice">
                                <img id="office-img" src="/images/jwt/officex1/4_London.jpg"
                                     srcset="/images/jwt/officex2/4_London.jpg 2x" alt="">
                            </div>
                            <div class="item" id="itemIDoffice">
                                <img id="office-img" src="/images/jwt/officex1/5_New-York.jpg"
                                     srcset="/images/jwt/officex2/5_New-York.jpg 2x" alt="">
                            </div>
                            <div class="item" id="itemIDoffice">
                                <img id="office-img" src="/images/jwt/officex1/6_Riyadh.jpg"
                                     srcset="/images/jwt/officex2/6_Riyadh.jpg 2x" alt="">
                            </div>
                            <div class="item" id="itemIDoffice">
                                <img id="office-img" src="/images/jwt/officex1/7_Sidney.jpg"
                                     srcset="/images/jwt/officex2/7_Sidney.jpg 2x" alt="">
                            </div>
                            <div class="item" id="itemIDoffice">
                                <img id="office-img" src="/images/jwt/officex1/8_Tokyo.jpg"
                                     srcset="/images/jwt/officex2/8_Tokyo.jpg 2x" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-progress-parent" id="office-slide-progress-parent">
                    <div class="slide-progress-office">
                        <div id="infoOffice">1 / 8</div>
                    </div>
                </div>
            </div>
        </section>


        <!--Number " +3 " behind the text -->
        <div class="container" id="number-container">
            <div class="number-3">
                <h1>+3</h1>
                <div class="number-text">
                    <p>@lang('jwt.company_name')</p>
                </div>
                <div class="number-text-mobile">
                    <p>JWT Group</p>
                </div>
            </div>
        </div>

        <!--  Demos -->
        <section class="demo-slider-elements" id="demos">
            <div class="row">
                <div class="large-12 columns">
                    <div class="carousel-inner-parts">
                        <div class="owl-carousel owl-theme owl-element">
                            <div class="item" id="itemID">
                                <div class="square-parent">
                                    <div class="square" id="square-Geometry">
                                        <img src="/images/jwt/logo/geometry.svg" alt="">
                                    </div>
                                </div>
                                <div class="text-row">
                                    <div class="container" id="geometry-container">

                                        <h2 id="activation">@lang('jwt.activation_commerce')
                                        </h2>
                                        <div class="entry-columns">
                                            <div class="col">
                                                <p>@lang('jwt.geometry_global')</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="item" id="itemID">
                                <div class="square-parent">
                                    <div class="square" id="inovation-square">
                                        <img src="/images/jwt/logo/Inovation%20group.svg" alt="">
                                    </div>
                                </div>
                                <div class="text-row">
                                    <div class="container" id="geometry-container">
                                        <h2 id="intelligence">@lang('jwt.intelligence')</h2>
                                        <div class="entry-columns">
                                            <div class="col">
                                                <p>@lang('jwt.about_intelligence')</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item" id="itemID">
                                <div class="square-parent">
                                    <div class="square" id="square-mirum">
                                        <img src="/images/jwt/logo/MIrum_logo.svg" alt="">
                                    </div>
                                </div>
                                <div class="text-row">
                                    <div class="container" id="geometry-container">
                                        <h2 id="digital">@lang('jwt.digital_transformation')</h2>
                                        <div class="entry-columns">
                                            <div class="col">
                                                <p>@lang('jwt.about_digital_transformation')</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="slide-progress-parent">
                    <div class="slide-progress">
                        <div id="info">1 / 3</div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Clients of J.Walter Thompson -->
        <div class="clients-full">
            <div class="container">
                <h2>@lang('jwt.jwt_partners')</h2>
                <h2 id="70brands">@lang('jwt.brands')</h2>
                <ul>
                    <li><img src="/images/jwt/partners/Avon.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/Treasury.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/Kimberly-Clark.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/Edgewell.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/Shell.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/Rolex.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/Tudor.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/Nestle.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/HSBC.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/Johnson.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/Unilever.svg" alt=""></li>
                    <li><img src="/images/jwt/partners/newell.svg" alt=""></li>
                </ul>
            </div>
        </div>

        <!--Cooperation Background image and text -->
        <div class="case-title" id="cooperation-background">
            <div class="text-row" id="cooperation-text-row">
                <div class="container">
                    <div class="entry" id="cooperation-entry-desktop">
                        <div class="entry-cols">
                            <div class="col">
                                <h2>@lang('jwt.cooperation')</h2>
                            </div>
                        </div>
                        <div class="entry-cols">
                            <div class="col">
                                <p>@lang('jwt.cooperation_first')</p>
                            </div>
                            <div class="col">
                                <p>@lang('jwt.cooperation_second')</p>

                            </div>
                        </div>
                    </div>

                    <div class="entry" id="cooperation-entry-mobile">
                        <div class="entry-cols">
                            <div class="col">
                                <h2>@lang('jwt.cooperation')</h2>
                            </div>
                        </div>
                        <div class="entry-cols">
                            <div class="col">
                                <p>@lang('jwt.cooperation_third')
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--Solution Container -->
        <div class="solution-container">
            <div class="solution-background">
                <!--Solution Top Left Half -->
                <div class="text-row">
                    <div class="entry-cols left">
                        <div class="left-half">
                            <div class="text-col">
                                <div class="number-01">
                                    <h1>01</h1>
                                    <div class="solution-text-left">
                                        <h2>@lang('jwt.voice_recognition')</h2>
                                        <p>@lang('jwt.voice_recognition_text_first')</p>
                                    </div>
                                </div>
                            </div>
                            <div class="img-col">
                                <img src="/images/jwt/solution/x1/Sprite_x1.jpg"
                                     srcset="/images/jwt/solution/x2/Sprite_x2.jpg 2x"
                                     alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!--Solution Bottom Right Half -->
                <div class="text-row">
                    <div class="entry-cols right">
                        <div class="right-half">
                            <div class="img-col">
                                <img src="/images/jwt/solution/x1/Shake-it_x1.jpg"
                                     srcset="/images/jwt/solution/x2/Shake-it_x2.jpg 2x" alt="">
                            </div>
                            <div class="text-col">
                                <div class="number-02">
                                    <h1>02</h1>
                                    <div class="solution-text-right">
                                        <h2>@lang('jwt.mix_it')</h2>
                                        <p>@lang('jwt.mix_it_text_first')</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- The Solution - Smart Phones Style -->
        <div class="text-row" id="usability-mobile">
            <div class="container" id="container-usability">
                <div class="center-cols">
                    <div class="number-solution-mobile">
                        <h1>01</h1>
                        <div class="col-text" id="solution-mobile-text">
                            <h2>@lang('jwt.voice_recognition')</h2>
                            <p>@lang('jwt.voice_recognition_text_second')</p>
                        </div>
                    </div>
                    <div class="col" id="right-side-img">
                        <img src="/images/jwt/solution/x1/Sprite_x1.jpg"
                             srcset="/images/jwt/solution/x2/Sprite_x2.jpg 2x"
                             alt="">
                    </div>
                    <div class="number-solution-mobile" id="nsm-bottom">
                        <h1>02</h1>
                        <div class="col-text" id="solution-mobile-text">
                            <h2>@lang('jwt.mix_it')</h2>
                            <p>@lang('jwt.mix_it_text_second')</p>
                        </div>
                    </div>
                    <div class="col right" id="left-side-img">
                        <img src="/images/jwt/solution/x1/Shake-it_x1.jpg"
                             srcset="images/jwt/solution/x2/Shake-it_x2.jpg 2x"
                             alt="">
                    </div>
                </div>
            </div>
        </div>

        <!-- JWT LEgacy Since 1864 -->
        <div class="legacy-parent">
            <div class="legacy-top-block">

            </div>
            <div class="legacy-image">
                <img id="legacy-image-desktop" src="/images/jwt/legacy/x1.png" alt="">
                <img id="legacy-image-mobile" src="/images/jwt/legacy/legacymobile.png" alt="">
            </div>
            <div class="legacy-bottom-block">
                <div class="header-legacy">
                    <h2>@lang('jwt.jwt_legacy')</h2>
                    <div class="text-legacy">
                        <p>@lang('jwt.history')</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Explore JWT Now Block -->
        <div class="try-case">
            <div class="entry">
                <h3>@lang('jwt.explore_now')</h3>
                <p id="entryParagraph">@lang('jwt.why')</p>
            </div>
            <a href="https://jwt.com/" class="btn" target="_blank"><span>WWW.JWT.COM</span></a>
        </div>

        <!-- / body -->

        <!-- / body -->
        <div class="start-project color-invert">
            <a href="#plan">
                <div class="text">
                    <div class="label">@lang('base.need_experts')</div>
                    <div class="start">@lang('base.hire_us')</div>
                </div>
            </a>
        </div>
    </div>

    </body>
@endsection
@section('js')
    <script src="/js/main.min.js"></script>
    <script src="/js/fadingbg.js"></script>
    <script src="/js/casestudies/owl.carousel.js"></script>
    <script src="/js/casestudies/jwscarousel2.js"></script>
@endsection

@section('css')
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/jws.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
    <link rel="stylesheet" media="all" href="/css/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" media="all" href="/css/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection
