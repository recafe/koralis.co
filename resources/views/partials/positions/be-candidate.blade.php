<form action="#" method="post" id="test-form" enctype="multipart/form-data">
    <div class="form-apply js-from-bottom">
        <h3>@lang('be-candidate.be_candidate')</h3>
        <div class="form job-form">
            <label>@lang('be-candidate.attach_cv')</label>
            <div class="file">
                <label>
                    <span class="choose">@lang('be-candidate.choose_file')</span>
                    <input name="files[]" id="files">
                    <span class="result"><span class="to-hide">@lang('be-candidate.files_chosen')</span> <strong>0</strong></span>
                </label>
            </div>
            <div class="or">
                @lang('be-candidate.or')
            </div>
            <label>@lang('be-candidate.add_linked_in')</label>
            <input type="text" name="textas" placeholder="" id="textas">
            <button type="submit" onclick="this.onclick=null;" name="candidate" data-done="@lang('be-candidate.thx_bye')" id="submit-test-form"
                    class="visible btn">@lang('be-candidate.apply')
            </button>

        </div>

        <!--
        <div class="share">
            <h3>REFER A FRIEND</h3>
            <ul>
                <li><a href="#"><img src="images/ico_fb.png" srcset="images/ico_fb@2x.png 2x" alt=""></a></li>
                <li><a href="#"><img src="images/ico_linked.png" srcset="images/ico_linked@2x.png 2x" alt=""></a></li>
            </ul>
        </div>
        -->
    </div>
</form>
