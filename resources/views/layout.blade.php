<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/koralis.jpg">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ $title }}</title>
    {{--<base href="<?php echo $BASE; ?>">--}}
    @yield('css')
    @include('partials.header')
    <script> gtag('event', 'page_view', {'send_to': 'AW-854091409', 'user_id': 'replace with value'}); </script>
</head>
@include('partials.nav')

@yield('content')

@include('partials.contact-modal')
@include('partials.new-project')
@include('partials.footer')
@include('partials.jqueryHolder')

@yield('js')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
</html>
