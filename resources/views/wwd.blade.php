@extends('layout')

@section('content')
    <body class="what-we-do">
    <div class="page-content">
        <div class="fadingBG"></div>

        <div id="fade-featured-title" class="featured-title">
            <div class="container">
                <h1>@lang('wwd.what_we_do')</h1>
                <h2>@lang('wwd.at_koralis') <br>@lang('wwd.we_offer')</h2>
            </div>
        </div>
        @include('partials.follow-widget')
        <div class="body">
            <div class="fadeGap"></div>
            <div class="fadePoint"></div>
            <div class="service-box hasFade" id="fadingBlock">
                <div class="container">
                    <div class="ico">
                        <img src="/images/s1.svg" alt="">
                    </div>
                    <h2>@lang('wwd.web_development.title')</h2>
                    <div class="desc">
                        <p>@lang('wwd.web_development.description')</p>
                        <ul>
                            <li>@lang('wwd.web_development.first_line')</li>
                            <li>@lang('wwd.web_development.second_line')</li>
                        </ul>
                        <ul>
                            <li>@lang('wwd.web_development.third_line')</li>
                            <li>@lang('wwd.web_development.fourth_line')</li>
                        </ul>
                    </div>
                    <div class="related">
                        <h6>@lang('wwd.related_projects')</h6>
                        <p>Coda, <a href="/{{app()->getLocale()}}/case-studies/newmood/">Newmood</a>, ASG Worldwide, Disney, Moofe, <a
                                href="/{{app()->getLocale()}}/case-studies/deskbookers/">Deskbookers</a>, <a href="/{{app()->getLocale()}}/case-studies/packpin/">Packpin</a>,
                            JWT, Pokernews, Universal, Nike, BBPWood, Geralda, KVK, Sprite, Klaipeda University,
                            Klaipeda Tourism Agency, Svyturys Arena, EA Games</p>
                    </div>
                </div>
            </div>

            <div class="quote-box-image color-invert" style="background-image: url('/images/c0.png')">
                <div class="container">
                    <blockquote>
                        <p>@lang('wwd.quotes.darius_quote')</p>
                        <cite>@lang('wwd.quotes.darius_full_name')</cite>
                    </blockquote>
                </div>
            </div>

            <div class="service-box">
                <div class="container">
                    <div class="ico">
                        <img src="/images/s3.svg" alt="">
                    </div>
                    <h2>@lang('wwd.design.title')</h2>
                    <div class="desc">
                        <p>@lang('wwd.design.description')</p>
                        <ul>
                            <li>@lang('wwd.design.first_line')</li>
                            <li>@lang('wwd.design.second_line')</li>
                        </ul>
                        <ul>
                            <li>@lang('wwd.design.third_line')</li>
                            <li>@lang('wwd.design.fourth_line')</li>
                        </ul>
                    </div>
                    <div class="related">
                        <h6>@lang('wwd.related_projects')</h6>
                        <p><a href="/{{app()->getLocale()}}/case-studies/jwt">JWT</a>, Coda, ASG Worldwide, Disney,<a
                                href="/{{app()->getLocale()}}/case-studies/moofe"> Moofe</a>, <a href="/{{app()->getLocale()}}/case-studies/packpin">Packpin</a>, <a
                                href="/{{app()->getLocale()}}/case-studies/dfds">DFDS Seaways</a>,<a href="/{{app()->getLocale()}}/case-studies/jazz"> Klaipeda Jazz
                                Festival</a>, Geralda, Glassbel, Mestila, Klaipeda Tourism Agency, Svyturys Arena</p>
                    </div>
                </div>
            </div>

            <div class="service-box">
                <div class="container">
                    <div class="ico">
                        <img src="/images/s2.svg" alt="">
                    </div>
                    <h2>@lang('wwd.mobile_development.title')</h2>
                    <div class="desc">
                        <p>@lang('wwd.mobile_development.description')</p>
                        <ul>
                            <li>@lang('wwd.mobile_development.first_line')</li>
                            <li>@lang('wwd.mobile_development.second_line')</li>
                        </ul>
                        <ul>
                            <li>@lang('wwd.mobile_development.third_line')</li>
                            <li>@lang('wwd.mobile_development.fourth_line')</li>
                        </ul>
                    </div>
                    <div class="related">
                        <h6>@lang('wwd.related_projects')</h6>
                        <p><a href="/{{app()->getLocale()}}/case-studies/deskbookers/">Deskbookers</a>, <a href="/{{app()->getLocale()}}/case-studies/packpin/">Packpin</a>,
                            DFDS Seaways, Coda, Moofe, Pokernews, Klaipeda Municipality, BBPWood, Krantas Travel</p>
                    </div>
                </div>
            </div>


            <div class="quote-box-image color-invert" style="background-image: url('/images/c1.png')">
                <div class="container">
                    <blockquote>
                        <p>@lang('wwd.quotes.eimis_quote')</p>
                        <cite>@lang('wwd.quotes.eimis_full_name')</cite>
                    </blockquote>
                </div>
            </div>

        </div>

        <div class="start-project color-invert">
            <a href="#plan">
                <div class="text">
                    <div class="label">@lang('base.need_experts')</div>
                    <div class="start">@lang('base.hire_us')</div>
                </div>
            </a>
        </div>
    </div>

    </body>

    @endsection


    @section('js')
        <script src="/js/main.min.js"></script>
        <script src="/js/fadingbg.js"></script>
    @endsection

    @section('css')
        <link rel="stylesheet" media="all" href="/css/style.min.css">
        <link rel="stylesheet" media="all" href="/css/new.css">
@endsection
