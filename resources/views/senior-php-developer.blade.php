@extends('layout')


@section('content')
    <body class="home inner-career">
    <div class="pre-loader">
        <span class="logo-w"></span>
    </div>
    <div class="featured-title carrer-inner hasFade">
        <div class="container">
            <!-- <span class="status">raw</span> -->
            <h1>@lang('senior-php-dev.senior_php_dev')</h1>
            <p>@lang('senior-php-dev.experience_required')</p>
        </div>
    </div>
    <div class="body">
        <div class="center-image">
            <div class="container js-from-bottom">
                <img src="/images/hiring/seniordev.jpg" alt="">
            </div>
        </div>
        <div class="position-details">
            <div class="container">
                <div class="text js-from-bottom">
                    <h3>@lang('senior-php-dev.position.about')</h3>
                    <p>@lang('senior-php-dev.position.description')</p>
                    <h3>@lang('senior-php-dev.position.desired_skills')</h3>
                    <ul>
                        <li>@lang('senior-php-dev.position.first_skill')</li>
                        <li>@lang('senior-php-dev.position.second_skill')</li>
                        <li>@lang('senior-php-dev.position.third_skill')</li>
                        <li>@lang('senior-php-dev.position.fourth_skill')</li>
                        <li>@lang('senior-php-dev.position.fifth_skill')</li>
                        <li>@lang('senior-php-dev.position.sixth_skill')</li>
                        <li>@lang('senior-php-dev.position.seventh_skill')</li>
                        <li>@lang('senior-php-dev.position.eighth_skill')</li>
                        <li>@lang('senior-php-dev.position.ninth_skill')</li>
                    </ul>
                    <h3>@lang('senior-php-dev.position.in_exchange.title')</h3>
                    <ul>
                        <li>@lang('senior-php-dev.position.in_exchange.first')</li>
                        <li>@lang('senior-php-dev.position.in_exchange.second')</li>
                        <li>@lang('senior-php-dev.position.in_exchange.third')</li>
                        <li>@lang('senior-php-dev.position.in_exchange.fourth')</li>
                        <li>@lang('senior-php-dev.position.in_exchange.fifth')</li>
                        <li>@lang('senior-php-dev.position.in_exchange.sixth')</li>
                        <li>@lang('senior-php-dev.position.in_exchange.seventh')</li>
                        <li>@lang('senior-php-dev.position.in_exchange.eighth')</li>
                    </ul>

                </div>
                <!--Apply to Position -->
                @include('partials.positions.be-candidate')
            </div>
        </div>
        <div class="car-positions similar">
            <div class="container">
                <a href="../career" class="back">@lang('senior-php-dev.all_open_positions')</a>
            </div>
        </div>
    </div>
    <!-- / body -->
    <div class="start-project left-h">
    </div>
    </body>
@endsection

@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../../js/main.min.js"></script>
    <script src="../../js/sendjobappliance.js"></script>
    <script src="../../js/js.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>
    <script src="../../js/dropzone.js"></script>
@endsection

@section('css')
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
@endsection
