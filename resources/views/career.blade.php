@extends('layout')

@section('content')
    <body class="home">
    <header class="header nav-top">
        <div class="navigation">
            <div class="menu-trigger"><span></span><span></span><span></span></div>
            <div class="logo-block">
                <a href="/" class="logo reverse" title="Koralis">@lang('base.company_name')</a>
            </div>
        </div>
    </header>
    <div class="pre-loader">
        <span class="logo-w"></span>
    </div>

    <div class="page-content" style="background-color: #ffffff">
        <div class="featured-title carrer hasFade">
            <div class="container">
                <h1>@lang('career.challenge')</h1>
                <p>@lang('career.forget_boring')</p>
            </div>
        </div>
        <div class="body">
            <div class="center-video">
                <div class="container">
                    <div class="vv">
                        <img src="/images/video.jpg" alt="">
                        <div class="overlay">

                            <a href="https://player.vimeo.com/video/264922778" class="play media"></a>

                            <a href="https://vimeo.com/264922778" class="play media"></a>

                            <span class="text">@lang('career.play_video')</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="car-positions" id="carID">
                <div class="container">
                    <h2>@lang('career.open_positions')</h2>
                    @include('partials.position-details')
                </div>
            </div>
            <div class="perk-desc">
                <div class="container">
                    <div class="text js-from-bottom">
                        <h3>@lang('career.biggest_perk')</h3>
                        <p>@lang('career.biggest_perk_text')</p>
                    </div>
                    <div class="image js-from-bottom">
                        <img src="/images/car1.png" srcset="/images/car1@2x.png 2x" alt="">
                    </div>
                </div>
            </div>
            <div class="practises">
                <div class="container">
                    <h2>@lang('career.we_practice')</h2>
                    <section>
                        <article class="js-from-bottom">
                            <div class="num">
                                01
                            </div>
                            <h3>@lang('career.first_card.title')</h3>
                            <p>@lang('career.first_card.text')</p>
                        </article>
                        <article class="js-from-bottom">
                            <div class="num">
                                02
                            </div>
                            <h3>@lang('career.second_card.title')</h3>
                            <p>@lang('career.second_card.text')</p>
                        </article>
                        <article class="js-from-bottom">
                            <div class="num">
                                03
                            </div>
                            <h3>@lang('career.third_card.title')</h3>
                            <p>@lang('career.third_card.text')</p>
                        </article>
                        <div class="image">
                            <img src="/images/pr.png" srcset="/images/pr@2x.png 2x" alt="">
                        </div>
                        <article class="js-from-bottom">
                            <div class="num">
                                04
                            </div>
                            <h3>@lang('career.fourth_card.title')</h3>
                            <p>@lang('career.fourth_card.text')</p>
                        </article>
                    </section>
                </div>
            </div>
            <div class="vibes">
                <div class="container">
                    <div class="text js-from-bottom">
                        <h2>@lang('career.good_vibes_only')</h2>
                        <p>@lang('career.good_vibes_text')</p>
                    </div>
                    <section>
                        <article class="js-from-bottom">
                            <div class="ico">
                                <img src="/images/vibe-1.svg" alt="">
                            </div>
                            <h3>@lang('career.xbox_games')</h3>
                        </article>
                        <article class="js-from-bottom">
                            <div class="ico">
                                <img src="/images/vibe-2.svg" alt="">
                            </div>
                            <h3>@lang('career.guitars')</h3>
                        </article>
                        <article class="js-from-bottom">
                            <div class="ico">
                                <img src="/images/vibe-3.svg" alt="">
                            </div>
                            <h3>@lang('career.basketball')</h3>
                        </article>
                        <article class="js-from-bottom">
                            <div class="ico">
                                <img src="/images/vibe-4.svg" alt="">
                            </div>
                            <h3>@lang('career.paintball')</h3>
                        </article>
                        <article class="js-from-bottom">
                            <div class="ico">
                                <img src="/images/vibe-5.svg" alt="">
                            </div>
                            <h3>@lang('career.foosball')</h3>
                        </article>
                        <article class="js-from-bottom">
                            <div class="ico">
                                <img src="/images/vibe-6.svg" alt="">
                            </div>
                            <h3>@lang('career.drums')</h3>
                        </article>
                        <article class="js-from-bottom">
                            <div class="ico">
                                <img src="/images/vibe-7.svg" alt="">
                            </div>
                            <h3>@lang('career.beer')</h3>
                        </article>
                        <article class="js-from-bottom">
                            <div class="ico">
                                <img src="/images/vibe-8.svg" alt="">
                            </div>
                            <h3>@lang('career.fruit_days')</h3>
                        </article>
                    </section>
                </div>
            </div>
            <div class="intro-text-b js-from-bottom">
                <div class="container">
                    <h3>@lang('career.and_most_important')</h3>
                    <h2>@lang('career.you_talk_we_listen')</h2>
                    <p>@lang('career.your_opinion_matters')<span class="grey">@lang('career.ok') <span class="career-gif-ok">
                        <img src="https://media.giphy.com/media/10tD7GP9lHfaPC/giphy.gif" alt="ok" class="career-gif-ok"></span> </span>@lang('career.to')<strong>@lang('career.great')<span class="career-gif-great">
                    <img src="https://media.giphy.com/media/Ve20ojrMWiTo4/giphy.gif" alt="great" class="career-gif-great"></span></strong>”</p>
                </div>
            </div>
        </div>
        <!-- / body -->
        <div class="start-project color-invert big" id="applyID">
            <a href="">
                <div class="text">
                    <div class="label">@lang('career.ready_for_something')</div>
                    <div class="start">@lang('career.apply_now')</div>
                </div>
            </a>
        </div>
    </div>
    </body>
@endsection

@section('js')
    <script src="/js/main.min.js"></script>
    <script src="/js/js.js"></script>
@endsection

@section('css')
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
@endsection
