@extends('layout')

@section('css')
    <link rel="stylesheet" media="all" href="/css/style.min.css">
    <link rel="stylesheet" media="all" href="/css/new.css">
@endsection

@section('content')
    <body class="home">


    <div class="pre-loader">
        <span class="logo-w"></span>
    </div>
    <div class="page-content" style="background-color: #ffffff; transition: background 400ms linear;">
        <header class="header nav-top">
            <div class="navigation">
                <div class="menu-trigger"><span></span><span></span><span></span></div>
                <div class="logo-block">
                    <a href="/{{app()->getLocale()}}" class="logo reverse" title="Koralis">@lang('base.company_name')</a>
                </div>
            </div>
        </header>
        <!-- -->
        <div class="featured-title bg-image">
            <div class="video hasFade">
                <video muted autoplay loop>
                    <source src="images/video.mp4" data-src="images/video.mp4" type="video/mp4">
                </video>
            </div>
            <div class="container">
                <h1>@lang('index.main_header_top')<br>@lang('index.main_header_bottom')</h1>
                <p>@lang('index.red_bottom')</p>
            </div>
        </div>
        @include('partials.follow-widget')
        <div class="body">
            <div class="text-box offer center">
                <div class="container">
                    <div class="text-holder">
                        <h2>@lang('index.description')</h2>
                        <div class="entry">
                            <p>@lang('index.intro')</p>
                        </div>
                    </div>
                    <div class="vertical-line-holder">
                        <div class="vertical-line">
                        </div>
                    </div>
                    <div class="services-list">
                        <section>
                            <article>
                                <div class="image">
                                    <img src="images/s1.svg" alt="" style="height: 50px;">
                                </div>
                                <h5>@lang('index.technology_stack_web.heading')</h5>
                                <ul>
                                    <li>@lang('index.technology_stack_web.first_line')</li>
                                    <li>@lang('index.technology_stack_web.second_line')</li>
                                    <li>@lang('index.technology_stack_web.third_line')</li>
                                    <li>@lang('index.technology_stack_web.fourth_line')</li>
                                </ul>
                                <img class="serviceListButton" id="codeBtn" src="images/serviceListButton.svg" alt="">
                                <div class="last-work-container">
                                    <p class="work-label">
                                        @lang('base.last_work')
                                    </p>
                                    <div class="work-name" id="codeWorks">
                                        <a href="/{{app()->getLocale()}}/case-studies/packpin">
                                            <p>Packpin
                                                <span></span>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </article>
                            <article>
                                <div class="image">
                                    <img src="images/s2.svg" alt="" style="height: 50px;">
                                </div>
                                <h5>@lang('index.technology_stack_design.heading')</h5>
                                <ul>
                                    <li>@lang('index.technology_stack_design.first_line')</li>
                                    <li>@lang('index.technology_stack_design.second_line')</li>
                                    <li>@lang('index.technology_stack_design.third_line')</li>
                                    <li>@lang('index.technology_stack_design.fourth_line')</li>
                                </ul>
                                <img class="serviceListButton" id="designBtn" src="images/serviceListButton.svg" alt="">
                                <div class="last-work-container">
                                    <p class="work-label">
                                        @lang('base.last_work')
                                    </p>
                                    <div class="work-name" id="designWorks">
                                        <a href="/{{app()->getLocale()}}/case-studies/jazz">
                                            <p>@lang('base.last_work_company_design')
                                                <span></span>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </article>
                            <article>
                                <div class="image">
                                    <img src="images/s3.svg" alt="" style="height: 50px;">
                                </div>
                                <h5>@lang('index.technology_stack_mobile.heading')</h5>
                                <ul>
                                    <li>@lang('index.technology_stack_mobile.first_line')</li>
                                    <li>@lang('index.technology_stack_mobile.second_line')</li>
                                    <li>@lang('index.technology_stack_mobile.third_line')</li>
                                    <li>@lang('index.technology_stack_mobile.fourth_line')</li>
                                </ul>
                                <img class="serviceListButton" id="supportBtn" src="images/serviceListButton.svg"
                                     alt="">
                                <div class="last-work-container">
                                    <p class="work-label">
                                        @lang('base.last_work')
                                    </p>
                                    <div class="work-name" id="supportWorks">
                                        <a href="/{{app()->getLocale()}}/case-studies/newmood">
                                            <p>@lang('base.last_work_company_mobile')
                                                <span></span>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </article>
                        </section>
                    </div>
                </div>
            </div>

            <div class="work center index-clients darkBG">
                <div class="container">
                    <div class="world-map">
                        <div class="lithuaniaDot">
                            <div class="lithuaniaDotMain"></div>
                            <div class="lithuaniaDotContent"></div>
                        </div>
                    </div>
                    <div class="clients-logo st0">
                        <ul class="logo-list visible">
                            <li><img src="images/index/logo/coda.svg" alt=""></li>
                            <li><img src="images/index/logo/nike.svg" alt=""></li>
                            <li><img src="images/index/logo/electrolux.svg" alt=""></li>
                            <li><img src="images/index/logo/asg.svg" alt=""></li>
                        </ul>
                        <ul class="logo-list visible">
                            <li><img src="images/index/logo/disney.svg" alt=""></li>
                            <li><img src="images/index/logo/microsoft.svg" alt=""></li>
                            <li><img src="images/index/logo/technorama.svg" alt=""></li>
                            <li><img src="images/index/logo/moofe.svg" alt=""></li>
                        </ul>
                        <ul class="logo-list visible">
                            <li><img src="images/index/logo/Jwt.svg" alt=""></li>
                            <li><img src="images/index/logo/dfds.svg" alt=""></li>
                            <li><img src="images/index/logo/pokernews.svg" alt=""></li>
                            <li><img src="images/index/logo/deskbooker.svg" alt=""></li>
                        </ul>
                    </div>
                    <div class="entry visible">
                        <p>
                            @lang('index.case_studies_intro')
                        </p>
                        <a href="/{{app()->getLocale()}}/case-studies" class="btn"><span>@lang('index.view_case_studies')</span></a>
                    </div>
                </div>
            </div>

            <div class="text-box-parent">
                <div class="top-textbox-container">
                    <div class="textbox-leftSide">
                        <img src="images/index/career1.png">
                        <div class="textbox-leftSide-text">
                            <h1>
                                @lang('index.who_said_about_work')
                            </h1>
                        </div>
                    </div>
                    <div class="textbox-rightSide">
                        <div class="text js-from-bottom">
                            <h1>
                                @lang('index.who_said_about_work')
                            </h1>
                            <p>@lang('index.work_description')</p>
                        </div>
                        <div class="textbox-rightSide-button">
                            <a href="/{{app()->getLocale()}}/career" class="btn"><span>@lang('index.lets_roll')<img
                                        src="images/index/sm-arrow.svg"></span></a>
                        </div>
                    </div>
                </div>
                <div class="bottom-textbox-container">
                    <div class="bottom-textbox-leftSide">
                        <div class="bottom-textbox-leftSide-imgContainer">
                            <img src="images/index/work1.JPG">
                        </div>
                    </div>

                    <div class="bottom-textbox-center-imgContainer">
                        <div class="bottom-textbox-center">
                            <img src="images/index/Shake-it_x1.jpg"
                                 srcset="images/index/Shake-it_x2.jpg 2x" alt="">
                        </div>
                    </div>

                    <div class="bottom-textbox-rightSide">
                        <img src="images/index/career4.png">
                    </div>
                    <div class="instagram-list">
                        <div class="container">
                            <section style="position: relative;">
                                <article style="position: absolute; left: 25px; top: 659px;" class="visible">
                                    <div class="tilter sm photo-holder" style="cursor: default;">
                                        <figure class="figure"
                                                style="transform: translateX(1.01663px) translateY(-1.72458px) translateZ(0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg);">
                                            <img src="images/index/career1.png">
                                        </figure>
                                    </div>
                                </article>
                                <article style="position: absolute; left: 25px; top: 0px;" class="visible">
                                    <div class="tilter sm photo-holder" style="cursor: default;">
                                        <figure class="figure">
                                            <img src="images/index/work1.JPG">
                                        </figure>
                                    </div>
                                </article>
                                <article style="position: absolute; left: 25px; top: 264px;" class="visible">
                                    <div class="tilter md photo-holder" style="cursor: default;">
                                        <figure class="figure">
                                            <img src="images/index/career3.png" alt="">
                                        </figure>
                                    </div>
                                </article>
                                <article style="position: absolute; left: 25px; top: 659px;" class="visible">
                                    <div class="tilter sm photo-holder" style="cursor: default;">
                                        <figure class="figure"
                                                style="transform: translateX(1.01663px) translateY(-1.72458px) translateZ(0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg);">
                                            <img src="images/index/career4.png">
                                        </figure>
                                    </div>
                                </article>
                            </section>
                        </div>
                    </div>
                    <div class="textbox-rightSide-button">
                        <a href="/{{app()->getLocale()}}/career" class="btn"><span>@lang('index.lets_roll')<img src="images/index/sm-arrow.svg"></span></a>
                    </div>
                </div>
            </div>

            <!-- / body -->
            <div class="start-project color-invert">
                <a href="#plan">
                    <div class="text">
                        <div class="label">@lang('base.need_experts')</div>
                        <div class="start">@lang('base.hire_us')</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    </body>
@endsection

@section('js')
    <script src="/js/main.min.js"></script>
    <script src="/js/homepagebgfade.js"></script>
@endsection
