@extends('layout')

@section('content')
    <body class="home inner-career">
    <div class="pre-loader">
        <span class="logo-w"></span>
    </div>
    <header class="header nav-top">
        <div class="navigation">
            <div class="menu-trigger"><span></span><span></span><span></span></div>
            <div class="logo-block">
                <a href="/" class="logo reverse" title="Koralis">Koralis</a>
            </div>
        </div>
    </header>
    <div class="featured-title carrer-inner hasFade">
        <div class="container">
            <!-- <span class="status">raw</span> -->
            <h1>@lang('senior-frontend-dev.senior_frontend')</h1>
            <p>@lang('senior-frontend-dev.experience_required')</p>
        </div>
    </div>
    <div class="body">
        <div class="center-image">
            <div class="container js-from-bottom">
                <img src="/images/hiring/frontend.png" alt="">
            </div>
        </div>
        <div class="position-details">
            <div class="container">
                <div class="text js-from-bottom">
                    <h3>@lang('senior-frontend-dev.position.title')</h3>
                    <p>@lang('senior-frontend-dev.position.description')</p>
                    <h3>@lang('senior-frontend-dev.position.desired_skills')</h3>
                    <ul>
                        <li>@lang('senior-frontend-dev.position.first_skill')</li>
                        <li>@lang('senior-frontend-dev.position.second_skill')</li>
                        <li>@lang('senior-frontend-dev.position.third_skill')</li>
                        <li>@lang('senior-frontend-dev.position.fourth_skill')</li>
                        <li>@lang('senior-frontend-dev.position.fifth_skill')</li>
                        <li>@lang('senior-frontend-dev.position.sixth_skill')</li>
                        <li>@lang('senior-frontend-dev.position.seventh_skill')</li>
                    </ul>
                    <h3>@lang('senior-frontend-dev.in_exchange.title')</h3>
                    <ul>
                        <li>@lang('senior-frontend-dev.in_exchange.first')</li>
                        <li>@lang('senior-frontend-dev.in_exchange.second')</li>
                        <li>@lang('senior-frontend-dev.in_exchange.third')</li>
                        <li>@lang('senior-frontend-dev.in_exchange.fourth')</li>
                        <li>@lang('senior-frontend-dev.in_exchange.fifth')</li>
                        <li>@lang('senior-frontend-dev.in_exchange.sixth')</li>
                        <li>@lang('senior-frontend-dev.in_exchange.seventh')</li>
                        <li>@lang('senior-frontend-dev.in_exchange.eight')</li>
                    </ul>
                </div>
                @include('partials.positions.be-candidate')
            </div>
            <div class="car-positions similar">
                <div class="container">
                    <a href="../career" class="back">@lang('senior-frontend-dev.all_open_positions')</a>
                </div>
            </div>
        </div>
        <!-- / body -->
        <div class="start-project left-h">
        </div>
    </div>
    <body class="home inner-career">
    @endsection


    @section('js')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../../js/main.min.js"></script>
        <script src="../../js/sendjobappliance.js"></script>
        <script src="../../js/js.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>
        <script src="../../js/dropzone.js"></script>
    @endsection

    @section('css')
        <link rel="stylesheet" media="all" href="/css/style.min.css">
        <link rel="stylesheet" media="all" href="/css/new.css">
@endsection
