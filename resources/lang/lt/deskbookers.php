<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.7
 * Time: 17.23
 */

return [
    'description' => 'Fast, Easy & Flexible booking of work spaces',
    'location' => 'Located in Amsterdam, Deskbookers was founded in 2013 and enables short term bookings of office
                        space
                        of all kinds.',
    'company_info_title' => 'Company info',
    'company_info_text' => 'Deskbookers offers a trusted online platform, where users can find and book unique
                                    and
                                    inspiring working spaces all over Europe. Platform offers different types of
                                    workspaces
                                    to bring professionals together for a unique working experience whenever and
                                    wherever
                                    they want.',
    'company_info_quote' => 'Deskbookers believe that to be successful in your job, you choose a workspace which
                                    best
                                    reflects the activities for that day.',
    'development' => [
        'title' => 'Back-end development & Support',
        'first_paragraph' => 'As an early stage startup, Deskbookers were growing and they needed a trusted partner
                                    to
                                    help them with development powers. We developed variaus features for Deskbookers,
                                    such
                                    as an A/B test ready website, which enabled their marketing team run A/B test much
                                    quicker leading to better informed business decisions.',
        'second_paragraph' => 'Koralis team did a lot of back-end solutions and worked on their KPI monitoring tool
                                    and
                                    a dashboard. At Startups it is all about measuring KPIs and adapting fast right?',
    ],
    'technologies_we_used' => 'Technologies <br><span>we used</span>',
    'quote_about_koralis' => 'The team of Koralis is a pleasure to work with. They have impeccable management and <br> a
                            broad
                            team of development specialists, and we would <br> definitely consider them again as our
                            business partner.',
    'quote_about_koralis_author' => 'Jeroen Arts',
    'quote_about_koralis_author_position' => 'Director & Founder',
    'try_deskbookers' => 'Try Deskbookers yourself',
    'why_try_deskbookers' => 'Earn money renting unused space. Get new customers, track your space availability and manage
                            your
                            bookings all through one platform.'
];
