<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.6
 * Time: 15.11
 */

return [
    'be_candidate' => 'BE A CANDIDATE',
    'attach_cv' => 'Attach your CV, if you have:',
    'choose_file' => 'Choose file',
    'files_chosen' => 'Files chosen: ' ,
    'or' => 'Or',
    'add_linked_in' => 'Add your Linkedin account:',
    'apply' => 'Apply',
    'thx_bye' => 'K. THX. BYE'
];
