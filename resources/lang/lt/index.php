<?php

return array (
  'case_studies_intro' => 'We are proud to work with many global brands that trust their business to our team of experts.
                        While working with various ambitious companies, we ourselves learned new project management
                        techniques, grew our technical stack and were able to deliver more intricate solutions as a
                        result.',
  'description' => 'WE TURN IDEAS INTO PRODUCTS',
  'intro' => 'And we use our expertise to build top notch web & design solutions for our clients.
                            PHP, Laravel, React and Angular are just a few programming languages and frameworks our team of developers use everyday. Whether you’re looking for a digital solution to optimise your business or a way to scale it, Koralis is definitely the right choice for you.',
  'lets_roll' => 'LET\'S ROLL',
  'main_header_bottom' => 'Development Agency',
  'main_header_top' => 'Design &',
  'red_bottom' => 'Quality is not a coincidence',
  'technology_stack_design' => 
  array (
    'first_line' => '- Web design',
    'fourth_line' => '- Mobile design',
    'heading' => 'Design & UX',
    'second_line' => '- Wireframing & sketching',
    'third_line' => '- Application design',
  ),
  'technology_stack_mobile' => 
  array (
    'first_line' => '- Responsive development',
    'fourth_line' => '- Ionic & PhoneGap development',
    'heading' => 'Mobile development',
    'second_line' => '- iOS & Android development',
    'third_line' => '- React Native mobile apps',
  ),
  'technology_stack_web' => 
  array (
    'first_line' => '- PHP, JavaScript, ElasticSearch development',
    'fourth_line' => '- API Development',
    'heading' => 'Web Development',
    'second_line' => '- Laravel, Symfony frameworks',
    'third_line' => '- Angular, React, Node.js, Vue.js development',
  ),
  'view_case_studies' => 'VIEW CASE STUDIES',
  'who_said_about_work' => 'Who said work<br>
                            has to feel
                            like work?',
  'work_description' => 'When you work with a great team, hard work doesn’t seem so hard anymore. And we’re always
                            looking for peeps for expand the definition of “great.',
);
