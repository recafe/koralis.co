<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.7
 * Time: 17.06
 */

return [
    'ecommerce' => 'Ecommerce Shipment Tracker',
    'packpin_description' => 'Packpin is an e-commerce module we created from MVP to a final SaaS product that received
                        pre-seed funding. Packpin helps e-commerce retailers track shipments on their online shop,
                        streamline their post-purchase customer experience and helps them retain customers.',
    'challenge' => [
        'title' => 'The challenge',
        'first_challenge' => 'The idea was born from one of our projects requirements where client was looking for
                                    a shipment tracking solution on his Magento shop and there was not a single app on
                                    the market landscape that would make shipment tracking easy and valuable at the same
                                    time, for both, retailer & shopper.',
        'second_challenge' => 'We quickly pushed the project forward to cover all major ecommerce platforms and
                                    created a cloud platform that enables continues development and scaling. The
                                    platform helped Packpin provide smooth and easy custom integrations for established
                                    eCommerce brands regardless of their tech-stack. ',
        'third_challenge' => 'Packpin required a thorough research and reimagining the customer journey from
                                    placing an order to receiving a package at a doorstep. We dug deep to crystalize
                                    solutions to the most prominent customer pain points post-purchase. Didn’t take our
                                    team long to come up with the product bundle that includes worldwide shipment
                                    tracking, automatic notifications to the customers, fulfillment monitoring and
                                    various marketing enablements that directly affect customer loyalty &
                                    retention. ',
    ],
    'typography' => [
        'title' => 'Typography',
        'primary' => 'Primary / Headings',
        'secondary' => 'Secondary / Body text',
        'monza' => 'Monza',
        'crimson' => 'Crimson',
        'woodsmoke' => 'Woodsmoke',
        'alabaster' => 'Alabaster'
    ],
    'packpin_connects' => 'Packpin connects over 150 <br>package delivery companies worldwide',
    'and_many_more' => 'And many  more...',
    'pricing' => 'Simple and <br>transparent pricing',
    'try_packpin' => 'Try packpin yourself',
    'why_try_packpin' => 'Integrate Packpin and streamline your post-purchase customer experience that promotes
                            engagement & boosts revenue. '
];
