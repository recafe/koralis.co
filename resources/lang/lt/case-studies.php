<?php


return [
    'case_studies' => 'Case studies',
    'we_are_proud' => 'We are proud',
    'to_work' => 'to work with',
    'with_these' => 'these brands',
    'view_case' => 'View case study',

    'newmood' => [
        'title' => 'Newmood',
        'text' => 'E-commerce platform for one of the largest online fashion retailer in Baltic States. A unique and custom solution that we are very proud of.'
    ],
    'packpin' => [
        'title' => 'Packpin',
        'text' => 'With Packpin, we are changing the after-purchase customer experience. Our team created a unique API, plugins for Magento, Shopify, Prestashop and integrated more that 180 shipping companies.'
    ],
    'deskbookers' => [
        'title' => 'Deskbookers',
        'text' => 'A back-end development solution and ongoing support for a Dutch startup Deskbookers. Our team helped Deskbookers to enter other markets quicker with automated marketing solutions.'
    ],
    'moofe' => [
        'title' => 'Moofe',
        'text' => 'As a demanding customer with a clear view of what it required visually, Moofe presented a challenge that was delivered by Koralis.',
        'cite' => 'Koralis has developed an integrated multimedia Moofe website for the advertising and design market.<br>Our partners have carried out all project tasks to a high level of quality and on time. These have ranged from consultations on implementing process decisions to software development and website creation. <br>Koralis constantly strives to meet our expectations.',
        'author' => 'Douglas Fisher',
        'author_position' => 'Director and founder www.moofe.com'
    ],
    'jwt' => [
        'title' => 'JWT',
        'text' => 'Established over 150 years ago, J. Walter Thompson is a global leading marketing communications brand with 200 offices worldwide.'
    ],
    'dfds' => [
        'title' => 'DFDS',
        'text' => 'Since 2008 we’ve been DFDS Seaways’ exclusive partners, developing various IT and digital solutions for their Lithuanian, Swedish and Baltic markets.'
    ],
    'jazz' => [
        'title' => 'JAZZ',
        'text' => 'Klaipeda Castle Jazz Festival is an annual event held during June or July in Klaipeda which celebrates jazz, art, world-known musicians, and most importantly, gathers thousands of music lovers to Klaipeda to enjoy the art of jazz.'
    ],
    'disney' => [
        'title' => 'Disney',
        'text' => 'Even Disney has business process and assets that has to be managed. We are proud to work with Disney.'
    ],
    'asg' => [
        'title' => 'ASG',
        'text' => 'ASG worldwide needed to improve their asset management system functionality to make the online ordering smooth and simple across countries and different languages.'
    ],
    'coda' => [
        'title' => 'Coda',
        'text' => 'A world-class software solutions for a world class music agency based in London. For more than 4 years, Koralis is a close partner with Coda Music Agency, developing business and CRM solutions.'
    ],
    'nike' => [
        'title' => 'Nike',
        'text' => 'Known for their awe-inspiring marketing campaigns, we had a lot of fun technically enabling one of the NIKE’s UK-wide campaigns.'
    ],
    'pokernews' => [
        'title' => 'Pokernews',
        'text' => 'The world’s leading source of latest poker news, exclusive videos, live reporting from tournaments, reviews and podcasts.'
    ],

];
