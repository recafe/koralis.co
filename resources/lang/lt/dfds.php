<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.7
 * Time: 23.20
 */

return [

    'dfds_title' => 'Northern Europe\'s leading<br/> transportion provider',
    'about_dfds' => 'Headquartered in Copenhagen, DFDS was founded in 1866, when three biggest
                            Danish
                            steamship companies merged. DFDS Seaways operates 50 passenger and shipping services on 25
                            routes across North Sea, Baltic Sea and the English Channel.',
    'ferry_routes' => [
        'title' => 'Ferry routes',
        'text' => 'Apart from freight transportation, DFDS seaways also operates passenger
                                                ferries and mini-cruises among the major European, Nordic and Baltic
                                                countries, connecting more than 20 countries through 550 weekly
                                                departures.'
    ],
    'scratch_games' => [
        'title' => 'Scratch game ',
        'text' => 'As part of their Lithuanian nationwide marketing
                                    campaign,
                                    DFDS called upon Koralis to help them create a medium to engage users with various
                                    prize
                                    drawing. We always love working with DFDS due to their creative and large-scale
                                    marketing campaigns. For this specific assignment we created a browser-based scratch
                                    game where users were to provide their email to enter the game, scratch off a
                                    lottery
                                    ticket and win various prizes.',
        'banners' => [
            'title' => 'Banners',
            'first_paragraph' => 'One of the key elements of DFDS advertising strategy was to help sell ferry trip
                                        bookings during the slow season. When DFDS decided to
                                        promote ferry routes online, it turned to Koralis to design banners of various
                                        formats, to tap the right audiences at the perfect time and drive
                                        conversions.',
            'second_paragraph' => 'Our designers quickly learned DFDS needs and
                                        based
                                        on DFDS BIM (Building Information Modeling), created numerous prototypes for
                                        banner
                                        ads to quickly convey the meaning and help turn traffic into real revenue during
                                        slow season.',
            'third_paragraph' => 'One of the key elements of DFDS advertising strategy was to help sell ferry trip
                                        bookings during the slow season. When DFDS decided to promote ferry routes
                                        online,
                                        it turned to Koralis to design banners of various formats, to tap the right
                                        audiences at the perfect time and drive conversions.'
        ],
    ],
    'video' => 'Video',
    'video_time' => '150 yeas in 150 seconds',
    'quotes' => [
        'text' => 'The team of Koralis is a pleasure to work with. They have impeccable management and a <br/>
                            broad
                            team of development specialists, and we would definitely consider them again as our <br/>
                            business partner.',
        'author' => 'VAIDAS KLUMBYS',
        'author_position' => 'General Manager, Passenger Sales'
    ],
    'sail_with_dfds' => 'Sail with DFDS now',
    'sail_why' => 'Discover cruise and ferry routes, book the desired dates online and experience <br/> Europe
                            and
                            Scandinavia the DFDS way.'

];
