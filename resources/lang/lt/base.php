<?php

return [

    'company_name' => 'Koralis',
    'last_work' => 'Last work:',
    'last_work_company_web' => 'Packpin',
    'last_work_company_design' => 'Jazz Festival',
    'last_work_company_mobile' => 'Newmood',

    'case_studies' => 'Case studies',

    'need_experts' => 'NEED EXPERTS?',
    'hire_us' => 'Hire us'
];
