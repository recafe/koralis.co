<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.7
 * Time: 23.28
 */

return [
    'about' => 'Klaipeda Castle Jazz Festival',
    'about_text' => 'Klaipeda Castle Jazz Festival is an annual event held during June or July in Klaipeda which
                        celebrates
                        jazz, art, world-known musicians, and most importantly, gathers thousands of music lovers to
                        Klaipeda to
                        enjoy the art of jazz.',
    'surreal' => [
        'title' => 'Surreal</br>Trumpeter',
        'first_paragraph' => 'Jazz and art go hand in hand, and a unique symbol was
                                    chosen to
                                    represent this union. Created
                                    by a famous Lithuanian artist Algis Krisciunas, this pop art depiction of American
                                    jazz
                                    legend Louis Armstrong became a symbol of 23rd Klaipeda Castle Jazz Festival. The
                                    "Surreal
                                    Trumpeter", as the artist calls it himself, embodies the spirit and emotion of jazz
                                    music -
                                    exciting, free and spontaneous.',
        'second_paragraph' => 'This artwork became not only the symbol of the festival, but also set the whole art
                                    direction for the following work - throughout whole festival design you can sense
                                    the same
                                    vibrant and affixing emotion.',
        'third_paragraph' => 'Jazz and art go hand in hand, and a unique symbol was chosen
                                    to
                                    represent this union. Created
                                    by a famous Lithuanian artist Algis Krisciunas, this pop art depiction of American
                                    jazz
                                    legend Louis Armstrong became a symbol of 23rd Klaipeda Castle Jazz Festival. The
                                    "Surreal
                                    Trumpeter", as the artist calls it himself, embodies the spirit and emotion of jazz
                                    music -
                                    exciting, free and spontaneous.',
    ],
    'web_design' => [
        'title' => 'Web design</br>& development',
        'text' => 'We had a clear goal while working on the website - we knew that we need to make it
                                    more
                                    engaging and convenient. Keeping these two ideas in mind and following the art
                                    direction set
                                    by Algis Krisciunas, we worked closely with organisers to create a beautiful design
                                    and a
                                    functional website. ',
    ],
    'artists' => [
        'title' => 'Top Artists',
        'first_paragraph' => '23rd Jazz festival has organized a unique cocktail of jazz and world music that is rich in
                            musical
                            styles and genre to satisfy all listeners. Few of the prominent artists who performed - Till
                            Brönner, Funk\'n\'stein, David Katz, Vern Spevak and Nina Attal.',
        'second_paragraph' => '23rd Jazz festival hosted many artists, such as Funk\'n\'stein, David Katz, Vern Spevak and
                            Nina
                            Attal.',
    ],

    'overview' => [
        'title' => 'Overview of process',
        'text' => 'Since the start of the project, we worked a lot with event organisers to discuss their needs,
                            so we
                            could deliver the best results. We worked on numerous designs after which all was left to do
                            was to
                            bring them to life by coding and A/B testing the website to deliver a beautiful and secure
                            website.',
    ],
    'typography' => [
        'slogan' => 'Slogan',
        'headings' => 'Headings',
        'secondary' => 'Secondary / Body text',
        'ebony' => 'Ebony Clay',
        'comet' => 'Comet',
        'dodger' => 'Dodger Blue',
        'white' => 'White'
    ],
    'print_design' => [
        'title' => 'Print design',
        'text' => 'Koralis also took care of print design to create a cohesive look. Symbols and colors
                                    that
                                    were chosen for print designs reflect the festival logo and Algis Krisciunas
                                    artwork. '
    ],
    'photo_gallery' => [
        'title' => 'Photo gallery',
        'first_paragraph' => 'Various events and performances gathered music lovers to Klaipeda\'s Old Town for a
                                    rare
                                    opportunity to hear some of the world-known artists. Take a look at some of the
                                    memories
                                    that ',
        'second_paragraph' => 'were captured throughout the festival, so you too, can have a little taste of the
                                    unforgettable 23rd Jazz Festival.',
        'third_paragraph' => 'Various events and performances gathered music lovers to Klaipeda\'s Old Town for a
                                    rare
                                    opportunity to hear some of the world-known artists. Take a look at some of the
                                    memories
                                    that were captured throughout the festival, so you too, can have a little taste of
                                    the
                                    unforgettable 23rd Jazz Festival.',
    ],
    'quotes' => [
        'text' => 'We trust the team and the creative process, and it feels good knowing that the image of
                            Klaipeda Castle Jazz Festival is in good hands. Koralis’ innovative and modern thinking
                            always results in stunning visuals that impress not only festival organisers, but performing
                            artists all around the world as well.',
        'author' => 'INGA GRUBLIAUSKIENĖ',
        'author_position' => 'CEO and Manager of the Festival'
    ],
    'visit' => 'Visit the upcoming festival',
    'visit_why' => 'Take part in celebrating jazz music and join the upcoming festival in Klaipeda. '
];
