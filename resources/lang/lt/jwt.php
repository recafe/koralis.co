<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.7
 * Time: 22.33
 */

return [
    'company_title' => 'Globally leading marketing
                        communications company',
    'about_company' => 'J. Walter Thompson has been pioneering brands since 1864. They pioneer where others only dare and
                        by
                        doing so they rebel against convention. They do this with courage, ambition and just the right
                        amount of
                        swagger.',
    'company_info' => [
        'title' => 'Company info',
        'first_paragraph' => 'J. Walter Thompson Worldwide, the global scale marketing communications brand, has
                                    been
                                    creating pioneering solutions that build enduring brands and businesses for more
                                    than 150
                                    years. Headquartered in New York, J. Walter Thompson is a true global ',
        'second_paragraph' => ' network with more than
                                    200 offices in over 90 countries, employing nearly 12,000 marketing professionals.
                                    Company continually ranks among the top
                                    agency networks globally and by tapping into their 150 years of wisdom they manage
                                    to stay
                                    on the leading edge of the industry.',
        'third_paragraph' => 'J. Walter Thompson Worldwide, the global scale marketing communications brand, has
                                    been
                                    creating pioneering solutions that build enduring brands and businesses for more
                                    than 150
                                    years. Headquartered in New York, J. Walter Thompson is a true global network with
                                    more than
                                    200 offices in over 90 countries, employing nearly 12,000 marketing professionals.
                                    Company
                                    continually ranks among the top agency networks globally and by tapping into their
                                    150 years
                                    of wisdom they manage to stay on the leading edge of the industry.',
    ],
    'company_name' => 'J.Walther Thompson Worldwide<br/>Group',
    'activation_commerce' => 'Activation &<br/>Commerce',
    'geometry_global' => 'Geometry Global, the world’s largest and most international brand
                                                    activation agency, drives conversion, action and purchase through
                                                    award-winning programs that change behavior and inspire people to
                                                    buy
                                                    well. With teams in 56 markets, Geometry Global has expertise in
                                                    shopper, digital, experiential, relationship, promotional and trade
                                                    marketing.',
    'intelligence' => 'Intelligence',
    'about_intelligence' => 'J. Walter Thompson Intelligence is a new specialized practice,
                                                    offering a
                                                    unique blend of research, innovation and data analytics across its
                                                    global
                                                    network. Comprising three units; SONAR™, Analytics, and the
                                                    Innovation
                                                    Group., it provides J. Walter Thompson Company clients with
                                                    essential
                                                    consumer insight, inspiration, future forecasting, data and analysis
                                                    on a
                                                    global scale. Together, this unique group unearths human truths and
                                                    brand
                                                    insights. It also allows us to adapt and improve our performance at
                                                    speed,
                                                    and foresee future trends and cultural change.',
    'digital_transformation' => 'Digital<br/>Transformation',
    'about_digital_transformation' => 'Mirum is a borderless agency of over 2400 digital savants,
                                                    storytellers,
                                                    makers and relentlessly curious minds who are united by an uncommon
                                                    drive to
                                                    make what’s next. Active in 24 countries, we work across our global
                                                    network
                                                    of expertise to transform business, design innovative digital
                                                    experiences
                                                    and activate commerce at a global and local level. Mirum is part of
                                                    the J.
                                                    Walter Thompson Company and the WPP Network. Visit mirumagency.com
                                                    for more
                                                    information.',
    'jwt_partners' => 'JWT Partners',
    'brands' => '+70 brands',
    'cooperation' => 'The<br/>
                                    Cooperation',
    'cooperation_first' => 'We were more than happy partnering with the best-known and one of the oldest
                                    marketing
                                    companies worldwide - JWT. Copenhagen office is very well known for their creativity
                                    and
                                    unique, award-winning solutions. Our partnership was based on a simple business
                                    model -
                                    agency creates solutions and designs, we code them, do a little magic, deploy and
                                    support.',
    'cooperation_second' => 'IT was a perfect combination for an advertising agency, where they could focus on
                                    their key
                                    business operations, while we did all the web development works for different
                                    mediums. We
                                    are proud to have worked with JWT and global brands under their name.',
    'cooperation_third' => 'We were more than happy partnering with the best-known and one of the oldest
                                    marketing
                                    companies worldwide - JWT. Copenhagen office is very well known for their creativity
                                    and
                                    unique, award-winning solutions. Our partnership was based on a simple business
                                    model -
                                    agency creates solutions and designs, we code them, do a little magic, deploy and
                                    support.
                                    IT was a perfect combination for an advertising agency, where they could focus on
                                    their key
                                    business operations, while we did all the web development works for different
                                    mediums. We
                                    are proud to have worked with JWT and global brands under their name.',
    'voice_recognition' => 'Voice<br/>recognition app',
    'voice_recognition_text_first' => 'As one of the long-established JWT clients, Koralis worked on Sprite
                                            marketing
                                            campaign
                                            in collaboration with JWT Copenhagen branch to technically facilitate and
                                            put
                                            flesh
                                            on
                                            their concepts. As a result, Koralis developed a sound recognition mobile
                                            app
                                            for
                                            detecting Sprite-related songs.',
    'mix_it' => 'Mix it, share it,<br/>enjoy it',
    'mix_it_text_first' => 'Shake-it is a drink recipe online catalogue allowing users to search,
                                            discover,
                                            and get
                                            inspired by range of recipes created by professional bar chefs, tailored for
                                            different
                                            occasions. Koralis team developed the web platform from ground-up based on
                                            the
                                            visualizations and concepts passed on by JWT.',
    'voice_recognition_text_second' => 'As one of the long-established JWT clients, Koralis worked on Sprite marketing campaign
                                in
                                collaboration with JWT Copenhagen branch to technically facilitate and put flesh on
                                their
                                concepts.
                                As a result, Koralis developed a sound recognition mobile app for detecting
                                Sprite-related
                                songs.',
    'mix_it_text_second' => 'Shake-it is a drink recipe online catalogue allowing users to search, discover, and get
                                inspired
                                by
                                range of recipes created by professional bar chefs, tailored for different occasions.
                                Koralis
                                team
                                developed the web platform from ground-up based on the visualizations and concepts
                                passed on
                                by
                                JWT.',
    'jwt_legacy' => 'JWT Legacy',
    'history' => 'Born in Pittsfield, Massachusetts on October 28, 1847, Thompson started working at Carlton
                            and
                            Smith
                            small agency as a bookkeeper, soon after Thompson realized that sales were much more
                            profitable
                            and
                            became very effective salesman at the agency. In 1877, Thompson bought the agency for $500
                            and
                            renamed
                            it J. Walter Thompson Company.',
    'explore_now' => 'Explore JWT NOW',
    'why' => 'Find out how JWT can help you create and voice your brand, tap into your commerce
                    possibilities
                    or help you take a next step in your career.'


];
