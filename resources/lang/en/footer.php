<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.6
 * Time: 15.27
 */

return [

    'case_studies' => 'Case studies',
    'career' => 'CAREER',
    'wwd' => 'What we do?',
    'contact_us' => 'Contact us',
    'need_experts' => 'Need Experts?',
    'privacy_policy' => 'Privacy Policy',

    'we_are_keen_to_hear' => 'We’re keen to hear about your challenges and to develop tailored solutions that fit your interests
                    the best.',

    'location' => 'Location',
    'we_hiring' => 'WE’RE hiring',
    'join_our_team' => 'Feel like you would be a great addition to our team? Send your resume to:',
    'cv_email' => 'cv@koralis.co',
    'copy_right' => 'Copyright',
    'all_rights_reserved' => 'All rights reserved',
    'cookies_warning' => 'To help ensure the best of your browsing experience, we use cookies on this site. By clicking on the
                “Accept” button or continuing to browse, you agree to their usage. You can find out how to cancel it
                and more about why we use cookies, at our',
    'accept' => 'Accept'

];
