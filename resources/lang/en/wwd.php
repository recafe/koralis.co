<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.6
 * Time: 21.27
 */

return [
    'what_we_do' => 'What we do?',
    'at_koralis' => 'At Koralis ',
    'we_offer' => 'we offer',
    'related_projects' => 'Related projects',
    'web_development' => [
        'title' => 'Web Development',
        'description' => 'At Koralis, we offer high-quality technology solutions, which will make any of your digital property stand out from your competitors. We attract top talents & innovation to build more efficient and scalable products, to maximize productivity and to expand your company’s capacity.',
        'first_line' => 'PHP, JavaScript, ElasticSearch development',
        'second_line' => 'Laravel, Symfony frameworks',
        'third_line' => 'Angular, React, Node.js, Vue.js development',
        'fourth_line' => 'API Development',
    ],
    'quotes' => [
        'darius_quote' => 'It’s important to talk to customers. But it is more important   to listen. We use what customers say to improve their experience.',
        'darius_full_name' => 'DARIUS MIKALAUSKAS. UI/UX DESIGNER',
        'eimis_quote' => 'We are here to always be your reliable partner that takes   care of your projects, making your digital properties safer    and creating a better experience for your customers.',
        'eimis_full_name' => 'EIMANTAS RUDMINAS, CTO'
    ],

    'design' => [
        'title' => 'Design & UX',
        'description' => 'An enriched UI doesn’t always provide a good user experience,    while a simple UI can lead to a wonderful and exceptional UX.   We design products that are useful, easy to use, and are delightful   to interact with. Our UX designers have vast skills in building   interactive interfaces that are pleasing to the users and offer   competitive advantages.',
        'first_line' => 'Web design',
        'second_line' => 'Wireframing & sketching',
        'third_line' => 'Application design',
        'fourth_line' => 'Mobile design',
    ],

    'mobile_development' => [
        'title' => 'Mobile development',
        'description' => 'With a Mobile First mindset, we always strive to make your product or web app to shine on mobile. Constantly learning and adopting new technologies is allowing us to delivery good stuff. And no compromises.',
        'first_line' => 'Responsive development',
        'second_line' => 'iOS & Android development',
        'third_line' => 'React Native mobile apps',
        'fourth_line' => 'Ionic & PhoneGap apps'
    ]
];
