<?php

return [
    'challenge' => 'Challenge your<br> experience',
    'forget_boring' => 'And forget what “boring” means',
    'play_video' => 'PLAY VIDEO',
    'open_positions' => 'Open positions',
    'biggest_perk' => 'Our biggest perk ',
    'biggest_perk_text' => 'You ask what we value the most? The answer is simple - our people. All these laptops, foosball tables
                            & other stuff today called “perks” are easily copied and anyone can have them. Our biggest perk is
                            the people behind Koralis who continue to push boundaries every single day. ',
    'we_practice' => 'We practice <br>what we preach',
    'first_card' => [
        'title' => 'Use good judgement',
        'text' => 'We don’t have many rules around the office - who needs them working with a great team? The only
                                thing we ask is to make conscious decisions and you’ll see the wondrous things it does.'
    ],
    'second_card' => [
        'title' => 'We value openness...',
        'text' => '...and we cannot stretch that enough. We share openly and are remarkably transparent with our
                                team. And we expect others to value these qualities as much as we do.'
    ],
    'third_card' => [
        'title' => 'The bar is high',
        'text' => 'We are pretty picky when it comes to the people we hire (hence, a great team), the clients we
                                work with, and with the work we produce. The standard we set for ourselves is quite high, we
                                push hard to reach it and we expect our team to do the same.'
    ],
    'fourth_card' => [
        'title' => 'Use good judgement',
        'text' => 'At Koralis we favor autonomy and take ownership. We want to empower a collaborative workspace,
                                that’s why you’ll sometimes see our CEO cleaning up the mess in the office or HR hanging
                                Christmas decorations.'
    ],
    'good_vibes_only' => 'Good vibes<br>only!',
    'good_vibes_text' => 'Our culture is the most important thing for us - we go above and beyond to create a positive
                            environment for everyone. And sometimes all you need for that are a foosball table, a set of guitars
                            and drums, an Xbox and good ol’ fashioned “Yo mamma” jokes in the office. ',
    'xbox_games' => 'XBOX games',
    'guitars' => 'guitars',
    'basketball' => 'basketball',
    'paintball' => 'paintball',
    'foosball' => 'FOOSBALL',
    'drums' => 'drums',
    'beer' => 'BEER',
    'fruit_days' => 'FRUIT DAYS',

    'and_most_important' => '… and most importantly',
    'you_talk_we_listen' => 'You talk - we listen',

    'your_opinion_matters' => 'Your opinion matters. How you feel matters. And we are here to listen to you - sometimes it’s all it
                        takes to take your day from “',
    'ok' => 'ok',
    'to' => '” to “',
    'great' => 'great!',
    'ready_for_something' => 'Ready for something new?',
    'apply_now' => 'Apply now'

];
