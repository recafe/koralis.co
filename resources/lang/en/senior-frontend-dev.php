<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.6
 * Time: 17.38
 */

return [
    'senior_frontend' => 'Senior Frontend Developer',
    'experience_required' => '4+ year experience',
    'position' => [
        'title' => 'About POSITION',
        'description' => 'Flexibility, passion and dedication - besides the technical stack (which is listed below), these
                        are the qualities we are looking for. We need a person who is capable of working on big scale
                        projects and can effectively manage his time.
                        While working as an Senior Frontend developer at Koralis, you will be responsible for the
                        overall architecture and long-term strategy, overseeing and setting priorities for internal and
                        external developers; supporting and documenting integrations with back-end systems.',
        'desired_skills' => 'Desired skills',
        'first_skill' => '4+ years of professional experience with Javascript',
        'second_skill' => 'Good knowledge of one or more JS framework (Angular, React.JS, etc.)',
        'third_skill' => 'HTML5 and CSS3',
        'fourth_skill' => 'Fluent with Git (Bitbucket) and working as part of a distributed development team',
        'fifth_skill' => 'Great UX sensibility and a good eye for design',
        'sixth_skill' => 'Experience working in Linux/Unix environment',
        'seventh_skill' => 'Fluent in English',
    ],
    'in_exchange' => [
        'title' => 'What you get in exchange',
        'first' => 'Compelling and attractive compensation',
        'second' => 'Loads of opportunities for professional development',
        'third' => 'Strong company culture (office traditions, activities together, etc.)',
        'fourth' => 'Experience working with big scale international projects',
        'fifth' => 'Work in strong & stable company',
        'sixth' => 'All supplies needed for productive & effective work',
        'seventh' => 'An opportunity to execute your ideas & learn from others',
        'eight' => 'Motivating office environment',
    ],
    'all_open_positions' => 'All open positions'
];
