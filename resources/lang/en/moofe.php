<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.7
 * Time: 22.09
 */
return [

    'about' => 'Unique CGI imagery service',
    'about_text' => 'Moofe is a provider of the next generation, high quality, complete pre-shot photographic
                        locations and original imagery for the use in various CGI projects. Moofe’s backplates are
                        widely used by marketing agencies and automotive manufacturers worldwide.',
    'company_info' => [
        'title' => 'Company info',
        'first_paragraph' => 'Moofe was founded in 2008 by a team of world-class famous photographers in London.
                                    Known for their expertise and 50 years of experience in photography and CGI, it
                                    established a strong platform on which Moofe was built. Moofe is a comprehensive
                                    image collection ',
        'second_paragraph' => 'of CG-ready backplates and High Dynamic Range Images (HDRIs) for use in producing
                                    imagery for advertising and marketing from CAD data. Moofe CGIs are widely used by
                                    automotive industry leaders such as Renault, Porsche & Audi.',
        'third_paragraph' => 'Moofe was founded in 2008 by a team of world-class famous photographers in London.
                                    Known for their expertise and 50 years of experience in photography and CGI, it
                                    established a strong platform on which Moofe was built. Moofe is a comprehensive
                                    image collection of CG-ready backplates and High Dynamic Range Images (HDRIs) for
                                    use in producing imagery for advertising and marketing from CAD data. Moofe CGIs are
                                    widely used by automotive industry leaders such as Renault, Porsche & Audi. '
    ],
    'moofie_clients' => 'Moofe clients',
    'automakers' => '+15 automakers',
    'solution' => [
        'title' => 'The <br><span>Solution</span>',
        'first_solution' => 'We built a comprehensive e-commerce solution through continuous integration with
                                        its own custom CRM from scratch & have been partners with Moofe for more than 4
                                        years. Our team worked closely with Moofe since the inception of the idea
                                        to ',
        'second_solution' => 'transform their vision and design ideas into highly functional enterprise
                                        platform. Our developers and software engineers quickly took over the project to
                                        start prototyping and back up the bold expectations of the design mockups.',
        'final_solution' => 'We built a comprehensive e-commerce solution through continuous integration with
                                        its own custom CRM from scratch & have been partners with Moofe for more than 4
                                        years. Our team worked closely with Moofe since the inception of the idea to
                                        transform their vision and design ideas into highly functional enterprise
                                        platform. Our developers and software engineers quickly took over the project to
                                        start prototyping and back up the bold expectations of the design mockups.'
    ],
    'technologies_used' => 'Technologies <br><span>we used</span>',
    'usability' => [
        'usability_style' => 'Usability & Style',
        'first_paragraph' => 'From the moment we started working on Moofe, we knew that it had to be highly extensible
                                and visually stunning, focusing on usability at the same time. It had to stand out with
                                its uniqueness, yet provide intuitive user experience with ease, from the point customer
                                enters a website all the way to checkout completion.',
        'second_paragraph' => 'From the moment we started working on Moofe, we knew that it had to be highly extensible
                                and visually stunning, focusing on usability at the same time. It had to stand out with
                                its uniqueness, yet provide intuitive user experience with ease, from the point customer
                                enters a website all the way to checkout completion.',
     ],
    'quotes' => [
        'text' => 'The knowledge and ability that Koralis has brought to our project has elevated it beyond our
                            expectations. I would not <br/> hesitate to recommend Koralis as a suitable partner in the
                            creation of online solutions for any business.',
        'author' => 'DOUGLAS FISHER',
        'author_position' => 'Director & Co-Founder www.moofe.com'
    ],
    'browser_moofe' => 'Browse Moofe now',
    'browser_why' => 'Discover a great collection of high quality CG ready imagery for your next campaign.'

];
