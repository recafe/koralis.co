<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.7
 * Time: 13.22
 */

return [
    'latest_fashions' => 'Latest Fashion Online',
    'newmood_description' => 'Newmood is one of the largers online fashion retailer in the Baltic states, offering apparel &
                        clothing
                        of high-end fashion brands and independent designers. Few of the names include Armani Jeans,
                        Tommy
                        Hilfiger, Lacoste, Diadora & Adidas by Rita Ora.',
    'challenge' => 'The challenge',
    'first_challenge' => 'Newmood needed a multipurpose e-commerce platform that would accommodate their fast
                                    growing
                                    product inventory and offer smooth user experience to online shoppers through latest
                                    technologies.',
    'second_challenge' => 'In addition to that, we had to enable company\'s marketing efforts through intuitive
                                    CMS-driven platform and various third party integrations to automate & streamline
                                    day-to-day
                                    operations.',
    'mobile_friendly' => [
        'title' => 'Mobile-friendly',
        'text' => 'Minimalist design puts the focus on product display and fluid grid format ensures the
                                display is
                                clean and simple to use for a smooth shopping experience.'
    ],

    'typography' => [
        'title' => 'Typography',
        'primary' => 'Primary / Headings',
        'primary_color' => 'Primary color',
        'black' => 'Black',
        'secondary_colors' => 'Secondary colors',
        'crimson' => 'Crimson',
        'alto' => 'Alto'
    ],
    'trade_marks' => 'Trademarks',
    'brands' => '+150 brands',

    'solution' => [
        'title' => 'The solution',
        'first_paragraph' => 'After seeing Newmood\'s design ideas and the direction they wanted to take, our team
                                quickly picked up the project and started continuous prototyping.',
        'second_paragraph' => 'Through strong teamwork and Agile culture we enabled our customer to closely watch the
                                project progress up to its completion. We created a unique e-commerce platform to unite
                                their accounting, marketing.',
    ],
    'shop' => 'Shop new collection',
    'free_shipping' => 'Free shipping and free returns'
];
