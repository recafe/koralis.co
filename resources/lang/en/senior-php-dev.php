<?php
/**
 * Created by PhpStorm.
 * User: koralis
 * Date: 20.4.6
 * Time: 21.12
 */

return [

    'senior_php_dev' => 'Senior PHP Developer',
    'experience_required' => '4+ year experience',
    'position' => [
        'about' => 'About POSITION',
        'description' => 'Flexibility, passion and dedication - besides the technical stack (which is listed below), these are the qualities we are looking for. We need a person who is capable of working on big scale projects and can effectively manage his time.
                    While working as an Senior PHP developer at Koralis, you will be responsible for the overall architecture and long-term strategy, overseeing and setting priorities for internal and external developers; supporting and documenting integrations with back-end systems.',
        'desired_skills' => 'Desired skills',
        'first_skill' => '4+ years of professional experience with PHP, MySQL  ',
        'second_skill' => 'Practical experience with at least one MVC framework',
        'third_skill' => 'Experience with Laravel framework',
        'fourth_skill' => 'Front-end basics (AngularJS, React.JS, etc.)',
        'fifth_skill' => 'OOP principles',
        'sixth_skill' => 'Understanding and practical application of TDD and Unit testing',
        'seventh_skill' => 'Experience working in Linux/Unix environment',
        'eighth_skill' => 'Fluent in English',
        'ninth_skill' => 'ElasticSearch understanding - nice to have',
        'in_exchange' => [
            'title' => 'What you get in exchange',
            'first' => 'Compelling and attractive compensation',
            'second' => 'Loads of opportunities for professional development',
            'third' => 'Strong company culture (office traditions, activities together, etc.)',
            'fourth' => 'Experience working with big scale international projects',
            'fifth' => 'Work in strong & stable company',
            'sixth' => 'All supplies needed for productive & effective work',
            'seventh' => 'An opportunity to execute your ideas & learn from others',
            'eighth' => 'Motivating office environment',
        ]
    ],
    'all_open_positions' => 'All open positions'
];
