<?php

return [

    'contact_us' => 'CONTACT US',
    'lets_make_contact' => 'Let’s make a contact',
    'contact_us_body' => 'We’re keen to hear about your challenges and to develop tailored solutions that fit your interests the
                best.',
    'contact_email' => 'hello@koralis.co',
    'contact_address' => 'Turgaus sq.23<br>91246 Klaipėda, Lithuania',
    'contact_phone_no' => '+370 620 21915'

];
